# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.3...v2.4.4) (2023-01-22)

Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

### Enhancements
- **osp**: add X310 platform configurations for single ethernet connectivity (!10)(a81971d9)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Enhancements
- **osp**: add support for the X310 with UBX-160 transceiver card. (!7)(3292d7a3)

### Bug Fixes
- **doc**: fix incorrect system.xml example for x310 platform. (!8)(b9d68f53)
- **osp **: fix reference clock selection and enable on non transceiver x310 platform configurations(!9) (af4050a9)

### Miscellaneous
- **osp**: update `Project.xml` to specify `PackageName` and `PackagePrefix`. (!5)(a22166b7)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.1...v2.4.2) (2022-05-26)

Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

### Miscellaneous
- **osp**: update `Project.xml` to specify `PackageName` and `PackagePrefix`. (!5)(a22166b7)

# [v2.4.1](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.4.0...v2.4.1) (2022-03-16)

No Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

# [v2.4.0](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.4...v2.4.0) (2022-01-25)

No Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

# [v2.3.4](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.1...v2.3.2) (2021-11-08)

Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

### Enhancements
- **osp**: remove build of ettus_n310_v4 toolchain and replace install with a download. (!4)(df3cf2e0)

# [v2.3.1](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.0...v2.3.1) (2021-10-13)

No Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

# [v2.3.0](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

No Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.2.1...v2.3.0-rc.1) (2021-08-26)

No Changes/additions since [OpenCPI Release v2.2.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.1)

# [v2.2.1](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/v2.2.0...v2.2.1) (2021-07-22)

Changes/additions since [OpenCPI Release v2.2.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.0)

### Miscellaneous
- **osp**: add ettus_n310_v4 software platform to pipelines. (!2)(d01deeb2)

# [v2.2.0](https://gitlab.com/opencpi/osp/ocpi.osp.ettus/-/compare/a54991a2...v2.2.0) (2021-07-08)

Initial release of Ettus N310 v4.0.0 for OpenCPI
