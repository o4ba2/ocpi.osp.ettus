.. x310 Platform worker


.. _x310-platform-worker:


``x310`` Platform Worker
===================================
OpenCPI Ettus USRP X310 platform worker. 

The Ettus Research USRP X310 is a high-performance, scalable software defined radio (SDR) platform 
for designing and deploying next generation wireless communications systems. The hardware architecture 
combines two extended-bandwidth daughterboard slots covering DC to 6 GHz with up to 160 MHz of baseband bandwidth, 
multiple high-speed interface options (PCIe, dual 10 GigE, dual 1 GigE), and a large user-programmable 
Kintex-7 FPGA in a convenient desktop or rack-mountable half-wide 1U form factor.

* Xilinx Kintex-7 XC7K410T FPGA
* 14 bit 200 MS/s ADC
* 16 bit 800 MS/s DAC
* Frequency range: DC - 6 GHz with suitable daughterboard
* Up 160MHz bandwidth per channel
* Two wide-bandwidth RF daughterboard slots
* Optional GPSDO
* Multiple high-speed interfaces (Dual 10G, PCIe Express, ExpressCard, Dual 1G)

The Ettus USRP X310 supports a range of plugin daughterboards that contain the baseband to
RF frequency modulation, PA and LNA functionality. This platform worker supports the ``UBX160-160`` daughterboard only.

Detail
------
The x310 platform includes the following device workers

* **time_server**
* **dgrdma_config_dev**
* **lmk04816**
* **x310_gpio**
* **ubx160_eeprom**
* **dac_spi**

One or two instances of the daughter board device workers can be used with this platform. These workers are

* **data_sink_qdac_csts**
* **ad9146**
* **data_src_qadc_csts**
* **ads62p48**
* **max2871 (RXLO1, RXLO2, TXLO1, TXLO2)**
* **ubx160_cpld**
* **ubx160_io**

Assemblies can be built using this platform worker that support either one (single) or two (dual) ethernet ports

**time_server.hdl**

This device worker provides a Time Service Interface to OpenCPI. 

**dgrdma_config_dev.hdl**

This device worker is required when using DGRDMA OpenCPI functionality. DGRDMA provides the necessary functionality for
interfacing between OpenCPI software (RCC) and FPGA implementation (HDL) using one or two ethernet interfaces.
This worker provides property access used for control and configuration of the OpenCPI DGRDMA components.

**lmk04816.hdl**

This device worker is used for configuration of the LMK04816 clock generator IC present on the x310 mother board.
Configuration of the LMK04816 is performed by the drc_x310.rcc Digital-Radio-Controller worker. It is not
necessary for an application using the x310 platform to read or write to the properties of this worker.

**x310_gpio.hdl**

This device worker provides access to the 12 front-panel GPIO signals (AUX IO connector). 
This worker is not required for DGRDMA or x310 Digital-radio-controller functionality. It is 
provided to aid assembly and application development.It provides a means to connect input or 
output signals to the FPGA for debug and verification purposes.

**ubx160_eeprom.hdl**

A single instance of this device worker is included in the x310 platform. This provides access to the 
mother board and daughter board EEPROMs.

**dac_spi**

This worker is a sub-device of the ad9146.hdl device worker. It provides access to the hardware registers 
of the DAC ICs over its SPI interface. One instance of this sub-device worker is required to control both
DACs. Note also that the DAC SPI uses a separate SPI bus to that used for controlling
the ADCs.

One or two instance of the following device workers are used to support the UBX160 daughter board hardware,
depending on the number fitted. The worker instance names are prefixed with DB0 or DB1 to distinguish
between the two daughter boards.

**data_sink_qdac_csts**

An instance of this worker is used for each of the two transmit data streams (one for each daughter board).
It is used to connect the transmit data stream to the DAC device.

**ad9146**

The AD9146 is a dual 16-bit digital-to-analogue converter (DAC). There are two AD9146 devices on the x310 mother board.
Each device generates the analogue I and Q baseband signals for a single daughter board.

**data_src_qadc_csts**

An instance of this worker is used for each of the two receive data streams (one for each daughter board).
It is used to connect the ADC, producing data on the receive data stream.

**ads62p48**

The ADS62P48 is a dual 14-bit analogue-to-digital converter (ADC). There are two ADS62P48 devices on the x310 mother board.
Each devices digitises the analogue I and Q baseband signals from a single daughter board.

**max2871 (RXLO1, RXLO2, TXLO1, TXLO2)**

The MAX2871 is an ultra-wideband phase-locked loop (PLL) with integrated voltage control oscillators (VCOs).
Each UBX160 daughter board has 4 MAX2871 devices. These are used for generating the transmit (TXLO1/TXLO2)
and receiver (RXLO1/RXLO2) clocks.

**ubx160_cpld**

The ubx160_cpld.hdl device worker provides the means to control the CPLD device on the UBX160 daughter board.
This is used to control the signal paths on the UBX160 daughter card.

**ubx160_io**

The UBX160_io.hdl device worker provides the means to read and write a number of GPIO signals used to control a UBX160 daughter board.
The ubx160_io.hdl device worker also contains the implementation of the SPI interface used by the AD9146, ADS62P48
and UBX160_CPLD devices.


.. ocpi_documentation_worker::

Utilization
-----------
.. .. ocpi_documentation_utilization::
