#
# Copyright 2014 Ettus Research LLC
#

#*******************************************************************************
# X3x0 Pin Mapping
#*******************************************************************************

#*******************************************************************************
## SFP Lanes

# SFP clock pins now come from their own ucf files.  See _10ge.ucf, _1ge.ucf, and _cpri.ucf
# NOTE: In the schematic SFP0 signals are prefixed SFP1 and SFP1 signals are prefixed SFP2
set_property PACKAGE_PIN   AA3      [get_ports SFP0_RX_n]
set_property PACKAGE_PIN   AA4      [get_ports SFP0_RX_p]
# set_property IOSTANDARD    LVDS     [get_ports {SFP0_RX_*}]

set_property PACKAGE_PIN   Y1       [get_ports SFP0_TX_n]
set_property PACKAGE_PIN   Y2       [get_ports SFP0_TX_p]
# set_property IOSTANDARD    LVDS     [get_ports {SFP0_TX_*}]

set_property PACKAGE_PIN   T5       [get_ports SFP1_RX_n]
set_property PACKAGE_PIN   T6       [get_ports SFP1_RX_p]
# set_property IOSTANDARD    LVDS     [get_ports {SFP1_RX_*}]

set_property PACKAGE_PIN   P1       [get_ports SFP1_TX_n]
set_property PACKAGE_PIN   P2       [get_ports SFP1_TX_p]
# set_property IOSTANDARD    LVDS     [get_ports {SFP1_TX_*}]

#*******************************************************************************
## ADC 0

set_property PACKAGE_PIN   L27      [get_ports DB0_ADC_DA0_N]
set_property PACKAGE_PIN   L26      [get_ports DB0_ADC_DA0_P]
set_property PACKAGE_PIN   K29      [get_ports DB0_ADC_DA1_N]
set_property PACKAGE_PIN   K28      [get_ports DB0_ADC_DA1_P]
set_property PACKAGE_PIN   L28      [get_ports DB0_ADC_DA2_N]
set_property PACKAGE_PIN   M28      [get_ports DB0_ADC_DA2_P]
set_property PACKAGE_PIN   C30      [get_ports DB0_ADC_DA3_N]   ;# In 3.3V bank
set_property PACKAGE_PIN   D29      [get_ports DB0_ADC_DA3_P]   ;# In 3.3V bank
set_property PACKAGE_PIN   J24      [get_ports DB0_ADC_DA4_N]
set_property PACKAGE_PIN   J23      [get_ports DB0_ADC_DA4_P]
set_property PACKAGE_PIN   L23      [get_ports DB0_ADC_DA5_N]
set_property PACKAGE_PIN   L22      [get_ports DB0_ADC_DA5_P]
set_property PACKAGE_PIN   K24      [get_ports DB0_ADC_DA6_N]
set_property PACKAGE_PIN   K23      [get_ports DB0_ADC_DA6_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_ADC_DA*}]

set_property DIFF_TERM     TRUE     [get_ports {DB0_ADC_DA*}]
# Bit 3 is in the 3.3V bank and does no support diff termination
set_property DIFF_TERM     FALSE    [get_ports DB0_ADC_DA3_*]

set_property PACKAGE_PIN   K21      [get_ports DB0_ADC_DB0_N]
set_property PACKAGE_PIN   L21      [get_ports DB0_ADC_DB0_P]
set_property PACKAGE_PIN   J22      [get_ports DB0_ADC_DB1_N]
set_property PACKAGE_PIN   J21      [get_ports DB0_ADC_DB1_P]
set_property PACKAGE_PIN   L20      [get_ports DB0_ADC_DB2_N]
set_property PACKAGE_PIN   M20      [get_ports DB0_ADC_DB2_P]
set_property PACKAGE_PIN   H29      [get_ports DB0_ADC_DB3_N]
set_property PACKAGE_PIN   J29      [get_ports DB0_ADC_DB3_P]
set_property PACKAGE_PIN   J28      [get_ports DB0_ADC_DB4_N]
set_property PACKAGE_PIN   J27      [get_ports DB0_ADC_DB4_P]
set_property PACKAGE_PIN   K30      [get_ports DB0_ADC_DB5_N]
set_property PACKAGE_PIN   L30      [get_ports DB0_ADC_DB5_P]
set_property PACKAGE_PIN   J26      [get_ports DB0_ADC_DB6_N]
set_property PACKAGE_PIN   K26      [get_ports DB0_ADC_DB6_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_ADC_DB*}]

set_property DIFF_TERM     TRUE     [get_ports {DB0_ADC_DB*}]

set_property PACKAGE_PIN   K25      [get_ports DB0_ADC_DCLK_N]
set_property PACKAGE_PIN   L25      [get_ports DB0_ADC_DCLK_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_ADC_DCLK_*}]

#*******************************************************************************
## ADC 1

set_property PACKAGE_PIN   D18      [get_ports DB1_ADC_DA0_N]
set_property PACKAGE_PIN   D17      [get_ports DB1_ADC_DA0_P]
set_property PACKAGE_PIN   D19      [get_ports DB1_ADC_DA1_N]
set_property PACKAGE_PIN   E19      [get_ports DB1_ADC_DA1_P]
set_property PACKAGE_PIN   L18      [get_ports DB1_ADC_DA2_N]
set_property PACKAGE_PIN   L17      [get_ports DB1_ADC_DA2_P]
set_property PACKAGE_PIN   J13      [get_ports DB1_ADC_DA3_N]   ;# In 3.3V bank
set_property PACKAGE_PIN   K13      [get_ports DB1_ADC_DA3_P]   ;# In 3.3V bank
set_property PACKAGE_PIN   H17      [get_ports DB1_ADC_DA4_N]
set_property PACKAGE_PIN   J17      [get_ports DB1_ADC_DA4_P]
set_property PACKAGE_PIN   F18      [get_ports DB1_ADC_DA5_N]
set_property PACKAGE_PIN   G18      [get_ports DB1_ADC_DA5_P]
set_property PACKAGE_PIN   H19      [get_ports DB1_ADC_DA6_N]
set_property PACKAGE_PIN   J19      [get_ports DB1_ADC_DA6_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_ADC_DA*}]

set_property DIFF_TERM     TRUE     [get_ports {DB1_ADC_DA*}]
# Bit 3 is in the 3.3V bank and does no support diff termination
set_property DIFF_TERM     FALSE    [get_ports DB1_ADC_DA3_*]

set_property PACKAGE_PIN   J18      [get_ports DB1_ADC_DB0_N]
set_property PACKAGE_PIN   K18      [get_ports DB1_ADC_DB0_P]
set_property PACKAGE_PIN   C21      [get_ports DB1_ADC_DB1_N]
set_property PACKAGE_PIN   D21      [get_ports DB1_ADC_DB1_P]
set_property PACKAGE_PIN   K20      [get_ports DB1_ADC_DB2_N]
set_property PACKAGE_PIN   K19      [get_ports DB1_ADC_DB2_P]
set_property PACKAGE_PIN   F22      [get_ports DB1_ADC_DB3_N]
set_property PACKAGE_PIN   G22      [get_ports DB1_ADC_DB3_P]
set_property PACKAGE_PIN   G20      [get_ports DB1_ADC_DB4_N]
set_property PACKAGE_PIN   H20      [get_ports DB1_ADC_DB4_P]
set_property PACKAGE_PIN   C22      [get_ports DB1_ADC_DB5_N]
set_property PACKAGE_PIN   D22      [get_ports DB1_ADC_DB5_P]
set_property PACKAGE_PIN   H22      [get_ports DB1_ADC_DB6_N]
set_property PACKAGE_PIN   H21      [get_ports DB1_ADC_DB6_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_ADC_DB*}]

set_property DIFF_TERM     TRUE     [get_ports {DB1_ADC_DB*}]

set_property PACKAGE_PIN   E20      [get_ports DB1_ADC_DCLK_N]
set_property PACKAGE_PIN   F20      [get_ports DB1_ADC_DCLK_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_ADC_DCLK_*}]

#*******************************************************************************
## DAC 0

set_property PACKAGE_PIN   M30      [get_ports DB0_DAC_D0_N]
set_property PACKAGE_PIN   M29      [get_ports DB0_DAC_D0_P]
set_property PACKAGE_PIN   M27      [get_ports DB0_DAC_D1_N]
set_property PACKAGE_PIN   N27      [get_ports DB0_DAC_D1_P]
set_property PACKAGE_PIN   N30      [get_ports DB0_DAC_D2_N]
set_property PACKAGE_PIN   N29      [get_ports DB0_DAC_D2_P]
set_property PACKAGE_PIN   N26      [get_ports DB0_DAC_D3_N]
set_property PACKAGE_PIN   N25      [get_ports DB0_DAC_D3_P]
set_property PACKAGE_PIN   N20      [get_ports DB0_DAC_D4_N]
set_property PACKAGE_PIN   N19      [get_ports DB0_DAC_D4_P]
set_property PACKAGE_PIN   N22      [get_ports DB0_DAC_D5_N]
set_property PACKAGE_PIN   N21      [get_ports DB0_DAC_D5_P]
set_property PACKAGE_PIN   N24      [get_ports DB0_DAC_D6_N]
set_property PACKAGE_PIN   P23      [get_ports DB0_DAC_D6_P]
set_property PACKAGE_PIN   P22      [get_ports DB0_DAC_D7_N]
set_property PACKAGE_PIN   P21      [get_ports DB0_DAC_D7_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_DAC_D*}]

set_property PACKAGE_PIN   M23      [get_ports DB0_DAC_DCI_N]
set_property PACKAGE_PIN   M22      [get_ports DB0_DAC_DCI_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_DAC_DCI_*}]

set_property PACKAGE_PIN   M25      [get_ports DB0_DAC_FRAME_N]
set_property PACKAGE_PIN   M24      [get_ports DB0_DAC_FRAME_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB0_DAC_FRAME_*}]

#*******************************************************************************
## DAC 1

set_property PACKAGE_PIN   B17      [get_ports DB1_DAC_D0_N]
set_property PACKAGE_PIN   C17      [get_ports DB1_DAC_D0_P]
set_property PACKAGE_PIN   F17      [get_ports DB1_DAC_D1_N]
set_property PACKAGE_PIN   G17      [get_ports DB1_DAC_D1_P]
set_property PACKAGE_PIN   A17      [get_ports DB1_DAC_D2_N]
set_property PACKAGE_PIN   A16      [get_ports DB1_DAC_D2_P]
set_property PACKAGE_PIN   A18      [get_ports DB1_DAC_D3_N]
set_property PACKAGE_PIN   B18      [get_ports DB1_DAC_D3_P]
set_property PACKAGE_PIN   A21      [get_ports DB1_DAC_D4_N]
set_property PACKAGE_PIN   A20      [get_ports DB1_DAC_D4_P]
set_property PACKAGE_PIN   B20      [get_ports DB1_DAC_D5_N]
set_property PACKAGE_PIN   C20      [get_ports DB1_DAC_D5_P]
set_property PACKAGE_PIN   A22      [get_ports DB1_DAC_D6_N]
set_property PACKAGE_PIN   B22      [get_ports DB1_DAC_D6_P]
set_property PACKAGE_PIN   B19      [get_ports DB1_DAC_D7_N]
set_property PACKAGE_PIN   C19      [get_ports DB1_DAC_D7_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_DAC_D*}]

set_property PACKAGE_PIN   E21      [get_ports DB1_DAC_DCI_N]
set_property PACKAGE_PIN   F21      [get_ports DB1_DAC_DCI_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_DAC_DCI_*}]

set_property PACKAGE_PIN   C16      [get_ports DB1_DAC_FRAME_N]
set_property PACKAGE_PIN   D16      [get_ports DB1_DAC_FRAME_P]
set_property IOSTANDARD    LVDS_25  [get_ports {DB1_DAC_FRAME_*}]

#*******************************************************************************
## DB0 GPIO

set_property PACKAGE_PIN   G25      [get_ports DB0_RX_IO_0]
set_property PACKAGE_PIN   G30      [get_ports DB0_RX_IO_1]
set_property PACKAGE_PIN   H30      [get_ports DB0_RX_IO_2]
set_property PACKAGE_PIN   H27      [get_ports DB0_RX_IO_3]
set_property PACKAGE_PIN   H26      [get_ports DB0_RX_IO_4]
set_property PACKAGE_PIN   F30      [get_ports DB0_RX_IO_5]
set_property PACKAGE_PIN   G29      [get_ports DB0_RX_IO_6]
set_property PACKAGE_PIN   F27      [get_ports DB0_RX_IO_7]
set_property PACKAGE_PIN   G27      [get_ports DB0_RX_IO_8]
set_property PACKAGE_PIN   F28      [get_ports DB0_RX_IO_9]
set_property PACKAGE_PIN   G28      [get_ports DB0_RX_IO_10]
set_property PACKAGE_PIN   H25      [get_ports DB0_RX_IO_11]
set_property PACKAGE_PIN   H24      [get_ports DB0_RX_IO_12]
set_property PACKAGE_PIN   E30      [get_ports DB0_RX_IO_13]
set_property PACKAGE_PIN   E29      [get_ports DB0_RX_IO_14]
set_property PACKAGE_PIN   A30      [get_ports DB0_RX_IO_15]
set_property IOSTANDARD    LVCMOS33 [get_ports {DB0_RX_IO*}]

set_property PACKAGE_PIN   B25      [get_ports DB0_TX_IO_0]
set_property PACKAGE_PIN   C25      [get_ports DB0_TX_IO_1]
set_property PACKAGE_PIN   C26      [get_ports DB0_TX_IO_2]
set_property PACKAGE_PIN   D26      [get_ports DB0_TX_IO_3]
set_property PACKAGE_PIN   A26      [get_ports DB0_TX_IO_4]
set_property PACKAGE_PIN   A25      [get_ports DB0_TX_IO_5]
set_property PACKAGE_PIN   A28      [get_ports DB0_TX_IO_6]
set_property PACKAGE_PIN   B28      [get_ports DB0_TX_IO_7]
set_property PACKAGE_PIN   B24      [get_ports DB0_TX_IO_8]
set_property PACKAGE_PIN   C24      [get_ports DB0_TX_IO_9]
set_property PACKAGE_PIN   A27      [get_ports DB0_TX_IO_10]
set_property PACKAGE_PIN   B27      [get_ports DB0_TX_IO_11]
set_property PACKAGE_PIN   G24      [get_ports DB0_TX_IO_12]
set_property PACKAGE_PIN   G23      [get_ports DB0_TX_IO_13]
set_property PACKAGE_PIN   E26      [get_ports DB0_TX_IO_14]
set_property PACKAGE_PIN   F26      [get_ports DB0_TX_IO_15]
set_property IOSTANDARD    LVCMOS33 [get_ports {DB0_TX_IO*}]

#*******************************************************************************
## DB1 GPIO

set_property PACKAGE_PIN   F16      [get_ports DB1_RX_IO_0]
set_property PACKAGE_PIN   A15      [get_ports DB1_RX_IO_1]
set_property PACKAGE_PIN   B14      [get_ports DB1_RX_IO_2]
set_property PACKAGE_PIN   B15      [get_ports DB1_RX_IO_3]
set_property PACKAGE_PIN   C15      [get_ports DB1_RX_IO_4]
set_property PACKAGE_PIN   A13      [get_ports DB1_RX_IO_5]
set_property PACKAGE_PIN   B13      [get_ports DB1_RX_IO_6]
set_property PACKAGE_PIN   C14      [get_ports DB1_RX_IO_7]
set_property PACKAGE_PIN   D14      [get_ports DB1_RX_IO_8]
set_property PACKAGE_PIN   E15      [get_ports DB1_RX_IO_9]
set_property PACKAGE_PIN   E14      [get_ports DB1_RX_IO_10]
set_property PACKAGE_PIN   E16      [get_ports DB1_RX_IO_11]
set_property PACKAGE_PIN   F15      [get_ports DB1_RX_IO_12]
set_property PACKAGE_PIN   C11      [get_ports DB1_RX_IO_13]
set_property PACKAGE_PIN   D11      [get_ports DB1_RX_IO_14]
set_property PACKAGE_PIN   A12      [get_ports DB1_RX_IO_15]
set_property IOSTANDARD    LVCMOS33 [get_ports {DB1_RX_IO*}]

set_property PACKAGE_PIN   F13      [get_ports DB1_TX_IO_0]
set_property PACKAGE_PIN   G13      [get_ports DB1_TX_IO_1]
set_property PACKAGE_PIN   G14      [get_ports DB1_TX_IO_2]
set_property PACKAGE_PIN   H14      [get_ports DB1_TX_IO_3]
set_property PACKAGE_PIN   H12      [get_ports DB1_TX_IO_4]
set_property PACKAGE_PIN   H11      [get_ports DB1_TX_IO_5]
set_property PACKAGE_PIN   H16      [get_ports DB1_TX_IO_6]
set_property PACKAGE_PIN   J16      [get_ports DB1_TX_IO_7]
set_property PACKAGE_PIN   J12      [get_ports DB1_TX_IO_8]
set_property PACKAGE_PIN   J11      [get_ports DB1_TX_IO_9]
set_property PACKAGE_PIN   G15      [get_ports DB1_TX_IO_10]
set_property PACKAGE_PIN   H15      [get_ports DB1_TX_IO_11]
set_property PACKAGE_PIN   K11      [get_ports DB1_TX_IO_12]
set_property PACKAGE_PIN   L11      [get_ports DB1_TX_IO_13]
set_property PACKAGE_PIN   J14      [get_ports DB1_TX_IO_14]
set_property PACKAGE_PIN   K14      [get_ports DB1_TX_IO_15]
set_property IOSTANDARD    LVCMOS33 [get_ports {DB1_TX_IO*}]

#*******************************************************************************
## DB0 SPI

set_property PACKAGE_PIN   AE14     [get_ports DB0_DAC_SEN]
set_property IOSTANDARD    LVCMOS18 [get_ports DB0_DAC_SEN]

set_property PACKAGE_PIN   B29      [get_ports DB0_ADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_ADC_SEN]

set_property PACKAGE_PIN   E24      [get_ports DB0_MOSI]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_MOSI]

set_property PACKAGE_PIN   C27      [get_ports DB0_RX_LSADC_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_RX_LSADC_MISO]

set_property PACKAGE_PIN   E28      [get_ports DB0_RX_LSADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_RX_LSADC_SEN]

set_property PACKAGE_PIN   D27      [get_ports DB0_RX_LSDAC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_RX_LSDAC_SEN]

set_property PACKAGE_PIN   C29      [get_ports DB0_RX_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_RX_MISO]

set_property PACKAGE_PIN   D28      [get_ports DB0_RX_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_RX_SEN]

set_property PACKAGE_PIN   D24      [get_ports DB0_SCLK]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_SCLK]

set_property PACKAGE_PIN   B23      [get_ports DB0_TX_LSADC_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_TX_LSADC_MISO]

set_property PACKAGE_PIN   A23      [get_ports DB0_TX_LSADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_TX_LSADC_SEN]

set_property PACKAGE_PIN   F23      [get_ports DB0_TX_LSDAC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_TX_LSDAC_SEN]

set_property PACKAGE_PIN   E23      [get_ports DB0_TX_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_TX_MISO]

set_property PACKAGE_PIN   D23      [get_ports DB0_TX_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB0_TX_SEN]

#*******************************************************************************
# DB1 SPI

set_property PACKAGE_PIN   AE15     [get_ports DB1_DAC_SEN]
set_property IOSTANDARD    LVCMOS18 [get_ports DB1_DAC_SEN]

set_property PACKAGE_PIN   B12      [get_ports DB1_ADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_ADC_SEN]

set_property PACKAGE_PIN   L12      [get_ports DB1_MOSI]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_MOSI]

set_property PACKAGE_PIN   D13      [get_ports DB1_RX_LSADC_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_RX_LSADC_MISO]

set_property PACKAGE_PIN   F12      [get_ports DB1_RX_LSADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_RX_LSADC_SEN]

set_property PACKAGE_PIN   D12      [get_ports DB1_RX_LSDAC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_RX_LSDAC_SEN]

set_property PACKAGE_PIN   C12      [get_ports DB1_RX_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_RX_MISO]

set_property PACKAGE_PIN   E13      [get_ports DB1_RX_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_RX_SEN]

set_property PACKAGE_PIN   L13      [get_ports DB1_SCLK]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_SCLK]

set_property PACKAGE_PIN   L16      [get_ports DB1_TX_LSADC_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_TX_LSADC_MISO]

set_property PACKAGE_PIN   K16      [get_ports DB1_TX_LSADC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_TX_LSADC_SEN]

set_property PACKAGE_PIN   G12      [get_ports DB1_TX_LSDAC_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_TX_LSDAC_SEN]

set_property PACKAGE_PIN   L15      [get_ports DB1_TX_MISO]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_TX_MISO]

set_property PACKAGE_PIN   K15      [get_ports DB1_TX_SEN]
set_property IOSTANDARD    LVCMOS33 [get_ports DB1_TX_SEN]

#*******************************************************************************
## DB Misc

set_property PACKAGE_PIN   AB15     [get_ports DB_DAC_MOSI]
set_property IOSTANDARD    LVCMOS18 [get_ports DB_DAC_MOSI]

set_property PACKAGE_PIN   AA15     [get_ports DB_DAC_SCLK]
set_property IOSTANDARD    LVCMOS18 [get_ports DB_DAC_SCLK]

set_property PACKAGE_PIN   F25      [get_ports DB_SCL]
set_property IOSTANDARD    LVCMOS33 [get_ports DB_SCL]

set_property PACKAGE_PIN   E25      [get_ports DB_SDA]
set_property IOSTANDARD    LVCMOS33 [get_ports DB_SDA]

set_property PACKAGE_PIN   B30      [get_ports DB_ADC_RESET]
set_property IOSTANDARD    LVCMOS33 [get_ports DB_ADC_RESET]

set_property PACKAGE_PIN   AC16     [get_ports DB_DAC_RESET]
set_property IOSTANDARD    LVCMOS18 [get_ports DB_DAC_RESET]

set_property PACKAGE_PIN   AC14     [get_ports DB0_DAC_ENABLE]
set_property IOSTANDARD    LVCMOS18 [get_ports DB0_DAC_ENABLE]

set_property PACKAGE_PIN   AC15     [get_ports DB1_DAC_ENABLE]
set_property IOSTANDARD    LVCMOS18 [get_ports DB1_DAC_ENABLE]

#*******************************************************************************
## STC3 Pin Mapping

set_property PACKAGE_PIN AC27  [get_ports aIoResetIn_n]
set_property IOSTANDARD  LVTTL [get_ports aIoResetIn_n]

set_property PACKAGE_PIN AH29  [get_ports aIoReadyIn]
set_property IOSTANDARD  LVTTL [get_ports aIoReadyIn]

set_property PACKAGE_PIN AE28  [get_ports aIoReadyOut]
set_property IOSTANDARD  LVTTL [get_ports aIoReadyOut]

set_property PACKAGE_PIN AD18  [get_ports IoRxClock]
set_property PACKAGE_PIN AE18  [get_ports IoRxClock_n]
set_property IOSTANDARD  LVDS  [get_ports {IoRxClock*}]
set_property DIFF_TERM   TRUE  [get_ports {IoRxClock*}]

set_property PACKAGE_PIN AK18  [get_ports {irIoRxData_n[0]}]
set_property PACKAGE_PIN AJ17  [get_ports {irIoRxData_n[1]}]
set_property PACKAGE_PIN AK19  [get_ports {irIoRxData_n[2]}]
set_property PACKAGE_PIN AG14  [get_ports {irIoRxData_n[3]}]
set_property PACKAGE_PIN AH15  [get_ports {irIoRxData_n[4]}]
set_property PACKAGE_PIN Y18   [get_ports {irIoRxData_n[5]}]
set_property PACKAGE_PIN AG17  [get_ports {irIoRxData_n[6]}]
set_property PACKAGE_PIN AE19  [get_ports {irIoRxData_n[7]}]
set_property PACKAGE_PIN AC17  [get_ports {irIoRxData_n[8]}]
set_property PACKAGE_PIN AD16  [get_ports {irIoRxData_n[9]}]
set_property PACKAGE_PIN AJ16  [get_ports {irIoRxData_n[10]}]
set_property PACKAGE_PIN AC19  [get_ports {irIoRxData_n[11]}]
set_property PACKAGE_PIN AB18  [get_ports {irIoRxData_n[12]}]
set_property PACKAGE_PIN AF16  [get_ports {irIoRxData_n[13]}]
set_property PACKAGE_PIN AH19  [get_ports {irIoRxData_n[14]}]
set_property PACKAGE_PIN AK15  [get_ports {irIoRxData_n[15]}]

set_property PACKAGE_PIN AJ18  [get_ports {irIoRxData[0]}]
set_property PACKAGE_PIN AH17  [get_ports {irIoRxData[1]}]
set_property PACKAGE_PIN AJ19  [get_ports {irIoRxData[2]}]
set_property PACKAGE_PIN AF15  [get_ports {irIoRxData[3]}]
set_property PACKAGE_PIN AG15  [get_ports {irIoRxData[4]}]
set_property PACKAGE_PIN Y19   [get_ports {irIoRxData[5]}]
set_property PACKAGE_PIN AF17  [get_ports {irIoRxData[6]}]
set_property PACKAGE_PIN AD19  [get_ports {irIoRxData[7]}]
set_property PACKAGE_PIN AB17  [get_ports {irIoRxData[8]}]
set_property PACKAGE_PIN AD17  [get_ports {irIoRxData[9]}]
set_property PACKAGE_PIN AH16  [get_ports {irIoRxData[10]}]
set_property PACKAGE_PIN AB19  [get_ports {irIoRxData[11]}]
set_property PACKAGE_PIN AA18  [get_ports {irIoRxData[12]}]
set_property PACKAGE_PIN AE16  [get_ports {irIoRxData[13]}]
set_property PACKAGE_PIN AG19  [get_ports {irIoRxData[14]}]
set_property PACKAGE_PIN AK16  [get_ports {irIoRxData[15]}]

set_property IOSTANDARD  LVDS  [get_ports {irIoRxData*}]
set_property DIFF_TERM   TRUE  [get_ports {irIoRxData*}]

set_property IOSTANDARD  LVDS  [get_ports {irIoRxHeader*}]
set_property DIFF_TERM   TRUE  [get_ports {irIoRxHeader*}]
set_property PACKAGE_PIN AF18  [get_ports irIoRxHeader]
set_property PACKAGE_PIN AG18  [get_ports irIoRxHeader_n]

set_property IOSTANDARD  LVDS_25 [get_ports {IoTxClock*}]
set_property PACKAGE_PIN AE24    [get_ports IoTxClock_n]
set_property PACKAGE_PIN AD23    [get_ports IoTxClock]

set_property IOSTANDARD  LVDS_25 [get_ports {itIoTxData*}]
set_property PACKAGE_PIN AF25    [get_ports {itIoTxData_n[0]}]
set_property PACKAGE_PIN AC25    [get_ports {itIoTxData_n[1]}]
set_property PACKAGE_PIN AH22    [get_ports {itIoTxData_n[2]}]
set_property PACKAGE_PIN AF21    [get_ports {itIoTxData_n[3]}]
set_property PACKAGE_PIN AD24    [get_ports {itIoTxData_n[4]}]
set_property PACKAGE_PIN AB20    [get_ports {itIoTxData_n[5]}]
set_property PACKAGE_PIN AA21    [get_ports {itIoTxData_n[6]}]
set_property PACKAGE_PIN AH25    [get_ports {itIoTxData_n[7]}]
set_property PACKAGE_PIN AB23    [get_ports {itIoTxData_n[8]}]
set_property PACKAGE_PIN AK25    [get_ports {itIoTxData_n[9]}]
set_property PACKAGE_PIN AK24    [get_ports {itIoTxData_n[10]}]
set_property PACKAGE_PIN AD22    [get_ports {itIoTxData_n[11]}]
set_property PACKAGE_PIN AF23    [get_ports {itIoTxData_n[12]}]
set_property PACKAGE_PIN AE21    [get_ports {itIoTxData_n[13]}]
set_property PACKAGE_PIN AC21    [get_ports {itIoTxData_n[14]}]
set_property PACKAGE_PIN AA23    [get_ports {itIoTxData_n[15]}]
set_property PACKAGE_PIN AE25    [get_ports {itIoTxData[0]}]
set_property PACKAGE_PIN AB24    [get_ports {itIoTxData[1]}]
set_property PACKAGE_PIN AG22    [get_ports {itIoTxData[2]}]
set_property PACKAGE_PIN AF20    [get_ports {itIoTxData[3]}]
set_property PACKAGE_PIN AC24    [get_ports {itIoTxData[4]}]
set_property PACKAGE_PIN AA20    [get_ports {itIoTxData[5]}]
set_property PACKAGE_PIN Y21     [get_ports {itIoTxData[6]}]
set_property PACKAGE_PIN AG25    [get_ports {itIoTxData[7]}]
set_property PACKAGE_PIN AB22    [get_ports {itIoTxData[8]}]
set_property PACKAGE_PIN AJ24    [get_ports {itIoTxData[9]}]
set_property PACKAGE_PIN AK23    [get_ports {itIoTxData[10]}]
set_property PACKAGE_PIN AC22    [get_ports {itIoTxData[11]}]
set_property PACKAGE_PIN AE23    [get_ports {itIoTxData[12]}]
set_property PACKAGE_PIN AD21    [get_ports {itIoTxData[13]}]
set_property PACKAGE_PIN AC20    [get_ports {itIoTxData[14]}]
set_property PACKAGE_PIN AA22    [get_ports {itIoTxData[15]}]

set_property IOSTANDARD  LVDS_25 [get_ports {itIoTxHeader*}]
set_property PACKAGE_PIN Y24     [get_ports itIoTxHeader_n]
set_property PACKAGE_PIN Y23     [get_ports itIoTxHeader]

set_property IOSTANDARD  LVTTL   [get_ports aIrq]
set_property PACKAGE_PIN AF28    [get_ports aIrq]

set_property IOSTANDARD  LVCMOS33 [get_ports aIoPort2Restart]
set_property PULLUP      TRUE     [get_ports aIoPort2Restart]
set_property PACKAGE_PIN AE30     [get_ports aIoPort2Restart]

set_property IOSTANDARD  LVCMOS33 [get_ports aStc3Gpio7]
set_property PACKAGE_PIN AF30     [get_ports aStc3Gpio7]

#*******************************************************************************
## Front Panel LEDs

set_property PACKAGE_PIN   AJ26     [get_ports LED_ACT1]
set_property PACKAGE_PIN   AE26     [get_ports LED_ACT2]
set_property PACKAGE_PIN   AF27     [get_ports LED_LINK1]
set_property PACKAGE_PIN   AK26     [get_ports LED_LINK2]
set_property PACKAGE_PIN   AF26     [get_ports LED_PPS]
set_property PACKAGE_PIN   AH27     [get_ports LED_REFLOCK]
set_property PACKAGE_PIN   U30      [get_ports LED_GPSLOCK]
set_property PACKAGE_PIN   V27      [get_ports LED_LINKSTAT]
set_property PACKAGE_PIN   V29      [get_ports LED_LINKACT]
set_property PACKAGE_PIN   AD29     [get_ports LED_RX1_RX]
set_property PACKAGE_PIN   AB30     [get_ports LED_RX2_RX]
set_property PACKAGE_PIN   AA30     [get_ports LED_TXRX1_RX]
set_property PACKAGE_PIN   Y30      [get_ports LED_TXRX1_TX]
set_property PACKAGE_PIN   AB29     [get_ports LED_TXRX2_RX]
set_property PACKAGE_PIN   AE29     [get_ports LED_TXRX2_TX]
set_property IOSTANDARD    LVCMOS33 [get_ports {LED_*}]

#*******************************************************************************
## Front panel GPIO on DB15

set_property PACKAGE_PIN   Y25      [get_ports {FrontPanelGpio_0}]
set_property PACKAGE_PIN   AD27     [get_ports {FrontPanelGpio_1}]
set_property PACKAGE_PIN   AD28     [get_ports {FrontPanelGpio_2}]
set_property PACKAGE_PIN   AG30     [get_ports {FrontPanelGpio_3}]
set_property PACKAGE_PIN   AH30     [get_ports {FrontPanelGpio_4}]
set_property PACKAGE_PIN   AC26     [get_ports {FrontPanelGpio_5}]
set_property PACKAGE_PIN   AD26     [get_ports {FrontPanelGpio_6}]
set_property PACKAGE_PIN   AJ27     [get_ports {FrontPanelGpio_7}]
set_property PACKAGE_PIN   AK28     [get_ports {FrontPanelGpio_8}]
set_property PACKAGE_PIN   AG27     [get_ports {FrontPanelGpio_9}]
set_property PACKAGE_PIN   AG28     [get_ports {FrontPanelGpio_10}]
set_property PACKAGE_PIN   AH26     [get_ports {FrontPanelGpio_11}]
set_property IOSTANDARD    LVCMOS33 [get_ports {FrontPanelGpio*}]
set_property PULLDOWN      TRUE     [get_ports {FrontPanelGpio*}]

#*******************************************************************************
## LMK04816 Clock Control

set_property PACKAGE_PIN   Y26      [get_ports LMK_Status_0]
set_property PACKAGE_PIN   AA26     [get_ports LMK_Status_1]
set_property PACKAGE_PIN   W27      [get_ports LMK_Holdover]
set_property PACKAGE_PIN   W28      [get_ports LMK_Lock]
set_property PACKAGE_PIN   T27      [get_ports LMK_Sync]
set_property PACKAGE_PIN   U19      [get_ports LMK_SEN]
set_property PACKAGE_PIN   Y28      [get_ports LMK_MOSI]
set_property PACKAGE_PIN   AA28     [get_ports LMK_SCLK]
set_property IOSTANDARD    LVCMOS33 [get_ports {LMK_*}]

#*******************************************************************************
# Micrel chip control

set_property PACKAGE_PIN   W29      [get_ports ClockRefSelect_0]
set_property PACKAGE_PIN   Y29      [get_ports ClockRefSelect_1]
set_property IOSTANDARD    LVCMOS33 [get_ports {ClockRefSelect*}]

#*******************************************************************************
## PPS, GPS and Timing

set_property PACKAGE_PIN   AB28     [get_ports GPS_LOCK_OK]
# set_property PACKAGE_PIN  AA25     [get_ports GPS_NMEA_TX]
set_property PACKAGE_PIN   AB25     [get_ports GPS_SER_IN]
set_property PACKAGE_PIN   AC29     [get_ports GPS_SER_OUT]
set_property PACKAGE_PIN   AA27     [get_ports GPS_PPS_OUT]
set_property IOSTANDARD    LVCMOS33 [get_ports {GPS_*}]

set_property PACKAGE_PIN   AC30     [get_ports EXT_PPS_IN]
set_property PACKAGE_PIN   T25      [get_ports EXT_PPS_OUT]
set_property IOSTANDARD    LVCMOS33 [get_ports {EXT_PPS_*}]

#*******************************************************************************
## Clocks

set_property PACKAGE_PIN   AB27     [get_ports FPGA_125MHz_CLK]
set_property IOSTANDARD    LVCMOS33 [get_ports FPGA_125MHz_CLK]

set_property PACKAGE_PIN   AG23     [get_ports FPGA_CLK_N]
set_property IOSTANDARD    LVDS_25  [get_ports FPGA_CLK_N]
set_property DIFF_TERM     TRUE     [get_ports FPGA_CLK_N]

set_property PACKAGE_PIN   AF22     [get_ports FPGA_CLK_P]
set_property IOSTANDARD    LVDS_25  [get_ports FPGA_CLK_P]
set_property DIFF_TERM     TRUE     [get_ports FPGA_CLK_P]

set_property PACKAGE_PIN   E11      [get_ports GPSDO_PWR_ENA]
set_property IOSTANDARD    LVCMOS33 [get_ports GPSDO_PWR_ENA]

set_property PACKAGE_PIN   A11      [get_ports TCXO_ENA]
set_property IOSTANDARD    LVCMOS33 [get_ports TCXO_ENA]

set_property PACKAGE_PIN   AG24     [get_ports FPGA_REFCLK_10MHz_p]
set_property IOSTANDARD    LVDS_25  [get_ports FPGA_REFCLK_10MHz_p]

set_property PACKAGE_PIN   AH24     [get_ports FPGA_REFCLK_10MHz_n]
set_property IOSTANDARD    LVDS_25  [get_ports FPGA_REFCLK_10MHz_n]

set_property PACKAGE_PIN   AA17     [get_ports CPRI_CLK_OUT_P]
set_property IOSTANDARD    LVDS     [get_ports CPRI_CLK_OUT_P]

set_property PACKAGE_PIN   AA16     [get_ports CPRI_CLK_OUT_N]
set_property IOSTANDARD    LVDS     [get_ports CPRI_CLK_OUT_N]

#*******************************************************************************
## SFP low-speed IO

set_property PACKAGE_PIN   U24      [get_ports SFPP0_SCL]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_SCL]

set_property PACKAGE_PIN   V22      [get_ports SFPP0_SDA]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_SDA]

set_property PACKAGE_PIN   W22      [get_ports SFPP0_RS0]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_RS0]

set_property PACKAGE_PIN   W19      [get_ports SFPP0_RS1]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_RS1]

set_property PACKAGE_PIN   V21      [get_ports SFPP0_TxDisable]   ;# Open drain output
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_TxDisable]

set_property PACKAGE_PIN   V24      [get_ports SFPP0_ModAbs]      ;# (IJB) Should pullup on pcb
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_ModAbs]

set_property PACKAGE_PIN   W21      [get_ports SFPP0_RxLOS]       ;# Input
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_RxLOS]

set_property PACKAGE_PIN   U23      [get_ports SFPP0_TxFault]     ;# Input
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP0_TxFault]

set_property PACKAGE_PIN   V19      [get_ports SFPP1_SCL]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_SCL]

set_property PACKAGE_PIN   W26      [get_ports SFPP1_SDA]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_SDA]

set_property PACKAGE_PIN   W24      [get_ports SFPP1_RS0]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_RS0]

set_property PACKAGE_PIN   U22      [get_ports SFPP1_RS1]
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_RS1]

set_property PACKAGE_PIN   V25      [get_ports SFPP1_TxDisable]   ;# Open drain output
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_TxDisable]

set_property PACKAGE_PIN   V20      [get_ports SFPP1_ModAbs]      ;# (IJB) Should pullup on pcb
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_ModAbs]

set_property PACKAGE_PIN   W23      [get_ports SFPP1_RxLOS]       ;# Input
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_RxLOS]

set_property PACKAGE_PIN   V30      [get_ports SFPP1_TxFault]     ;# Input
set_property IOSTANDARD    LVCMOS33 [get_ports SFPP1_TxFault]

#*******************************************************************************
## Config Flash Interface

set_property PROHIBIT      TRUE     [get_sites {P24}]
set_property PROHIBIT      TRUE     [get_sites {R25}]
set_property PROHIBIT      TRUE     [get_sites {R20}]
set_property PROHIBIT      TRUE     [get_sites {R21}]
set_property PROHIBIT      TRUE     [get_sites {T20}]
set_property PROHIBIT      TRUE     [get_sites {T21}]
set_property PROHIBIT      TRUE     [get_sites {T22}]
set_property PROHIBIT      TRUE     [get_sites {T23}]
set_property PROHIBIT      TRUE     [get_sites {U20}]
set_property PROHIBIT      TRUE     [get_sites {P29}]
set_property PROHIBIT      TRUE     [get_sites {R29}]
set_property PROHIBIT      TRUE     [get_sites {P27}]
set_property PROHIBIT      TRUE     [get_sites {P28}]
set_property PROHIBIT      TRUE     [get_sites {T30}]
set_property PROHIBIT      TRUE     [get_sites {P26}]
set_property PROHIBIT      TRUE     [get_sites {R26}]

#*******************************************************************************
# Miscellaneous

# Expose this pin to work around a silicon bug in Series 7 FPGA where
# race condition with the reading of PUDC during the erase of the FPGA
# image cause glitches on output IO pins
set_property PACKAGE_PIN   R23      [get_ports FPGA_PUDC_B]
set_property IOSTANDARD    LVCMOS33 [get_ports FPGA_PUDC_B]
set_property PULLUP        TRUE     [get_ports FPGA_PUDC_B]
#
# Copyright 2014 Ettus Research LLC
#

#*******************************************************************************
## Primary clock definitions

# Define clocks
create_clock -name FPGA_125MHz_CLK     -period   8.000 -waveform {0.000 4.000}   [get_ports FPGA_125MHz_CLK]
create_clock -name IoRxClock           -period   4.000 -waveform {0.000 2.000}   [get_ports IoRxClock]
create_clock -name FPGA_REFCLK_10MHz_p -period   100.0 -waveform {0.000 50.000}  [get_ports FPGA_REFCLK_10MHz_p]

# Set clock properties

# The PCIe specific 40MHz and 200MHz clocks are only active in clock regious X0Y0 and X1Y0 so we use BUFHs
# to distribute them. To do so, we have to use a PLL because the MMCM in that region is used by radio_clk_gen
# Since that MMCM is LOC constrained, we must LOC constrain this PLL as well.
set_property LOC PLLE2_ADV_X0Y0 [get_cells -hierarchical -filter {NAME =~ "*/pcie_clk_pll2_inst"}]


#*******************************************************************************
## Generated clock definitions

create_generated_clock -name IoTxClock -multiply_by 1 \
                       -source [get_pins -hier -filter {NAME =~ */lvfpga_chinch_inst/*/TxClockGenx/TxUseMmcm.TxMmcm/CLKOUT0}] \
                       [get_ports {IoTxClock}]

#*******************************************************************************
## Aliases for auto-generated clocks

create_generated_clock -name ce_clk                   [get_pins -hierarchical -filter {NAME =~ "*/bus_clk_pll2_inst/CLKOUT0"}]
create_generated_clock -name ioport2_clk              [get_pins -hierarchical -filter {NAME =~ "*/bus_clk_pll2_inst/CLKFBOUT"}]
create_generated_clock -name rio40_clk                [get_pins -hierarchical -filter {NAME =~ "*/pcie_clk_pll2_inst/CLKOUT0"}]
create_generated_clock -name ioport2_idelay_ref_clk   [get_pins -hierarchical -filter {NAME =~ "*/pcie_clk_pll2_inst/CLKOUT1"}]


#*******************************************************************************
## Asynchronous clock groups

set_clock_groups -asynchronous -group [get_clocks ioport2_clk]  -group [get_clocks rio40_clk]
set_clock_groups -asynchronous -group [get_clocks ioport2_clk]  -group [get_clocks IoPort2Wrapperx/RxLowSpeedClk]
set_clock_groups -asynchronous -group [get_clocks timeclk_unbuf] -group [get_clocks ce_clk]


#*******************************************************************************
## IoPort2

# Constrain the location of the IDELAYCTERL associated with the interface trainer IDELAYs
set_property LOC IDELAYCTRL_X1Y0 [get_cells -hierarchical -filter { NAME =~ "*/lvfpga_chinch_inst/IDELAYCTRLx" }]

# RX Pad Input constraints
set_input_delay -clock [get_clocks IoRxClock] -max 2.580                        [get_ports {irIoRx*}]
set_input_delay -clock [get_clocks IoRxClock] -min 2.280                        [get_ports {irIoRx*}]
set_input_delay -clock [get_clocks IoRxClock] -max 2.580 -clock_fall -add_delay [get_ports {irIoRx*}]
set_input_delay -clock [get_clocks IoRxClock] -min 2.280 -clock_fall -add_delay [get_ports {irIoRx*}]

# Note: The input clock N-Side ISERDES is not constrained for IO timing since
# adding an input delay does not work as the clock and data are the same.
# Since the architecture requires dedicated routes, the build-to-build
# variablilty will be zero and therefore, no separate timing constraint
# is necessary for the N-Side pin. The RxClock delay is constrained because
# of the input delay constraints on the rest of the bus. This path does, however,
# require a max delay constraint in order to override the default analysis:
set_max_delay -from [get_ports {IoRxClock*}]                                                       \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Wrapperx/RxClockGenx/RxClockSerdes*}] \
              2.0 -datapath_only

# TX Pad Output constraints
set_output_delay -clock [get_clocks IoTxClock] -max 1.600                        [get_ports {itIoTx*}]
set_output_delay -clock [get_clocks IoTxClock] -min 0.400                        [get_ports {itIoTx*}]
set_output_delay -clock [get_clocks IoTxClock] -max 1.600 -clock_fall -add_delay [get_ports {itIoTx*}]
set_output_delay -clock [get_clocks IoTxClock] -min 0.400 -clock_fall -add_delay [get_ports {itIoTx*}]

# These signals are all treated as async signals so no stringent timing requirements are needed.
set_max_delay -to [get_ports aIrq*]              10.000
set_max_delay -from [get_ports aIoResetIn_n]     10.000
set_max_delay -from [get_ports aIoReadyIn]       10.000
set_max_delay -to [get_ports aIoReadyOut]        10.000
set_max_delay -to [get_ports aIoPort2Restart]    10.000
set_false_path -from [get_ports aStc3Gpio7]

# Async reset
set_false_path -from [get_cells -hier -filter {NAME =~ */lvfpga_chinch_inst/*StartupFsmx/aResetLcl*}]

# Double Sync
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Wrapperx/tIoResetSync/DoubleSyncBasex/iDlySig*}]                            \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Wrapperx/tIoResetSync/DoubleSyncBasex/DoubleSyncAsyncInBasex/oSig_ms*}]     \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Wrapperx/bIoResetAckSync/DoubleSyncBasex/iDlySig*}]                         \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Wrapperx/bIoResetAckSync/DoubleSyncBasex/DoubleSyncAsyncInBasex/oSig_ms*}]  \
              6.0 -datapath_only

# Constrains HandshakeSLVx and IClkToPushClkHs in ControlIoDelayClockCross
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*iLclStoredData*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*ODataFlop*}]      \
              8.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/iPushToggle}]           \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/BlkOut.oPushToggle0_ms*}] \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/BlkOut.oPushToggle0_ms*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/BlkOut.oPushToggle1*}]    \
              4.0

set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*oPushToggleToReady*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*iRdyPushToggle_ms*}]  \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*iRdyPushToggle_ms*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/*ControlIoDelayClockCrossx/*/HBx/*iRdyPushToggle*}]    \
              4.0

# SamplerResultsHandshake and SamplerControlHandshake
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*iLclStoredData*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*ODataFlop*}]      \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/iPushToggle}]           \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/BlkOut.oPushToggle0_ms*}] \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/BlkOut.oPushToggle0_ms*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/BlkOut.oPushToggle1*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*oPushToggleToReady*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*iRdyPushToggle_ms*}]   \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*iRdyPushToggle_ms*}]   \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/ClockSamplerBlock.Sampler*Handshake/HBx/*iRdyPushToggle*}]      \
              4.0

# Constrain PhyResetSync PulseSync
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/iHoldSigInx*}]     \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/oHoldSigIn_msx*}]  \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/oHoldSigIn_msx*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/oLocalSigOutCEx*}] \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/oLocalSigOutCEx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/iSigOut_msx*}]     \
              4.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/iSigOut_msx*}]     \
              -to   [get_cells -hier -filter {NAME =~ *IoPortClkDelayTrainerx/TrainerBlock.PhyResetSync/PulseSyncBasex/iSigOutx*}]        \
              4.0

# IoPort2 Core Clock Crossings
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoPort2Receiverx/PacketReceivedDoublesync*iDlySigx*}]               \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoPort2Receiverx/PacketReceivedDoublesync*DoubleSyncAsyncInBasex*}] \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoPort2Receiverx/PacketReceivedDoublesync*DoubleSyncAsyncInBasex/oSig_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoPort2Receiverx/PacketReceivedDoublesync*DoubleSyncAsyncInBasex/oSigx*}]    \
              6.0

# Handshake
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*iLclStoredData*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*ODataFlop*}]      \
              10.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/iPushToggle}]           \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/BlkOut.oPushToggle0_ms*}] \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/BlkOut.oPushToggle0_ms*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/BlkOut.oPushToggle1*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*oPushToggleToReady*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*iRdyPushToggle_ms*}]  \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*iRdyPushToggle_ms*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/CreditManager*/HBx/*iRdyPushToggle*}]     \
              4.0

# FIFO Clock Crossings
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/FifoFlags/ieInputCountGrayx*}]    \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/FifoFlags/oInputCountGray_msx*}]  \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/FifoFlags/oInputCountGray_msx*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/FifoFlags/oInputCountGrayx*}]     \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/PacketFullyReceived/ieInputCountGrayx*}]   \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/PacketFullyReceived/oInputCountGray_msx*}] \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/PacketFullyReceived/oInputCountGray_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/ReceiveSide.IoReceiveFifoBasex/PacketFullyReceived/oInputCountGrayx*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/ieInputCountGrayx*}]    \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/oInputCountGray_msx*}]  \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/oInputCountGray_msx*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/oInputCountGrayx*}]     \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/oeOutputCountGrayx*}]   \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/iOutputCountGray_msx*}] \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/iOutputCountGray_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.PacketFullyReceived/iOutputCountGrayx*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/ieInputCountGrayx*}]    \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/oInputCountGray_msx*}]  \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/oInputCountGray_msx*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/oInputCountGrayx*}]     \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/oeOutputCountGrayx*}]   \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/iOutputCountGray_msx*}] \
              5.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/iOutputCountGray_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/iOutputCountGrayx*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo.InputFifo.FifoFlags/oeOutputCountGrayx*}]   \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/TransmitFifo*DualPortRAMx*oDlyAddr*}]                    \
              5.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*iLclStoredData*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*ODataFlop*}]      \
              10.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/iPushToggle}]           \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/BlkOut.oPushToggle0_ms*}] \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/BlkOut.oPushToggle0_ms*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/BlkOut.oPushToggle1*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*oPushToggleToReady*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*iRdyPushToggle_ms*}]  \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*iRdyPushToggle_ms*}]  \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/TransmitSide.IoTransmitFifox/CreditManager.HandshakeCredits/HBx/*iRdyPushToggle*}]     \
              4.0

# Double Sync
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/Startup.DoubleSyncEnableTransmit/iDlySigx*}]                         \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/Startup.DoubleSyncEnableTransmit/*DoubleSyncAsyncInBasex/oSig_msx*}] \
              6.0 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/Startup.DoubleSyncEnableTransmit/*DoubleSyncAsyncInBasex/oSig_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2x/IoPort2Basex/Startup.DoubleSyncEnableTransmit/*DoubleSyncAsyncInBasex/oSigx*}]    \
              4.0
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/DoubleSyncWidePortMode.DoubleSync*WidePortMode/iDlySigx*}]               \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/DoubleSyncWidePortMode.DoubleSync*WidePortMode/DoubleSyncAsyncInBasex*}] \
              6.0 -datapath_only -quiet
set_max_delay -from [get_cells -hier -filter {NAME =~ *IoPort2Basex/DoubleSyncWidePortMode.DoubleSync*WidePortMode/DoubleSyncAsyncInBasex/oSig_msx*}] \
              -to   [get_cells -hier -filter {NAME =~ *IoPort2Basex/DoubleSyncWidePortMode.DoubleSync*WidePortMode/DoubleSyncAsyncInBasex/oSigx*}]    \
              5.0 -quiet

#*******************************************************************************
## Asynchronous paths

set_false_path -to   [get_ports LED_*]
set_false_path -to [get_pins -of_objects [get_cells -hier -filter { NAME =~ *s_reset_hold_reg* }] -filter {IS_PRESET || IS_RESET}]
set_false_path -from [get_pins -hierarchical "*s_src_data_reg*/C"] -to [get_pins -hierarchical "*dst_out_reg*/D"]
set_false_path -to [get_pins -hierarchical "*s_dest_reg_reg*/D"]
set_false_path -to   [get_ports {SFPP*_RS0 SFPP*_RS1 SFPP*_SCL SFPP*_SDA SFPP*_TxDisable}]
set_false_path -from [get_ports {SFPP*_ModAbs SFPP*_RxLOS SFPP*_SCL SFPP*_SDA SFPP*_TxFault}]

# #*******************************************************************************
# ## Miscellaneous Interfaces
#
# # Dboard and Front-Panel GPIO Interfaces
# # We force the registers closest to the PADs into the IOB to achieve lowest skew between individual bits
# # in the parallel bus. However, as a sanity check we add the following constraints that will fail if the
# # registers don't get placed in the IOB for whatever reason.
# set_max_delay 6.000 -to   [get_ports * -filter {(DIRECTION == OUT || DIRECTION == INOUT) && NAME =~ "DB*_*X_IO*"}]
# set_max_delay 3.000 -from [get_ports * -filter {(DIRECTION == IN  || DIRECTION == INOUT) && NAME =~ "DB*_*X_IO*"}]
# set_max_delay 6.000 -to   [get_ports * -filter {(DIRECTION == OUT || DIRECTION == INOUT) && NAME =~ "FrontPanelGpio*"}]
# set_max_delay 3.000 -from [get_ports * -filter {(DIRECTION == IN  || DIRECTION == INOUT) && NAME =~ "FrontPanelGpio*"}]
#
# # SPI Lines
# set_max_delay 10.000 -datapath_only \
#                      -from [get_ports {DB*_*X*MISO*}]
# set_max_delay 10.000 -to   [get_ports {DB*_*SCLK DB*_*SEN DB*_*MOSI}]
# set_max_delay 10.000 -to   [get_ports {DB_SCL DB_SDA DB0_DAC_ENABLE DB1_DAC_ENABLE DB_ADC_RESET DB_DAC_RESET}]
# set_max_delay 10.000 -from [get_ports {DB_SCL DB_SDA DB_DAC_MOSI}]
#
# # Clock distribution chip control
# set_max_delay -from   [get_ports {LMK_Status[*] LMK_Holdover LMK_Lock LMK_Sync}] 10.000
# set_max_delay -to     [get_ports {LMK_SEN LMK_MOSI LMK_SCLK}]                    10.000
# set_max_delay -to     [get_ports {ClockRefSelect*}]                              10.000
# set_max_delay -to     [get_ports {TCXO_ENA}]                                     10.000
## From Xilinx ten_gig_eth_pcs_pma_example_design
##
## (c) Copyright 2009 - 2014 Xilinx, Inc. All rights reserved.
##
## This file contains confidential and proprietary information
## of Xilinx, Inc. and is protected under U.S. and
## international copyright and other intellectual property
## laws.
##
## DISCLAIMER
## This disclaimer is not a license and does not grant any
## rights to the materials distributed herewith. Except as
## otherwise provided in a valid license issued to you by
## Xilinx, and to the maximum extent permitted by applicable
## law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
## WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
## AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
## BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
## INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
## (2) Xilinx shall not be liable (whether in contract or tort,
## including negligence, or under any other theory of
## liability) for any loss or damage of any kind or nature
## related to, arising under or in connection with these
## materials, including for any direct, or any indirect,
## special, incidental, or consequential loss or damage
## (including loss of data, profits, goodwill, or any type of
## loss or damage suffered as a result of any action brought
## by a third party) even if such damage or loss was
## reasonably foreseeable or Xilinx had been advised of the
## possibility of the same.
##
## CRITICAL APPLICATIONS
## Xilinx products are not designed or intended to be fail-
## safe, or for use in any application requiring fail-safe
## performance, such as life-support or safety devices or
## systems, Class III medical devices, nuclear facilities,
## applications related to the deployment of airbags, or any
## other applications that could lead to death, personal
## injury, or severe property or environmental damage
## (individually and collectively, "Critical
## Applications"). Customer assumes the sole risk and
## liability of any use of Xilinx products in Critical
## Applications, subject only to applicable laws and
## regulations governing limitations on product liability.
##
## THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
## PART OF THIS FILE AT ALL TIMES.


## From Xilinx ten_gig_eth_pcs_pma_example_design

set_property PACKAGE_PIN   R8       [get_ports XG_CLK_p]
set_property PACKAGE_PIN   R7       [get_ports XG_CLK_n]

# GT location
set_property LOC GTXE2_CHANNEL_X0Y0 [get_cells -hierarchical -filter {NAME =~ "*/ten_gig_eth_phy_0_inst/*/gtxe2_i*" && PRIMITIVE_TYPE == IO.gt.GTXE2_CHANNEL}]
set_property LOC GTXE2_COMMON_X0Y0  [get_cells -hierarchical -filter {NAME =~ "*/ten_gig_eth_phy_0_inst/*" && PRIMITIVE_TYPE == IO.gt.GTXE2_COMMON}]

set_property LOC GTXE2_CHANNEL_X0Y4 [get_cells -hierarchical -filter {NAME =~ "*/ten_gig_eth_phy_1_inst/*/gtxe2_i*" && PRIMITIVE_TYPE == IO.gt.GTXE2_CHANNEL}]
set_property LOC GTXE2_COMMON_X0Y1  [get_cells -hierarchical -filter {NAME =~ "*/ten_gig_eth_phy_1_inst/*" && PRIMITIVE_TYPE == IO.gt.GTXE2_COMMON}]

## From the IP core constraints

create_clock -period 6.400 [get_ports XG_CLK_p]
create_clock -period 3.103 [get_pins -of_objects [get_cells * -hierarchical -filter {REF_NAME=~ GTXE2_CHANNEL && NAME =~ *eth_pcs_pma_0_inst*}] -filter {NAME =~ *RXOUTCLK}]
create_clock -period 3.103 [get_pins -of_objects [get_cells * -hierarchical -filter {REF_NAME=~ GTXE2_CHANNEL && NAME =~ *eth_pcs_pma_0_inst*}] -filter {NAME =~ *TXOUTCLK}]

### 0

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR || PRIMITIVE_SUBGROUP =~ srl)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR || PRIMITIVE_SUBGROUP =~ srl)}]

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -of [filter [all_fanout -flat -endpoints_only -from [get_pins -filter {NAME=~*/Q} -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*rxratecounter_i*rxusrclk2_en156*}]]] {NAME =~ *WE}]]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -of [filter [all_fanout -flat -endpoints_only -from [get_pins -filter {NAME=~*/Q} -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*rxratecounter_i*rxusrclk2_en156*}]]] {NAME =~ *WE}]]

### 1

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*d4_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*d5_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -to   [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i*outreg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}]

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR || PRIMITIVE_SUBGROUP =~ srl)}]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR || PRIMITIVE_SUBGROUP =~ srl)}]

set_multicycle_path 2       -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -of [filter [all_fanout -flat -endpoints_only -from [get_pins -filter {NAME=~*/Q} -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*rxratecounter_i*rxusrclk2_en156*}]]] {NAME =~ *WE}]]
set_multicycle_path -hold 1 -from [get_cells * -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*mcp1_* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -of [filter [all_fanout -flat -endpoints_only -from [get_pins -filter {NAME=~*/Q} -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*rxratecounter_i*rxusrclk2_en156*}]]] {NAME =~ *WE}]]

# Set max delays between clock domain crossing data path regs in the rx elastic buffer
### 0
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*rd_truegray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*rag_writesync0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*wr_gray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*wr_gray_rdclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*rd_lastgray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*elastic*rd_lastgray_wrclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_false_path -from [get_pins -of [get_cells -of [all_fanin -flat [get_pins -of [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*asynch_fifo_i/dp_ram_i/rd_data_reg*}] -filter {NAME =~ *D}]]  -filter {is_sequential == 1 && NAME =~ "*ten_gig_eth_phy_0_inst*ten_gig_disti_ram*"}] -filter {NAME =~ *CLK}]  -to [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*asynch_fifo_i/dp_ram_i/rd_data_reg*}]
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*rd_truegray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*rag_writesync0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*wr_gray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*wr_gray_rdclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*rd_lastgray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*txrate*rd_lastgray_wrclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100

### 1
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*rd_truegray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*rag_writesync0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*wr_gray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*wr_gray_rdclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*rd_lastgray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*elastic*rd_lastgray_wrclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_false_path -from [get_pins -of [get_cells -of [all_fanin -flat [get_pins -of [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*asynch_fifo_i/dp_ram_i/rd_data_reg*}] -filter {NAME =~ *D}]]  -filter {is_sequential == 1 && NAME =~ "*ten_gig_eth_phy_1_inst*ten_gig_disti_ram*"}] -filter {NAME =~ *CLK}]  -to [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*asynch_fifo_i/dp_ram_i/rd_data_reg*}]
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*rd_truegray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*rag_writesync0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*wr_gray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*wr_gray_rdclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 6.400
set_max_delay  -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*rd_lastgray_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*txrate*rd_lastgray_wrclk0_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -datapath_only 3.100

# Set false paths and max delays between clock domain crossing reset regs
### 0
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
#set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*resyncs*d1_reg}] -filter {NAME =~ *D}]
#set_max_delay -datapath_only -from [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*pcs_reset_core_reg_reg}] -to [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resets_resyncs_i/d1_reg}] 3.100
set_max_delay -from [get_cells -of [all_fanin -flat [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*resyncs*d1_reg}] -filter {NAME =~ *D}]] -filter {IS_SEQUENTIAL=="1" && NAME !~ "*resyncs*d1_reg"}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*resyncs*d1_reg}] -filter {NAME =~ *D}] 3.100 -datapath_only
set_false_path -from [get_cells -hierarchical -filter {(PRIMITIVE_SUBGROUP =~ gt || PRIMITIVE_SUBGROUP =~ GT)}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
set_false_path -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*cable_*pull_reset_reg}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
# False paths for async reset removal synchronizers
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*sync1_r_reg*}] -filter {NAME =~ *PRE}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*sync1_r_reg*}] -filter {NAME =~ *CLR}]
# False paths to the resetdone registers which merely tidy up combo logic on the way to synchronizers
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*gt0_rxresetdone_reg*}] -filter {NAME =~ *D}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*gt0_txresetdone_reg*}] -filter {NAME =~ *D}]

### 1
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
#set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*resyncs*d1_reg}] -filter {NAME =~ *D}]
#set_max_delay -datapath_only -from [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*pcs_reset_core_reg_reg}] -to [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resets_resyncs_i/d1_reg}] 3.100
set_max_delay -from [get_cells -of [all_fanin -flat [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*resyncs*d1_reg}] -filter {NAME =~ *D}]] -filter {IS_SEQUENTIAL=="1" && NAME !~ "*resyncs*d1_reg"}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*resyncs*d1_reg}] -filter {NAME =~ *D}] 3.100 -datapath_only
set_false_path -from [get_cells -hierarchical -filter {(PRIMITIVE_SUBGROUP =~ gt || PRIMITIVE_SUBGROUP =~ GT)}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
set_false_path -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*cable_*pull_reset_reg}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*sync1_r_reg[0]}] -filter {NAME =~ *D}]
# False paths for async reset removal synchronizers
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*sync1_r_reg*}] -filter {NAME =~ *PRE}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*sync1_r_reg*}] -filter {NAME =~ *CLR}]
# False paths to the resetdone registers which merely tidy up combo logic on the way to synchronizers
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*gt0_rxresetdone_reg*}] -filter {NAME =~ *D}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*gt0_txresetdone_reg*}] -filter {NAME =~ *D}]

# Max delays to control skew into coherent synchronizers
### 0
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports XG_CLK_p]] -to [get_pins -of_objects [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_timer_125us_resync/*synchc_inst*d1_reg}] -filter {NAME =~ *D}] 6.400
#set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports refclk_p]] -to [get_pins -of_objects [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_0_inst*coreclk_rxusrclk2_resyncs_i/*synchc_inst*d1_reg}] -filter {NAME =~ *D}] 6.400
### 1
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports XG_CLK_p]] -to [get_pins -of_objects [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_timer_125us_resync/*synchc_inst*d1_reg}] -filter {NAME =~ *D}] 6.400
#set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports refclk_p]] -to [get_pins -of_objects [get_cells -hier -filter {NAME =~ *ten_gig_eth_phy_1_inst*coreclk_rxusrclk2_resyncs_i/*synchc_inst*d1_reg}] -filter {NAME =~ *D}] 6.400

# DRP clock crossing logic
### 0
set_max_delay -datapath_only -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*drp_ipif_i*synch_*d_reg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*drp_ipif_i*synch_*q_reg*}] -filter {NAME =~ *D || NAME =~ *R || NAME =~ *S}] 3.100

set_false_path -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*synch_*d1_reg}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*can_insert_synch*d1_reg}] -filter {NAME =~ *D}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_0_inst*psynch_*newedge_reg_reg}] -filter {NAME =~ *D}]

### 1
set_max_delay -datapath_only -from [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*drp_ipif_i*synch_*d_reg_reg* && (PRIMITIVE_SUBGROUP =~ flop || PRIMITIVE_SUBGROUP =~ SDR)}] -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*drp_ipif_i*synch_*q_reg*}] -filter {NAME =~ *D || NAME =~ *R || NAME =~ *S}] 3.100

set_false_path -to [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*synch_*d1_reg}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*can_insert_synch*d1_reg}] -filter {NAME =~ *D}]
set_false_path -to [get_pins -of_objects [get_cells -hierarchical -filter {NAME =~ *ten_gig_eth_phy_1_inst*psynch_*newedge_reg_reg}] -filter {NAME =~ *D}]
#
# Copyright 2014 Ettus Research LLC
#

create_clock -name FPGA_CLK -period 5.000 -waveform {0.000 2.500} [get_ports {FPGA_CLK_P}]

set_input_jitter [get_clocks FPGA_CLK] 0.05

set var_fpga_clk_delay  1.545    ;# LMK_Delay=0.900ns, LMK->FPGA=0.645ns
set var_fpga_clk_skew   0.100
set_clock_latency -source -early [expr $var_fpga_clk_delay - $var_fpga_clk_skew/2] [get_clocks FPGA_CLK]
set_clock_latency -source -late  [expr $var_fpga_clk_delay + $var_fpga_clk_skew/2] [get_clocks FPGA_CLK]

# FPGA_CLK_p/n is externally phase shifted to allow for crossing from the ADC clock domain
# to the radio_clk (aka FPGA_CLK_p/n) clock domain. To ensure this timing is consistent,
# lock the locations of the MMCM and BUFG to generate radio_clk.
set_property LOC MMCME2_ADV_X0Y0 [get_cells -hierarchical -filter {NAME =~ "*lmk04816*mmcm_inst"}]
set_property LOC BUFGCTRL_X0Y8   [get_cells -hierarchical -filter {NAME =~ "*lmk04816*rad_clk_bufg_inst"}]

create_generated_clock -name radio_clk [get_pins -hierarchical -filter {NAME =~ "*lmk04816*mmcm_inst/CLKOUT0"}]
set_clock_groups -asynchronous -group [get_clocks {ce_clk}] -group [get_clocks {radio_clk}]
#
# Copyright 2014 Ettus Research LLC
#

# ADS62P48 input constraints

create_clock -period 5.00 -name DB0_ADC_DCLK -waveform {0.000 2.50} [get_ports DB0_ADC_DCLK_P]
create_clock -period 5.00 -name DB1_ADC_DCLK -waveform {0.000 2.50} [get_ports DB1_ADC_DCLK_P]

set var_adc_clk_delay   8.440    ;# LMK->ADC=1.04ns, ADC->FPGA=0.750ns, ADC=6.65ns=(0.69*5ns)+5.7-2.5
set var_adc_clk_skew    0.500    ;# The real skew is ~3.5ns with which we will not meet static timing. Ignore skew by temp variations.
set_clock_latency -source -early [expr $var_adc_clk_delay - $var_adc_clk_skew/2] [get_clocks DB0_ADC_DCLK]
set_clock_latency -source -late  [expr $var_adc_clk_delay + $var_adc_clk_skew/2] [get_clocks DB0_ADC_DCLK]
set_clock_latency -source -early [expr $var_adc_clk_delay - $var_adc_clk_skew/2] [get_clocks DB1_ADC_DCLK]
set_clock_latency -source -late  [expr $var_adc_clk_delay + $var_adc_clk_skew/2] [get_clocks DB1_ADC_DCLK]

# At 200 MHz, static timing cannot be closed so we tune data delays on the capture
# interface from software at device creation time.
# The data is center aligned wrt to the SS Clock when it is launched from the ADC
# So we tune the data IDELAYS to half the range (16) so we have slack in both directions
# In the constraints we capture this by padding the dv_before and dv_after by half the
# tuning range of the IDELAY.

# Using typical values for ADC
set adc_clk_delay_wrt_center    0.000      ;# Possible {-1.340, -0.769, 0, 0.769}
set adc_in_dv_before_clk_edge   0.550      ;# Typical: 0.90ns
set adc_in_dv_after_clk_edge    0.550      ;# Typical: 0.95ns
set idelay_tune_range           2.500      ;# Refclk for IDELAY is 200MHz. Range of idelay is 0.5*period

set adc_in_delay_max [expr 2.500 - $adc_in_dv_before_clk_edge - $idelay_tune_range + $adc_clk_delay_wrt_center]
set adc_in_delay_min [expr $adc_in_dv_after_clk_edge + $idelay_tune_range - $adc_clk_delay_wrt_center]

set_input_delay -clock DB0_ADC_DCLK -max $adc_in_delay_max                         [get_ports {DB0_ADC_DA*}]
set_input_delay -clock DB0_ADC_DCLK -min $adc_in_delay_min                         [get_ports {DB0_ADC_DA*}]
set_input_delay -clock DB0_ADC_DCLK -max $adc_in_delay_max -clock_fall -add_delay  [get_ports {DB0_ADC_DA*}]
set_input_delay -clock DB0_ADC_DCLK -min $adc_in_delay_min -clock_fall -add_delay  [get_ports {DB0_ADC_DA*}]

set_input_delay -clock DB0_ADC_DCLK -max $adc_in_delay_max                         [get_ports {DB0_ADC_DB*}]
set_input_delay -clock DB0_ADC_DCLK -min $adc_in_delay_min                         [get_ports {DB0_ADC_DB*}]
set_input_delay -clock DB0_ADC_DCLK -max $adc_in_delay_max -clock_fall -add_delay  [get_ports {DB0_ADC_DB*}]
set_input_delay -clock DB0_ADC_DCLK -min $adc_in_delay_min -clock_fall -add_delay  [get_ports {DB0_ADC_DB*}]

set_input_delay -clock DB1_ADC_DCLK -max $adc_in_delay_max                         [get_ports {DB1_ADC_DA*}]
set_input_delay -clock DB1_ADC_DCLK -min $adc_in_delay_min                         [get_ports {DB1_ADC_DA*}]
set_input_delay -clock DB1_ADC_DCLK -max $adc_in_delay_max -clock_fall -add_delay  [get_ports {DB1_ADC_DA*}]
set_input_delay -clock DB1_ADC_DCLK -min $adc_in_delay_min -clock_fall -add_delay  [get_ports {DB1_ADC_DA*}]

set_input_delay -clock DB1_ADC_DCLK -max $adc_in_delay_max                         [get_ports {DB1_ADC_DB*}]
set_input_delay -clock DB1_ADC_DCLK -min $adc_in_delay_min                         [get_ports {DB1_ADC_DB*}]
set_input_delay -clock DB1_ADC_DCLK -max $adc_in_delay_max -clock_fall -add_delay  [get_ports {DB1_ADC_DB*}]
set_input_delay -clock DB1_ADC_DCLK -min $adc_in_delay_min -clock_fall -add_delay  [get_ports {DB1_ADC_DB*}]

# We use a simple synchronizer to cross ADC data over from the ADC_CLK domain to the radio_clk domain
# Use max delay constraints to ensure that the transition happens safely
set_min_delay                0.700 -from [get_cells -hier -filter {NAME =~ *ads62p48*iddr_inst}] \
                                   -to   [get_cells -hier -filter {NAME =~ *ads62p48*syncstages_ff_reg[0][*]}]
set_max_delay -datapath_only 0.950 -from [get_cells -hier -filter {NAME =~ *ads62p48*iddr_inst}] \
                                   -to   [get_cells -hier -filter {NAME =~ *ads62p48*syncstages_ff_reg[0][*]}]
#
# Copyright 2014 Ettus Research LLC
#

create_clock -name VIRT_DAC_CLK -period 2.50 -waveform {0.00 1.25}
create_generated_clock -name DB0_DAC_DCI -source [get_pins {ftop/pfconfig_i/db0_ad9146_i/worker/dci_oddr_inst/C}] -divide_by 1 [get_ports {DB0_DAC_DCI_P}]
create_generated_clock -name DB1_DAC_DCI -source [get_pins {ftop/pfconfig_i/db1_ad9146_i/worker/dci_oddr_inst/C}] -divide_by 1 [get_ports {DB1_DAC_DCI_P}]

# DCI System-Sync Timing

# The DCI clock driven to the DACs must obey setup and hold timing with respect to
# the reference clock driven to the DACs (same as the FPGA_CLK, driven by the LMK).
# Define the minimum and maximum clock propagation delays through the FPGA in order to
# meet this system-wide timing.
set dac0_clk_offset_out_max  1.350
set dac0_clk_offset_out_min  0.225
set dac1_clk_offset_out_max  1.350
set dac1_clk_offset_out_min  0.225

# The absolute latest the DCI clock should change is the sum of the maximum delay through
# the FPGA and the latest the sourcing clock (FPGA_CLK) can arrive at the FPGA. This is an
# artifact of the set_clock_latency constraints and doing system-wide timing. Typically,
# these Early/Late delays are automatically compensated for by the analyzer. However this
# is only the case for signals that start and end in the same PRIMARY clock domain. In
# our case, VIRT_DAC_CLK and radio_clk are not the same clock domain and
# therefore we have to manually remove the added Early/Late values from analysis.
set dac0_dci_out_delay_max [expr $dac0_clk_offset_out_max + $var_fpga_clk_delay + $var_fpga_clk_skew/2]
set dac0_dci_out_delay_min [expr $dac0_clk_offset_out_min + $var_fpga_clk_delay - $var_fpga_clk_skew/2]
set dac1_dci_out_delay_max [expr $dac1_clk_offset_out_max + $var_fpga_clk_delay + $var_fpga_clk_skew/2]
set dac1_dci_out_delay_min [expr $dac1_clk_offset_out_min + $var_fpga_clk_delay - $var_fpga_clk_skew/2]

# The min set_output_delay is the earliest the DCI clock should change BEFORE the current
# edge of interest. Here it is inverted (negated) because the earliest the clock should
# change is dac0_dci_out_delay_min AFTER the launch edge of the virtual clock.
set_output_delay -clock VIRT_DAC_CLK -min [expr - $dac0_dci_out_delay_min]                        [get_ports {DB0_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -min [expr - $dac0_dci_out_delay_min] -clock_fall -add_delay [get_ports {DB0_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -min [expr - $dac1_dci_out_delay_min]                        [get_ports {DB1_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -min [expr - $dac1_dci_out_delay_min] -clock_fall -add_delay [get_ports {DB1_DAC_DCI_*}]

# The max set_output_delay is the time the data should be stable before the next
# edge of interest. Since we are DDR, this is the falling edge. Hence we subtract
# latest time the data should change, dac0_dci_out_delay_max, from the falling edge
# time, dci_period/2 = 1.25ns.
set_output_delay -clock VIRT_DAC_CLK -max [expr 1.25 - $dac0_dci_out_delay_max]                         [get_ports {DB0_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -max [expr 1.25 - $dac0_dci_out_delay_max] -clock_fall -add_delay  [get_ports {DB0_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -max [expr 1.25 - $dac1_dci_out_delay_max]                         [get_ports {DB1_DAC_DCI_*}]
set_output_delay -clock VIRT_DAC_CLK -max [expr 1.25 - $dac1_dci_out_delay_max] -clock_fall -add_delay  [get_ports {DB1_DAC_DCI_*}]


# Data to DCI Source-Sync Timing

# The data setup and hold values must be modified in order to pass timing in
# the FPGA. The correct values are 0.270 and 0.090 for setup and hold, respectively.
# The interface fails by around 390 ps in both directions, so we subtract the failing
# amount from the actual amount to get a passing constraint.
# NOTE: Any changes to the adjustment margin below would need to be validated over
#       multiple builds, process and temperature. Try not to change it!
set dac_data_setup      0.270
set dac_data_hold       0.090
set dac_setup_adj       0.390
set dac_hold_adj        0.390

# These are real trace delays from the timing spreadsheet. Note that we are assuming
# no variability in our clock delay.
set dac0_data_delay_max 1.036
set dac0_data_delay_min 0.898
set dac0_clk_delay_max  0.974
set dac0_clk_delay_min  0.974

set dac1_data_delay_max 0.941
set dac1_data_delay_min 0.833
set dac1_clk_delay_max  0.930
set dac1_clk_delay_min  0.930

set dac0_out_delay_max [expr $dac0_data_delay_max - $dac0_clk_delay_min + $dac_data_setup - $dac_setup_adj]
set dac0_out_delay_min [expr $dac0_data_delay_min - $dac0_clk_delay_max - $dac_data_hold  + $dac_hold_adj]
set dac1_out_delay_max [expr $dac1_data_delay_max - $dac1_clk_delay_min + $dac_data_setup - $dac_setup_adj]
set dac1_out_delay_min [expr $dac1_data_delay_min - $dac1_clk_delay_max - $dac_data_hold  + $dac_hold_adj]

set_output_delay -clock [get_clocks DB0_DAC_DCI] -max $dac0_out_delay_max                        [get_ports -regexp {DB0_DAC_D._. DB0_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB0_DAC_DCI] -max $dac0_out_delay_max -clock_fall -add_delay [get_ports -regexp {DB0_DAC_D._. DB0_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB0_DAC_DCI] -min $dac0_out_delay_min                        [get_ports -regexp {DB0_DAC_D._. DB0_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB0_DAC_DCI] -min $dac0_out_delay_min -clock_fall -add_delay [get_ports -regexp {DB0_DAC_D._. DB0_DAC_FRAME_.}]

set_output_delay -clock [get_clocks DB1_DAC_DCI] -max $dac1_out_delay_max                        [get_ports -regexp {DB1_DAC_D._. DB1_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB1_DAC_DCI] -max $dac1_out_delay_max -clock_fall -add_delay [get_ports -regexp {DB1_DAC_D._. DB1_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB1_DAC_DCI] -min $dac1_out_delay_min                        [get_ports -regexp {DB1_DAC_D._. DB1_DAC_FRAME_.}]
set_output_delay -clock [get_clocks DB1_DAC_DCI] -min $dac1_out_delay_min -clock_fall -add_delay [get_ports -regexp {DB1_DAC_D._. DB1_DAC_FRAME_.}]
