#
# Copyright 2014 Ettus Research LLC
#

create_clock -name FPGA_CLK -period 5.000 -waveform {0.000 2.500} [get_ports {FPGA_CLK_P}]

set_input_jitter [get_clocks FPGA_CLK] 0.05

set var_fpga_clk_delay  1.545    ;# LMK_Delay=0.900ns, LMK->FPGA=0.645ns
set var_fpga_clk_skew   0.100
set_clock_latency -source -early [expr $var_fpga_clk_delay - $var_fpga_clk_skew/2] [get_clocks FPGA_CLK]
set_clock_latency -source -late  [expr $var_fpga_clk_delay + $var_fpga_clk_skew/2] [get_clocks FPGA_CLK]

# FPGA_CLK_p/n is externally phase shifted to allow for crossing from the ADC clock domain
# to the radio_clk (aka FPGA_CLK_p/n) clock domain. To ensure this timing is consistent,
# lock the locations of the MMCM and BUFG to generate radio_clk.
set_property LOC MMCME2_ADV_X0Y0 [get_cells -hierarchical -filter {NAME =~ "*lmk04816*mmcm_inst"}]
set_property LOC BUFGCTRL_X0Y8   [get_cells -hierarchical -filter {NAME =~ "*lmk04816*rad_clk_bufg_inst"}]

create_generated_clock -name radio_clk [get_pins -hierarchical -filter {NAME =~ "*lmk04816*mmcm_inst/CLKOUT0"}]
set_clock_groups -asynchronous -group [get_clocks {ce_clk}] -group [get_clocks {radio_clk}]
