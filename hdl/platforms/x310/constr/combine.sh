#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

set -eu

# Get the path to the directory containing this script
script_dir="$(dirname "$(realpath "$0")")"

# Combine the xdc fragments together into a single constraints file
cat \
    ${script_dir}/base_pins.xdc \
    ${script_dir}/base_timing.xdc \
    ${script_dir}/single_eth.xdc \
    > ${script_dir}/../cfg_base_single_eth.xdc

# Combine the xdc fragments together into a single constraints file
cat \
    ${script_dir}/base_pins.xdc \
    ${script_dir}/base_timing.xdc \
    ${script_dir}/dual_eth.xdc \
    > ${script_dir}/../cfg_base_dual_eth.xdc

# Combine the xdc fragments together into a single constraints file
cat \
    ${script_dir}/cfg_dual_ubx_160_pins.xdc \
    ${script_dir}/base_timing.xdc \
    ${script_dir}/dual_eth.xdc \
    ${script_dir}/ubx160_timing.xdc \
    ${script_dir}/adc_timing.xdc \
    ${script_dir}/dac_timing.xdc \
    > ${script_dir}/../cfg_dual_ubx_160.xdc

# Combine the xdc fragments together into a single constraints file
cat \
    ${script_dir}/cfg_single_ubx_160_pins.xdc \
    ${script_dir}/base_timing.xdc \
    ${script_dir}/dual_eth.xdc \
    ${script_dir}/ubx160_timing.xdc \
    ${script_dir}/adc_timing.xdc \
    ${script_dir}/dac_timing.xdc \
    > ${script_dir}/../cfg_single_ubx_160.xdc
