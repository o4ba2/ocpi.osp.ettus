.. x310 Getting Started Guide Documentation


.. _x310-gsg:

``x310`` Getting Started Guide
==============================

This platform can be used to run OpenCPI applications on the Ettus USRP X310 hardware. 

The Ettus Research USRP X310 is a high-performance, scalable software defined radio (SDR) platform 
for designing and deploying next generation wireless communications systems. The hardware architecture 
combines two extended-bandwidth daughterboard slots covering DC to 6 GHz with up to 160 MHz of baseband bandwidth.
The Ettus USRP X310 supports a range of plugin daughterboards that contain the baseband to
RF frequency modulation, PA and LNA functionality. This platform supports the ``UBX160-160`` daughterboard only.

The x310 platform can be configured to operate with one or two 10Gbps ethernet interfaces, and one or two UBX160 daughter boards.

A calibration application is provided that can be used to perform I/Q balancing and DC offset correction. This application
produces frequency dependent correction parameters for both UBX160 daughter cards. 

The Ettus USRP X310 Getting Started Guide can be found in the 
`X310 Getting Started Guide <https://kb.ettus.com/X300/X310_Getting_Started_Guides>`_

The Ettus USRP X310 manual can be found here 
`USRP Hardware Driver and USRP Manual <https://files.ettus.com/manual/page_usrp_x3x0.html>`_


Revision History
----------------

.. csv-table:: x310 Getting Started Guide: Revision History
   :header: "Revision", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v1.0", "Initial Release", "date"

Software Prerequisites
----------------------

A Linux host PC with a Vivado 2019.2 installation (Lab Edition at a minimum to
run applications, Design Edition or Webpack to build a bitstream) and an
OpenCPI installation (Version 2.4 or later).
The Ethernet interfaces use raw sockets to communicate with the device, which
requires the CAP_NET_RAW capability. This can be accomplished either by running
as root, or (preferably) by setting this capability on the application executable.
For example, to work through the rest of this guide, run

.. code-block:: bash

   $ sudo setcap CAP_NET_RAW+eip $(readlink -f $(which ocpihdl))
   $ sudo setcap CAP_NET_RAW+eip $(readlink -f $(which ocpirun))

to enable raw sockets for ``ocpihdl`` and ``ocpirun``. Note that to use an ACI
application rather than ``ocpirun``, the capability needs to be set on the
application binary as part of the installation process. If you want to debug
an application, the ``gdb`` executable also needs the capability.

.. code-block:: bash

   $ sudo setcap CAP_NET_RAW+eip $(readlink -f $(which gdb))


Hardware Prerequisites
----------------------

The OpenCPI x310 platform has been tested using
* Ettus USRP X310 fitted with one or two UBX160 daughter boards
* National Instruments 783345-01 10 Gigabit Ethernet Card (PCIe)
* 2 x 10 Gigabit Ethernet Cable w/SFP+ Terminations

The USRP X310 provides three interface options – 1 Gigabit Ethernet (1 GigE), 10 Gigabit Ethernet (10 GigE), 
and PCI-Express (PCIe). Using this OpenCPI platform requires use of either one or both 10 GigE interfaces. 

.. figure:: ./doc/x310_gsg_0.svg


A more detailed description of the x310 ethernet connection using the SFP+ connectors can be found in the
`X310 Interfaces and Connectivity <https://kb.ettus.com/X300/X310#Interfaces_and_Connectivity>`_

Platform details
----------------

This platform provides one dataplane transport called ``ether``, which can be
used to connect input and output ports in the container XML file.

The x310 platform has several configurations. These are defined in files platforms/x310/cfg_xxx.xml.
All configurations support DGRDMA operation. Configurations are defined for dual and single ethernet 
interface usage, as well as dual and single UBX-160 support.
Note that the dual UBX-160 configuration can be used with applications that require only a single
UBX-160 daughter board.

+----------------------------+-----------+----------------+--------------------------------+
| Platform Configuration     | Ethernet  | Daughter Board | Usage                          |
+============================+===========+================+================================+
| cfg_base_single_eth.xml    | single    | none           | Configuration with no x310     | 
|                            |           |                | hardware (ADC, DAC, UBX-160)   |
|                            |           |                | support. Used during           | 
|                            |           |                | OpenCPI platform and DGRDMA    |
|                            |           |                | development                    | 
+----------------------------+-----------+----------------+--------------------------------+
| cfg_base_dual_eth.xml      | dual      | none           | Configuration with no x310     | 
|                            |           |                | hardware (ADC, DAC, UBX-160)   |
|                            |           |                | support. Used during           |
|                            |           |                | OpenCPI platform and DGRDMA    | 
|                            |           |                | development                    | 
+----------------------------+-----------+----------------+--------------------------------+
| cfg_single_ubx_160.xml     | dual      | single         | dual Ethernet / single UBX-160 | 
|                            |           |                | can be used if one UBX-160     | 
|                            |           |                | daughter board is present.     |
|                            |           |                | uses fewer FGPA resources than |
|                            |           |                | dual UBX-160 configuration     | 
+----------------------------+-----------+----------------+--------------------------------+
| cfg_dual_ubx_160.xml       | dual      | dual           | dual Ethernet / dual UBX-160   | 
|                            |           |                | used by calibration assembly   | 
|                            |           |                | and loopback example           |
|                            |           |                | application                    | 
+----------------------------+-----------+----------------+--------------------------------+

If dual ethernet is required then ``dual_ethernet`` must be set to ``true`` in the configuration XML 
(as is done in the relevant configurations above).

Example Assemblies / Containers
-------------------------------

The X310 platform has a number of example assemblies. These are located in directory 
opencpi/projects/osps/ocpi.osp.ettus/hdl/assemblies.

+----------------------+-----------------------+-----------------------+--------------------------------+
| Assembly             | Container             | Configuration         | Usage                          |
+======================+=======================+=======================+================================+
| calibration_assy.xml | cnt_dual_ubx_160.xml  | cfg_dual_ubx_160.xml  | UBX-160 calibration            |
+----------------------+-----------------------+-----------------------+--------------------------------+
| loopback_assy.xml    | cnt_dual_ubx_160.xml  | cfg_dual_ubx_160.xml  | Simple test application        |
+----------------------+-----------------------+-----------------------+--------------------------------+

Setup Guide
-----------


Build the example bitstreams 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the platform by running

.. code-block:: bash

   $ ocpiadmin install platform x310 --package-id ocpi.osp.ettus --minimal

See the `ocpiadmin(1) man page <https://opencpi.gitlab.io/releases/latest/man/ocpiadmin.1.html>`_ for command usage details.

To build the loopback assembly go to the x310 directory and run

.. code-block:: bash
   
   $ ocpidev build project --hdl-platform x310 --workers-as-needed


Connect the host PC to the x310 hardware
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If using a single Ethernet connection, connect the PC (network interface card) 
to the x310 (Rear Panel 1G/10G ETH Port 0).

.. figure:: ./doc/x310_gsg_2.svg

If using two Ethernet interfaces, connect the two PC (network interface card)
interfaces to the x310 Rear Panel 1G/10G Ports 0 and 1. The OpenCPI DGRDMA functionality
requires both ethernet interfaces to be bonded together to form a single aggregated link.
Packets sent between the x310 hardware and the PC are then able to use both physical 
ethernet interfaces. Treating them as a single link.

Linux bonded interface as follows:

Run the following commands **as root** to create a Linux bonded interface (assuming the
two interfaces on the PC are named ``eth1`` and ``eth2``):

.. code-block:: bash

   $ ip link add bond0 type bond
   $ echo balance-rr > /sys/class/net/bond0/bonding/mode
   $ echo 100 > /sys/class/net/bond0/bonding/miimon
   $ ip link set eth1 down
   $ ip link set eth2 down
   $ ip link set eth1 master bond0
   $ ip link set eth2 master bond0
   $ ip link set bond0 up

This creates a bonded network interface called ``bond0`` using the ``balance-rr``
method (which uses a round-robin algorithm to dispatch outbound packets on the
two interfaces). The bonded interface name and MAC address should be used for all
OpenCPI configuration as described below (usually the MAC address of the first
physical interface added is used, ``eth1`` in this case).

Modify the setup-runtime.sh script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The script (opencpi/projects/osps/ocpi.osp.ettus/applications/setup-runtime.sh), which needs to be run prior to running ocpirun or
an ACI application, needs to be modified to set the `OCPI_ETHER_INTERFACE` environment variable
to the name of the `local` ethernet interface that OpenCPI will use.

.. code-block:: bash

   export OCPI_LIBRARY_PATH=/opencpi/projects/assets/artifacts:/opencpi/projects/core/artifacts:/opencpi/projects/osps/ocpi.osp.ettus/exports/artifacts:/opencpi/projects/tutorial/artifacts   
   export OCPI_SYSTEM_CONFIG=/opencpi/projects/osps/ocpi.osp.ettus/applications/system.xml
   export OCPI_ENABLE_HDL_NETWORK_DISCOVERY=1
   export OCPI_ETHER_INTERFACE=eth0

If dual ethernet operation is being used `OCPI_ETHER_INTERFACE` will need to be set to `bond0`. 

Loading a bitstream into the x310 FPGA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The x310 FPGA is programmed with a bitstream using the USB-JTAG interface (USB-B).
Connect the Front Panel JTAG / USB connector to the PC. 

.. figure:: ./doc/x310_gsg_1.svg

Programming is performed by instructing Vivado to execute a .tcl script in batch mode.

.. code-block:: bash

   $ Xilinx/Vivado_Lab/2019.2/bin/vivado_lab -mode batch -source program_fpga.tcl

The .tcl script uses the hardware manager to connect to the FPGA hardware and to program the FPGA.

.. code-block:: tcl

   open_hw_manager
   connect_hw_server -allow_non_jtag
   open_hw_target
   current_hw_device [get_hw_devices xc7k410t_0]
   refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7k410t_0] 0]
   set_property PROBES.FILE {} [get_hw_devices xc7k410t_0]
   set_property FULL_PROBES.FILE {} [get_hw_devices xc7k410t_0]
   set_property PROGRAM.FILE {<bitstream-name>.bit} [get_hw_devices xc7k410t_0]
   program_hw_devices [get_hw_devices xc7k410t_0]
   refresh_hw_device [lindex [get_hw_devices xc7k410t_0] 0]
   close_hw_target
   disconnect_hw_server


Modify the ``system.xml`` file specifying the system configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The opencpi/projects/osps/ocpi.osp.ettus/applications directory contains the system.xml file used with the example applications.

.. code-block:: xml

   <opencpi>
      <container>
         <rcc load="1"/>
         <hdl load="1" discovery="static">
            <device name="Ether:enp4s0f0/00:50:c2:85:3f:ff" device="xc7k410t" platform="x310" esn="2516350FA123A" static="true">
               <instance worker="dgrdma_config_dev">
                  <property name="remote_mac_addr_d" value="0xa0369f6abf08"/>
               </instance>
            </device>
         </hdl>
      </container>
      <transfer smbsize="10M">
         <datagram2-ether load="1"/>
      </transfer>
   </opencpi>


The `local (PC)` ethernet interface name and the `remote (FPGA)`` MAC address needs to be set in the <device> element.
When using dual ethernet configuration the ethernet interface will be bond0, otherwise this will be the interface name.
(this can be found using the command ip link). The local (PC) MAC address needs to be set in the `remote_mac_addr_d` property.


Running an Application
----------------------

Ensure that you have a valid ``system.xml`` file. Ensure that  the following environment
variables are set:

.. csv-table:: Environment variables
   :header: "Name", "Value", "Remarks"
   :class: tight-table

   "``OCPI_ENABLE_HDL_NETWORK_DISCOVERY``", "1", "Required to find network devices"
   "``OCPI_ETHER_INTERFACE``", "name of local Ethernet interface", "Used by transfer driver"
   "``OCPI_MAX_ETHER_PAYLOAD_SIZE``", "MTU for packets sent by the PC", "Optional; if not present, the default (1498) will be used. This must not be set larger than the actual MTU configured in the Linux network interface."
   "``OCPI_SYSTEM_CONFIG``", "Path to ``system.xml``", "Required by OpenCPI framework"
   "``OCPI_LIBRARY_PATH``", "Colon-separated list of artifact paths", "Required by OpenCPI framework"

Now you can run the loopback application:

.. code-block:: xml

   $ cd opencpi/projects/osps/ocpi.osp.ettus/applications
   $ ocpirun -d -l 8 ../loopback/loopback.xml

The loopback application comprises

* File Reader, reads baseband samples from Test_file_in.dat at 2Msps
* Interpolator (ocpi.comp.sdr.dsp.cic_interpolator_xs) hdl worker used to increase the sample rate to 200 Msps
* Digital-Radio-Controller (modulates the baseband signal to a carrier frequency of 2400 GHz)
* Decimator (ocpi.comp.sdr.dsp.cic_decimator_xs) hdl worker used to reduce the received baseband samples down from 200 Msps 
* File Writer, write receive samples to file Test_file_out.dat at 2 Msps

Known Issues
------------
