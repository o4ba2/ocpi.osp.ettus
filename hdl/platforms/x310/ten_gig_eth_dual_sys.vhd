-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library unisim; use unisim.vcomponents.all;

entity ten_gig_eth_dual_sys is
  port (
    -- 156 MHz reference clock
    refclk_p              : in  std_logic;
    refclk_n              : in  std_logic;

    areset                : in  std_logic;

    --- PHY 0 -----------------------------------------------------------------

    -- XGMII interface
    xgmii_0_tx_clk        : out std_logic;
    xgmii_0_tx_reset      : out std_logic;
    xgmii_0_txd           : in  std_logic_vector(63 downto 0);
    xgmii_0_txc           : in  std_logic_vector(7 downto 0);
    xgmii_0_rx_clk        : out std_logic;
    xgmii_0_rx_reset      : out std_logic;
    xgmii_0_rxd           : out std_logic_vector(63 downto 0);
    xgmii_0_rxc           : out std_logic_vector(7 downto 0);

    -- Transmit and receive lanes
    txp_0                 : out std_logic;
    txn_0                 : out std_logic;
    rxp_0                 : in  std_logic;
    rxn_0                 : in  std_logic;

    link_up_0             : out std_logic;
    activity_0            : out std_logic;

    signal_detect_0       : in  std_logic;
    tx_fault_0            : in  std_logic;
    tx_disable_0          : out std_logic;

    --- PHY 1 -----------------------------------------------------------------

    -- XGMII interface
    xgmii_1_tx_clk        : out std_logic;
    xgmii_1_tx_reset      : out std_logic;
    xgmii_1_txd           : in  std_logic_vector(63 downto 0);
    xgmii_1_txc           : in  std_logic_vector(7 downto 0);
    xgmii_1_rx_clk        : out std_logic;
    xgmii_1_rx_reset      : out std_logic;
    xgmii_1_rxd           : out std_logic_vector(63 downto 0);
    xgmii_1_rxc           : out std_logic_vector(7 downto 0);

    -- Transmit and receive lanes
    txp_1                 : out std_logic;
    txn_1                 : out std_logic;
    rxp_1                 : in  std_logic;
    rxn_1                 : in  std_logic;

    link_up_1             : out std_logic;
    activity_1            : out std_logic;

    signal_detect_1       : in  std_logic;
    tx_fault_1            : in  std_logic;
    tx_disable_1          : out std_logic
  );
end ten_gig_eth_dual_sys;

architecture rtl of ten_gig_eth_dual_sys is

  -- Clocks
  signal refclk  : std_logic;
  signal coreclk : std_logic;

begin
  -- Clocks
  ten_gige_phy_clk_gen_inst : entity work.ten_gige_phy_clk_gen
    port map (
      areset    => areset,
      refclk_p  => refclk_p,
      refclk_n  => refclk_n,
      refclk    => refclk,
      clk156    => coreclk
    );

  ten_gig_eth_phy_0_inst : entity work.ten_gig_eth_phy
    port map (
      refclk              => refclk,
      coreclk             => coreclk,

      areset              => areset,

      xgmii_tx_clk        => xgmii_0_tx_clk,
      xgmii_tx_reset      => xgmii_0_tx_reset,
      xgmii_txd           => xgmii_0_txd,
      xgmii_txc           => xgmii_0_txc,

      xgmii_rx_clk        => xgmii_0_rx_clk,
      xgmii_rx_reset      => xgmii_0_rx_reset,
      xgmii_rxd           => xgmii_0_rxd,
      xgmii_rxc           => xgmii_0_rxc,

      txp                 => txp_0,
      txn                 => txn_0,

      rxp                 => rxp_0,
      rxn                 => rxn_0,

      link_up             => link_up_0,
      activity            => activity_0,

      signal_detect       => signal_detect_0,
      tx_fault            => tx_fault_0,
      tx_disable          => tx_disable_0
    );

  ten_gig_eth_phy_1_inst : entity work.ten_gig_eth_phy
    port map (
      refclk              => refclk,
      coreclk             => coreclk,

      areset              => areset,

      xgmii_tx_clk        => xgmii_1_tx_clk,
      xgmii_tx_reset      => xgmii_1_tx_reset,
      xgmii_txd           => xgmii_1_txd,
      xgmii_txc           => xgmii_1_txc,

      xgmii_rx_clk        => xgmii_1_rx_clk,
      xgmii_rx_reset      => xgmii_1_rx_reset,
      xgmii_rxd           => xgmii_1_rxd,
      xgmii_rxc           => xgmii_1_rxc,

      txp                 => txp_1,
      txn                 => txn_1,

      rxp                 => rxp_1,
      rxn                 => rxn_1,

      link_up             => link_up_1,
      activity            => activity_1,

      signal_detect       => signal_detect_1,
      tx_fault            => tx_fault_1,
      tx_disable          => tx_disable_1
    );

end rtl;
