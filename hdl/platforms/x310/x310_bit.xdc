#-- From build_x300.tcl -------------------------------------------------------

# STC3 Requirement: Disable waiting for DCI Match
set_property BITSTREAM.STARTUP.MATCH_CYCLE  NoWait  [current_design]

# STC3 Requirement: No bitstream compression
set_property BITSTREAM.GENERAL.COMPRESS     False   [current_design]

# Use 6MHz clock to configure bitstream
set_property BITSTREAM.CONFIG.CONFIGRATE    6       [current_design]
