-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library unisim; use unisim.vcomponents.all;

entity ten_gige_phy_clk_gen is
  port (
    areset   : in  std_logic;

    -- 156 MHz reference clock input
    refclk_p : in  std_logic;
    refclk_n : in  std_logic;

    -- GTX reference
    refclk   : out std_logic;

    -- 156 MHz frabric clock
    clk156   : out std_logic
  );
end entity ten_gige_phy_clk_gen;

architecture rtl of ten_gige_phy_clk_gen is

  signal refclk_i   : std_logic;

begin

  refclk <= refclk_i;

  ibufds_inst : IBUFDS_GTE2
    port map (
      -- Reference clock input ports
      I     => refclk_p,
      IB    => refclk_n,

      -- Active low asynchronous clock enable
      CEB   => '0',

      -- Output clocks
      O     => refclk_i,
      ODIV2 => open
    );

  clk156_bufg_inst : BUFG
    port map (
      I     => refclk_i,
      O     => clk156
    );

end rtl;
