-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all; use ieee.std_logic_unsigned.all;
library unisim; use unisim.vcomponents.all;
library cdc;

entity clk_gen is
  port (
    -- Input clock
    FPGA_125MHz_CLK        : in std_logic;
    FPGA_REFCLK_10MHz      : in std_logic;

    -- Generated platform clock and reset
    clk                    : out std_logic;
    reset                  : out std_logic;

    -- Generated PCIe clocks
    ioport2_clk            : out std_logic;
    rio40_clk              : out std_logic;
    ioport2_idelay_ref_clk : out std_logic;

    -- Generated Time clock
    timeclk                : out std_logic;

    -- PCIe PLL locked and reset
    rio40_clk_locked       : out std_logic;
    rio40_clk_reset        : in  std_logic
  );
end clk_gen;

architecture rtl of clk_gen is

  signal fpga_clk125                  : std_logic;

  signal clk_i                        : std_logic;
  signal ioport2_clk_i                : std_logic;

  signal timeclk_unbuf                : std_logic;
  signal timeclk_i                    : std_logic;
  signal ioport2_clk_unbuf            : std_logic;
  signal clk_unbuf                    : std_logic;
  signal pcie_clk_pll_clkfbout_unbuf  : std_logic;
  signal pcie_clk_pll_clkfbout        : std_logic;
  signal rio40_clk_unbuf              : std_logic;
  signal ioport2_idelay_ref_clk_unbuf : std_logic;
  signal ioport2_idelay_ref_clk_buf   : std_logic;
  signal ioport3_idelay_ref_clk_unbuf : std_logic;
  signal ioport3_idelay_ref_clk_buf   : std_logic;
  signal rio40_clk_buf                : std_logic;
  signal rio40_lock                   : std_logic;
  signal rio40_rst                    : std_logic;
  signal rio40_cnt                    : std_logic_vector(7 downto 0);
  signal fbclk2_unbuf                 : std_logic;
  signal fbclk2                       : std_logic;
  signal fpga_10mhz                   : std_logic;

  signal bus_clk_locked               : std_logic;
  signal timeclk_locked               : std_logic;
  signal platform_clks_locked         : std_logic;

begin

  clk         <= clk_i;
  ioport2_clk <= ioport2_clk_i;
  timeclk     <= timeclk_i;

  platform_clks_locked <= bus_clk_locked and timeclk_locked;

  -- Input clock buffer. This clock comes from an on-board 125 MHz crystal.
  fpga_clk125MHz_ibufg_inst : IBUFG
    port map (
      I => FPGA_125MHz_CLK,
      O => fpga_clk125
    );

  -- Bus clock PLL ------------------------------------------------------------

  bus_clk_pll2_inst : PLLE2_BASE
    generic map (
      CLKIN1_PERIOD  => 8.000,
      DIVCLK_DIVIDE  => 1,
      CLKFBOUT_MULT  => 12,
      CLKOUT0_DIVIDE => 7
    )
    port map (
      CLKFBOUT => ioport2_clk_unbuf,
      CLKFBIN  => ioport2_clk_i,

      CLKIN1   => fpga_clk125,

      CLKOUT0  => clk_unbuf,
      CLKOUT1  => open,
      CLKOUT2  => open,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,

      LOCKED   => bus_clk_locked,
      PWRDWN   => '0',
      RST      => '0'
    );

  -- Generated 125 MHz clock. Used by the PCIe interface.
  ioport2_clk_bufg_isnt : BUFG
    port map (
      I => ioport2_clk_unbuf,
      O => ioport2_clk_i
    );

  -- Generated 214.286 MHz clock. This clock is used for the control plane.
  clk_bufg_inst : BUFG
    port map (
      I => clk_unbuf,
      O => clk_i
    );

  -- Hold the platform in reset until the main PLL is locked
  reset_inst : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '0',
      RST_DELAY     => 2
    )
    port map (
      src_rst   => platform_clks_locked,
      dst_clk   => clk_i,
      dst_rst   => reset,
      dst_rst_n => open
    );

  -- PCIe clock PLL -----------------------------------------------------------

  pcie_clk_pll2_inst : PLLE2_BASE
    generic map (
      CLKIN1_PERIOD  => 8.000,
      DIVCLK_DIVIDE  => 1,
      CLKFBOUT_MULT  => 8,
      CLKOUT0_DIVIDE => 25,
      CLKOUT1_DIVIDE => 5,
      CLKOUT2_DIVIDE => 5
    )
    port map (
      CLKFBOUT => pcie_clk_pll_clkfbout_unbuf,
      CLKFBIN  => pcie_clk_pll_clkfbout,

      CLKIN1   => fpga_clk125,

      CLKOUT0  => rio40_clk_unbuf,
      CLKOUT1  => ioport2_idelay_ref_clk_unbuf,
      CLKOUT2  => ioport3_idelay_ref_clk_unbuf,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,

      LOCKED   => rio40_lock,
      PWRDWN   => '0',
      RST      => rio40_clk_reset
    );

  rio40_clk_locked <= rio40_lock;

  -- PLL feedback
  clkfbout_bufh_inst : BUFH
    port map (
      I => pcie_clk_pll_clkfbout_unbuf,
      O => pcie_clk_pll_clkfbout
    );

  -- Generated 40 MHz clock
  rio40_clk_bufh_inst : BUFH
    port map (
      I => rio40_clk_unbuf,
      O => rio40_clk_buf
    );

  rio40_clk <= rio40_clk_buf;

  -- Generated 200 MHz clock
  ioport2_idelay_ref_clk_bufh_inst : BUFH
    port map (
      I => ioport2_idelay_ref_clk_unbuf,
      O => ioport2_idelay_ref_clk_buf
    );

  ioport2_idelay_ref_clk <= ioport2_idelay_ref_clk_buf;

  ioport3_idelay_ref_clk_bufh_inst : BUFG
    port map (
      I => ioport3_idelay_ref_clk_unbuf,
      O => ioport3_idelay_ref_clk_buf
    );

  -- Generate reset for delay after locked
  process(rio40_clk_buf)
  begin
    if rising_edge(rio40_clk_buf) then
      if (rio40_clk_reset = '1') then
        rio40_cnt <= "00000000";
        rio40_rst <= '0';
      elsif (rio40_lock = '0') then
        rio40_cnt <= "00000000";
        rio40_rst <= '1';
      elsif (rio40_cnt = "11111111") then
        rio40_cnt <= "11111111";
        rio40_rst <= '1';
      else
        rio40_cnt <= rio40_cnt + 1;
        rio40_rst <= '0';
      end if;
    end if;
  end process;

  idelctl_inst : IDELAYCTRL
    port map (
      REFCLK => ioport3_idelay_ref_clk_buf,
      RST    => rio40_rst,
      RDY    => open
    );

  fpga_10mhz_bufg_inst: BUFG
  port map (
    I => FPGA_REFCLK_10MHz,
    O => fpga_10mhz
  );

  timeclk_mmcm_inst : MMCME2_BASE
  generic map (
    BANDWIDTH        => "OPTIMIZED",
    CLKFBOUT_MULT_F  => 60.000,
    CLKFBOUT_PHASE   => 0.000,
    CLKIN1_PERIOD    => 100.000,
    CLKOUT0_DIVIDE_F => 3.000,
    CLKOUT0_DUTY_CYCLE => 0.500,
    CLKOUT0_PHASE    => 0.000,
    DIVCLK_DIVIDE    => 1,
    STARTUP_WAIT     => FALSE
  )
  port map (
    CLKFBOUT  => fbclk2_unbuf,
    CLKFBOUTB => open,
    CLKOUT0  => timeclk_unbuf,
    CLKOUT1  => open,
    CLKOUT2  => open,
    CLKOUT3  => open,
    CLKOUT4  => open,
    CLKOUT5  => open,
    LOCKED   => timeclk_locked,
    CLKFBIN   => fbclk2,
    CLKIN1   => fpga_10mhz,
    PWRDWN   => '0',
    RST      => '0'
  );

  -- Generated 200 MHz clock. Used by the time service.
  timeclk_bufg_inst: BUFG
  port map (
    I => timeclk_unbuf,
    O => timeclk_i
  );

  fbclk2_bufg_inst: BUFG
  port map (
    I => fbclk2_unbuf,
    O => fbclk2
  );


end rtl;
