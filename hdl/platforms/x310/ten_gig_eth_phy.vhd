-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library unisim; use unisim.vcomponents.all;
library eth_pcs_pma; use eth_pcs_pma.all;
library cdc;

entity ten_gig_eth_phy is
  port(
    refclk              : in std_logic;
    coreclk             : in std_logic;

    areset              : in std_logic;

    -- XGMII interface
    xgmii_tx_clk        : out std_logic;
    xgmii_tx_reset      : out std_logic;
    xgmii_txd           : in  std_logic_vector(63 downto 0);
    xgmii_txc           : in  std_logic_vector(7 downto 0);
    xgmii_rx_clk        : out std_logic;
    xgmii_rx_reset      : out std_logic;
    xgmii_rxd           : out std_logic_vector(63 downto 0);
    xgmii_rxc           : out std_logic_vector(7 downto 0);

    -- Transmit and receive lanes
    txp                 : out std_logic;
    txn                 : out std_logic;
    rxp                 : in  std_logic;
    rxn                 : in  std_logic;

    link_up             : out std_logic;
    activity            : out std_logic;

    signal_detect       : in  std_logic;
    tx_fault            : in  std_logic;
    tx_disable          : out std_logic
  );
end ten_gig_eth_phy;

architecture rtl of ten_gig_eth_phy is

  -- Configuration and status vectors
  signal phy_config_vector      : std_logic_vector(535 downto 0);
  signal phy_status_vector      : std_logic_vector(447 downto 0);
  signal core_status            : std_logic_vector(7 downto 0);

  -- XGMII signals
  signal xgmii_txd_reg          : std_logic_vector(63 downto 0);
  signal xgmii_txc_reg          : std_logic_vector(7 downto 0);
  signal xgmii_rxd_int          : std_logic_vector(63 downto 0);
  signal xgmii_rxc_int          : std_logic_vector(7 downto 0);

  signal xgmii_tx_reset_temp    : std_logic;
  signal xgmii_rx_reset_temp    : std_logic;

  -- Signal declarations
  signal areset_coreclk         : std_logic;
  signal qpllreset              : std_logic;
  signal txoutclk               : std_logic;

  signal qplloutclk             : std_logic;
  signal qplloutrefclk          : std_logic;
  signal qplllock               : std_logic;
  signal txusrclk               : std_logic;
  signal txusrclk2              : std_logic;
  signal qplllock_txusrclk2     : std_logic;
  signal gttxreset_txusrclk2    : std_logic;

  signal gttxreset              : std_logic;
  signal gtrxreset              : std_logic;

  signal txuserrdy              : std_logic;

  signal reset_counter          : unsigned(8 downto 0) := B"0_0000_0000";
  signal reset_pulse            : std_logic_vector(3 downto 0);
  signal reset_counter_done     : std_logic;
  signal tx_resetdone_int       : std_logic;
  signal rx_resetdone_int       : std_logic;
  signal signal_detect_coreclk  : std_logic;

  -- GT Dynamic Re-Programming
  signal drp_gnt                : std_logic;
  signal drp_req                : std_logic;
  signal drp_den_o              : std_logic;
  signal drp_dwe_o              : std_logic;
  signal drp_daddr_o            : std_logic_vector(15 downto 0);
  signal drp_di_o               : std_logic_vector(15 downto 0);
  signal drp_drdy_o             : std_logic;
  signal drp_drpdo_o            : std_logic_vector(15 downto 0);
  signal drp_den_i              : std_logic;
  signal drp_dwe_i              : std_logic;
  signal drp_daddr_i            : std_logic_vector(15 downto 0);
  signal drp_di_i               : std_logic_vector(15 downto 0);
  signal drp_drdy_i             : std_logic;
  signal drp_drpdo_i            : std_logic_vector(15 downto 0);

begin
  -- XGMII Clocks
  xgmii_tx_clk   <= coreclk;
  xgmii_rx_clk   <= coreclk;

  -- XGMII TX reset
  xgmii_tx_reset_temp <= (NOT tx_resetdone_int) OR areset_coreclk;

  tx_reset_inst : cdc.cdc.reset
  generic map (
    SRC_RST_VALUE => '1',
    RST_DELAY     => 4
  )
  port map (
    src_rst   => xgmii_tx_reset_temp,
    dst_clk   => coreclk,
    dst_rst   => xgmii_tx_reset,
    dst_rst_n => open
  );

  -- Signal detect

  signal_detect_coreclk_sync_i : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '0',
      RST_DELAY     => 5
    )
    port map (
      src_rst   => signal_detect,
      dst_clk   => coreclk,
      dst_rst   => open,
      dst_rst_n => signal_detect_coreclk
    );

  -- XGMII RX reset
  xgmii_rx_reset_temp <= ( (NOT rx_resetdone_int) OR areset_coreclk OR
                            (NOT signal_detect_coreclk) );

  rx_reset_inst : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '1',
      RST_DELAY     => 4
    )
    port map (
      src_rst   => xgmii_rx_reset_temp,
      dst_clk   => coreclk,
      dst_rst   => xgmii_rx_reset,
      dst_rst_n => open
    );

  -- GTXE2_COMMON Transceiver common block - containing the transceiver's PLL

  -- PLL settings were originally derived with the following Verilog:
  --   localparam QPLL_FBDIV_TOP =  66;
  --
  --   localparam QPLL_FBDIV_IN  =  (QPLL_FBDIV_TOP == 16)  ? 10'b0000100000 :
	-- 			(QPLL_FBDIV_TOP == 20)  ? 10'b0000110000 :
	-- 			(QPLL_FBDIV_TOP == 32)  ? 10'b0001100000 :
	-- 			(QPLL_FBDIV_TOP == 40)  ? 10'b0010000000 :
	-- 			(QPLL_FBDIV_TOP == 64)  ? 10'b0011100000 :
	-- 			(QPLL_FBDIV_TOP == 66)  ? 10'b0101000000 :
	-- 			(QPLL_FBDIV_TOP == 80)  ? 10'b0100100000 :
	-- 			(QPLL_FBDIV_TOP == 100) ? 10'b0101110000 : 10'b0000000000;
  --
  --  localparam QPLL_FBDIV_RATIO = (QPLL_FBDIV_TOP == 16)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 20)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 32)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 40)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 64)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 66)  ? 1'b0 :
	-- 			(QPLL_FBDIV_TOP == 80)  ? 1'b1 :
	-- 			(QPLL_FBDIV_TOP == 100) ? 1'b1 : 1'b1;

  gtxe2_common_i: GTXE2_COMMON
    generic map (
      -- Simulation attributes
      -- SIM_RESET_SPEEDUP   => true,
      SIM_QPLLREFCLK_SEL  => B"001",
      SIM_VERSION         => "4.0",

      ----------------COMMON BLOCK Attributes---------------
      BIAS_CFG                  => x"00_00_04_00_00_00_10_00",
      COMMON_CFG                => x"00_00_00_00",
      QPLL_CFG                  => b"000_0110_1000_0000_0001_1000_0001",  -- 27x"0_68_01_81",
      QPLL_CLKOUT_CFG           => b"0000",
      QPLL_COARSE_FREQ_OVRD     => b"010000",
      QPLL_COARSE_FREQ_OVRD_EN  => '0',
      QPLL_CP                   => b"0000011111",
      QPLL_CP_MONITOR_EN        => '0',
      QPLL_DMONITOR_SEL         => '0',
      QPLL_FBDIV                => b"01_0100_0000", --QPLL_FBDIV_IN,
      QPLL_FBDIV_MONITOR_EN     => '0',
      QPLL_FBDIV_RATIO          => '0', --QPLL_FBDIV_RATIO,
      QPLL_INIT_CFG             => x"00_00_06",
      QPLL_LOCK_CFG             => x"21_E8",
      QPLL_LPF                  => b"1111",
      QPLL_REFCLK_DIV           => 1
    )
    port map (
        ----------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
        DRPADDR                 => (others => '0'),
        DRPCLK                  => '0',
        DRPDI                   => (others => '0'),
        DRPDO                   => open,
        DRPEN                   => '0',
        DRPRDY                  => open,
        DRPWE                   => '0',
        -------------------- Common Block  - Ref Clock Ports ---------------------
        GTGREFCLK               => '0',
        GTNORTHREFCLK0          => '0',
        GTNORTHREFCLK1          => '0',
        GTREFCLK0               => refclk,
        GTREFCLK1               => '0',
        GTSOUTHREFCLK0          => '0',
        GTSOUTHREFCLK1          => '0',
        --------------------- Common Block - Clocking Ports ----------------------
        QPLLOUTCLK              => qplloutclk,
        QPLLOUTREFCLK           => qplloutrefclk,
        REFCLKOUTMONITOR        => open,
        ----------------------- Common Block - QPLL Ports ------------------------
        QPLLDMONITOR            => open,
        QPLLFBCLKLOST           => open,
        QPLLLOCK                => qplllock,
        QPLLLOCKDETCLK          => '0',
        QPLLLOCKEN              => '1',
        QPLLOUTRESET            => '0',
        QPLLPD                  => '0',
        QPLLREFCLKLOST          => open,
        QPLLREFCLKSEL           => b"001",
        QPLLRESET               => qpllreset,
        QPLLRSVD1               => b"0000_0000_0000_0000",
        QPLLRSVD2               => b"1_1111",
        ------------------------------- QPLL Ports -------------------------------
        BGBYPASSB               => '1',
        BGMONITORENB            => '1',
        BGPDB                   => '1',
        BGRCALOVRD              => b"1_1111",
        PMARSVD                 => b"0000_0000",
        RCALENB                 => '1'
    );

  --------------------------------------------------------------------------------
  -- Implement the shared clock and reset functions
  -- from ten_gig_eth_pcs_pma_shared_clock_and_reset.v
  --------------------------------------------------------------------------------
  -- Buffer the tx clock (322.26 MHz)
  txoutclk_bufg_i : BUFG
    port map (
      i   => txoutclk,
      o   => txusrclk
    );

  txusrclk2 <= txusrclk;

  -- Asynch reset synchronizers...
  areset_coreclk_sync_i : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '1',
      RST_DELAY     => 5
    )
    port map (
      src_rst   => areset,
      dst_clk   => coreclk,
      dst_rst   => areset_coreclk,
      dst_rst_n => open
    );

  qplllock_txusrclk2_sync_i : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '0',
      RST_DELAY     => 5
    )
    port map (
      src_rst   => qplllock,
      dst_clk   => txusrclk2,
      dst_rst   => open,
      dst_rst_n => qplllock_txusrclk2
    );

  -- Hold off the GT resets until 500ns after configuration.
  -- 256 ticks at the minimum possible 2.56ns period (390MHz) will be >> 500 ns.

  RESET_COUNT_PROC: process (coreclk)
  begin
    if rising_edge(coreclk) then
      if (reset_counter(8) = '0') then
        reset_counter <= reset_counter + 1;
      else
        reset_counter <= reset_counter;
      end if;
    end if;
  end process RESET_COUNT_PROC;

  reset_counter_done <= reset_counter(8);

  RESET_PULSE_PROC: process (coreclk)
  begin
    if rising_edge(coreclk) then
      if (areset_coreclk = '1') then
        reset_pulse   <=   B"1110";
      else
        reset_pulse   <=   '0' & reset_pulse(3 downto 1);
      end if;
    end if;
  end process RESET_PULSE_PROC;

  qpllreset <= reset_pulse(0);
  gttxreset <= reset_pulse(0);
  gtrxreset <= reset_pulse(0);

  gttxreset_txusrclk2_sync_i : cdc.cdc.reset
    generic map (
      SRC_RST_VALUE => '1',
      RST_DELAY     => 5
    )
    port map (
      src_rst   => gttxreset,
      dst_clk   => txusrclk2,
      dst_rst   => gttxreset_txusrclk2,
      dst_rst_n => open
    );

  TXUSERRDY_CLK_PROC: process (txusrclk2)
  begin
    if rising_edge(txusrclk2) then
      if (gttxreset_txusrclk2 = '1') then
        txuserrdy <= '0';
      else
        txuserrdy <= qplllock_txusrclk2;
      end if;
    end if;
  end process TXUSERRDY_CLK_PROC;

  --------------------------------------------------------------------------------
  -- End of shared clock and reset functions
  --------------------------------------------------------------------------------

  -- Set configuration
  phy_config_vector(14 downto 1)    <= (others => '0');
  phy_config_vector(79 downto 17)   <= (others => '0');
  phy_config_vector(109 downto 84)  <= (others => '0');
  phy_config_vector(175 downto 170) <= (others => '0');
  phy_config_vector(239 downto 234) <= (others => '0');
  phy_config_vector(269 downto 246) <= (others => '0');
  phy_config_vector(511 downto 272) <= (others => '0');
  phy_config_vector(515 downto 513) <= (others => '0');
  phy_config_vector(517 downto 517) <= (others => '0');
  phy_config_vector(0)              <= '0';             -- pma_loopback
  phy_config_vector(15)             <= '0';             -- pma_reset
  phy_config_vector(16)             <= '0';             -- global_tx_disable
  phy_config_vector(83 downto 80)   <= (others => '0'); -- pma_vs_loopback
  phy_config_vector(110)            <= '0';             -- pcs_loopback
  phy_config_vector(111)            <= '0';             -- pcs_reset
  phy_config_vector(169 downto 112) <= (others => '0'); -- test_patt_a
  phy_config_vector(233 downto 176) <= (others => '0'); -- test_patt_b
  phy_config_vector(240)            <= '0';             -- data_patt_sel
  phy_config_vector(241)            <= '0';             -- test_patt_sel
  phy_config_vector(242)            <= '0';             -- rx_test_patt_en
  phy_config_vector(243)            <= '0';             -- tx_test_patt_en
  phy_config_vector(244)            <= '0';             -- prbs31_tx_en
  phy_config_vector(245)            <= '0';             -- prbs31_rx_en
  phy_config_vector(271 downto 270) <= (others => '0'); -- pcs_vs_loopback
  phy_config_vector(512)            <= '0';             -- set_pma_link_status
  phy_config_vector(516)            <= '0';             -- set_pcs_link_status
  phy_config_vector(518)            <= '0';             -- clear_pcs_status2
  phy_config_vector(519)            <= '0';             -- clear_test_patt_err_count
  phy_config_vector(535 downto 520) <= (others => '0');

  -- Use the PCS Block Lock signal for link up.
  --
  -- For further details, see Xilinx' PG068, Table 2-11 (core_status[0] signal).
  link_up <= core_status(0);

  -- Add a pipeline to the xmgii_tx inputs, to aid timing closure
  pipeline_mdio: process (coreclk)
  begin
    if rising_edge(coreclk) then
      xgmii_txd_reg <= xgmii_txd;
      xgmii_txc_reg <= xgmii_txc;
    end if;
  end process;

  -- Add a pipeline to the xmgii_rx outputs, to aid timing closure
  pipeline_rx: process (coreclk)
  begin
    if rising_edge(coreclk) then
      xgmii_rxd <= xgmii_rxd_int;
      xgmii_rxc <= xgmii_rxc_int;
    end if;
  end process;

  activity_detect : process (coreclk)
    variable payload : std_logic;
    variable counter : integer range 0 to 156_000 := 0;
  begin
    if rising_edge(coreclk) then
      -- The control bit is '0' if the corresponding data byte is payload data
      payload := '0';
      for i in 0 to 7 loop
        payload := payload or (not xgmii_rxc_int(i)) or (not xgmii_txc_reg(i));
      end loop;

      if payload = '1' then
        counter := 156_000;
        activity <= '1';
      elsif counter /= 0 then
        counter := counter - 1;
      else
        activity <= '0';
      end if;
    end if;
  end process;

  -- If no arbitration is required on the GT DRP ports then connect REQ to GNT
  -- and connect other signals i <= o (PG068 v5.0 p20)
  drp_gnt       <= drp_req;
  drp_den_i     <= drp_den_o;
  drp_dwe_i     <= drp_dwe_o;
  drp_daddr_i   <= drp_daddr_o;
  drp_di_i      <= drp_di_o;
  drp_drdy_i    <= drp_drdy_o;
  drp_drpdo_i   <= drp_drpdo_o;

  eth_pcs_pma_0_inst : eth_pcs_pma_pkg.eth_pcs_pma
    port map (
      rxrecclk_out            => open, -- recovered receive clock - not used
      coreclk                 => coreclk,
      dclk                    => coreclk,
      txusrclk                => txusrclk,
      txusrclk2               => txusrclk2,
      areset                  => areset,
      txoutclk                => txoutclk,
      areset_coreclk          => areset_coreclk,
      gttxreset               => gttxreset,
      gtrxreset               => gtrxreset,
      txuserrdy               => txuserrdy,
      qplllock                => qplllock,
      qplloutclk              => qplloutclk,
      qplloutrefclk           => qplloutrefclk,
      reset_counter_done      => reset_counter_done,
      txp                     => txp,
      txn                     => txn,
      rxp                     => rxp,
      rxn                     => rxn,
      sim_speedup_control     => '0',
      xgmii_txd               => xgmii_txd_reg,
      xgmii_txc               => xgmii_txc_reg,
      xgmii_rxd               => xgmii_rxd_int,
      xgmii_rxc               => xgmii_rxc_int,
      configuration_vector    => phy_config_vector,
      status_vector           => phy_status_vector,
      core_status             => core_status,
      tx_resetdone            => tx_resetdone_int,
      rx_resetdone            => rx_resetdone_int,
      signal_detect           => signal_detect,
      tx_fault                => tx_fault,
      drp_req                 => drp_req,
      drp_gnt                 => '1', -- drp_gnt, -- verilog-ethernet project sets to '1'
      drp_den_o               => drp_den_o,
      drp_dwe_o               => drp_dwe_o,
      drp_daddr_o             => drp_daddr_o,
      drp_di_o                => drp_di_o,
      drp_drdy_o              => drp_drdy_o,
      drp_drpdo_o             => drp_drpdo_o,
      drp_den_i               => '0', -- drp_den_i,
      drp_dwe_i               => '0', -- drp_dwe_i,
      drp_daddr_i             => x"00_00", -- drp_daddr_i,
      drp_di_i                => x"00_00", -- drp_di_i,
      drp_drdy_i              => '0', -- drp_drdy_i,
      drp_drpdo_i             => x"00_00", -- drp_drpdo_i,
      tx_disable              => tx_disable,
      -- 10GBASE-R only: Set this to a constant to define the PMA/PMD
      -- type as described in IEEE 802.3 section 45.2.1.6:
      -- 111 = 10GBASE-SR
      -- 110= 10GBASE-LR
      -- 101 = 10GBASE-ER
      pma_pmd_type            => B"101"
    );

end rtl;
