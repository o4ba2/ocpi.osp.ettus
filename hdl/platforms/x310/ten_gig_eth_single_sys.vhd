-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library unisim; use unisim.vcomponents.all;

entity ten_gig_eth_single_sys is
  port (
    -- 156 MHz reference clock
    refclk_p            : in  std_logic;
    refclk_n            : in  std_logic;

    areset              : in  std_logic;

    -- XGMII interface
    xgmii_tx_clk        : out std_logic;
    xgmii_tx_reset      : out std_logic;
    xgmii_txd           : in  std_logic_vector(63 downto 0);
    xgmii_txc           : in  std_logic_vector(7 downto 0);
    xgmii_rx_clk        : out std_logic;
    xgmii_rx_reset      : out std_logic;
    xgmii_rxd           : out std_logic_vector(63 downto 0);
    xgmii_rxc           : out std_logic_vector(7 downto 0);

    -- Transmit and receive lanes
    txp                 : out std_logic;
    txn                 : out std_logic;
    rxp                 : in  std_logic;
    rxn                 : in  std_logic;

    link_up             : out std_logic;
    activity            : out std_logic;

    signal_detect       : in  std_logic;
    tx_fault            : in  std_logic;
    tx_disable          : out std_logic
  );
end ten_gig_eth_single_sys;

architecture rtl of ten_gig_eth_single_sys is

  -- Clocks
  signal refclk  : std_logic;
  signal coreclk : std_logic;

begin
  -- Clocks
  ten_gige_phy_clk_gen_inst : entity work.ten_gige_phy_clk_gen
    port map (
      areset    => areset,
      refclk_p  => refclk_p,
      refclk_n  => refclk_n,
      refclk    => refclk,
      clk156    => coreclk
    );

  ten_gig_eth_phy_0_inst : entity work.ten_gig_eth_phy
    port map (
      refclk              => refclk,
      coreclk             => coreclk,

      areset              => areset,

      xgmii_tx_clk        => xgmii_tx_clk,
      xgmii_tx_reset      => xgmii_tx_reset,
      xgmii_txd           => xgmii_txd,
      xgmii_txc           => xgmii_txc,

      xgmii_rx_clk        => xgmii_rx_clk,
      xgmii_rx_reset      => xgmii_rx_reset,
      xgmii_rxd           => xgmii_rxd,
      xgmii_rxc           => xgmii_rxc,

      txp                 => txp,
      txn                 => txn,

      rxp                 => rxp,
      rxn                 => rxn,

      link_up             => link_up,
      activity            => activity,

      signal_detect       => signal_detect,
      tx_fault            => tx_fault,
      tx_disable          => tx_disable
    );

end rtl;
