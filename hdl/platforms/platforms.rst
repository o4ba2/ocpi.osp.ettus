.. Platform directory index page


Platforms
=========
Available platforms.

.. toctree::
   :maxdepth: 2
   :glob:

   */*-gsg
   */*-worker
