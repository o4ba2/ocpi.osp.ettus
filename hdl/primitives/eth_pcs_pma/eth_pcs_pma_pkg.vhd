-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

package eth_pcs_pma_pkg is

-- Component without shared logic included
component eth_pcs_pma
  port (
    rxrecclk_out         : out std_logic;
    coreclk              : in  std_logic;
    dclk                 : in  std_logic;
    txusrclk             : in  std_logic;
    txusrclk2            : in  std_logic;
    areset               : in  std_logic;
    txoutclk             : out std_logic;
    areset_coreclk       : in  std_logic;
    gttxreset            : in  std_logic;
    gtrxreset            : in  std_logic;
    txuserrdy            : in  std_logic;
    qplllock             : in  std_logic;
    qplloutclk           : in  std_logic;
    qplloutrefclk        : in  std_logic;
    reset_counter_done   : in  std_logic;
    txp                  : out std_logic;
    txn                  : out std_logic;
    rxp                  : in  std_logic;
    rxn                  : in  std_logic;
    sim_speedup_control  : in  std_logic;
    xgmii_txd            : in  std_logic_vector(63 downto 0);
    xgmii_txc            : in  std_logic_vector(7 downto 0);
    xgmii_rxd            : out std_logic_vector(63 downto 0);
    xgmii_rxc            : out std_logic_vector(7 downto 0);
    configuration_vector : in  std_logic_vector(535 downto 0);
    status_vector        : out std_logic_vector(447 downto 0);
    core_status          : out std_logic_vector(7 downto 0);
    tx_resetdone         : out std_logic;
    rx_resetdone         : out std_logic;
    signal_detect        : in  std_logic;
    tx_fault             : in  std_logic;
    drp_req              : out std_logic;
    drp_gnt              : in  std_logic;
    drp_den_o            : out std_logic;
    drp_dwe_o            : out std_logic;
    drp_daddr_o          : out std_logic_vector(15 downto 0);
    drp_di_o             : out std_logic_vector(15 downto 0);
    drp_drdy_o           : out std_logic;
    drp_drpdo_o          : out std_logic_vector(15 downto 0);
    drp_den_i            : in  std_logic;
    drp_dwe_i            : in  std_logic;
    drp_daddr_i          : in  std_logic_vector(15 downto 0);
    drp_di_i             : in  std_logic_vector(15 downto 0);
    drp_drdy_i           : in  std_logic;
    drp_drpdo_i          : in  std_logic_vector(15 downto 0);
    tx_disable           : out std_logic;
    pma_pmd_type         : in  std_logic_vector(2 downto 0)
  );
end component;

end package eth_pcs_pma_pkg;
