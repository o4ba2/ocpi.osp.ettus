#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

set -eu

if ! command -v vivado &> /dev/null
then
  echo "ERROR: Vivado must be on the path"
  exit 1
fi

# Get the path to the directory containing this script
script_dir="$(dirname "$(realpath "$0")")"

# Work in a temporary directory
mkdir "$script_dir/tmp"
pushd "$script_dir/tmp" > /dev/null

# Generate the IP
vivado -mode batch -source ../gen_ip.tcl -tclargs eth_pcs_pma 0

# Clean up the temporary directory
popd > /dev/null
rm -rf "$script_dir/tmp"
