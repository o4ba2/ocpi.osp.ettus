# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# The name of the generated core
set ip_module [lindex $argv 0]

# Should the shared logic be included?
set shared_logic [lindex $argv 1]

# The part on the X310
set ip_part {xc7k410tffg900-2}

# The IP core that we are creating
set ip_name {ten_gig_eth_pcs_pma}

# Create an IP project to work inside
create_project managed_ip_project managed_ip_project -ip -force -part $ip_part

# Create the IP
create_ip -name $ip_name -vendor xilinx.com -library ip -module_name $ip_module -dir .

# Configure the IP
set_property -dict [subst {
  CONFIG.MDIO_Management {false}
  CONFIG.base_kr         {BASE-R}
  CONFIG.SupportLevel    $shared_logic
  CONFIG.DClkRate        {125}
}] [get_ips $ip_module]

# Ensure that the XCI file on disk is up to date
validate_ip -save_ip [get_ips $ip_module]

# Generate the target data for the IP
generate_target all [get_ips $ip_module]

# Extract the constraints files
set xdc_files [get_files -regexp {.*xdc} -of_objects [get_ips $ip_module]]
file copy -force {*}$xdc_files ../../

# Run OOC synthesis
create_ip_run [get_ips $ip_module]
launch_runs ${ip_module}_synth_1 -jobs 8
wait_on_run ${ip_module}_synth_1

# Open the run and write it out as an EDIF
open_run ${ip_module}_synth_1 -name ${ip_module}_synth_1
write_edif -security_mode all ../../${ip_module}.edf
