.. eth_pcs_pma documentation


.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive below.

.. meta::
   :keywords: skeleton example


.. _eth_pcs_pma-primitive:


10Gb Ethernet PCS and PMA (``eth_pcs_pma``)
===========================================

Xilinx 10G Ethernet PCS/PMA IP Core.

Design
------

This primitive instantiates the Xilinx 10G Ethernet PCS/PMA IP Core, which
implements the Physical Coding Sublayer (PCS) and Physical Media Attachment (PMA)
for 10GBASE-R Ethernet.

Implementation
--------------

The IP Core is built into a ``.edf`` netlist and exposed as an OpenCPI
primitive. The process for building the netlist is as follows:

* A Makefile triggers the build by executing a shell script
  (``./scripts/gen_ip.sh``).
* The shell script in turn invokes Vivado in batch mode, sourcing a TCL script
  (``./scripts/gen_ip.tcl``).
* The TCL script instantiates and configures the IP core, synthesizes it and
  saves the resulting netlist as an ``.edf``.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``eth_pcs_pma`` are:

 * None
