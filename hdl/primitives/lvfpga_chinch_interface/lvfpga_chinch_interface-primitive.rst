.. lvfpga_chinch_interface documentation

.. _lvfpga_chinch_interface-primitive:


NI CHInCh Interface (``lvfpga_chinch_interface``)
=================================================

Interface to the NI STC3 PCIe chip present on the X310 motherboard.

Design
------

This primitive implements the interface to the NI STC3 PCIe chip. While the X310
OSP does not make use of the PCIe interface, the X310 documentation states that
it is required to implement this interface to avoid hardware damage.

Implementation
--------------

The implementation is provided as an ``.ngc`` netlist released under the LGPLv3
license as part of the Ettus UHD codebase.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``lvfpga_chinch_interface`` are:

 * None
