-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

package lvfpga_chinch_interface_pkg is

component LvFpga_Chinch_Interface
  port (
    aIoResetIn_n              : in  std_logic;
    bBusReset                 : out std_logic;

    BusClk                    : in  std_logic;
    Rio40Clk                  : in  std_logic;
    IDelayRefClk              : in  std_logic;
    aRioClkPllLocked          : in  std_logic;
    aRioClkPllReset           : out std_logic;

    aIoReadyOut               : out std_logic;
    aIoReadyIn                : in  std_logic;
    aIoPort2Restart           : out std_logic;

    IoRxClock                 : in  std_logic;
    IoRxClock_n               : in  std_logic;
    irIoRxData                : in  std_logic_vector (15 downto 0);
    irIoRxData_n              : in  std_logic_vector (15 downto 0);
    irIoRxHeader              : in  std_logic;
    irIoRxHeader_n            : in  std_logic;

    IoTxClock                 : out std_logic;
    IoTxClock_n               : out std_logic;
    itIoTxData                : out std_logic_vector (15 downto 0);
    itIoTxData_n              : out std_logic_vector (15 downto 0);
    itIoTxHeader              : out std_logic;
    itIoTxHeader_n            : out std_logic;

    bDmaRxData                : in  std_logic_vector (383 downto 0);
    bDmaRxValid               : in  std_logic_vector (5 downto 0);
    bDmaRxReady               : out std_logic_vector (5 downto 0);
    bDmaRxEnabled             : out std_logic_vector (5 downto 0);
    bDmaRxFifoFreeCnt         : out std_logic_vector (65 downto 0);

    bDmaTxData                : out std_logic_vector (383 downto 0);
    bDmaTxValid               : out std_logic_vector (5 downto 0);
    bDmaTxReady               : in  std_logic_vector (5 downto 0);
    bDmaTxEnabled             : out std_logic_vector (5 downto 0);
    bDmaTxFifoFullCnt         : out std_logic_vector (65 downto 0);

    bUserRegPortInWt          : out std_logic;
    bUserRegPortInRd          : out std_logic;
    bUserRegPortInAddr        : out std_logic_vector (19 downto 0);
    bUserRegPortInData        : out std_logic_vector (31 downto 0);
    bUserRegPortInSize        : out std_logic_vector (1 downto 0);
    bUserRegPortOutData       : in  std_logic_vector (31 downto 0);
    bUserRegPortOutDataValid  : in  std_logic;
    bUserRegPortOutReady      : in  std_logic;

    bChinchRegPortOutWt       : in  std_logic;
    bChinchRegPortOutRd       : in  std_logic;
    bChinchRegPortOutAddr     : in  std_logic_vector (31 downto 0);
    bChinchRegPortOutData     : in  std_logic_vector (63 downto 0);
    bChinchRegPortOutSize     : in  std_logic_vector (1 downto 0);
    bChinchRegPortInData      : out std_logic_vector (63 downto 0);
    bChinchRegPortInDataValid : out std_logic;
    bChinchRegPortInReady     : out std_logic;

    aIrq                      : out std_logic
  );
end component;

end package lvfpga_chinch_interface_pkg;
