.. ubx160_cpld documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _ubx160_cpld:


UBX160 CPLD (``ubx160_cpld``)
=============================
Ettus x310 UBX160 daughter board IO worker.

Design
------
This worker provides control of the CPLD device located on the UBX160 daughter card.
An instance of this worker is required for each daughter board present in the x310.

Interface
---------
The interface of the ubx160_cpld.hdl worker is defined in ``./ubx160_cpld.xml`` 

.. .. literalinclude:: ../specs/ubx160_cpld-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``./ubx160_cpld.xml``
These properties are used by the drc_x310.rcc and do not need to be set by an application using the drc_x310.

.. .. ocpi_documentation_properties::

Ports
~~~~~
The ubx160_cpld.hdl worker does not have any component ports. 

.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ubx160_cpld.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

The max2871.hdl worker (is supported by) worker 

 * ``ocpi.osp.ettus.devices.ubx160_io.hdl``
 
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``ubx160_cpld`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
