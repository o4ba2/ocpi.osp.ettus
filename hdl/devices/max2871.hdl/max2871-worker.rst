.. max2871 HDL worker


.. _max2871-HDL-worker:


``max2871`` HDL Worker
======================
Ettus UBX160 daughter board HDL Device Worker for the MAX2871 Fractional Integer-N Synthesizer / VCO. 

Detail
------
The Ettus x310 mother board can be fitted with one or two UBX160 daughter boards. 
The UBX160 contains hardware used to combine the separate baseband I and Q signals from the mother board and
to modulate them to form the RF signal for transmission. The received carrier signal undergoes the reverse process.
The block diagram of the UBX160 is shown below.

.. figure:: ./doc/max2871_0.svg

Four MAX2871 Synthesiser / VCOs are used on the daughter board, two are used in the transmit path and two in the receive path.
The two MAX2871 devices for each direction are used to provide the local oscillator signals used to shift the baseband signal 
to the desired carrier frequency. 
The REF_IN clock of the MAX2871 is supplied by the x310 mother board. The REF_IN clock is dependent on the x310 operating sample rate.

+----------------+----------------+
| Sample Rate    | REF_IN         |
+================+================+
| 200 MHz        | 50 MHz         |
+----------------+----------------+
| 184.32 MHz     | 49.625         |
+----------------+----------------+

The use of each of the MAX2871 devices is shown in the more detailed block diagram below.

.. figure:: ./doc/max2871_1.svg


The MAX2871 device usage and frequencies being set depend on the desired carrier frequency. 
TXLO1 is used to modulate the baseband signal to between 400MHz and 6GHz. 
TXLO2 is used to down convert the resulting signal by between 10MHz and 500MHz.

Transmit carrier frequency VCO configuration

+-------------------------+----------------+----------------+----------------+
| Carrier Frequency Range | TXLO1          | TXLO1 Filter   | TXL02          |
+=========================+================+================+================+
|  < 500 MHz              | 2100 MHz       | 2.2 GHz        | (TXLO1 - freq) |
+-------------------------+----------------+----------------+----------------+
|  500 MHz - 800 MHz      | freq           | 0.8 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+
|  800 MHz - 1000 MHz     | freq           | 0.8 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+
|  1000 MHz - 2200 MHz    | freq           | 2100 MHz       | Not used       |
+-------------------------+----------------+----------------+----------------+
|  2200 MHz - 2500 MHz    | freq           | 2100 MHz       | Not used       |
+-------------------------+----------------+----------------+----------------+
|  2500 MHz - 6000 MHz    | freq           | None           | Not used       |
+-------------------------+----------------+----------------+----------------+

When operating with a carrier frequency of less than 500 MHz, TXLO2 is used to down convert the IF frequency of 2100MHz
down to the desired carrier. When operating with a carrier of 500MHz or greater, TXLO2 is not used and the direct conversion
path is selected.

Receive carrier frequency VCO configuration

+-------------------------+----------------+----------------+----------------+----------------+
| Carrier Frequency Range | LNA            | RXLO1          | RXLO1 Filter   | RXL02          |
+=========================+================+================+================+================+
|  < 100 MHz              | 10MHz - 1.5GHz | 2380 MHz       | None           | (RXLO1-freq)   |
+-------------------------+----------------+----------------+----------------+----------------+
|  100 MHz - 500 MHz      | 10MHz - 1.5GHz | 2440 MHz       | None           | (RXLO1-freq)   |
+-------------------------+----------------+----------------+----------------+----------------+
|  500 MHz - 800 MHz      | 10MHz - 1.5GHz | freq           | 0.8 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+
|  800 MHz - 1000 MHz     | 10MHz - 1.5GHz | freq           | 0.8 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+
|  1000 MHz - 1500 MHz    | 10MHz - 1.5GHz | freq           | 2.2 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+
|  1500 MHz - 2200 MHz    | 1.5GHz - 6GHz  | freq           | 2.2 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+
|  2200 MHz - 2500 MHz    | 1.5GHz - 6GHz  | freq           | 2.2 GHz        | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+
|  2500 MHz - 6000 MHz    | 1.5GHz - 6GHz  | freq           | None           | Not used       |
+-------------------------+----------------+----------------+----------------+----------------+

When operating with a carrier of less than 500 MHz, RXLO2 is used to first up convert the signal to an IF of 2380 or 2440 MHz.
RXLO1 then converts the signal to baseband.

An individual instance of the max2871.hdl worker is used for each device on the UBX160 daughter card.
The worker provides access to the raw MAX2871 device registers. The MAX2871 contains six writable 32-bit registers,
these are accessed using a single SPI register (the register address is specified in the three least significant bits of the 32-bit word written).
The SPI write is performed on behalf of this worker by the ubx160_io.hdl worker.

The registers of the MAX2871 (accessed through the max2871.hdl worker) are written by the drc_x310.rcc Digital-Radio-Controller worker.
The application is not expected to access the registers itself. 

MAX2871 Block Diagram.

.. figure:: ./doc/max2871_2.svg

Utilization
-----------
.. ocpi_documentation_utilization::
