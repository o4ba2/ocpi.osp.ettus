.. drc_x310 documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _drc_x310:


X310 Digital Radio Controller (``drc_x310``)
============================================
Digital-Radio-Controller (DRC) for the Ettus x310

Design
------
This DRC component is used by an application to use the x310 radio hardware.
The DRC is used to configure and control the radio hardware. The application
connects its transmit and receive data streams to its ports.
This DRC supports both single and dual daughter board configurations.

Interface
---------
The interface of the drc_x310 is defined in ``../specs/drc_x310-spec.xml``

.. .. literalinclude:: ../specs/drc_x310-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
Both the transmit and receive data stream ports of the DRC use protocol complex_short_timed_sample-prot.xml
Transmit (DAC) data streams ignore all opcodes except **sample**.
Receive (ADC) data streams can produce Discontinuity OpCode (dependent on the setting of DRC property report_data_loss)

Properties
~~~~~~~~~~
The drc_x310 implements the properties defined for a digital-radio-controller.
These can be found in ``drc-spec.xml``
A description of the use of these properties can be found in the OpenCPI Platform Development Guide.

.. .. ocpi_documentation_properties::

..   property_name: Skeleton outline: List any additional text for properties, which will be included in addition to the description field in the component specification XML.

Ports
~~~~~
The drc_x310 has two data stream ports

* tx (an input port of the DRC, data from this port is transmitted by the radio hardware)
* rx (an output port of the DRC, data received by the radio hardware is sent to this port)

Both the tx and rx ports are arrays (of size 2).
Index=0 of the tx and rx arrays is used to transmit / receive using daughter board 0
Index=1 of the tx and rx arrays is used to transmit / receive using daughter board 1.

Both ports use the ``complex_short_timed_sample`` protocol

.. .. ocpi_documentation_ports::

..    input: Primary input samples port.
..    output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../drc_x310.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``ocpi.platform.devices.data_sink_qdac_csts.hdl``
 * ``ocpi.platform.devices.data_src_qadc_csts.hdl``

The DRC makes use of the following OpenCPI (RCC) software

 * ``OcpiDrcProxyApi.hh``
 * ``ocpi::drc::DrcProxyBase``

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``drc_x310`` are:

 * The DRC only supports the UBX160 daughter cards
 * Only sample rates 200MHz and 184.32MHz are supported
 * Transmit frequencies between 50MHz and 6000MHz are supported
 * Receive frequencies between 50MHz and 6000MHz are supported
 * The transmit and receive bandwidth is fixed at 160MHz
 * The default builds for the DRC support single UBX160 daughter card 0 or dual UBX160 daughter cards
 * The DRC supports a maximum of 2 configurations
 * The DRC supports a maximum of 4 channels (TX and RX for each of the two daughter boards)

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
