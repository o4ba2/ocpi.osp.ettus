-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;
library unisim; use unisim.vcomponents.all;
library xpm; use xpm.vcomponents.all;

architecture rtl of worker is

  signal radclk : std_logic;
  signal radclk2x : std_logic;
  signal dciclk : std_logic;
  signal idle   : std_logic; --disable outputs when not operating

  signal radclk2rst : std_logic;
  signal radclkrst : std_logic; --reset in radclk domain

  signal datai  : std_logic_vector(15 downto 0);
  signal dataq  : std_logic_vector(15 downto 0);
  signal phase  : std_logic;
  signal phased : std_logic;
  signal iqsel  : std_logic; --High = I , Low = Q
  signal iqseln : std_logic;
  signal dataiq : std_logic_vector(15 downto 0);
  signal dci    : std_logic;
  signal frame  : std_logic;
  signal dout   : std_logic_vector(7 downto 0);

begin

  radclk      <= rfclk_in.radclk;
  radclk2x    <= rfclk_in.radclk2x;
  dciclk      <= rfclk_in.dciclk;

  dev_out.clk <= rfclk_in.radclk;

  -- Delagate our raw properties to the rawprops interface
  rawprops_out.present     <= '1';
  rawprops_out.reset       <= ctl_in.reset;
  rawprops_out.raw         <= props_in.raw;
  props_out.raw            <= rawprops_in.raw;

  process (ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        idle <= '1';
      elsif ctl_in.is_operating = '1' then
        idle <= '0';
      end if;
    end if;
  end process;

  --Clock domain crossing
  syncrst_inst : xpm_cdc_sync_rst
  generic map (
    DEST_SYNC_FF   => 4,
    INIT           => 1,
    INIT_SYNC_FF   => 0,
    SIM_ASSERT_CHK => 0
  )
  port map (
    dest_rst       => radclkrst,
    dest_clk       => radclk,
    src_rst        => ctl_in.reset
  );

  process (radclk) is
  begin
    if rising_edge(radclk) then
      if props_in.enable and dev_in.valid = '1' then
        datai <= dev_in.data_i;
        dataq <= dev_in.data_q;
      end if;
    end if;
  end process;

  process (radclk) is
  begin
    if rising_edge(radclk) then
      if radclkrst = '1' then
        phase <= '0';
      else
        phase <= not phase;
      end if;
    end if;
  end process;

  process (radclk2x) is
  begin
    if rising_edge(radclk2x) then
      phased <= phase;
      if phase = phased then
        iqsel <= '1';
      else
        iqsel <= '0';
      end if;
      if iqsel = '1' then
        dataiq <= dataq;
      else
        dataiq <= datai;
      end if;
    end if;
  end process;

  d0_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(0),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(8),
      D2           => dataiq(0),
      S            => '0',
      R            => '0'
    );

  d1_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(1),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(9),
      D2           => dataiq(1),
      S            => '0',
      R            => '0'
    );

  d2_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(2),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(10),
      D2           => dataiq(2),
      S            => '0',
      R            => '0'
    );

  d3_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(3),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(11),
      D2           => dataiq(3),
      S            => '0',
      R            => '0'
    );

  d4_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(4),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(12),
      D2           => dataiq(4),
      S            => '0',
      R            => '0'
    );

  d5_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(5),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(13),
      D2           => dataiq(5),
      S            => '0',
      R            => '0'
    );

  d6_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(6),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(14),
      D2           => dataiq(6),
      S            => '0',
      R            => '0'
    );

  d7_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dout(7),
      C            => radclk2x,
      CE           => '1',
      D1           => dataiq(15),
      D2           => dataiq(7),
      S            => '0',
      R            => '0'
    );

  d0_obufds_inst : OBUFDS
    port map (
      I  => dout(0),
      O  => D0_P,
      OB => D0_N
    );

  d1_obufds_inst : OBUFDS
    port map (
      I  => dout(1),
      O  => D1_P,
      OB => D1_N
    );

  d2_obufds_inst : OBUFDS
    port map (
      I  => dout(2),
      O  => D2_P,
      OB => D2_N
    );

  d3_obufds_inst : OBUFDS
    port map (
      I  => dout(3),
      O  => D3_P,
      OB => D3_N
    );

  d4_obufds_inst : OBUFDS
    port map (
      I  => dout(4),
      O  => D4_P,
      OB => D4_N
    );

  d5_obufds_inst : OBUFDS
    port map (
      I  => dout(5),
      O  => D5_P,
      OB => D5_N
    );

  d6_obufds_inst : OBUFDS
    port map (
      I  => dout(6),
      O  => D6_P,
      OB => D6_N
    );

  d7_obufds_inst : OBUFDS
    port map (
      I  => dout(7),
      O  => D7_P,
      OB => D7_N
    );

  --Enable DAC when in operating mode
  ENABLE <= ctl_in.is_operating;

  -- Generate framing signal to identify I and Q
  iqseln  <= not iqsel;

  frame_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => frame,
      C            => radclk2x,
      CE           => '1',
      D1           => iqseln,
      D2           => iqseln,
      S            => '0',
      R            => '0'
    );

  frame_obufds_inst : OBUFDS
    port map (
      I  => frame,
      O  => FRAME_P,
      OB => FRAME_N
    );

  -- Source synchronous clk
  dci_oddr_inst : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE"
    ) port map (
      Q            => dci,
      C            => dciclk,
      CE           => '1',
      D1           => '1',
      D2           => '0',
      S            => '0',
      R            => '0'
    );

  dci_obufds_inst : OBUFDS
    port map (
      I  => dci,
      O  => DCI_P,
      OB => DCI_N
    );

end rtl;
