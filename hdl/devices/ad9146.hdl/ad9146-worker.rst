.. ad9146 HDL worker


.. _ad9146-HDL-worker:


``ad9146`` HDL Worker
=====================
Device worker for the AD9146 Dual, 16-Bit, 1230 MSPS DAC.

Detail
------

Registers
^^^^^^^^^

The raw properties corresponding to the AD9146 registers are directly
connected to the ``rawprops`` interface. This is required to be connected to a
separate SPI worker that converts reads and writes to the raw properties to SPI
transfers to the device.

Data transmission
^^^^^^^^^^^^^^^^^

The AD9146 receives data over an 8-bit parallel LVDS bus with a source
synchronous clock. The data is transmitted as edge aligned DDR data at twice the
sample rate, with the I channel data transferred on the first two clock edges
and the Q channel data transferred on the second two clock edges. This is
illustrated by the following diagram from the datasheet:

.. figure:: ./doc/ddr.png

To transmit the data several clock domains are used that are generated by the
LMK04816 worker and distributed of a devsignals interface. It is important that
these clocks have the correct phase relationship, and that the correct
constraints are used, to ensure that correct timing is achieved on the LVDS
interface.

The data is received over a devsignals interface using the ``qdac-16-signals``
protocol. This is intended to interface to a ``data_sink_qdac_csts`` device
worker. The ``radclk`` clock domain is supplied over the devsignals interface
to be used to clock the data.

The data transmitted to the DAC is updated when both the valid signal from the
devsignals interface and the ``enable`` property are asserted. This allows the
DRC to enable the data stream when the full RF path has been configured.

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
