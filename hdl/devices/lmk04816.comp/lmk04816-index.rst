.. lmk04816 documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _lmk04816:


LMK04816 Clock Generator (``lmk04816``)
=======================================
Ettus x310 HDL Device Worker for the LMK04816 low noise clock generator. 

Design
------
This Device Worker is used by Ettus x310 Digital-radio-controller (DRC) for control and configuration of the LMK04816.
This chip generates the following clock signals

* FPGA_CLK, CPRI_CLK_CLEAN
* DB0_DAC_REFCLK, DB1_DAC_REFCLK
* DB0_ADC_REFCLK, DB1_DAC_REFCLK
* DB0_RX_REFCLK, DB1_RX_REFCLK
* DB0_TX_REFCLK, DB1_TX_REFCLK
* REFCLK_out, CLKOUT

Interface
---------
The interface of the lmk04816.hdl worker is defined in ``../specs/lmk04816-spec.xml`` 

.. .. literalinclude:: ../specs/lmk04816-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``../specs/lmk04816-spec.xml``
These properties give access to the LMK04816 hardware registers. 
They are used by the drc_x310.rcc and do not need to be set by an application using the drc_x310.

.. .. ocpi_documentation_properties::

..    property_name: Skeleton outline: List any additional text for properties, which will be included in addition to the description field in the component specification XML.

Ports
~~~~~
The lmk04816.hdl worker does not have any component ports. 
It interfaces with several other of the Ettus x310 device workers using signals only.

.. .. ocpi_documentation_ports::

..    input: Primary input samples port.
..    output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../lmk04816.hdl

.. Example Application
.. -------------------
.. .. literalinclude:: example_app.xml
..    :language: xml

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

The lmk04816.hdl worker (supports) these workers 

 * ``ocpi.osp.ettus.devices.ad9146.hdl``
 * ``ocpi.osp.ettus.devices.ads62p48.hdl``

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``lmk04816`` are:

 * This worker is to be used only with the drc_x310.rcc (DRC proxy)

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
