.. dac_spi HDL worker


.. _dac_spi-HDL-worker:


``dac_spi`` HDL Worker
======================
Ettus USRP X310 DAC SPI device worker. Provides access to the hardware registers of Daughter board 0 and 1 AD9146 DACs.

Detail
------
This worker is used to control both AD9146 DACs present on the X310 mother board. It drives the SPI signals
connected to both instances of the AD9146 DAC, providing write access to the hardware registers of both AD9146 DACs.
The worker provides a raw property interface to the ad9146.hdl worker, which in turn provides named register
access to the drc_x310.rcc (Digital-Radio-Controller).

This worker is defined as supporting two instances of the ad9146.hdl worker. 

.. code-block:: xml

   <supports worker="ad9146">
      <connect port="rawprops" to="rawprops" index="0"/>
   </supports>

   <supports worker="ad9146">
      <connect port="rawprops" to="rawprops" index="1"/>
   </supports>

This worker is responsible for driving the following hardware signals

+--------------+-----------+---------------------------------------------+
| Signal       | Direction | Description                                 |
+==============+===========+=============================================+
| DB_DAC_MOSI  | Input     | serial data input / output. shared between  |
|              | Output    | both DACs                                   |
+--------------+-----------+---------------------------------------------+
| DB_DAC_SCLK  | Output    | 1MHz SPI serial clock out.                  |
|              |           | drives both DACs                            |
+--------------+-----------+---------------------------------------------+
| DB0_DAC_SEN  | Output    | selects daughter board 0 DAC                |
+--------------+-----------+---------------------------------------------+
| DB1_DAC_SEN  | Output    | selects daughter board 1 DAC                |
+--------------+-----------+---------------------------------------------+
| DB_DAC_RESET | Output    | active low reset                            |
+--------------+-----------+---------------------------------------------+

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
