-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;

entity three_wire_spi is
  generic (
    clock_divisor : positive := 16;
    addr_width    : positive := 7;
    data_width    : positive := 8
  );
  port (
    clk           : in  std_logic;
    reset         : in  std_logic;

    renable       : in  std_logic;
    wenable       : in  std_logic;
    done          : out std_logic;

    addr          : in  std_logic_vector(addr_width-1 downto 0);
    wdata         : in  std_logic_vector(data_width-1 downto 0);
    rdata         : out std_logic_vector(data_width-1 downto 0);

    sclk          : out std_logic;
    sen           : out std_logic;
    sdata_i       : in  std_logic;
    sdata_o       : out std_logic;
    sdata_oe      : out std_logic
  );
end entity three_wire_spi;

architecture rtl of three_wire_spi is

  type   state_t is (idle_e, starting_e, busy_e, done_e);
  signal state_r      : state_t := idle_e;
  signal data_sr      : std_logic_vector(data_width + addr_width downto 0) := (others => '0');
  signal clk_count_r  : natural range 0 to clock_divisor - 1 := 0;
  signal bit_count_r  : natural range 0 to data_sr'high + 1 := 0;
  signal sclk_r       : std_logic := '0';
  signal tristate_r   : std_logic := '1';
  signal read_bit_r   : std_logic;

begin

  done     <= '1' when state_r = done_e else '0';
  rdata    <= data_sr(data_width - 1 downto 0);
  sclk     <= sclk_r;
  sen      <= '0' when state_r = starting_e or state_r = busy_e else '1';
  sdata_o  <= data_sr(data_sr'left);
  sdata_oe <= not tristate_r;

  p: process(clk) is begin
    if rising_edge(clk) then
      if reset = '1' then
        state_r    <= idle_e;
        sclk_r     <= '0';
        data_sr    <= (others => '0');
        tristate_r <= '1';
      else
        case state_r is

          when idle_e =>
            if renable = '1' or wenable = '1' then
              state_r <= starting_e;
            end if;

          when starting_e =>
            state_r     <= busy_e;
            data_sr     <= renable & addr & wdata;
            tristate_r  <= '0';
            bit_count_r <= 0;
            clk_count_r <= 0;

          when busy_e =>
            if clk_count_r = clock_divisor - 1 then
              -- End of bit cycle
              clk_count_r <= 0;
              bit_count_r <= bit_count_r + 1;

              -- Shift a bit
              data_sr      <= data_sr(data_sr'length - 2 downto 0) & read_bit_r;
              sclk_r       <= '0';

              if bit_count_r = addr_width then
                -- End of address phase
                tristate_r <= renable;
              elsif bit_count_r = (data_width + addr_width) then
                -- End of access
                state_r     <= done_e;
              end if;
            else
              clk_count_r <= clk_count_r + 1;

              if clk_count_r = clock_divisor/2 - 1 then
                -- Mid-cycle, read data is captured
                sclk_r     <= '1';
                read_bit_r <= sdata_i;
              end if;
            end if;

          when done_e =>
            state_r <= idle_e;

        end case;
      end if;
    end if;
  end process p;

end rtl;
