-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This SPI bus is shared between two AD9146 DACs.
--
-- The SPI format is:
--   - 1 read/not-write bit followed by 7 address bits followed by 8 data bits
--   - MSB first, data captured on clock rising edge
--   - During read, the data line starts as an output and switches to an input
--     after the address bits
--
-- The basic ocpi.core.spi primitive does not support this three-wire SPI format
-- so we use a custom three wire spi component.

library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all, ieee.math_real.all;
library ocpi; use ocpi.types.all;

architecture rtl of worker is

  -- Calculate the clock divisor from the fabric clock frequency and the SPI
  -- clock frequency
  constant CP_CLK_FREQ_HZ_p_real : real := real(to_integer(CP_CLK_FREQ_HZ_p));
  constant SPI_CLK_FREQ_HZ_p_real : real := real(to_integer(SPI_CLK_FREQ_HZ_p));
  constant CLOCK_DIVISOR : natural := natural(ceil(CP_CLK_FREQ_HZ_p_real / SPI_CLK_FREQ_HZ_p_real));

  signal raw_in  : ocpi.wci.raw_prop_out_t;
  signal raw_out : ocpi.wci.raw_prop_in_t;

  signal sen        : std_logic;
  signal done       : std_logic;
  signal wdata      : std_logic_vector(7 downto 0);
  signal rdata      : std_logic_vector(7 downto 0);
  signal addr       : std_logic_vector(6 downto 0);
  signal index      : integer range 0 to 1;

begin

  -- Mux the slave enables based on who has won arbitration
  DB0_DAC_SEN <= sen when index = 0 else '1';
  DB1_DAC_SEN <= sen when index = 1 else '1';

  DB_DAC_RESET <= not raw_in.reset;

  raw_out.present   <= (others => raw_in.present);
  raw_out.raw.done  <= done;
  raw_out.raw.error <= '0';

  -- The read data needs to be placed in the correct byte of the word based on
  -- the lsbs of the address - but the other bytes are don't care so we can just
  -- replicate the data 4 times
  raw_out.raw.data  <= rdata & rdata & rdata & rdata;

  -- Select the correct byte of the write data
  with raw_in.raw.address(1 downto 0) select
    wdata <=
      raw_in.raw.data(7  downto 0)  when "00",
      raw_in.raw.data(15 downto 8)  when "01",
      raw_in.raw.data(23 downto 16) when "10",
      raw_in.raw.data(31 downto 24) when others; -- aka "11"

  -- Select the address
  addr <= std_logic_vector(raw_in.raw.address(6 downto 0));

  -- Use the generic raw property arbiter between the two DACs
  arb : ocpi.wci.raw_arb
    generic map(
      nusers => 2
    )
    port map(
      clk         => wci_clk,
      reset       => wci_reset,
      from_users  => rawprops_in,
      to_users    => rawprops_out,
      from_device => raw_out,
      to_device   => raw_in,
      index       => index
    );

  -- Drive SPI from the arbitrated raw interface
  spi : entity work.three_wire_spi
    generic map(
      clock_divisor => CLOCK_DIVISOR,
      addr_width    => 7,
      data_width    => 8
    )
    port map(
      clk      => wci_clk,
      reset    => raw_in.reset,

      renable  => raw_in.raw.is_read,
      wenable  => raw_in.raw.is_write,
      done     => done,

      addr     => addr,
      wdata    => wdata,
      rdata    => rdata,

      sclk     => DB_DAC_SCLK,
      sen      => sen,
      sdata_i  => DB_DAC_MOSI_I,
      sdata_o  => DB_DAC_MOSI_O,
      sdata_oe => DB_DAC_MOSI_OE
    );

end rtl;
