-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This SPI bus is shared between several devices:
--
--   - An ADS62P48 ADC
--       - 8 address bits followed by 8 data bits
--       - MSB first, data captured on clock falling edge
--       - Supports readback, but it is insane
--
--   - A UBX160 CPLD
--       - 32 data bits
--       - MSB first, data captured on clock rising edge
--       - Readback may not implemented?
--
--   - Four MAX2870 PLLs
--       - 29 data bits followed by 3 address bits
--       - MSB first, data captured on clock rising edge
--       - Supports readback, but it is insane
--
-- Because of the different transaction formats that need to be supported the
-- basic ocpi.core.spi primitive cannot be used. Instead a custom spi component
-- is used that takes the data width as a port.

library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all, ieee.math_real.all;
library ocpi; use ocpi.types.all;

architecture rtl of worker is

  -- Calculate the clock divisor from the fabric clock frequency and the SPI
  -- clock frequency
  constant CP_CLK_FREQ_HZ_p_real : real := real(to_integer(CP_CLK_FREQ_HZ_p));
  constant SPI_CLK_FREQ_HZ_p_real : real := real(to_integer(SPI_CLK_FREQ_HZ_p));
  constant CLOCK_DIVISOR : natural := natural(ceil(CP_CLK_FREQ_HZ_p_real / SPI_CLK_FREQ_HZ_p_real));

  signal raw_in        : ocpi.wci.raw_prop_out_t;
  signal raw_out       : ocpi.wci.raw_prop_in_t;
  signal raw_arb_reset : std_logic;

  signal sen        : std_logic;
  signal done       : std_logic;
  signal wdata_byte : std_logic_vector(7 downto 0);
  signal data       : std_logic_vector(31 downto 0);
  signal data_width : unsigned(4 downto 0);
  signal invert_clk : std_logic;
  signal index      : integer range 0 to 5;

  -- spi mux address indexes: MUST MATCH PLATFORM WIRING
  constant index_CPLD  : integer := 1;
  constant index_RXLO1 : integer := 2;
  constant index_RXLO2 : integer := 3;
  constant index_TXLO1 : integer := 4;
  constant index_TXLO2 : integer := 5;

  -- spi mux address values: matches CPLD code
  constant SPI_ADDR_TXLO1 : unsigned(2 downto 0) := to_unsigned(0, 3);
  constant SPI_ADDR_TXLO2 : unsigned(2 downto 0) := to_unsigned(1, 3);
  constant SPI_ADDR_RXLO1 : unsigned(2 downto 0) := to_unsigned(2, 3);
  constant SPI_ADDR_RXLO2 : unsigned(2 downto 0) := to_unsigned(3, 3);
  constant SPI_ADDR_CPLD  : unsigned(2 downto 0) := to_unsigned(4, 3);
  constant SPI_ADDR_RESET : unsigned(2 downto 0) := to_unsigned(7, 3);
  signal spi_addr : unsigned(2 downto 0);

  -- GPIO buffers
  signal buf_cFE_RXATT     : std_logic_vector(5 downto 0);
  signal buf_MCTRL_RESETn  : std_logic;
  signal buf_MCTRL_RX2_EN  : std_logic;
  signal buf_MCTRL_ATR_TX  : std_logic;
  signal buf_MCTRL_ATR_RX  : std_logic;
  signal buf_cFE_TXATT     : std_logic_vector(5 downto 0);

begin

  -- Mux the slave enables based on who has won arbitration
  ADC_SEN <= sen when index  = 0 else '1';
  TX_SEN  <= sen when index /= 0 else '1';

  -- SPI address selection via the CPLD
  process (index)
  begin
    case index is
      when index_CPLD =>
        spi_addr <= SPI_ADDR_CPLD;
      when index_RXLO1 =>
        spi_addr <= SPI_ADDR_RXLO1;
      when index_RXLO2 =>
        spi_addr <= SPI_ADDR_RXLO2;
      when index_TXLO1 =>
        spi_addr <= SPI_ADDR_TXLO1;
      when index_TXLO2 =>
        spi_addr <= SPI_ADDR_TXLO2;
      when others => -- includes index 0: ADS62P48 which bypasses CPLD
        spi_addr <= SPI_ADDR_RESET;
    end case;
  end process;
  MCTRL_SPI_ADR <= std_logic_vector(spi_addr);

  -- We don't make use of the low speed ADCs and DACs or the daugterboard RX SPI
  RX_SEN       <= '1';
  RX_LSADC_SEN <= '1';
  RX_LSDAC_SEN <= '1';
  TX_LSADC_SEN <= '1';
  TX_LSDAC_SEN <= '1';

  ADC_RESET <= rawprops_in(0).reset;

  raw_arb_reset_proc : process(rawprops_in) is begin
    -- Enable arbitration if any of the presnet rawprops_in workers
    -- are not in reset.
    raw_arb_reset <= '1';
    for i in rawprops_in'range loop
      if rawprops_in(i).present = '1' and rawprops_in(i).reset = '0' then
        raw_arb_reset <= '0';
      end if;
    end loop;
  end process;

  -- Use the generic raw property arbiter between: ADC, 4x MAX workers, CPLD
  arb : ocpi.wci.raw_arb
    generic map(
      nusers => 6
    )
    port map(
      clk         => ctl_in.clk,
      reset       => raw_arb_reset,
      from_users  => rawprops_in,
      to_users    => rawprops_out,
      from_device => raw_out,
      to_device   => raw_in,
      index       => index
    );

  spi : entity work.spi
    generic map(
      clock_divisor => CLOCK_DIVISOR
    )
    port map(
      clk        => ctl_in.clk,
      reset      => raw_in.reset,

      enable     => raw_in.raw.is_write,
      done       => done,

      data       => data,
      data_width => data_width,
      invert_clk => invert_clk,

      sclk       => SCLK,
      sen        => sen,
      mosi       => MOSI
    );

  raw_out.present   <= (others => raw_in.present);

  -- NOTE: Reads are not supported, so just return 0 on read
  raw_out.raw.done  <= done or raw_in.raw.is_read;
  raw_out.raw.error <= '0';
  raw_out.raw.data  <= (others => '0');

  with raw_in.raw.address(1 downto 0) select
    wdata_byte <=
      raw_in.raw.data(7  downto 0)  when "00",
      raw_in.raw.data(15 downto 8)  when "01",
      raw_in.raw.data(23 downto 16) when "10",
      raw_in.raw.data(31 downto 24) when others; -- aka "11"

  process(index, raw_in.raw.address, raw_in.raw.data, wdata_byte) is begin
    case index is

      when 0 =>
        -- ADS62P48: 8 address bits followed by 8 data bits, data latched on falling edge
        data(31 downto 24) <= std_logic_vector(raw_in.raw.address(7 downto 0));
        data(23 downto 16) <= wdata_byte;
        data(15 downto 0)  <= x"0000";
        data_width         <= to_unsigned(15, data_width'length);
        invert_clk         <= '1';

      when others =>
        -- CPLD: 32 data bits, data latched on rising edge
        -- MAX2871: 29 data bits followed by 3 address bits (included in data from rawprops); data latched on rising edge
        data(31 downto 0)  <= raw_in.raw.data(31 downto 0);
        data_width         <= to_unsigned(31, data_width'length);
        invert_clk         <= '0';

    end case;
  end process;

  -- GPIO properties
  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if its(ctl_in.reset) then
        buf_cFE_RXATT    <= (others => '0');
        buf_MCTRL_RESETn <= '0';
        buf_MCTRL_RX2_EN <= '1';
        buf_MCTRL_ATR_TX <= '1';
        buf_MCTRL_ATR_RX <= '1';
        buf_cFE_TXATT    <= (others => '0');
      else
        if its(props_in.CPLD_RST_N_written) then
          buf_MCTRL_RESETn <= props_in.CPLD_RST_N(0);
        end if;
        if its(props_in.RX_GAIN_written) then
          buf_cFE_RXATT <= from_uchar(props_in.RX_GAIN)(5 downto 0);
        end if;
        if its(props_in.TX_GAIN_written) then
          buf_cFE_TXATT <= from_uchar(props_in.TX_GAIN)(5 downto 0);
        end if;
        if its(props_in.RX_ANT_written) then
          buf_MCTRL_RX2_EN <= props_in.RX_ANT(0);
        end if;
        if its(props_in.TX_EN_N_written) then
          buf_MCTRL_ATR_TX <= props_in.TX_EN_N(0);
        end if;
        if its(props_in.RX_EN_N_written) then
          buf_MCTRL_ATR_RX <= props_in.RX_EN_N(0);
        end if;
      end if;
    end if;
  end process;

  -- GPIO reads
  props_out.RX_LO_LOCKED <= (0 => LOCKD_RX, others => '0');
  props_out.TX_LO_LOCKED <= (0 => LOCKD_TX, others => '0');
  props_out.RXLO1_SYNC   <= (0 => CRXLO1_MUX, others => '0');
  props_out.RXLO2_SYNC   <= (0 => CRXLO2_MUX, others => '0');
  props_out.TXLO1_SYNC   <= (0 => CTXLO1_MUX, others => '0');
  props_out.TXLO2_SYNC   <= (0 => CTXLO2_MUX, others => '0');
  props_out.CPLD_RST_N   <= (0 => buf_MCTRL_RESETn, others => '0');
  props_out.RX_GAIN      <= to_uchar("00" & buf_cFE_RXATT);
  props_out.TX_GAIN      <= to_uchar("00" & buf_cFE_TXATT);
  props_out.RX_ANT       <= (0 => buf_MCTRL_RX2_EN, others => '0');
  props_out.TX_EN_N      <= (0 => buf_MCTRL_ATR_TX, others => '0');
  props_out.RX_EN_N      <= (0 => buf_MCTRL_ATR_RX, others => '0');

  -- GPIO writes
  cFE_RXATT    <= buf_cFE_RXATT;
  MCTRL_RESETn <= buf_MCTRL_RESETn;
  MCTRL_RX2_EN <= buf_MCTRL_RX2_EN;
  MCTRL_ATR_TX <= buf_MCTRL_ATR_TX;
  MCTRL_ATR_RX <= buf_MCTRL_ATR_RX;
  cFE_TXATT    <= buf_cFE_TXATT;

  -- LEDs
  process(props_in)
  begin

    if props_in.TX_EN_N(0) = '0' then
      -- transmit enabled (LED on)
      LED_TXRX_TX <= '0';
    else
      -- transmit disabled (LED off)
      LED_TXRX_TX <= '1';
    end if;

    if props_in.RX_EN_N(0) = '0' then

      -- receive enabled (LED on)
      if props_in.RX_ANT(0) = '1' then
        LED_RX <= '0';
        LED_TXRX_RX <= '1';
      else
        LED_RX <= '1';
        LED_TXRX_RX <= '0';
      end if;

    else
      -- receive disabled (LED off)
      LED_RX <= '1';
      LED_TXRX_RX <= '1';
    end if;

  end process;

end rtl;
