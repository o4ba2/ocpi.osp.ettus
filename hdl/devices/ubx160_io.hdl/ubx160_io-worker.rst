.. ubx160_io HDL worker


.. _ubx160_io-HDL-worker:


``ubx160_io`` HDL Worker
========================
Ettus x310 UBX160 daughter board IO worker.

Detail
------
This worker provides control for the following sub-devices

* ads62p48.hdl (ADC)
* ubx160_cpld.hdl (UBX160 daughter board CPLD) 
* max2871.hdl (Frequency Synthesizer / VCO - RXLO1)
* max2871.hdl (Frequency Synthesizer / VCO - RXLO2)
* max2871.hdl (Frequency Synthesizer / VCO - TXLO1)
* max2871.hdl (Frequency Synthesizer / VCO - TXLO2)

It drives the a number of GPIO signals, LEDs and SPI buses used for controlling the hardware 
on or associated with the x310 daughter board. Two instances of this worker (one for each daughter board) 
are used by the x310 platform.The worker connects to the Raw Property ports of its sub-devices, and uses 
these to drive the GPIO and SPI signals.
This worker is controlled by the drc_x310.rcc worker. It is not necessary for the application to
access the worker's properties.

The following LEDs are driven by this worker

+----------------+----------------+-------------------+-------------------------------------+
| LED            | Signal         | Controlling       | Usage                               |
|                |                | Property          |                                     |
+================+================+===================+=====================================+
| TXRX_TX        | LED_TXRX_TX    | ubx160_io.TX_EN_N | Front panel RF TX/RX LED (RED)      |
+----------------+----------------+-------------------+-------------------------------------+
| TXRX_RX        | LED_TXRX_RX    | ubx160_io.RX_EN_N | Front panel RF TX/RX LED (GREEN)    |
+----------------+----------------+-------------------+-------------------------------------+
| RX             | LED_RX         | ubx160_io.RX_ANT  | Front panel RF RX2 LED (GREEN)      |
+----------------+----------------+-------------------+-------------------------------------+

The RX front panel LED is illuminated by the Digital-radio-controller when the RX path is 
configured to use the RX2 input (SMA connector). The LED will be off when the RX2 input is not being used.

The TXRX front panel LED will be red when the TXRX (SMA connector) is actively transmitting.
The LED will be green when the TXRX (SMA connector) is operating in receive mode. 
The LED will be off when the TXRX port is not being used.
Setting the TX/RX port to be an input or output is controlled by the RX_ANT (receive antenna) property. 

The following input GPIO signals are used to set the properties defined in the table below

+----------------+-----------+--------------+--------------------------------+
| GPIO           | Direction | Property     | Usage                          |
+================+===========+==============+================================+
| LOCKD_RX       | input     | RX_LO_LOCKED | RXLO lock detect. Active high  |
+----------------+-----------+--------------+--------------------------------+
| LOCKD_TX       | input     | TX_LO_LOCKED | TXLO lock detect. Active high  |
+----------------+-----------+--------------+--------------------------------+
| RXLO1_SYNC     | input     | RXLO1_SYNC   | MAX2871 MUXOUT for RXLO1       |
|                |           |              | set by the DRC to provide DLD  |
|                |           |              | digital lock detect            |
+----------------+-----------+--------------+--------------------------------+
| RXLO2_SYNC     | input     | RXLO2_SYNC   | MAX2871 MUXOUT for RXLO2       |
|                |           |              | set by the DRC to provide DLD  |
|                |           |              | digital lock detect            |
+----------------+-----------+--------------+--------------------------------+
| TXLO1_SYNC     | input     | TXLO1_SYNC   | MAX2871 MUXOUT for TXLO1       |
|                |           |              | set by the DRC to provide DLD  |
|                |           |              | digital lock detect            |
+----------------+-----------+--------------+--------------------------------+
| TXLO2_SYNC     | input     | TXLO2_SYNC   | MAX2871 MUXOUT for TXLO2       |
|                |           |              | set by the DRC to provide DLD  |
|                |           |              | digital lock detect            |
+----------------+-----------+--------------+--------------------------------+

The following GPIO signals are driven by this worker

+----------------+-----------+--------------+--------------------------------+
| GPIO           | Direction | Property     | Usage                          |
+================+===========+==============+================================+
| ADC_RESET      | output    |              | ADS62P4 (ADC) device reset     |
+----------------+-----------+--------------+--------------------------------+
| MCTRL_RESETn   | output    | CPLD_RST_N   | Active low reset of UBX160     |
|                |           |              | daughter board CPLD            |
+----------------+-----------+--------------+--------------------------------+
| MCTRL_RX2_EN   | output    | RX_ANT       | Selects the receive antenna    |
|                |           |              | 0 - RX2, 1 - TXRX              |
+----------------+-----------+--------------+--------------------------------+
| MCTRL_ATR_TX   | output    | TX_EN_N      | Active low, enables transmit   |
+----------------+-----------+--------------+--------------------------------+
| MCTRL_ATR_RX   | output    | RX_EN_N      | Active low, enables receive    |
+----------------+-----------+--------------+--------------------------------+
| cFE_TXATT      | output    | TX_GAIN      | 6-bit transmit gain control    |
+----------------+-----------+--------------+--------------------------------+
| cFE_RXATT      | output    | RX_GAIN      | 6-bit receive gain control     |
+----------------+-----------+--------------+--------------------------------+
| MCTRL_SPI_ADR  | output    |              | CPLD SPI address selection     |
|                |           |              | The address selects between    |
|                |           |              | CPLD, RXLO1, RXLO2, TXLO1      |
|                |           |              | and TXLO2 devices depending    |
|                |           |              | on the property being written  |
+----------------+-----------+--------------+--------------------------------+
| ADC_SEN        | output    |              | ads62p48 slave enable          |
|                |           |              | selects between                |
|                |           |              | ads62p48 and db SPI devices    |
+----------------+-----------+--------------+--------------------------------+
| TX_SEN         | output    |              | daughter board MCTRL-SEN signal|
|                |           |              | slave enable, selects between  |
|                |           |              | ads62p48 and db SPI devices    |
+----------------+-----------+--------------+--------------------------------+
| RX_SEN         | output    |              | daughter board SEN_RX signal   |
|                |           |              | set to '1' not used            |
+----------------+-----------+--------------+--------------------------------+
| RX_LSADC_SEN   | output    |              | low speed ADC slave enable     |
|                |           |              | set to '1' not used            |
+----------------+-----------+--------------+--------------------------------+
| RX_LSDAC_SEN   | output    |              | low speed DAC slave enable     |
|                |           |              | set to '1' not used            |
+----------------+-----------+--------------+--------------------------------+
| TX_LSADC_SEN   | output    |              | low speed ADC slave enable     |
|                |           |              | set to '1' not used            |
+----------------+-----------+--------------+--------------------------------+
| TX_LSDAC_SEN   | output    |              | low speed DAC slave enable     |
|                |           |              | set to '1' not used            |
+----------------+-----------+--------------+--------------------------------+

TX_GAIN controls an HMC624A Digital Attenuator. Attenuation is in 0.5 dB steps to a maximum of 31.5 dB.
RX_GAIN controls an HMC624A Digital Attenuator. Attenuation is in 0.5 dB steps to a maximum of 31.5 dB.
The use of the motherboard fitted low speed ADC and DACs are not supported by this worker

The ubx160_io.hdl worker drives a single SPI bus. Note that the RX_LSADC, TX_LSADC. RX_MISO, TX_MISO
signals are not driven.

The following SPI signals are driven by this worker

+----------------+-----------+--------------------------------+
| Signal         | Direction | Usage                          |
+================+===========+================================+
| DBx_SCLK       | output    | SPI clock, 1 MHz               |
+----------------+-----------+--------------------------------+
| DBx_MOSI       | output    | SPI master out                 |
+----------------+-----------+--------------------------------+
| DBx_ADC_SEN    | output    | Select ADC slave               |
+----------------+-----------+--------------------------------+
| DBx_TX_SEN     | output    | Select other slave             |
+----------------+-----------+--------------------------------+

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
