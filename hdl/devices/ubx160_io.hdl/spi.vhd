-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;

entity spi is
  generic (
    clock_divisor : positive := 16
  );
  port (
    clk           : in  std_logic;
    reset         : in  std_logic;

    enable        : in  std_logic;
    done          : out std_logic;

    data          : in  std_logic_vector(31 downto 0);
    data_width    : in  unsigned(4 downto 0);  -- Width of the data in bits, minus 1
    invert_clk    : in  std_logic;  -- If true then clk starts high and data is latched on falling edge

    sclk          : out std_logic;
    sen           : out std_logic;
    mosi          : out std_logic
  );
end entity spi;

architecture rtl of spi is

  type   state_t is (idle_e, starting_e, busy_e, done_e);
  signal state_r      : state_t := idle_e;
  signal clk_count_r  : natural range 0 to clock_divisor - 1 := 0;
  signal bit_count_r  : natural range 0 to data'high + 1 := 0;
  signal data_sr      : std_logic_vector(31 downto 0) := (others => '0');
  signal sclk_r       : std_logic := '0';

begin

  sclk <= sclk_r xor invert_clk;
  mosi <= data_sr(31);
  sen  <= '0' when state_r = starting_e or state_r = busy_e else '1';
  done <= '1' when state_r = done_e else '0';

  process (clk) is begin
    if rising_edge(clk) then
      if reset = '1' then
        state_r <= idle_e;
        sclk_r  <= '0';
        data_sr <= (others => '0');
      else
        case state_r is

          when idle_e =>
            if enable = '1' then
              state_r <= starting_e;
            end if;

          when starting_e =>
            state_r     <= busy_e;
            data_sr     <= data;
            clk_count_r <= 0;
            bit_count_r <= 0;

          when busy_e =>
            if clk_count_r = clock_divisor - 1 then
              -- End of bit cycle
              clk_count_r <= 0;
              bit_count_r <= bit_count_r + 1;

              -- Shift out a bit
              data_sr <= data_sr(30 downto 0) & '0';
              sclk_r  <= '0';

              if bit_count_r = natural(to_integer(data_width)) then
                -- End of access
                state_r <= done_e;
              end if;
            else
              clk_count_r <= clk_count_r + 1;

              if clk_count_r = clock_divisor/2 - 1 then
                -- Mid-cycle
                sclk_r <= '1';
              end if;
            end if;

          when done_e =>
            state_r <= idle_e;

        end case;
      end if;
    end if;
  end process;

end rtl;
