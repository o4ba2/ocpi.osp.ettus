.. dac_spi documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _dac_spi:


DAC SPI Bus (``dac_spi``)
=========================
Ettus USRP X310 DAC SPI device worker.

Design
------
Provides access to the hardware registers of Daughter board 0 and 1 AD9146 DACs.

Interface
---------
The interface of the dac_spi.hdl worker is defined in ``./dac_spi.xml`` 

.. .. literalinclude:: ../specs/dac_spi-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``./dac_spi.xml``
This worker supports the AD9146 worker which provides named access to the hardware registers of the AD9146 DAC.
The properties are used by the drc_x310.rcc and do not need to be set by an application using the drc_x310.

.. .. ocpi_documentation_properties::

..    property_name: Skeleton outline: List any additional text for properties, which will be included in addition to the description field in the component specification XML.

Ports
~~~~~
The dac_spi.hdl worker does not have any component ports. 

.. .. ocpi_documentation_ports::

..    input: Primary input samples port.
..    output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../dac_spi.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ocpi.wci.raw_arb

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``dac_spi`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
