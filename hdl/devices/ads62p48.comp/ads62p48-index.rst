.. ads62p48 documentation

.. _ads62p48:


ADS62P48 ADC (``ads62p48``)
===========================
Device worker for the ADS62P48 Dual-Channel, 14-Bit, 210-MSPS ADC.

Design
------
This device worker is used by the Ettus X310 Digital-radio-controller (DRC) for
control and configuration of the two ADS62P48 chips on the X310 motherboard.

The device worker provides several functions:

* Expose the ADS62P48 register map as raw properties, which are delegated to a
  separate SPI worker over a rawprops interface.

* Capture data from the ADS62P48 and stream it over a ``qadc-16-signals``
  devsignals interface.

* Allow the data stream to be enabled/disabled by the DRC.

* Allow the DRC to perform delay line calibration for the high speed LVDS data
  interface.

Interface
---------

The interface is defined in-line in the OWD.

Opcode handling
~~~~~~~~~~~~~~~

This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~

The ADS62P48 registers are exposed as raw properties, which are defined in
``../specs/ads62p48-properties.xml``.

Additionally, the component is configured and controlled via the following
properties:

* ``idelay_valid``: Delay line value, in the range 0 - 31, for the LVDS data lanes.

  * Type: ``uchar``
  * Access:

    * Parameter: False
    * Writable: True
    * Initial: False
    * Volatile: False
  * Default value: 0

* ``pattern_check``: State of the pattern check circuit.

  * Type: ``uchar``
  * Access:

    * Parameter: False
    * Writable: False
    * Initial: False
    * Volatile: True

  * Default value: 0

* ``enable``: Controls the valid signal of the output data stream.

  * Type: ``bool``
  * Access:

    * Parameter: False
    * Writable: True
    * Initial: False
    * Volatile: False

  * Default value: 0

Ports
~~~~~
.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ads62p48.hdl

Example Application
-------------------
This component is not intended to be used in an application; instead it is
incorporated as part of the DRC slave assembly.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``unisim.vcomponents``

Limitations
-----------
Limitations of ``ads62p48`` are:

 * This worker is to be used only with the drc_x310.rcc (DRC proxy)

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
