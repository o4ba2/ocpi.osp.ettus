-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;
library unisim; use unisim.vcomponents.all;

architecture rtl of worker is

  signal radclk             : std_logic;
  signal dclk_bufds_to_bufg : std_logic;
  signal dclk               : std_logic;
  signal da                 : std_logic_vector(6 downto 0);
  signal delda              : std_logic_vector(6 downto 0);
  signal db                 : std_logic_vector(6 downto 0);
  signal deldb              : std_logic_vector(6 downto 0);
  signal data_i             : std_logic_vector(13 downto 0);
  signal data_q             : std_logic_vector(13 downto 0);
  signal dataq_inv          : std_logic_vector(13 downto 0);
  signal data_i_r           : std_logic_vector(13 downto 0);
  signal data_q_r           : std_logic_vector(13 downto 0);
  signal data_valid         : std_logic;
  signal idelay_value       : std_logic_vector (4 downto 0);

  type pattern_check_state_t is (
    pattern_check_init,     -- Discard initial samples
    pattern_check_sync,     -- Check minimum number of samples
    pattern_check_locked,   -- Valid pattern observed
    pattern_check_failed    -- Pattern error observed
  );

  signal pattern_check_state_r : pattern_check_state_t := pattern_check_failed;
  signal reset_pattern_check   : std_logic;
  signal pattern_check         : std_logic_vector (1 downto 0);
  signal pattern_valid_r       : std_logic;
  signal data_i_rr             : std_logic_vector(13 downto 0);
  signal data_q_rr             : std_logic_vector(13 downto 0);
  signal delta_i               : unsigned(13 downto 0);
  signal delta_q               : unsigned(13 downto 0);

  subtype pattern_check_count_t is integer range 0 to 1023;
  signal pattern_check_count_r : pattern_check_count_t := 0;

begin

  radclk <= rfclk_in.radclk;

  -- Delagate our raw properties to the rawprops interface
  rawprops_out.present <= '1';
  rawprops_out.reset   <= ctl_in.reset;
  rawprops_out.raw     <= props_in.raw;
  props_out.raw        <= rawprops_in.raw;

  dclk_ibufds_inst : IBUFDS
    port map (
      I  => DCLK_P,
      IB => DCLK_N,
      O  => dclk_bufds_to_bufg
    );

  dclk_buf_inst : BUFG
    port map (
      I  => dclk_bufds_to_bufg,
      O  => dclk
    );

  da0_ibufds_inst : IBUFDS
    port map (
      I  => DA0_P,
      IB => DA0_N,
      O  => da(0)
    );

  da1_ibufds_inst : IBUFDS
    port map (
      I  => DA1_P,
      IB => DA1_N,
      O  => da(1)
    );

  da2_ibufds_inst : IBUFDS
    port map (
      I  => DA2_P,
      IB => DA2_N,
      O  => da(2)
    );

  da3_ibufds_inst : IBUFDS
    port map (
      I  => DA3_P,
      IB => DA3_N,
      O  => da(3)
    );

  da4_ibufds_inst : IBUFDS
    port map (
      I  => DA4_P,
      IB => DA4_N,
      O  => da(4)
    );

  da5_ibufds_inst : IBUFDS
    port map (
      I  => DA5_P,
      IB => DA5_N,
      O  => da(5)
    );

  da6_ibufds_inst : IBUFDS
    port map (
      I  => DA6_P,
      IB => DA6_N,
      O  => da(6)
    );

  db0_ibufds_inst : IBUFDS
    port map (
      I  => DB0_P,
      IB => DB0_N,
      O  => db(0)
    );

  db1_ibufds_inst : IBUFDS
    port map (
      I  => DB1_P,
      IB => DB1_N,
      O  => db(1)
    );

  db2_ibufds_inst : IBUFDS
    port map (
      I  => DB2_P,
      IB => DB2_N,
      O  => db(2)
    );

  db3_ibufds_inst : IBUFDS
    port map (
      I  => DB3_P,
      IB => DB3_N,
      O  => db(3)
    );

  db4_ibufds_inst : IBUFDS
    port map (
      I  => DB4_P,
      IB => DB4_N,
      O  => db(4)
    );

  db5_ibufds_inst : IBUFDS
    port map (
      I  => DB5_P,
      IB => DB5_N,
      O  => db(5)
    );

  db6_ibufds_inst : IBUFDS
    port map (
      I  => DB6_P,
      IB => DB6_N,
      O  => db(6)
    );

  idelay_value <= std_logic_vector(props_in.idelay_value(4 downto 0));

  gen_idelay: for geni in 0 to 6 generate

    da_idelaye2_inst : IDELAYE2
      generic map (
        CINVCTRL_SEL          => "FALSE",
        DELAY_SRC             => "IDATAIN",
        HIGH_PERFORMANCE_MODE => "TRUE",
        IDELAY_TYPE           => "VAR_LOAD",
        IDELAY_VALUE          => 16,
        PIPE_SEL              => "FALSE",
        REFCLK_FREQUENCY      => 200.0,
        SIGNAL_PATTERN        => "DATA"
      ) port map (
        -- Data input and delayed output
        DATAIN         => '0',
        IDATAIN        => da(geni),
        DATAOUT        => delda(geni),

        -- Delay value control
        C              => ctl_in.clk,   -- Clock input
        LD             => '1',          -- Load IDELAY_VALUE input
        CE             => '0',          -- Active high enable increment/decrement input
        INC            => '0',          -- Increment / Decrement tap delay input
        CINVCTRL       => '0',          -- Dynamic clock inversion input
        CNTVALUEIN     => idelay_value, -- Counter value input
        CNTVALUEOUT    => open,         -- Counter value output
        LDPIPEEN       => '0',          -- Enable PIPELINE register to load data input
        REGRST         => '0'           -- Reset for the pipeline register. Only used in VAR_LOAD_PIPE mode.
      );

    db_idelaye2_inst : IDELAYE2
      generic map (
        CINVCTRL_SEL          => "FALSE",
        DELAY_SRC             => "IDATAIN",
        HIGH_PERFORMANCE_MODE => "TRUE",
        IDELAY_TYPE           => "VAR_LOAD",
        IDELAY_VALUE          => 16,
        PIPE_SEL              => "FALSE",
        REFCLK_FREQUENCY      => 200.0,
        SIGNAL_PATTERN        => "DATA"
      ) port map (
        -- Data input and delayed output
        DATAIN         => '0',
        IDATAIN        => db(geni),
        DATAOUT        => deldb(geni),

        -- Delay value control
        C              => ctl_in.clk,   -- Clock input
        LD             => '1',          -- Load IDELAY_VALUE input
        CE             => '0',          -- Active high enable increment/decrement input
        INC            => '0',          -- Increment / Decrement tap delay input
        CINVCTRL       => '0',          -- Dynamic clock inversion input
        CNTVALUEIN     => idelay_value, -- Counter value input
        CNTVALUEOUT    => open,         -- Counter value output
        LDPIPEEN       => '0',          -- Enable PIPELINE register to load data input
        REGRST         => '0'           -- Reset for the pipeline register. Only used in VAR_LOAD_PIPE mode.
      );

    da_iddr_inst : IDDR
      generic map (
        DDR_CLK_EDGE   => "SAME_EDGE_PIPELINED"
      ) port map (
        C              => dclk,
        CE             => '1',
        D              => delda(geni),
        R              => '0',
        S              => '0',
        Q1             => data_i(2 * geni),
        Q2             => data_i(2 * geni + 1)
      );

    db_iddr_inst : IDDR
      generic map (
        DDR_CLK_EDGE   => "SAME_EDGE_PIPELINED"
      ) port map (
        C              => dclk,
        CE             => '1',
        D              => deldb(geni),
        R              => '0',
        S              => '0',
        Q1             => data_q(2 * geni),
        Q2             => data_q(2 * geni + 1)
      );

  end generate gen_idelay;

  -- Synchronise back to radclk domain
  data_isync_inst : xpm_cdc_array_single
    generic map (
      DEST_SYNC_FF  => 2,
      SRC_INPUT_REG => 0,
      WIDTH         => 14
    ) port map (
      src_clk  => dclk,
      src_in   => data_i,

      dest_clk => radclk,
      dest_out => data_i_r
    );

  dataqsync_inst : xpm_cdc_array_single
    generic map (
      DEST_SYNC_FF  => 2,
      SRC_INPUT_REG => 0,
      WIDTH         => 14
    ) port map (
      src_clk  => dclk,
      src_in   => data_q,

      dest_clk => radclk,
      dest_out => data_q_r
    );

  xpm_cdc_single_inst : xpm_cdc_single
    generic map (
      DEST_SYNC_FF  => 2,
      SRC_INPUT_REG => 1
    ) port map (
      src_clk  => ctl_in.clk,
      src_in   => props_in.enable,

      dest_clk => radclk,
      dest_out => data_valid
    );

  -- Analogue diff pairs on I side are inverted for layout reasons (at the
  -- connector between the UBX160 and the X310), but the data diff pairs are
  -- all swapped (between the output of the ADS and the input of the FPGA).
  -- So I is double negative and thus is unchanged.  Q must be inverted.
  -- This is what the Ettus UHD code does.
  dataq_inv <= not data_q_r;

  dev_out.clk    <= radclk;
  dev_out.valid  <= data_valid;
  dev_out.data_i <= data_i_r & "00";
  dev_out.data_q <= dataq_inv & "00";

  -- Pattern checker ----------------------------------------------------------

  reset_pattern_check_cdc_inst : xpm_cdc_pulse
    generic map (
      DEST_SYNC_FF => 2,
      REG_OUTPUT => 1,
      RST_USED => 0
    )
    port map (
      src_clk => ctl_in.clk,
      src_pulse => props_in.idelay_value_written,

      dest_clk => radclk,
      dest_pulse => reset_pattern_check
    );

  -- Transfer the pattern checker state to the control clock domain so that we
  -- can expose it as a property
  --
  -- NOTE: This synchronises each bit separately, which does allow for the
  -- possibility of glitches being observed. However, as long as we are careful
  -- to check for either the failed or locked states in the C++ we are OK, as
  -- there are no glitches that can be mistakenly observed as one of these
  -- states
  pattern_check_cdc_inst : xpm_cdc_array_single
    generic map (
      DEST_SYNC_FF  => 2,
      SRC_INPUT_REG => 1,
      WIDTH         => 2
    ) port map (
      src_clk  => radclk,
      src_in   => std_logic_vector(to_unsigned(pattern_check_state_t'pos(pattern_check_state_r), 2)),

      dest_clk => ctl_in.clk,
      dest_out => pattern_check
    );

  props_out.pattern_check(1 downto 0) <= unsigned(pattern_check);
  props_out.pattern_check(7 downto 2) <= "000000";

  delta_i <= unsigned(data_i_rr) - unsigned(data_i_r);
  delta_q <= unsigned(data_q_rr) - unsigned(data_q_r);

  -- Check if two consecutive samples are decrementing
  --
  -- NOTE: The ADS62P48 generates an incrementing ramp, but the LVDS lanes are
  -- all inverted which results in a decrementing ramp here.
  process (radclk) begin
    if rising_edge(radclk) then
      data_i_rr <= data_i_r;
      data_q_rr <= data_q_r;
      if delta_i = to_unsigned(1, 14) and delta_q = to_unsigned(1, 14) then
        pattern_valid_r <= '1';
      else
        pattern_valid_r <= '0';
      end if;
    end if;
  end process;

  process (radclk) begin
    if rising_edge(radclk) then
      if reset_pattern_check = '1' then
        pattern_check_state_r <= pattern_check_init;
        pattern_check_count_r <= pattern_check_count_t'high;
      else
        -- Sample counter
        if pattern_check_count_r = 0 then
          pattern_check_count_r <= pattern_check_count_t'high;
        else
          pattern_check_count_r <= pattern_check_count_r - 1;
        end if;

        -- FSM
        case pattern_check_state_r is

          -- Discard the first samples while the delay updates
          when pattern_check_init =>
            if pattern_check_count_r = 0 then
              pattern_check_state_r <= pattern_check_sync;
            end if;

          -- Check that the correct pattern is observed for a period of time
          when pattern_check_sync =>
            if not pattern_valid_r then
              pattern_check_state_r <= pattern_check_failed;
            elsif pattern_check_count_r = 0 then
              pattern_check_state_r <= pattern_check_locked;
            end if;

          -- Success! But keep monitoring for failures
          when pattern_check_locked =>
            if not pattern_valid_r then
              pattern_check_state_r <= pattern_check_failed;
            end if;

          -- Wait until the delay line is updated again
          when pattern_check_failed =>
            null;

        end case;
      end if;
    end if;

  end process;

end rtl;
