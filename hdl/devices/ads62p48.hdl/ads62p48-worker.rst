.. ads62p48 HDL worker


.. _ads62p48-HDL-worker:


``ads62p48`` HDL Worker
=======================
Device worker for the ADS62P48 Dual-Channel, 14-Bit, 210-MSPS ADC.

Detail
------

Registers
^^^^^^^^^

The raw properties corresponding to the ADS62P48 registers are directly
connected to the ``rawprops`` interface. This is required to be connected to a
separate SPI worker that converts reads and writes to the raw properties to SPI
transfers to the device.

Data capture
^^^^^^^^^^^^

The ADS62P48 transfers captured samples to the worker over a 14-bit parallel
LVDS bus with a source synchronous clock. The data is transmitted as centre
aligned double data rate (DDR). This is illustrated by the following diagram
from the datasheet:

.. figure:: ./doc/centre-aligned-ddr.png

The circuit used to capture the data is shown below:

.. figure:: ./doc/ads62p48_capture_logic.svg

For each data lane an IDDR element is used to capture the bits on each edge of
the DCLK. In order to correctly sample the signal clock-to-data delay
calibration is required (see `Calibration`_); to support this the delay value of
the IDELAY elements can be configured via the ``idelay_value`` property and a
pattern checker is implemented to detect correct data reception.

The Q data channel is inverted. This is to compensate for an inversion data
inversion in the PCB layout.

The data is transferred from the source synchronous clock domain to the ``radclk``
clock domain, which is created by a MMCM in the LMK04816 worker. This clock
domain crossing can be done via a simple synchronizer as these two clock
domains are synchronous.

The data is output over a devsignals interface using the ``qadc-16-signals``
protocol. This is intended to interface to a ``data_src_qadc_csts`` device
worker. The data is output on the ``radclk`` clock domain. The valid signal on
this interface is controlled via the ``enable`` property which allows the DRC to
enable the data stream when the full RF path has been configured.

Calibration
^^^^^^^^^^^

To calibrate the clock-to-data delay the DRC should use the following process:

1. Configure the ADC to output the ramp test pattern
2. Sweep the delay from 0 to 31 by writing to the ``idelay_value`` property
3. For each delay value, poll the ``pattern_check`` property until it reaches
   the locked (value of 2) or failed (value of 3) states.
4. Determine the largest span of delay values that resulted in the pattern
   checker reaching the lock state
5. Set the delay to the value in the centre of this span
6. Return the ADC to normal operation

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
