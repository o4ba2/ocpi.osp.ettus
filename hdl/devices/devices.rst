.. Device directory index page


Devices
=======
Available devices

.. toctree::
   :maxdepth: 2
   :glob:

   *.comp/*-index
