-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------
-- GPI
-------------------------------------------------------------------------------
--
-- Description:
--
-- The GPI device worker reads GPIO pins and stores the values in the property
-- mask_data and send the values to its (optional) output port.
--
-- It's (optional) output port uses a protocol that has one opcode and contains
-- data and a mask. It sends data regardless of whether or not the worker
-- downstream is ready, so it is something that cannot be piped directly to
-- software. See gpio_protocol.xml in ocpi.core/specs/gpio_protocol.xml for
-- further details on the protocol.
--
-- For the mask_data property and the output port data, the MSW is a mask and
-- the LSW are the values read from the GPIO pins.
--
-- The mask allows knowledge of which GPIO pins changed since the previous read
-- cycle. The mask is the current data XOR the previous data.
--
-- There are parameter properties that enable/disable build-time inclusion of 3
-- different circuits.
--
-- The USE_DEBOUNCE property enables/disables build-time inclusion of a debounce
-- circuit which debounces input from mechanical switches. There are the CLK_RATE_HZ
-- and DEBOUNCE_TIME_SEC properties used by the GPI device worker to calculate the
-- width of the debounce circuit's counter. The counter width is calculated by
-- using the formula: ceil(log2(CLK_RATE_HZ * DEBOUNCE_TIME_SEC)). It
-- takes 2^counter width + 3 clock cycles until the input is ready at the debounce
-- circuit's output. If the debounce circuit is not enabled, metastability
-- flip flops are used instead. These metastability flip flops have a latency of
-- two clock cycles.
--
-- The EDGE_MODE property enables/disables build-time inclusion of an edge detector
-- circuit. The edge detector detects rising and falling edge of an input
-- on the rising edge of the clock. There is a RISING property that is used to
-- select between using the rising edge output of the edge detector or the falling
-- edge. If RISING is true the rising edge is used otherwise the falling edge is
-- used. The outputs of the edge detector have a latency of one clock cycle. The
-- output of either the debounce circuit or metastability flip flops is fed to
-- the edge detector. If the edge detector is not used, the output of debounce
-- circuit or metastability flip flops is used instead.
--
-- The USE_TOGGLE property enables/disables build-time inclusion of a toggle
-- circuit. The toggle circuit has a register that stores the result of XOR'ing
-- the output of the edge detector, debounce circuit, or metastability flip flops
-- with the current value of the register. If toggle circuit is not used the output
-- of the edge detector, debounce circuit, or metastability flops is used instead.
--
-- The output of these chain of circuits is fed to device worker's output port.
--
-- There is a parameter property called EVENT_MODE. If EVENT_MODE is set to true
-- the output port's valid signal is driven high only when there is a change in
-- the data to be sent to the output port. If it is set to false, the output port
-- valid signal is always high and the data is valid every clock cycle.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all; use ieee.math_real.all; use ieee.std_logic_misc.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library util;
library misc_prims;

architecture rtl of worker is
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant c_COUNTER_WIDTH : positive := positive(ceil(log2(from_double(CLK_RATE_HZ)*(from_double(DEBOUNCE_TIME_PSEC)/1.0E12))));

  -- GPIO buffers
  signal gpio_pin_i      : std_logic_vector(11 downto 0);
  signal gpio_pin_o      : std_logic_vector(11 downto 0);
  signal gpio_pin_t      : std_logic_vector(11 downto 0);

  -- GPIO input
  signal s_gpi_q1        : std_logic_vector(11 downto 0);
  signal s_gpi_q2        : std_logic_vector(11 downto 0);
  signal s_gpi_sync      : std_logic_vector(11 downto 0);
  signal s_gpi_red       : std_logic_vector(11 downto 0);
  signal s_gpi_fed       : std_logic_vector(11 downto 0);
  signal s_gpi_ed        : std_logic_vector(11 downto 0);
  signal s_gpi_pulse     : std_logic_vector(11 downto 0);
  signal s_gpi_output    : std_logic_vector(11 downto 0);
  signal s_gpi_output_r  : std_logic_vector(11 downto 0);

  -- GPIO direction
  signal prop_dir_mask_data_slv : std_logic_vector(31 downto 0);
  signal dir_prop_mask   : std_logic_vector(11 downto 0);
  signal dir_prop_data   : std_logic_vector(11 downto 0);
  signal dir_port_mask   : std_logic_vector(11 downto 0);
  signal dir_port_data   : std_logic_vector(11 downto 0);
  signal dir_mask        : std_logic_vector(11 downto 0);
  signal dir_data        : std_logic_vector(11 downto 0);
  signal dir             : std_logic_vector(11 downto 0);
  signal dir_r           : std_logic_vector(11 downto 0);

  -- GPIO output
  signal prop_gpo_mask_data_slv   : std_logic_vector(31 downto 0);
  signal gpo_prop_mask   : std_logic_vector(11 downto 0);
  signal gpo_prop_data   : std_logic_vector(11 downto 0);
  signal gpo_port_mask   : std_logic_vector(11 downto 0);
  signal gpo_port_data   : std_logic_vector(11 downto 0);
  signal gpo_mask        : std_logic_vector(11 downto 0);
  signal gpo_data        : std_logic_vector(11 downto 0);
  signal gpo             : std_logic_vector(11 downto 0);
  signal gpo_r           : std_logic_vector(11 downto 0);

begin

  -- Properties ---------------------------------------------------------------

  props_out.pin_state(11 downto  0) <= unsigned(gpio_pin_o);
  props_out.pin_state(15 downto 12) <= (others => '0');

  -- GPIO Buffers -------------------------------------------------------------

  buffers_i : for ii in 0 to 11 generate

    iobuf_inst : IOBUF
      port map (
        I  => gpio_pin_o(ii),
        O  => gpio_pin_i(ii),
        T  => gpio_pin_t(ii),
        IO => gpio_pin(ii)
      );

  end generate buffers_i;

  -- GPIO Input ---------------------------------------------------------------

  input_condition_i : for ii in 0 to 11 generate

    -- metastability FFs built into debounce circuit
    debounce_gen : if its(USE_DEBOUNCE) generate
      debouncer : misc_prims.misc_prims.debounce
        generic map (
          COUNTER_WIDTH => c_COUNTER_WIDTH)
        port map (
          CLK    => ctl_in.clk,
          RST    => ctl_in.reset,
          BUTTON => gpio_pin_i(ii),
          RESULT => s_gpi_sync(ii));
    end generate debounce_gen;

    -- insert metastability FFs
    no_debounce_gen : if not(its(USE_DEBOUNCE)) generate
      process (ctl_in.clk)
      begin
        if rising_edge(ctl_in.clk) then
          if ctl_in.reset = '1' then
            s_gpi_q1 (ii) <= '0';
            s_gpi_q2(ii) <= '0';
          else
            s_gpi_q1 (ii) <= gpio_pin_i(ii);
            s_gpi_q2(ii) <= s_gpi_q1 (ii);
          end if;
        end if;
      end process;
    s_gpi_sync(ii) <= s_gpi_q2(ii);
    end generate no_debounce_gen;

    -- edge detect the input signal
    edge_detect_gen : if its(EDGE_MODE) generate
      ed : util.util.edge_detector
        port map (
          clk           => ctl_in.clk,
          reset         => ctl_in.reset,
          din           => s_gpi_sync(ii),
          rising_pulse  => s_gpi_red(ii),
          falling_pulse => s_gpi_fed(ii));
      s_gpi_ed(ii) <= s_gpi_red(ii) when its(RISING) else s_gpi_fed(ii);
    end generate edge_detect_gen;

    -- level mode
    no_edge_detect_gen : if not(its(EDGE_MODE)) generate
      s_gpi_ed(ii) <= s_gpi_sync(ii);
    end generate no_edge_detect_gen;

    -- toggle mode
    toggle_gen : if its(USE_TOGGLE) generate
      process (ctl_in.clk)
      begin
        if rising_edge(ctl_in.clk) then
          if ctl_in.reset = '1' then
            s_gpi_pulse(ii) <= '0';
          else
            s_gpi_pulse(ii) <= s_gpi_pulse(ii) xor s_gpi_ed(ii);
          end if;
        end if;
      end process;
      s_gpi_output(ii) <= s_gpi_pulse(ii);
    end generate toggle_gen;

    -- no toggle needed
    no_toggle_gen : if not(its(USE_TOGGLE)) generate
      s_gpi_output(ii) <= s_gpi_ed(ii);
    end generate no_toggle_gen;

    process (ctl_in.clk)
    begin
      if rising_edge(ctl_in.clk) then
        s_gpi_output_r(ii) <= s_gpi_output(ii);
      end if;
    end process;

    end generate input_condition_i;

    output_condition_i : for ii in 0 to 11 generate
      out_out.data(ii)    <= s_gpi_output(ii);
      -- The MSW is xor of old/new data
      out_out.data(ii+16) <= s_gpi_output_r(ii) xor s_gpi_output(ii);

      props_out.gpi_data(ii) <= s_gpi_output(ii);
    end generate output_condition_i;

    out_out.data(31 downto 28) <= (others=>'0');
    out_out.data(15 downto 12) <= (others=>'0');
    props_out.gpi_data(15 downto 12) <= (others=>'0');

  -- Event mode
  -- Drive out_out.valid only when there has been change in the detected signal
  event_mode_gen : if its(EVENT_MODE) generate
    out_out.valid <= or_reduce(s_gpi_output xor s_gpi_output_r);
  end generate event_mode_gen;

  -- Drive out_out.valid high to send output data every clock cycle
  no_event_mode_gen : if not(its(EVENT_MODE)) generate
    out_out.valid <= '1';
  end generate no_event_mode_gen;

  -- GPIO Direction -----------------------------------------------------------
  -- Data and mask parts as per input and output ports.
  -- Data bit = 1 means port is output
  -- Data bit = 0 means port is input

  dir_out.take <= dir_in.valid;

  -- Mix of Property and Data Port to control direction.  The property and port masks
  -- are OR'ed together and the data are OR'ed together.  Thus if the port and 
  -- property drive at the same time but with opposite values the high value will
  -- take precedence.

  -- Use property when the propery written line is strobed
  prop_dir_mask_data_slv <= std_logic_vector(props_in.direction_mask_data);
  dir_prop_mask <= prop_dir_mask_data_slv(27 downto 16) when its(props_in.direction_mask_data_written) else (others => '0');
  dir_prop_data <= prop_dir_mask_data_slv(11 downto 0) and dir_prop_mask;

  -- Use the port if the data is valid
  dir_port_mask <= dir_in.data(27 downto 16) when (dir_in.valid = '1') else (others => '0');
  dir_port_data <= dir_in.data(11 downto 0) and dir_port_mask;

  -- Combine
  dir_mask <= dir_prop_mask or dir_port_mask;
  dir_data <= dir_prop_data or dir_port_data;
  dir      <= (dir_r and not dir_mask) or (dir_data and dir_mask);

  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        dir_r <= (others => '0');
      else
        dir_r <= dir;
      end if;
    end if;
  end process;

  gpio_pin_t <= not dir_r;

  -- GPIO Output --------------------------------------------------------------

  in_out.take <= in_in.valid;

  -- Mix of Property and Data Port to control GPIO and drive props_out.gpo_mask_data
  -- The property and port masks are OR'ed together and the data are OR'ed together
  -- Thus if the port and property drive the same GPIO at the same time but the opposite
  -- values the high value will take precedence.

  -- Use gpo_prop_mask when the gpo_mask_enable(0) bit is high and the
  -- property written line is strobed
  prop_gpo_mask_data_slv   <= std_logic_vector(props_in.gpo_mask_data);
  gpo_prop_mask <= prop_gpo_mask_data_slv(27 downto 16) when (its(props_in.gpo_mask_enable(0)) and its(props_in.gpo_mask_data_written)) else (others => '0');
  gpo_prop_data <= prop_gpo_mask_data_slv(11 downto 0) and gpo_prop_mask;

  -- Use gpo_port_mask if the data is valid and the gpo_mask_enable(1) bit is high
  gpo_port_mask <= in_in.data(27 downto 16) when (in_in.valid='1' and its(props_in.gpo_mask_enable(1))) else (others => '0');
  gpo_port_data <= in_in.data(11 downto 0) and gpo_port_mask;

  gpo_mask <= gpo_prop_mask or gpo_port_mask;
  gpo_data <= gpo_prop_data or gpo_port_data;
  gpo      <= (gpo_r and not gpo_mask) or (gpo_data and gpo_mask);

  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        gpo_r <= (others => '0');
      else
        gpo_r <= gpo;
      end if;
    end if;
  end process;

  gpio_pin_o <= gpo_r;

end rtl;
