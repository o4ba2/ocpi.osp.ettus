.. x310_gpio HDL worker


.. _x310_gpio-HDL-worker:


``x310_gpio`` HDL Worker
========================

``x310_gpio`` implements interaction with the 12 front-panel GPIOs.  It was adapted from the OpenCPI GPI and GPO workers.

Detail
------
.. ocpi_documentation_worker::

The direction of each pin is controlled with the ``direction_mask_data`` property or the ``dir`` port.  GPI data is read from the ``gpi_data`` property or the ``out`` port.  GPO data can be sent to the ``gpo_mask_data`` property or the ``in`` port.

Refer to the OWD XML file for details on the terms 'mask' and 'data' and how to use each port and property.  In brief, the 'mask' value specifies which 'data' bits are to be changed.  The following example, in the context of the GP input, means that bits 2-3 have changed from ``0`` to ``1``: ::

   Bit:    12    8    4    0
   Mask: 0000 0000 0000 1100
   Data: 0000 0000 1100 1100

Although 16 bit data-types are used for mask and data fields only the lower 12 bits are used, corresponding to the 12 front-panel GPIOs on the X310.

Stumbling Blocks
````````````````

It is worth noting a few potential stumbling blocks:

* The GPI ``out`` port will drop messages if it runs out of OCPI-provided buffers.  This may result in missing updates at the application layer.  A simple solution is to increase the number of buffers using the ``buffercount`` and ``buffersize`` attributes documented in the Application Development Guide.  It is recommended to increase this value from the default of ``2`` because GPI changes are often presented in multiple messages in quick succession.
* Changes to the output pins from a single port message/property write will not necessarily be affected in the same FPGA clock cycle.  Likewise concurrent changes to the input pins will not necessarily be represented in the same port message/property read.
* If a port and property set an output (or direction) on the same cycle but to opposite values, the value used is ``1`` rather than ``0``.

Changes
```````

Changes in the VHDL with respect to the original OpenCPI GPI and GPO workers are as follows:

* The 16 bits of mask were removed from the GPI property.  These bits represented the change in GPI data between consecutive FPGA clock cycles and so were rarely anything but 0 because properties are read at a much lower rate.
* The use of ``gpo_mask_data`` and ``direction_mask_data`` properties are gated on the OCPI-provided ``_written`` strobe.  This means that each property does not interfere with subsequent messages from the corresponding ports.


Utilization
-----------
.. ocpi_documentation_utilization::
