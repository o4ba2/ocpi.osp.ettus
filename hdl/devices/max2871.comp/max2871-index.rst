.. max2871 documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _max2871:


MAX2871 Synthesiser (``max2871``)
=================================
Ettus UBX160 daughter board HDL Device Worker for the MAX2871 Fractional Integer-N Synthesizer / VCO. 

Design
------
This worker provides register access to the hardware registers of the MAX2871 Fractional Integer-N Synthesizer / VCO.
Four instances of this worker are required for each UBX160 daughter board fitted to the x310.
The MAX2871 devices generate the local oscillator signals used for modulation of the baseband signal to the desired RF carrier frequency, 
and for the demodulation of the received RF signal to a baseband signal.

Interface
---------
The interface of the max2871.hdl worker is defined in ``./max2871.xml`` 

.. .. literalinclude:: ../specs/max2871-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``./max2871.xml``
A single property (name: max2871_reg) provides access to the MAX2871 hardware registers.
This property is used by the drc_x310.rcc and does not need to be set by an application using the drc_x310.

.. .. ocpi_documentation_properties::

Ports
~~~~~
The max2871.hdl worker does not have any component ports. 

.. .. ocpi_documentation_ports::

..    input: Primary input samples port.
..    output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../max2871.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

The max2871.hdl worker (is supported by) worker 

 * ``ocpi.osp.ettus.devices.ubx160_io.hdl``
 
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``max2871`` are:

 * This worker is to be used only with the drc_x310.rcc (DRC proxy)

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
