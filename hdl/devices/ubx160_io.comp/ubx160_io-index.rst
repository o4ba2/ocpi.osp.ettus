.. ubx160_io documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _ubx160_io:


UBX160 GPIO and SPI Bus (``ubx160_io``)
=======================================
Ettus x310 UBX160 daughter board IO worker.

Design
------
This worker provides control for the following sub-devices

* ads62p48.hdl (ADC)
* ubx160_cpld.hdl (UBX160 daughter board CPLD) 
* max2871.hdl (Frequency Synthesizer / VCO - RXLO1)
* max2871.hdl (Frequency Synthesizer / VCO - RXLO2)
* max2871.hdl (Frequency Synthesizer / VCO - TXLO1)
* max2871.hdl (Frequency Synthesizer / VCO - TXLO2)

It drives the a number of GPIO signals, LEDs and SPI buses used for controlling the hardware 
on or associated with the x310 daughter board. Two instances of this worker (one for each daughter board) 
are used by the x310 platform.The worker connects to the Raw Property ports of its sub-devices, and uses 
these to drive the GPIO and SPI signals.

This worker is controlled by the drc_x310.rcc worker. It is not necessary for the application to
access the worker's properties.

Interface
---------
The interface of the ubx160_io.hdl worker is defined in ``../specs/ubx160_io-properties.xml`` 

.. .. literalinclude:: ../specs/ubx160_io-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``../specs/ubx160_io-properties.xml``
These properties are used by the drc_x310.rcc and do not need to be set by an application using the drc_x310.

.. .. ocpi_documentation_properties::

Ports
~~~~~
The ubx160_io.hdl worker does not have any component ports. 

.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ubx160_io.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``ocpi.wci.raw_arb``

The ubx160_io.hdl worker supports the following sub-devices

 * ``ocpi.osp.ettus.devices.ads62p48.hdl``
 * ``ocpi.osp.ettus.devices.ubx160_cpld.hdl``
 * ``ocpi.osp.ettus.devices.max2871.hdl``
 
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``
 * ``ieee.math_real``

Limitations
-----------
Limitations of ``ubx160_io`` are:

 * The low speed ADC hardware devices present on the mother board are not supported
 * The low speed DAC hardware devices present on the mother board are not supported
 * The LSADC SPI interface is not supported
 * The LSDAC SPI interface is not supported

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
