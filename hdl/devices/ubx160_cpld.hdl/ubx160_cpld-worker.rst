.. ubx160_cpld HDL worker


.. _ubx160_cpld-HDL-worker:


``ubx160_cpld`` HDL Worker
==========================
Ettus x310 UBX160 daughter board IO worker.

Detail
------
This worker provides control of the CPLD device located on the UBX160 daughter card.
An instance of this worker is required for each daughter board present in the x310.

The CPLD is controlled using an SPI interface. This SPI interface is provided by the
ubx160_io.hdl worker that supports this ubx160_cpld.hdl worker.

The ubx160 CPLD is configured by the drc_x310.rcc digital-Radio-Control. The application
does not have to configure the CPLD using this worker.

The CPLD has a single 32-bit register. This register is accessed through the worker's 
property (name: cpld_reg). This property is write only.

The bits of this register have the following functionality.

+------+----------------------+---------------------------------------------+
| Bit  | Name                 | Usage                                       |
+======+======================+=============================================+
| 0    | TXHB_SEL             | Set to '1' to select direct conversion path |
+------+----------------------+---------------------------------------------+
| 1    | TXLB_SEL             | Set to '1' to select down conversion path   |
+------+----------------------+---------------------------------------------+
| 2    | TXLO1_FSEL1          | TXLO1 Filter Select                         |
+------+----------------------+ Set bit to '1' to enable filter             +
| 3    | TXLO1_FSEL2          | FSEL1  Fc=0.8GHz                            |
+------+----------------------+ FSEL2  Fc=2.2GHz                            +
| 4    | TXLO1_FSEL3          | FSEL3  Fc=no filter                         |
+------+----------------------+---------------------------------------------+
| 5    | RXHB_SEL             | Set to '1' to select direct conversion path |
+------+----------------------+---------------------------------------------+
| 6    | RXLB_SEL             | Set to '1' to select down conversion path   |
+------+----------------------+---------------------------------------------+
| 7    | RXLO1_FSEL1          | RXLO1 Filter Select                         |
+------+----------------------+ Set bit to '1' to enable filter             +
| 8    | RXLO1_FSEL2          | FSEL1  Fc=0.8GHz                            |
+------+----------------------+ FSEL2  Fc=2.2GHz                            +
| 9    | RXLO1_FSEL3          | FSEL3  Fc=no filter                         |
+------+----------------------+---------------------------------------------+
| 10   | SEL_LNA1             | Set to '1' to select unfiltered LNA         |
+------+----------------------+---------------------------------------------+
| 11   | SEL_LNA2             | Set to '1' to select filtered LNA           |
+------+----------------------+---------------------------------------------+
| 12   | TXLO1_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 13   | TXLO2_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 14   | TXMOD_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 15   | TXMIXER_FORCEON      | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 16   | TXDRV_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 17   | RXLO1_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 18   | RXLO2_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 19   | RXDEMOD_FORCEON      | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 20   | RXMIXER_FORCEON      | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 21   | RXDRV_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 22   | RXAMP_FORCEON        | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 23   | RXLNA1_FORCEON       | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 24   | RXLNA2_FORCEON       | Hardware FORCE ON                           |
+------+----------------------+---------------------------------------------+
| 25   | CAL_ENABLE           | Used to set calibration (loopback) path     |
+------+----------------------+---------------------------------------------+

FORCE ON signals are used when operating in performance power mode as a means
to reduce the settling time by powering on the hardware before it is used.

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
