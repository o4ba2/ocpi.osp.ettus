.. ubx160_eeprom documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _ubx160_eeprom:


X310 EEPROM (``ubx160_eeprom``)
===============================
Ettus x310 UBX160 daughter board EEPROM worker.

Design
------
Each UBX160 daughter board is fitted with two EEPROMs. One is associated with the RF transmit path and one 
with the RF receive path. Both of these eeproms are accessed using a single I2C interface. A single instance
of this worker provides access to both of these eeproms.
In addition to the eeproms on the UBX160 daughter board, the x310 motherboard also has its own eeprom.
The mother board eeprom shares the same I2C interface with the UBX160 daughter boards and can be accessed
using this same worker.

It is not necessary for the application to read the contents of the eeproms.
The drc_x310.rcc Digital-Radio-Controller reads the eeprom contents, and displays
the contents (at logging level OCPI_LOG_INFO=8) when the application is run.

Interface
---------
The interface of the max2871.hdl worker is defined in ``../specs/ubx160_eeprom-properties.xml`` 

.. .. literalinclude:: ../specs/ubx160_eeprom-spec.xml
..    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~
The properties of this worker can be found in ``../specs/ubx160_eeprom-properties.xml``

.. .. ocpi_documentation_properties::

Ports
~~~~~
The ubx160_eeprom.hdl worker does not have any component ports. 

.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ubx160_eeprom.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``ubx160_eeprom`` are:

 * None 

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
