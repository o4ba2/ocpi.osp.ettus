.. ad9146 documentation

.. _ad9146:


AD9146 DAC (``ad9146``)
=======================
Device worker for the AD9146 Dual, 16-Bit, 1230 MSPS DAC.

Design
------
This device worker is used by the Ettus X310 Digital-radio-controller (DRC) for
control and configuration of the two AD9146 chips on the X310 motherboard.

The device worker provides several functions:

* Expose the AD9146 register map as raw properties, which are delegated to a
  separate SPI worker over a rawprops interface.

* Receive data from a ``qdac-16-signals`` devsignals interface and transmit it
  to the DAC.

* Allow the data stream to be enabled/disabled by the DRC.

Interface
---------

The interface is defined in-line in the OWD.

Opcode handling
~~~~~~~~~~~~~~~

This worker does not have a component port. As such no Opcodes are handled.

Properties
~~~~~~~~~~

The AD8146 registers are exposed as raw properties, which are defined in
``../specs/ad9146-properties.xml``.

Additionally, the component is configured and controlled via the following
properties:

* ``enable``: Controls the valid signal of the output data stream.

  * Type: ``bool``
  * Access:

    * Parameter: False
    * Writable: True
    * Initial: False
    * Volatile: False

  * Default value: 0

Ports
~~~~~
.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ad9146.hdl

Example Application
-------------------
This component is not intended to be used in an application; instead it is
incorporated as part of the DRC slave assembly.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``unisim.vcomponents``

Limitations
-----------
Limitations of ``ad9146`` are:

 * This worker is to be used only with the drc_x310.rcc (DRC proxy)

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
