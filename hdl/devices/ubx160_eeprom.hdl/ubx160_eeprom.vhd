-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.all, ocpi.types.all;
architecture rtl of worker is
  -- i2c_controller connections
  signal NUMBER_OF_BYTES : std_logic_vector(2 downto 0);
  signal IS_READ         : std_logic;
  signal IS_WRITE        : std_logic;
  signal SLAVE_ADDR      : std_logic_vector(6 downto 0);
  signal ADDR            : std_logic_vector(31 downto 0);
  signal ADDR_LEN_BYTES  : std_logic_vector(2 downto 0);
  signal WDATA           : std_logic_vector(31 downto 0);
  signal RDATA           : std_logic_vector(31 downto 0);
  signal DONE            : std_logic;
  signal ERROR           : std_logic;

  -- control logic
  signal is_read_buf : std_logic;
  signal out_buf : wci.raw_out_t;

  signal scl_oen_i : std_logic;
  signal sda_oen_i : std_logic;


begin

  SCL_OE <= not scl_oen_i;
  SDA_OE <= not sda_oen_i;

  i2c_controller : entity work.i2c_opencores_ctrl_var_addr
    generic map(
      CLK_CNT => clk_div
    )
    port map (
      WCI_CLK => ctl_in.clk,
      WCI_RESET => ctl_in.reset,
      NUMBER_OF_BYTES => NUMBER_OF_BYTES,
      IS_READ => IS_READ,
      IS_WRITE => IS_WRITE,
      SLAVE_ADDR => SLAVE_ADDR,
      ADDR => ADDR,
      ADDR_LEN_BYTES => ADDR_LEN_BYTES,
      WDATA => WDATA,
      RDATA => RDATA,
      DONE => DONE,
      ERROR => ERROR,
      SCL_I => SCL_I,
      SCL_O => SCL_O,
      SCL_OEN => scl_oen_i,
      SDA_I => SDA_I,
      SDA_O => SDA_O,
      SDA_OEN => sda_oen_i
    );

  -- never write to the eeprom
  is_write <= '0';
  wdata <= x"00000000";

  -- config one byte reads
  number_of_bytes <= "001";
  is_read <= is_read_buf;

  -- output buffering
  props_out.raw <= out_buf;

  -- Control Plane Logic
  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if its(ctl_in.reset) then
        is_read_buf <= '0';
        out_buf <= wci.raw_out_zero;
      else
        if (error = '1') then
          -- error from a pending read
          out_buf.error <= bTrue;
          is_read_buf <= '0';
        elsif (done = '1') then
          -- done from a pending read
          out_buf.done <= bTrue;
          out_buf.data(7 downto 0) <= rdata(31 downto 24);
          is_read_buf <= '0';
        elsif (out_buf.done = bTrue or out_buf.error = bTrue) then
          -- clear previous transaction (requires a cycle for is_write/is_read to be reset)
          out_buf <= wci.raw_out_zero;
        elsif (is_read_buf = '0') then
          -- no read/write in progress
          out_buf <= wci.raw_out_zero;

          if its(props_in.raw.is_write) then
            if (props_in.raw.address = to_ulong(0)) then -- control
              slave_addr <= props_in.raw.data(6 downto 0);
              out_buf.done <= bTrue;
            elsif (props_in.raw.address = to_ulong(4)) then -- addr_size_bytes
              addr_len_bytes <= props_in.raw.data(2 downto 0);
              out_buf.done <= bTrue;
            elsif (props_in.raw.address = to_ulong(8)) then -- addr
              addr <= props_in.raw.data;
              out_buf.done <= bTrue;
            else
              -- other properties should not be written
              out_buf.error <= bTrue;
            end if;
          elsif its(props_in.raw.is_read) then
            if (props_in.raw.address = to_ulong(12)) then -- read
              is_read_buf <= '1';
            else
              -- other properties should not be read
              out_buf.error <= bTrue;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

end rtl;
