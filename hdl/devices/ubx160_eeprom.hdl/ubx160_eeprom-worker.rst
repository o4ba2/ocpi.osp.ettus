.. ubx160_eeprom HDL worker


.. _ubx160_eeprom-HDL-worker:


``ubx160_eeprom`` HDL Worker
============================
Ettus x310 UBX160 daughter board EEPROM worker.

Detail
------
Each UBX160 daughter board is fitted with two EEPROMs. One is associated with the RF transmit path and one 
with the RF receive path. Both of these eeproms are accessed using a single I2C interface. A single instance
of this worker provides access to both of these eeproms.
In addition to the eeproms on the UBX160 daughter board, the x310 motherboard also has its own eeprom.
The mother board eeprom shares the same I2C interface with the UBX160 daughter boards and can be accessed
using this same worker.

It is not necessary for the application to read the contents of the eeproms.
The drc_x310.rcc Digital-Radio-Controller reads the eeprom contents, and displays
the contents (at logging level OCPI_LOG_INFO=8) when the application is run.

.. figure:: ./doc/ubx160_eeprom_0.svg

The mother board eeprom contains the following information: 

+------------------+-------------------------------------------+
| Field            | Usage                                     |
+==================+===========================================+
| revision         | x310 product revision                     |
+------------------+-------------------------------------------+
| product          | product identifier                        |
+------------------+-------------------------------------------+
| revision_compat  |  x310 product revision                    |
+------------------+-------------------------------------------+
| mac_addr 0       | Port 0 Ethernet MAC address               |
+------------------+-------------------------------------------+
| mac_addr 1       | Port 1 Ethernet MAC address               |
+------------------+-------------------------------------------+
| gateway          | IP Default gateway                        |
+------------------+-------------------------------------------+
| Subnet Masks     | Subnet masks for x310 ethernet interfaces |
| subnet[0]        | Port 0 interface - 1 Gb                   |
| subnet[1]        | Port 1 interface - 1 Gb                   |
| subnet[2]        | Port 0 interface - 10 Gb                  |
| subnet[3]        | Port 1 interface - 10 Gb                  |
+------------------+-------------------------------------------+
| IP Address       | IP addresses for ethernet interfaces      |
| ip_addr[0]       | Port 0 interface - 1 Gb                   |
| ip_addr[1]       | Port 1 interface - 1 Gb                   |
| ip_addr[2]       | Port 0 interface - 10 Gb                  |
| ip_addr[3]       | Port 1 interface - 10 Gb                  |
+------------------+-------------------------------------------+
| name             | Not used                                  |
+------------------+-------------------------------------------+
| serial           | PCB serial number                         |
+------------------+-------------------------------------------+

Example motherboard contents:
::

   OCPI( 0:689.0910): drc: Motherboard EEPROM:
   OCPI( 0:689.0910): drc:   revision:        10
   OCPI( 0:689.0910): drc:   product:         30818
   OCPI( 0:689.0910): drc:   revision_compat: 7
   OCPI( 0:689.0910): drc:   mac_addr0:       00:80:2f:27:5d:61
   OCPI( 0:689.0910): drc:   mac_addr1:       00:80:2f:27:5d:62
   OCPI( 0:689.0910): drc:   gateway:         192.168.10.1
   OCPI( 0:689.0910): drc:   subnet[0]:       255.255.255.0
   OCPI( 0:689.0910): drc:   subnet[1]:       255.255.255.0
   OCPI( 0:689.0910): drc:   subnet[2]:       255.255.255.0
   OCPI( 0:689.0910): drc:   subnet[3]:       255.255.255.0
   OCPI( 0:689.0910): drc:   ip_addr[0]:      192.168.10.2
   OCPI( 0:689.0910): drc:   ip_addr[1]:      192.168.20.2
   OCPI( 0:689.0910): drc:   ip_addr[2]:      192.168.30.2
   OCPI( 0:689.0910): drc:   ip_addr[3]:      192.168.40.2
   OCPI( 0:689.0910): drc:   name:            
   OCPI( 0:689.0910): drc:   serial:          30FA123


The daughter board eeproms contain: board identification, PCB revision and serial number:

+------------------+-------------------------------------+
| Field            | Usage                               |
|                  |                                     |
+==================+=====================================+
| id               | Daughter board identifier           |
+------------------+-------------------------------------+
| rev              | PCB revision                        |
+------------------+-------------------------------------+
| offset 0         |                                     |
+------------------+-------------------------------------+
| offset 1         |                                     |
+------------------+-------------------------------------+
| serial           | PCB Serial Number                   |
+------------------+-------------------------------------+

Example daughter board contents (two daughter boards fitted in x310):
::

   OCPI( 0:689.0910): drc: Daughterboard EEPROM: (Daughtboard 0, TX)
   OCPI( 0:689.0910): drc:   id:       122
   OCPI( 0:689.0910): drc:   rev:      3
   OCPI( 0:689.0910): drc:   offset_0: 0
   OCPI( 0:689.0910): drc:   offset_1: 0
   OCPI( 0:689.0910): drc:   serial:   30FED7E

   OCPI( 0:689.0910): drc: Daughterboard EEPROM: (Daughtboard 0, RX)
   OCPI( 0:689.0910): drc:   id:       121
   OCPI( 0:689.0910): drc:   rev:      3
   OCPI( 0:689.0910): drc:   offset_0: 0
   OCPI( 0:689.0910): drc:   offset_1: 0
   OCPI( 0:689.0910): drc:   serial:   30FED7E

   OCPI( 0:689.0910): drc: Daughterboard EEPROM: (Daughtboard 1, TX)
   OCPI( 0:689.0910): drc:   id:       126
   OCPI( 0:689.0910): drc:   rev:      5
   OCPI( 0:689.0910): drc:   offset_0: 0
   OCPI( 0:689.0910): drc:   offset_1: 0
   OCPI( 0:689.0910): drc:   serial:   3204E1F

   OCPI( 0:689.0910): drc: Daughterboard EEPROM: (Daughtboard 1, RX)
   OCPI( 0:689.0910): drc:   id:       125
   OCPI( 0:689.0910): drc:   rev:      5
   OCPI( 0:689.0910): drc:   offset_0: 0
   OCPI( 0:689.0910): drc:   offset_1: 0
   OCPI( 0:689.0910): drc:   serial:   3204E1F

Note that OpenCPI uses raw ethernet packets (not IP) to transfer control and data between
the rcc and hdl containers. 

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
