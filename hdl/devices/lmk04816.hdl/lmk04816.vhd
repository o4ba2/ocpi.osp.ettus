-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.all, ocpi.types.all;
library unisim; use unisim.vcomponents.all;
use work.all;

architecture rtl of worker is

  signal clk200M         : std_logic;
  signal fbclk_unbuf     : std_logic;
  signal fbclk           : std_logic;
  signal rad_clk_unbuf   : std_logic;
  signal rad_clk         : std_logic;
  signal rad_clk2x_unbuf : std_logic;
  signal rad_clk2x       : std_logic;
  signal dci_clk         : std_logic;
  signal dci_clk_unbuf   : std_logic;
  signal clk_locked      : std_logic;

  -- Output interface
  signal out_buf : wci.raw_out_t;

  -- Microwire interface
  constant uwire_data_width       : positive := 27;
  constant uwire_addr_width       : positive := 5;
  signal uwire_data               : std_logic_vector(uwire_data_width - 1 downto 0);
  signal uwire_address            : std_logic_vector(uwire_addr_width - 1 downto 0);
  signal uwire_address_and_data   : std_logic_vector(uwire_addr_width + uwire_data_width - 1 downto 0);
  signal uwire_three_extra_clocks : std_logic;
  signal uwire_hold_le_high       : std_logic;
  signal uwire_valid_buf          : std_logic;
  signal uwire_valid              : std_logic;
  signal uwire_ready              : std_logic;

begin

  -- Configure the clock mux to select the on-board TCXO as the 10 Meg clock
  -- source
  ClockRefSelect <= "10";

  -- Enable the on-board TCXO
  TCXO_ENA       <= '1';

  -- Indicated locked status on the appropriate LED
  -- LED_REFLOCK sinks current through LED when set so active low
  LED_REFLOCK <= not LOCK;

  -- The purpose of this LMK04816 clock input is to allow the RF clock domain
  -- to be derived from the CPRI clock recovered by the MGT interfaces in the
  -- FPGA.
  --
  -- We do not support this functionality, so just tie off the clock to 0
  cpri_clk_out_obufds_inst : OBUFDS
    port map (
      I  => '0',
      O  => CPRI_CLK_OUT_P,
      OB => CPRI_CLK_OUT_N
    );

  -- MicroWire Interface
  microwire_inst : entity work.microwire
    generic map (
      clk_div    => to_integer(clk_div),
      data_width => uwire_data_width + uwire_addr_width
    )
    port map (
      -- Inputs
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      data               => uwire_address_and_data,
      three_extra_clocks => uwire_three_extra_clocks,
      hold_le_high       => uwire_hold_le_high,
      valid              => uwire_valid,

      -- Outputs
      ready              => uwire_ready,
      MOSI               => MOSI,
      SCLK               => SCLK,
      SEN                => SEN
    );

  props_out.status(1 downto 0)  <= unsigned(STATUS);
  props_out.status(2)           <= HOLDOVER;
  props_out.status(3)           <= LOCK;
  props_out.status(4)           <= SYNC;
  props_out.status(5)           <= clk_locked;
  props_out.status(31 downto 6) <= (others => '0');

  uwire_address_and_data <= uwire_data & uwire_address;

  -- Control Plane Logic
  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if its(ctl_in.reset) then
        uwire_valid_buf <= '0';
        out_buf <= wci.raw_out_zero;
      else
        if (uwire_valid_buf = '1' and uwire_ready = '1') then
          -- complete pending transaction
          uwire_valid_buf <= '0';
          out_buf.done <= bTrue;
        elsif (out_buf.done = bTrue) then
          -- clear previous transaction (requires a cycle for is_write/is_read to be reset)
          out_buf <= wci.raw_out_zero;
        elsif (uwire_valid_buf = '0') then
          -- no read/write in progress
          out_buf <= wci.raw_out_zero;

          if its(props_in.raw.is_write) then
            if (((props_in.raw.address >= To_ulong(0)) and -- r0
                 (props_in.raw.address <= To_ulong(4 * 16))) or -- r16
                ((props_in.raw.address >= To_ulong(4 * 24)) and -- r24
                 (props_in.raw.address <= To_ulong(4 * 31))) -- r31
               ) then
               -- divide the address by four (byte address to 32 bit word address)
              uwire_address <= from_ulong(props_in.raw.address)(uwire_addr_width + 2 - 1 downto 2);
              -- extract the data for the microwire peripheral
              uwire_data               <= props_in.raw.data(uwire_data_width - 1 downto 0);
              uwire_three_extra_clocks <= props_in.raw.data(30);
              uwire_hold_le_high       <= props_in.raw.data(31);

              -- kick off microwire block
              uwire_valid_buf <= '1';
            else
              -- ignore writes to other properties
              out_buf.done <= bTrue;
            end if;
          elsif its(props_in.raw.is_read) then
            -- no support for reads: just return 0
            out_buf.done <= bTrue;
          end if;
        end if;
      end if;
    end if;
  end process;

  uwire_valid   <= uwire_valid_buf;
  props_out.raw <= out_buf;

  clk_ibufg_inst : IBUFGDS
    port map (
      I  => FPGA_CLK_P,
      IB => FPGA_CLK_N,
      O  => clk200M
    );

  mmcm_inst : MMCME2_BASE
    generic map (
      BANDWIDTH        => "OPTIMIZED",
      CLKFBOUT_MULT_F  => 6.000,
      CLKFBOUT_PHASE   => 0.000,
      CLKIN1_PERIOD    => 5.000,
      CLKOUT0_DIVIDE_F => 6.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT0_PHASE    => 0.000,
      CLKOUT1_DIVIDE   => 3,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT1_PHASE    => -45.000,
      CLKOUT2_DIVIDE   => 3,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKOUT2_PHASE    => 60.000,
      DIVCLK_DIVIDE    => 1,
      STARTUP_WAIT     => FALSE
    )
    port map (
      CLKFBOUT  => fbclk_unbuf,
      CLKFBOUTB => open,
      CLKOUT0  => rad_clk_unbuf,
      CLKOUT1  => rad_clk2x_unbuf,
      CLKOUT2  => dci_clk_unbuf,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => clk_locked,
      CLKFBIN   => fbclk,
      CLKIN1   => clk200M,
      PWRDWN   => '0',
      RST      => '0'
    );

  clkfb_bufg_inst : BUFG
    port map (
      I => fbclk_unbuf,
      O => fbclk
    );

  rad_clk_bufg_inst : BUFG
    port map (
      I => rad_clk_unbuf,
      O => rad_clk
    );

  rad_clk_2x_bufg_inst : BUFG
    port map (
      I => rad_clk2x_unbuf,
      O => rad_clk2x
    );

  dci_clk_bufg_inst : BUFG
    port map (
      I => dci_clk_unbuf,
      O => dci_clk
    );

  rfclk_distribution: for i in 0 to rfclks_out_array_t'length - 1 generate
    rfclks_out(i).radclk   <= rad_clk;
    rfclks_out(i).radclk2x <= rad_clk2x;
    rfclks_out(i).dciclk   <= dci_clk;
  end generate rfclk_distribution;

  -- Hold control in initialize until PLL locked
  process(ctl_in.control_op, clk_locked)
  begin
    case ctl_in.control_op is
      when ocpi.wci.INITIALIZE_e => ctl_out.done <= clk_locked;
      when others => ctl_out.done <= '1';
    end case;
  end process;

end rtl;
