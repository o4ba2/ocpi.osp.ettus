-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
use work.all;

entity microwire is
  generic (
    clk_div    : natural := 100;
    data_width : natural := 32
  );
  port (
    -- Inputs
    clk                : in std_logic;
    reset              : in std_logic;
    data               : in std_logic_vector(data_width - 1 downto 0);
    three_extra_clocks : in std_logic;
    hold_le_high       : in std_logic; -- only valid if three_extra_clocks also high
    valid              : in std_logic;

    -- Outputs
    ready              : out std_logic;
    MOSI               : out std_logic; -- aka DATAuWire
    SCLK               : out std_logic; -- aka CLKuWire
    SEN                : out std_logic  -- aka LEuWire
  );
end entity microwire;

architecture microwire_arch of microwire is

  -- State Type
  type state_labels is (
    Idle,
    Strobe_Clk_Low,
    Strobe_Clk_High,
    Post_Data_Clk_Low,
    LE_High,
    Clk_Low,
    Clk_High
  );

  -- Clock Reduction
  signal clk_count_r : integer range 0 to clk_div - 1;
  signal clk_slow_r  : std_logic;

  -- Ouput State Machine
  signal output_count_r   : integer range 0 to data_width - 1;
  signal state_r          : state_labels;
  signal output_go_r      : boolean;
  signal output_data_r    : std_logic_vector(data_width - 1 downto 0);

  -- Input Latching
  signal ready_r              : std_logic;
  signal data_r               : std_logic_vector(data_width - 1 downto 0);
  signal three_extra_clocks_r : std_logic;
  signal hold_le_high_r       : std_logic;

begin

  -- clock reduction
  process (clk)
  begin
    if reset = '1' then
      clk_count_r <= clk_div - 1;
      clk_slow_r  <= '0';
    elsif rising_edge(clk) then
      if clk_count_r = 0 then
        clk_count_r <= clk_div - 1;
        clk_slow_r  <= '1';
      else
        clk_count_r <= clk_count_r - 1;
        clk_slow_r  <= '0';
      end if;
    end if;
  end process;

  -- input latching and interaction with output state machine
  process (clk)
  begin
    if reset = '1' then
      ready_r     <= '0';
      output_go_r <= false;
    elsif rising_edge(clk) then
      if ready_r = '1' and valid = '1' then
        -- latch inputs
        data_r               <= data;
        three_extra_clocks_r <= three_extra_clocks;
        hold_le_high_r       <= hold_le_high;

        -- start processing data
        ready_r     <= '0';
        output_go_r <= true;
      elsif state_r /= Idle then
        -- started processing
        output_go_r <= false;
      elsif state_r = Idle and not output_go_r then
        -- idle and not waiting to start, ready for next
        ready_r <= '1';
      end if;
    end if;
  end process;

  ready <= ready_r;


  -- output processing
  process (clk)
  begin
    -- nb Risk of mid-sequence reset from work. Likely mitigation is to always
    -- write RESET to R0 twice, which should clear internal state if LMK04816
    -- is sensible implemented!
    if reset = '1' then
      state_r <= Idle;
      MOSI    <= '0';
      SCLK    <= '0';
      SEN     <= '0';
    elsif rising_edge(clk) and clk_slow_r = '1' then
      -- Output State Machine
      case state_r is
        -- Wait for data
        when Idle =>
          -- Idle state
          MOSI <= '0';
          SCLK <= '0';
          SEN  <= '0';

          if output_go_r then
            -- start processing data
            output_data_r  <= data_r;
            output_count_r <= data_width - 1;
            state_r        <= Strobe_Clk_Low;
          end if;

        -- Strobe data / address
        when Strobe_Clk_Low =>
          MOSI      <= output_data_r(data_width - 1);
          SCLK      <= '0';
          SEN       <= '0';
          state_r   <= Strobe_Clk_High;

        when Strobe_Clk_High =>
          MOSI <= output_data_r(data_width - 1);
          SCLK <= '1';
          SEN  <= '0';

          if output_count_r = 0 then
            state_r <= Post_Data_Clk_Low;
          else
            -- next bit
            output_count_r <= output_count_r - 1;
            output_data_r  <= output_data_r(data_width - 2 downto 0) & '0'; -- left shift data
            state_r        <= Strobe_Clk_Low;
          end if;

        -- post data clock low period
        when Post_Data_Clk_Low =>
          MOSI <= '0';
          SCLK <= '0';
          SEN  <= '0';

          state_r <= LE_High;

        -- pulse LE
        when LE_High =>
          MOSI <= '0';
          SCLK <= '0';
          SEN  <= '1';

          state_r <= Clk_Low;
          if three_extra_clocks_r = '1' then
            -- three clocks high (four low clocks)
            output_count_r <= 3;
          else
            -- back to idle after one low clock
            output_count_r <= 0;
          end if;

        -- final clock bursts
        when Clk_Low =>
          MOSI <= '0';
          SCLK <= '0';
          SEN  <= hold_le_high_r;

          if output_count_r = 0 then
            -- finished
            state_r <= Idle;
          else
            -- high pulse
            output_count_r <= output_count_r - 1;
            state_r        <= Clk_High;
          end if;

        when Clk_High =>
          MOSI <= '0';
          SCLK <= '1';
          SEN  <= hold_le_high_r;

          state_r <= Clk_Low;

        -- belt and braces
        when others =>
          state_r <= Idle;

      end case;
    end if;
  end process;
end architecture microwire_arch;
