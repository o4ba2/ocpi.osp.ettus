.. x310_gpio documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _x310_gpio:


X310 Front-panel GPIO (``x310_gpio``)
=====================================
Implements interaction with the 12 front-panel GPIOs.

Design
------

Provides read and write access and direction setting control to each of the X310 front-panel GPIOs via
both port and properties.

Interface
---------
.. literalinclude:: ../specs/gp_inout-spec.xml
   :language: xml

Opcode handling
~~~~~~~~~~~~~~~
All three ports (``dir``, ``in``, ``out``) use protocol gpio_protocol.xml which has a single opcode.

Properties
~~~~~~~~~~
.. .. ocpi_documentation_properties::

Properties can be used to interact with the GPIO pins.  See x310_gpio.xml for descriptions of each property.

Ports
~~~~~
.. .. ocpi_documentation_ports::

Ports can be used to interact with the GPIO pins.  See x310_gpio.xml for descriptions of each port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../x310_gpio.hdl

Example Application
-------------------
.. .. literalinclude:: example_app.xml
..    :language: xml

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
