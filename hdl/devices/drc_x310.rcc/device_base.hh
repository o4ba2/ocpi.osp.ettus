// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_DEVICE_BASE_HH__
#define OCPI_RCC_WORKER_DRC_X310_DEVICE_BASE_HH__

#include <stdarg.h>
#include "OcpiDebugApi.hh"


class DeviceControllerBase
{
public:
  void log(unsigned n, const char *fmt, ...);
};


inline void DeviceControllerBase::log(unsigned n, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  OCPI::OS::Log::printV(n, fmt, ap);
  va_end(ap);
}

#endif /* OCPI_RCC_WORKER_DRC_X310_DEVICE_BASE_HH__ */
