// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_IQCORRECTION_CONTROLLER_HH__
#define OCPI_RCC_WORKER_DRC_X310_IQCORRECTION_CONTROLLER_HH__

#include "RCC_Worker.hh"
using namespace OCPI::RCC;

#include <memory>
#include "device_base.hh"


// Forward declarations
class IQCorrectionXsInterface;


typedef struct calibration_tag
{
  double freq;
  double correction_real;
  double correction_imag;
} calibration_t;


class IQCorrectionController : DeviceControllerBase
{
public:
  IQCorrectionController(
    bool is_rx,
    bool apply_calib,
    std::string calib_path,
    std::string calib_base,
    std::shared_ptr<IQCorrectionXsInterface> slave);

  RCCResult start();
  RCCResult enable_correction(bool enable);
  RCCResult set_tx_correction(double freq_hz);
  RCCResult set_rx_correction(double freq_hz);
  RCCResult set_correction(double I_dc, double Q_dc, double A, double B);
  RCCResult set_calib_path_base(std::string calib_path, std::string calib_base);

private:
  RCCResult get_correction(std::string filename, double freq, double* i, double* q);
  RCCResult parse_calibration(std::string line, calibration_t* calib);
  RCCResult interp_correction(calibration_t lte, calibration_t gt, double freq, double* i, double* q);

  std::shared_ptr<IQCorrectionXsInterface> m_slave;

  // Which daughter board slot is this card in -- 0 or 1
  const bool      m_is_rx;
  const bool      m_apply_calib;

  // calibration file
  std::string     m_calib_path;
  std::string     m_calib_base;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_IQCORRECTION_CONTROLLER_HH__ */
