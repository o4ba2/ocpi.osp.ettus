// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_EEPROM_CONTROLLER_HH__
#define OCPI_RCC_WORKER_DRC_X310_EEPROM_CONTROLLER_HH__

#include <memory>
#include "RCC_Worker.hh"
#include "device_base.hh"


using OCPI::RCC::RCCResult;


// Forward declarations
class Ubx160EepromInterface;


// Contents of the motherboard EEPROM
struct MBEepromMap
{
  // Indentifying numbers
  uint16_t revision;
  uint16_t product;
  uint16_t revision_compat;

  // MAC addresses
  uint8_t mac_addr0[6];
  uint8_t mac_addr1[6];

  // IP addresses
  uint32_t gateway;
  uint32_t subnet[4];
  uint32_t ip_addr[4];

  // Name and serial
  uint8_t name[23];
  uint8_t serial[9];
};


// Contents of the daughterboard EEPROM
struct DBEepromMap
{
  // Check if the EEPROM contents are valid
  bool is_valid() const;

  uint8_t  magic_value;
  uint16_t id;
  uint16_t rev;
  uint16_t offset_0;
  uint16_t offset_1;
  uint8_t  serial[22];
};


class EepromController : DeviceControllerBase
{
public:
  EepromController(std::shared_ptr<Ubx160EepromInterface> slave);

  RCCResult start();

  // Get the contents of each of the EEPROMs
  //
  // These will be valid only *after* start has been called
  MBEepromMap const& mb_eeprom()               const { return m_mb_eeprom; }
  DBEepromMap const& db_rx_eeprom(size_t slot) const { return m_db_rx_eeprom[slot]; }
  DBEepromMap const& db_tx_eeprom(size_t slot) const { return m_db_tx_eeprom[slot]; }

private:
  // EEPROM device addresses
  enum class Device : uint8_t
  {
    MB = 0x50,
    DB0_TX = 0x54,
    DB0_RX = 0x55,
    DB1_TX = 0x56,
    DB1_RX = 0x57,
  };

  // Read a byte from the specified EEPROM
  uint8_t read_byte(Device device, uint16_t addr);

  // Read a structure from the specified EEPROM
  void read(Device device, MBEepromMap& data);
  void read(Device device, DBEepromMap& data);

  // Cache the EEPROM contents. Filled in `start()`
  MBEepromMap m_mb_eeprom;
  DBEepromMap m_db_rx_eeprom[2];
  DBEepromMap m_db_tx_eeprom[2];

  std::shared_ptr<Ubx160EepromInterface> m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_EEPROM_CONTROLLER_HH__ */
