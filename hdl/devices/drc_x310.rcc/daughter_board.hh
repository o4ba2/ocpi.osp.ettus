// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_DAUGHTER_BOARD_HH__
#define OCPI_RCC_WORKER_DRC_X310_DAUGHTER_BOARD_HH__

#include <memory>
#include "RCC_Worker.hh"
#include "x310_rf_port.hh"


using OCPI::RCC::RCCResult;


// Forward declarations
class Ad9146Controller;
class Ads62p48Controller;
class IQCorrectionController;
class IQCorrectionController;
class Ubx160Controller;


class DaughterBoard
{
public:
  DaughterBoard(
    std::shared_ptr<Ad9146Controller> ad9146,
    std::shared_ptr<Ads62p48Controller> ads62p48,
    std::shared_ptr<IQCorrectionController> rx_iqcorrection,
    std::shared_ptr<IQCorrectionController> tx_iqcorrection,
    std::shared_ptr<Ubx160Controller> ubx160);

  RCCResult start();
  double get_tx_freq();
  double get_rx_freq();

  RCCResult set_tx_fe_enable(bool enable);
  RCCResult set_rx_fe_enable(bool enable);
  RCCResult set_tx_dac_enable(bool enable);
  RCCResult set_rx_adc_enable(bool enable);
  RCCResult cache_dboard_clock_rate_hz(double rate_hz);
  RCCResult cache_tx_gain(double gain_db);
  RCCResult cache_rx_gain(double gain_db);
  RCCResult cache_tx_freq_set_iq_correction(double freq_hz);
  RCCResult cache_rx_freq_set_iq_correction(double freq_hz);
  RCCResult cache_tx_ant(Port port);
  RCCResult cache_rx_ant(Port port);
  RCCResult set_rx_correction(bool enable, double I_dc, double Q_dc, double A, double B);
  RCCResult set_tx_correction(bool enable, double I_dc, double Q_dc, double A, double B);
  RCCResult shutdown();
  RCCResult wait_lo_locked();
  RCCResult set_correction_calib_path_base(std::string calib_path, std::string calib_base);

private:
  std::shared_ptr<Ad9146Controller>       m_ad9146;
  std::shared_ptr<Ads62p48Controller>     m_ads62p48;
  std::shared_ptr<IQCorrectionController> m_rx_iqcorrection;
  std::shared_ptr<IQCorrectionController> m_tx_iqcorrection;
  std::shared_ptr<Ubx160Controller>       m_ubx160;

  bool                                    m_is_shutdown = true;
};


#endif /* OCPI_RCC_WORKER_DRC_X310_DAUGHTER_BOARD_HH__ */
