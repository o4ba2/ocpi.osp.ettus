// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_UBX160_FIELD_HH__
#define OCPI_RCC_WORKER_DRC_X310_UBX160_FIELD_HH__


enum ubx_cpld_field_id_t {
  TXHB_SEL        = 0,
  TXLB_SEL        = 1,
  TXLO1_FSEL1     = 2,
  TXLO1_FSEL2     = 3,
  TXLO1_FSEL3     = 4,
  RXHB_SEL        = 5,
  RXLB_SEL        = 6,
  RXLO1_FSEL1     = 7,
  RXLO1_FSEL2     = 8,
  RXLO1_FSEL3     = 9,
  SEL_LNA1        = 10,
  SEL_LNA2        = 11,
  TXLO1_FORCEON   = 12,
  TXLO2_FORCEON   = 13,
  TXMOD_FORCEON   = 14,
  TXMIXER_FORCEON = 15,
  TXDRV_FORCEON   = 16,
  RXLO1_FORCEON   = 17,
  RXLO2_FORCEON   = 18,
  RXDEMOD_FORCEON = 19,
  RXMIXER_FORCEON = 20,
  RXDRV_FORCEON   = 21,
  RXAMP_FORCEON   = 22,
  RXLNA1_FORCEON  = 23,
  RXLNA2_FORCEON  = 24,
  CAL_ENABLE      = 25
};


struct ubx_cpld_reg_t
{
  void set_field(ubx_cpld_field_id_t field, uint32_t val) {
    if (val != (val & 0x1)) {
      throw std::runtime_error("Invalid value");
    }
    if (val) {
      value |= uint32_t(1) << field;
    } else {
      value &= ~(uint32_t(1) << field);
    }
  }

  uint32_t value;
};


#endif /* OCPI_RCC_WORKER_DRC_X310_UBX160_FIELD_HH__ */