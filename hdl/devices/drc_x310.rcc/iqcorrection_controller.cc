// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "iqcorrection_controller.hh"
#include "slaves/iq_correction_xs_slave.hh"


IQCorrectionController::IQCorrectionController(
  bool is_rx,
  bool apply_calib,
  std::string calib_path,
  std::string calib_base,
  std::shared_ptr<IQCorrectionXsInterface> slave):
  m_slave(slave),
  m_is_rx(is_rx),
  m_apply_calib(apply_calib),
  m_calib_path(calib_path),
  m_calib_base(calib_base)
{
}


RCCResult IQCorrectionController::start()
{
  return RCC_OK;
}


RCCResult IQCorrectionController::enable_correction(bool enable)
{
  m_slave->set_enable(enable);
  return RCC_OK;
}


RCCResult IQCorrectionController::set_tx_correction(double freq_hz)
{
  RCCResult result;
  double i_corr, q_corr;

  if (m_apply_calib && !m_is_rx)
  {
    // Perform DC correction
    std::string calib_dc_fn = m_calib_path + "tx_dc_" + m_calib_base + ".csv";

    result = get_correction(calib_dc_fn, freq_hz, &i_corr, &q_corr);
    if (result == RCC_OK)
    {
      // scale and convert to signed 16-bit
      int16_t i_dc = (int16_t)round(i_corr * 32768.0);
      log(8, "DRC_X310: DC CORRECTION (I) %d", i_dc);
      m_slave->set_I_dc(i_dc);

      int16_t q_dc = (int16_t)round(q_corr * 32768.0);
      log(8, "DRC_X310: DC CORRECTION (Q) %d", q_dc);
      m_slave->set_Q_dc(q_dc);
    }

    // Perform IQ correction
    std::string calib_iq_fn = m_calib_path + "tx_iq_" + m_calib_base + ".csv";

    result = get_correction(calib_iq_fn, freq_hz, &i_corr, &q_corr);
    if (result == RCC_OK)
    {
      // scale and convert to signed 16-bit
      int16_t a = (int16_t)round(i_corr * 4096.0) + 4096;
      log(8, "DRC_X310: IQ CORRECTION (A) %d", a);
      m_slave->set_A(a);

      int16_t b = (int16_t)round(q_corr * 4096.0);
      log(8, "DRC_X310: IQ CORRECTION (B) %d", b);
      m_slave->set_B(b);
    }
  }

  return RCC_OK;
}


RCCResult IQCorrectionController::set_calib_path_base(std::string calib_path, std::string calib_base)
{
  m_calib_path = calib_path;
  m_calib_base = calib_base;
  return RCC_OK;
}


RCCResult IQCorrectionController::set_rx_correction(double freq_hz)
{
  RCCResult result;
  double i_corr, q_corr;

  if (m_apply_calib && m_is_rx)
  {
    // DC Correction is not required on the receive path
    m_slave->set_I_dc(0);
    m_slave->set_Q_dc(0);

    // Perform IQ correction
    std::string calib_iq_fn = m_calib_path + "rx_iq_" + m_calib_base + ".csv";

    result = get_correction(calib_iq_fn, freq_hz, &i_corr, &q_corr);
    if (result == RCC_OK)
    {
      // scale and convert to signed 16-bit
      int16_t a = (int16_t)round(i_corr * 4096.0) + 4096;
      log(8, "DRC_X310: IQ CORRECTION (A) %d", a);
      m_slave->set_A(a);

      int16_t b = (int16_t)round(q_corr * 4096.0);
      log(8, "DRC_X310: IQ CORRECTION (B) %d", b);
      m_slave->set_B(b);
    }
  }

  return RCC_OK;
}


RCCResult IQCorrectionController::get_correction(std::string filename, double freq, double* i, double* q)
{
  std::fstream fs;
  std::string line;
  calibration_t calib_lte = {freq, 0.0, 0.0};
  calibration_t calib_gt  = {freq, 0.0, 0.0};

  // open the calibration file
  fs.open(filename, std::ios::in);

  if (!fs.is_open())
  {
    log(0, "WARNING: file open failed %s", filename.c_str());
    return RCC_ERROR;
  }

  // discard the first lines of the calibration file
  while (getline(fs, line))
  {
    if (line.rfind("DATA STARTS HERE") != std::string::npos)
    {
      break;
    }
  }

  // discard the column names
  if (!fs.eof())
  {
    (void)getline(fs, line);
  }

  // read the calibration data
  while (getline(fs, line))
  {
    if (parse_calibration(line, &calib_gt) != RCC_OK)
    {
      fs.close();
      return RCC_ERROR;
    }

    if (calib_gt.freq > freq)
    {
      break;
    }
    else
    {
      calib_lte = calib_gt;
    }
  }

  // close the file
  fs.close();

  // interpolate between the rows
  return interp_correction(calib_lte, calib_gt, freq, i, q);
}


RCCResult IQCorrectionController::parse_calibration(std::string line, calibration_t* calib)
{
  const std::string DELIMITER = ",";
  size_t start = line.find_first_not_of(DELIMITER);
  size_t end = start;
  std::vector<std::string> columns;

  // split into columns
  while (start != std::string::npos)
  {
    // Find next occurence of delimiter
    end = line.find(DELIMITER, start);

    // Push back the token found into vector
     columns.push_back(line.substr(start, end-start));

    // Skip all occurences of the delimiter to find new start
    start = line.find_first_not_of(DELIMITER, end);
  }

  // check we have enough data
  if (columns.size() < 3)
  {
    return RCC_ERROR;
  }

  // return the calibration data as doubles
  calib->freq            = std::stod(columns[0]);
  calib->correction_real = std::stod(columns[1]);
  calib->correction_imag = std::stod(columns[2]);

  return RCC_OK;
}


RCCResult IQCorrectionController::interp_correction(calibration_t lte, calibration_t gt, double freq, double* i, double* q)
{
  if ((freq < lte.freq) || (freq > gt.freq) || (lte.freq >= gt.freq))
  {
    return RCC_ERROR;
  }

  *q = (((gt.correction_imag - lte.correction_imag) * (freq - lte.freq)) / (gt.freq - lte.freq)) + lte.correction_imag;
  *i = (((gt.correction_real - lte.correction_real) * (freq - lte.freq)) / (gt.freq - lte.freq)) + lte.correction_real;

  return RCC_OK;
}


RCCResult IQCorrectionController::set_correction(double I_dc, double Q_dc, double A, double B)
{
  int16_t i_dc_ = (int16_t)round(I_dc * 32768.0);
  log(8, "DRC_X310: DC CORRECTION (I) %d", i_dc_);
  m_slave->set_I_dc(i_dc_);

  int16_t q_dc_ = (int16_t)round(Q_dc * 32768.0);
  log(8, "DRC_X310: DC CORRECTION (Q) %d", q_dc_);
  m_slave->set_Q_dc(q_dc_);

  int16_t a_ = (int16_t)round(A * 4096.0) + 4096;
  log(8, "DRC_X310: IQ CORRECTION (A) %d", a_);
  m_slave->set_A(a_);

  int16_t b_ = (int16_t)round(B * 4096.0);
  log(8, "DRC_X310: IQ CORRECTION (B) %d", b_);
  m_slave->set_B(b_);

  return RCC_OK;
}
