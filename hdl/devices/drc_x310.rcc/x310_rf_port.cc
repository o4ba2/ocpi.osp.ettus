// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "x310_rf_port.hh"
#include "OcpiDebugApi.hh"


OCPI::RCC::RCCResult get_port(std::string port_name, uint8_t port_num, Port &port)
{
  if (!port_name.empty()) {
    if (port_name.compare("TX/RX_A") == 0) {
      port = Port::TXRX_A;
    } else if (port_name.compare("RX2_A") == 0) {
      port = Port::RX2_A;
    } else if (port_name.compare("TX/RX_B") == 0) {
      port = Port::TXRX_B;
    } else if (port_name.compare("RX2_B") == 0) {
      port = Port::RX2_B;
    } else if (port_name.compare("CAL_A") == 0) {
      port = Port::CAL_A;
    } else if (port_name.compare("CAL_B") == 0) {
      port = Port::CAL_B;
    } else {
      OCPI::OS::Log::print(OCPI_LOG_BAD, "Port name not recognised: %s", port_name.c_str());
      return OCPI::RCC::RCC_ERROR;
    }
  } else {
    if (port_num == 0) {
      port = Port::TXRX_A;
    } else if (port_num == 1) {
      port = Port::RX2_A;
    } else if (port_num == 2) {
      port = Port::TXRX_B;
    } else if (port_num == 3) {
      port = Port::RX2_B;
    } else if (port_num == 4) {
      port = Port::CAL_A;
    } else if (port_num == 5) {
      port = Port::CAL_B;
    } else {
      OCPI::OS::Log::print(OCPI_LOG_BAD, "Port number not recogised: %u", port_num);
      return OCPI::RCC::RCC_ERROR;
    }
  }

  return OCPI::RCC::RCC_OK;
};

std::string port_to_string(Port port)
{
  switch (port)
  {
    case Port::TXRX_A: return std::string("TX/RX_A");
    case Port::RX2_A:  return std::string("RX2_A");
    case Port::TXRX_B: return std::string("TX/RX_B");
    case Port::RX2_B:  return std::string("RX2_B");
    case Port::CAL_A:  return std::string("CAL_A");
    case Port::CAL_B:  return std::string("CAL_B");
    default:           return std::string("Unknown");
  }
}
