// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>
#include <thread>
#include "ad9146_controller.hh"
#include "slaves/ad9146_slave.hh"

using namespace OCPI::RCC;


static const uint32_t LOCK_TIMEOUT_MS = 1000;
static const bool DAC_RETRY_BACKEND_SYNC = false;


Ad9146Controller::Ad9146Controller(std::shared_ptr<Ad9146Interface> slave) :
  m_slave(slave),
  m_system_ref_rate(200e6)
{
}


RCCResult Ad9146Controller::start()
{
  // Power up all DAC subsystems
  m_slave->set_power_control(0x10); // Up: I DAC, Q DAC, Receiver, Voltage Ref, Clocks
  m_slave->set_tx_enable_control(0x00); // No extended delays. Up: Voltage Ref, PLL, DAC, FIFO, Filters

  // ADI recommendations:
  //- soft reset the chip before configuration
  //- put the chip in sleep mode during configuration and wake it up when done
  //- configure synchronization settings when sleeping
  RCCResult res = soft_reset();
  if (res != RCC_OK) {
    return res;
  }

  res = sleep_mode(true);
  if (res != RCC_OK) {
    return res;
  }

  res = init();
  if (res != RCC_OK) {
    return res;
  }

  // We run backend sync regardless of whether we need to sync multiple DACs
  // because we use the internal DAC FIFO to meet system synchronous timing
  // and we need to guarantee that the FIFO is not empty.
  res = backend_sync();
  if (res != RCC_OK) {
    return res;
  }

  res = sleep_mode(false);
  if (res != RCC_OK) {
    return res;
  }

  log(9, "AD9146 Proxy: Started");
  return RCC_OK;
}


RCCResult Ad9146Controller::set_tx_dac_enable(bool enable)
{
  m_slave->set_enable(enable);
  return RCC_OK;
}


RCCResult Ad9146Controller::init()
{
  m_slave->set_datapath_config(0x01); // Datasheet: "Set 1 for proper operation"
  m_slave->set_event_flag_0(0xFF); // Clear all event flags

  // Calculate N0 to be VCO friendly.
  // Aim for VCO between 1 and 2GHz, assert otherwise.
  const int N1 = 4;
  int N0_val, N0;
  for (N0_val = 0; N0_val < 3; N0_val++) {
    N0 = (1 << N0_val); // 1, 2, 4
    if ((m_system_ref_rate * N0 * N1) >= 1e9) {
      break;
    }
  }
  if ((m_system_ref_rate * N0 * N1) < 1e9) {
    log(0,
        "AD9146: Couldn't compute rate above 1GHZ, m_system_ref_rate=%f, N0=%d, N1=%d, total=%f",
        m_system_ref_rate,
        N0,
        N1,
        m_system_ref_rate * N0 * N1);
    return RCC_ERROR;
  }
  if ((m_system_ref_rate * N0 * N1) > 2e9) {
    log(0,
        "AD9146: Couldn't compute rate below 2GHZ, m_system_ref_rate=%f, N0=%d, N1=%d, total=%f",
        m_system_ref_rate,
        N0,
        N1,
        m_system_ref_rate * N0 * N1);
    return RCC_ERROR;
  }

  // Start PLL
  m_slave->set_event_flag_0(0xC0); // Clear PLL event flags
  m_slave->set_pll_control_1(0xD1); // Narrow PLL loop filter, Midrange charge pump.
  m_slave->set_pll_control_2(0xD1 | (N0_val << 2)); // N1=4, N2=16, N0 as calculated
  m_slave->set_pll_control_0(0xCF); // Auto init VCO band training as per datasheet
  m_slave->set_pll_control_0(0xA0); // See above.

  RCCResult res = check_pll();
  if (res != RCC_OK) {
    return res;
  }

  // Configure digital interface settings
  // Bypass DCI delay. We center the clock edge in the data
  // valid window in the FPGA by phase shifting the DCI going
  // to the DAC.
  m_slave->set_dci_delay(0x04);
  // 2's comp, I first, byte wide interface
  m_slave->set_data_format(0x00);
  // FPGA wants I,Q in the sample word:
  // - First transaction goes into low bits
  // - Second transaction goes into high bits
  //   therefore, we want Q to go first (bit 6 == 1)
  m_slave->set_data_format(1 << 6); // 2s comp, Q first, byte mode

  // Configure interpolation filters
  m_slave->set_hb1_control(0x00); // Configure HB1
  m_slave->set_hb2_control(0x00); // Configure HB2
  m_slave->set_datapath_control(0xE4); // Bypass: Modulator, InvSinc, IQ Bal

  // Disable sync mode by default (may get turned on later)
  m_slave->set_sync_control_0(0x40); // Disable SYNC mode.

  return RCC_OK;
}


RCCResult Ad9146Controller::backend_sync()
{
  m_slave->set_sync_control_0(0x40); // Disable SYNC mode to reset state machines.

  // SYNC Settings:
  //- SYNC = Enabled
  //- Data Rate Mode: Synchronize at the rate at which data is consumed and not at
  //                  the granularity of the FIFO
  //- Falling edge sync: For the X300, DACCLK is generated using RefClk. Within the
  //                     DAC, the RefClk is sampled by DACCLK to sync interpolation
  //                     stages across multiple DACs. To ensure that we capture the
  //                     RefClk when it is not transitioning, we sample on the
  //                     falling edge of DACCLK
  //- Averaging = MAX
  m_slave->set_sync_control_0(0xC7); // Enable SYNC mode. Falling edge sync. Averaging set to 128.

  // Wait for backend SYNC state machine to lock before proceeding. This guarantees
  // that the inputs and output of the FIFO have synchronized clocks
  RCCResult res = check_dac_sync();
  if (res != RCC_OK) {
    return res;
  }

  // FIFO write pointer offset
  // One of ADI's requirements to use data-rate synchronization in PLL mode is to
  // meet setup and hold times for RefClk -> DCI clock which we *do not* currently
  // meet in the FPGA. The DCI clock reaches a full RefClk cycle later which results
  // in the FIFO popping before the first push. This results in a steady-state FIFO
  // fullness of pointer - 1. To reach the optimal FIFO fullness of 4 we set the
  // pointer to 5.
  // FIXME: At some point we should meet timing on this interface
  m_slave->set_fifo_control(0x05);

  // We are requesting a soft FIFO align just to put the FIFO
  // in a known state. The FRAME will actually sync the
  // FIFO correctly when a stream is created
  m_slave->set_fifo_status_0(0x02); // Request soft FIFO align
  m_slave->set_fifo_status_0(0x00); // (See above)

  return RCC_OK;
}


RCCResult Ad9146Controller::check_pll()
{
  // Clear PLL event flags
  m_slave->set_event_flag_0(0xC0);

  // Verify PLL is Locked..
  // NOTE: Data sheet inconsistent about which pins give PLL lock status. FIXME!
  const auto exit_time = std::chrono::steady_clock::now() + std::chrono::milliseconds(LOCK_TIMEOUT_MS);
  while (true) {
    const size_t reg_e = m_slave->get_pll_status_0(); // PLL Status (Expect bit 7 = 1)
    const size_t reg_6 = m_slave->get_event_flag_0(); // Event Flags (Expect bit 7 = 0 and bit 6 = 1)
    if ((((reg_e >> 7) & 0x1) == 0x1) &&
        (((reg_6 >> 6) & 0x3) == 0x1)) {
      break;
    }
    if (exit_time < std::chrono::steady_clock::now()) {
      log(0, "AD9146: timeout waiting for DAC PLL to lock");
      return RCC_ERROR;
    }
    if (reg_6 & (1 << 7)) {// Lock lost?
      m_slave->set_event_flag_0(0xC0); // Clear PLL event flags
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  return RCC_OK;
}


RCCResult Ad9146Controller::check_dac_sync()
{
  // Clear Sync event flags
  m_slave->set_event_flag_0(0x30);
  m_slave->set_sync_status_0(0x00);

  const auto exit_time = std::chrono::steady_clock::now() + std::chrono::milliseconds(LOCK_TIMEOUT_MS);
  while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1)); // wait for sync to complete
    const size_t reg_12 = m_slave->get_sync_status_0(); // Sync Status (Expect bit 7 = 0, bit 6 = 1)
    const size_t reg_6 = m_slave->get_event_flag_0(); // Event Flags (Expect bit 5 = 0 and bit 4 = 1)
    if ((((reg_12 >> 6) & 0x3) == 0x1) &&
        (((reg_6 >> 4) & 0x3) == 0x1)) {
      break;
    }
    if (exit_time < std::chrono::steady_clock::now()) {
      log(0, "AD9146: timeout waiting for backend synchronization");
      return RCC_ERROR;
    }
    if (reg_6 & (1 << 5)) {
      m_slave->set_event_flag_0(0x30); // Clear Sync event flags
    }

    if (DAC_RETRY_BACKEND_SYNC) {
      if (reg_12 & (1 << 7)) { // Sync acquired and lost?
        m_slave->set_sync_control_0(0xC7); // Enable SYNC mode. Falling edge sync. Averaging set to 128.
        m_slave->set_sync_status_0(0x00); // Clear Sync event flags
      }
    }
  }

  return RCC_OK;
}


RCCResult Ad9146Controller::check_frontend_sync(bool failure_is_fatal)
{
  // Register 0x19 has a thermometer indicator of the FIFO depth
  const size_t reg_19 = m_slave->get_fifo_status_1();
  if ((reg_19 & 0xFF) != 0xF) {
    log(0,
        "AD9146: front-end sync failed. unexpected FIFO depth [0x%lx]",
        (reg_19 & 0xFF));
    if (failure_is_fatal) {
      return RCC_ERROR;
    }
  }

  return RCC_OK;
}


RCCResult Ad9146Controller::sleep_mode(bool sleep)
{
  uint8_t sleep_val = sleep ? (1 << 7) : 0x00;
  // Set sleep word and default fullscale value
  m_slave->set_i_dac_control(sleep_val | 0x01); // I DAC
  m_slave->set_q_dac_control(sleep_val | 0x01); // Q DAC

  return RCC_OK;
}


RCCResult Ad9146Controller::soft_reset()
{
  m_slave->set_comm(0x20); // Take DAC into reset.
  m_slave->set_comm(0x80); // Enable SPI reads and come out of reset

  return RCC_OK;
}
