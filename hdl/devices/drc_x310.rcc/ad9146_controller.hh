// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_AD9146_CONTROLLER_HH__
#define OCPI_RCC_WORKER_DRC_X310_AD9146_CONTROLLER_HH__

#include <memory>
#include "RCC_Worker.hh"
#include "device_base.hh"


using OCPI::RCC::RCCResult;


// Forward declarations
class Ad9146Interface;


class Ad9146Controller : DeviceControllerBase
{
public:
  Ad9146Controller(std::shared_ptr<Ad9146Interface> slave);

  // Reset and configure the DAC
  RCCResult start();

  // Enable or disable the device worker input over the dev signals
  RCCResult set_tx_dac_enable(bool enable);

private:
  RCCResult init();
  RCCResult backend_sync();
  RCCResult check_pll();
  RCCResult check_dac_sync();
  RCCResult check_frontend_sync(bool failure_is_fatal);
  RCCResult sleep_mode(bool sleep);
  RCCResult soft_reset();

  std::shared_ptr<Ad9146Interface> m_slave;

  double m_system_ref_rate;
};


#endif /* OCPI_RCC_WORKER_DRC_X310_AD9146_CONTROLLER_HH__ */
