#ifndef OCPI_RCC_WORKER_DRC_X310_LIMITS_HH__
#define OCPI_RCC_WORKER_DRC_X310_LIMITS_HH__

// constraints that do not have an associated property
constexpr double DRC_X310_MIN_TX_GAIN_DB = 0.0;
constexpr double DRC_X310_MAX_TX_GAIN_DB = 31.5;

constexpr double DRC_X310_MIN_RX_ANALOGUE_GAIN_DB = 0.0;
constexpr double DRC_X310_MAX_RX_ANALOGUE_GAIN_DB = 31.5;

constexpr double DRC_X310_MIN_RX_ADC_GAIN_DB = 0.0;
constexpr double DRC_X310_MAX_RX_ADC_GAIN_DB = 6.0;

constexpr double DRC_X310_MIN_TUNING_FREQ_MHZ = 50.0;
constexpr double DRC_X310_MAX_TUNING_FREQ_MHZ = 6000.0;

#endif /* OCPI_RCC_WORKER_DRC_X310_LIMITS_HH__ */
