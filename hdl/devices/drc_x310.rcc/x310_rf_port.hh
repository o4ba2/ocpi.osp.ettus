// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_RF_PORT_HH__
#define OCPI_RCC_WORKER_DRC_X310_RF_PORT_HH__

#include "RCC_Worker.hh"


// A is DB0, B is DB1
enum class Port {
  TXRX_A,
  RX2_A,
  TXRX_B,
  RX2_B,
  CAL_A,
  CAL_B,
};

enum class Direction {
  RX,
  TX,
};


OCPI::RCC::RCCResult get_port(std::string port_name, uint8_t port_num, Port &port);

std::string port_to_string(Port port);


#endif /* OCPI_RCC_WORKER_DRC_X310_RF_PORT_HH__ */
