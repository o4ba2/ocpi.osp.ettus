// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "drc_x310-worker.hh"
#include "x310_rf_port.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Drc_x310WorkerTypes;

#include "OcpiDrcProxyApi.hh"
namespace OD = OCPI::DRC;

#include "drc_x310_limits.hh"
#include "lmk04816_controller.hh"
#include "ad9146_controller.hh"
#include "ads62p48_controller.hh"
#include "ubx160_controller.hh"
#include "eeprom_controller.hh"
#include "iqcorrection_controller.hh"
#include "daughter_board.hh"

#include "slaves/ad9146_slave.hh"
#include "slaves/ads62p48_slave.hh"
#include "slaves/iq_correction_xs_slave.hh"
#include "slaves/lmk04816_slave.hh"
#include "slaves/max2871_slave.hh"
#include "slaves/ubx160_cpld_slave.hh"
#include "slaves/ubx160_eeprom_slave.hh"
#include "slaves/ubx160_io_slave.hh"

#include <map>

class Drc_x310Worker : public OD::DrcProxyBase
{
public:
  Drc_x310Worker()
  {
    log(8, "DRC_X310: CONSTRUCTOR");

    // eeprom controller
    m_eeprom = std::make_shared<EepromController>(std::make_shared<Ubx160EepromSlave<Slave2>>(slaves.ubx160_eeprom));

    // clock generator
    m_lmk04816 = std::make_shared<Lmk04816Controller>(std::make_shared<Lmk04816Slave<Slave1>>(slaves.lmk04816), m_eeprom);

    // daughter board 0, if present
#if (OCPI_PARAM_drc_x310_db0_is_present())
    log(8, "DRC_X310: CREATED - DB0 is present");

    m_daughter_board.push_back(
      std::make_shared<DaughterBoard>(
        std::make_shared<Ad9146Controller>(std::make_shared<Ad9146Slave<Slave3>>(slaves.db0_ad9146)),
        std::make_shared<Ads62p48Controller>(std::make_shared<Ads62p48Slave<Slave4>>(slaves.db0_ads62p48)),
        std::make_shared<IQCorrectionController>(
          true,
          OCPI_PARAM_drc_x310_calib_enable(),
          // Properties havn't been initialised here so these two strings
          // are empty.  Set them in start().
          std::string(m_properties.calib_path),
          std::string(m_properties.calib_db0_base),
          std::make_shared<IQCorrectionXsSlave<Slave13>>(slaves.db0_rx_iq_corrector)
        ),
        std::make_shared<IQCorrectionController>(
          false,
          OCPI_PARAM_drc_x310_calib_enable(),
          // Properties havn't been initialised here so these two strings
          // are empty.  Set them in start().
          std::string(m_properties.calib_path),
          std::string(m_properties.calib_db0_base),
          std::make_shared<IQCorrectionXsSlave<Slave14>>(slaves.db0_tx_iq_corrector)
        ),
        std::make_shared<Ubx160Controller>(
          0,
          std::make_shared<Ubx160IoSlave<Slave5>>(slaves.db0_ubx160_io),
          std::make_shared<Ubx160CpldSlave<Slave6>>(slaves.db0_ubx160_cpld),
          std::make_shared<Max2871Slave<Slave7>>(slaves.db0_max2871_RXLO1),
          std::make_shared<Max2871Slave<Slave8>>(slaves.db0_max2871_RXLO2),
          std::make_shared<Max2871Slave<Slave9>>(slaves.db0_max2871_TXLO1),
          std::make_shared<Max2871Slave<Slave10>>(slaves.db0_max2871_TXLO2),
          m_eeprom
        )
      )
    );
#endif

    // daughter board 1, if present
#if (OCPI_PARAM_drc_x310_db1_is_present())
    log(8, "DRC_X310: CREATED - DB1 is present");

    m_daughter_board.push_back(
      std::make_shared<DaughterBoard>(
        std::make_shared<Ad9146Controller>(std::make_shared<Ad9146Slave<Slave15>>(slaves.db1_ad9146)),
        std::make_shared<Ads62p48Controller>(std::make_shared<Ads62p48Slave<Slave16>>(slaves.db1_ads62p48)),
        std::make_shared<IQCorrectionController>(
          true,
          OCPI_PARAM_drc_x310_calib_enable(),
          // Properties havn't been initialised here so these two strings
          // are empty.  Set them in start().
          std::string(m_properties.calib_path),
          std::string(m_properties.calib_db1_base),
          std::make_shared<IQCorrectionXsSlave<Slave25>>(slaves.db1_rx_iq_corrector)
        ),
        std::make_shared<IQCorrectionController>(
          false,
          OCPI_PARAM_drc_x310_calib_enable(),
          // Properties havn't been initialised here so these two strings
          // are empty.  Set them in start().
          std::string(m_properties.calib_path),
          std::string(m_properties.calib_db1_base),
          std::make_shared<IQCorrectionXsSlave<Slave26>>(slaves.db1_tx_iq_corrector)
        ),
        std::make_shared<Ubx160Controller>(
          1,
          std::make_shared<Ubx160IoSlave<Slave17>>(slaves.db1_ubx160_io),
          std::make_shared<Ubx160CpldSlave<Slave18>>(slaves.db1_ubx160_cpld),
          std::make_shared<Max2871Slave<Slave19>>(slaves.db1_max2871_RXLO1),
          std::make_shared<Max2871Slave<Slave20>>(slaves.db1_max2871_RXLO2),
          std::make_shared<Max2871Slave<Slave21>>(slaves.db1_max2871_TXLO1),
          std::make_shared<Max2871Slave<Slave22>>(slaves.db1_max2871_TXLO2),
          m_eeprom
        )
      )
    );
#endif

    // initialise the (readable) constraints property
    // it should be possible to do this in the xml but
    // this doesn't seem to work
    m_properties.constraints.resize(2);
    {
      auto &constr = m_properties.constraints.data[0];
      strcpy(constr.rf_port, "TX/RX_A");
      constr.RX = false;
      constr.tuning_freq_MHz.resize(1);
      constr.tuning_freq_MHz.data[0].min = DRC_X310_MIN_ACHIEVABLE_TX_TUNING_FREQ_MHZ;
      constr.tuning_freq_MHz.data[0].max = DRC_X310_MAX_ACHIEVABLE_TX_TUNING_FREQ_MHZ;
      constr.bandwidth_3dB_MHz.resize(1);
      constr.bandwidth_3dB_MHz.data[0].min = DRC_X310_MIN_ACHIEVABLE_TX_BANDWIDTH_3DB_MHZ;
      constr.bandwidth_3dB_MHz.data[0].max = DRC_X310_MAX_ACHIEVABLE_TX_BANDWIDTH_3DB_MHZ;
      constr.sampling_rate_Msps.resize(1);
      constr.sampling_rate_Msps.data[0].min = DRC_X310_MIN_ACHIEVABLE_TX_SAMPLING_RATE_MSPS;
      constr.sampling_rate_Msps.data[0].max = DRC_X310_MAX_ACHIEVABLE_TX_SAMPLING_RATE_MSPS;
      constr.sample_type.real = false;
      constr.sample_type.complex = true;
    }

    {
      auto &constr = m_properties.constraints.data[1];
      strcpy(constr.rf_port, "RX2_A");
      constr.RX = true;
      constr.tuning_freq_MHz.resize(1);
      constr.tuning_freq_MHz.data[0].min = DRC_X310_MIN_RX_TUNING_FREQ_MHZ;
      constr.tuning_freq_MHz.data[0].max = DRC_X310_MAX_RX_TUNING_FREQ_MHZ;
      constr.bandwidth_3dB_MHz.resize(1);
      constr.bandwidth_3dB_MHz.data[0].min = DRC_X310_MIN_RX_BANDWIDTH_3DB_MHZ;
      constr.bandwidth_3dB_MHz.data[0].max = DRC_X310_MAX_RX_BANDWIDTH_3DB_MHZ;
      constr.sampling_rate_Msps.resize(1);
      constr.sampling_rate_Msps.data[0].min = DRC_X310_MIN_RX_SAMPLING_RATE_MSPS;
      constr.sampling_rate_Msps.data[0].max = DRC_X310_MAX_RX_SAMPLING_RATE_MSPS;
      constr.sample_type.real = false;
      constr.sample_type.complex = true;
    }
  }


  RCCResult start()
  {
    RCCResult result;
    log(8, "DRC_X310: START");

    // Set the iq correction path and base strings which we couldn't do in
    // the constructor.
    for (size_t i = 0; i < m_daughter_board.size(); i++)
    {
      const OCPI::RCC::RCCChar *calib_base = (i == 0) ? m_properties.calib_db0_base : m_properties.calib_db1_base;
      result = m_daughter_board[i]->set_correction_calib_path_base(std::string(m_properties.calib_path), std::string(calib_base));
      if (result != RCC_OK) {
        return result;
      }
    }

    // start the eeprom controller
    result = m_eeprom->start();
    if (result != RCC_OK) {
      return result;
    }

    for (size_t i = 0; i < m_daughter_board.size(); i++)
    {
      // make the daughter board serial numbers accessible
      strncpy(m_properties.daughter_board.data[i].sn, (const char*)m_eeprom->db_tx_eeprom(i).serial, 64);
    }

    // Dump EEPROM contents to the OCPI log
    debug_dump_eeprom();

    return DrcProxyBase::start();
  }


  RCCResult stop()
  {
    log(8, "DRC_X310: STOP");
    RCCResult result;
    for (size_t i = 0; i < m_daughter_board.size(); i++)
    {
      result = m_daughter_board[i]->shutdown();
      if (result != RCC_OK) {
        return result;
      }
    }
    return DrcProxyBase::stop();
  }


  RCCResult release()
  {
    log(8, "DRC_X310: RELEASE");
    return DrcProxyBase::release();
  }


  // notification that prepare property has been written
  RCCResult prepare_written()
  {
    log(8, "DRC_X310: DRC prepare_written");
    return DrcProxyBase::prepare_written();
  }

  // notification that start property has been written
  RCCResult start_written()
  {
    log(8, "DRC_X310: DRC start_written");
    return DrcProxyBase::start_written();
  }


  // notification that stop property has been written
  RCCResult stop_written()
  {
    log(8, "DRC_X310: DRC stop_written");
    return DrcProxyBase::stop_written();
  }


  // notification that release property has been written
  RCCResult release_written()
  {
    log(8, "DRC_X310: DRC release_written");
    return DrcProxyBase::release_written();
  }


  // notification that report_data_loss property has been written
  RCCResult report_data_loss_written()
  {
    log(8, "DRC_X310: DRC report_data_loss_written");

    #if (OCPI_PARAM_drc_x310_db0_is_present())
          slaves.db0_data_src_qadc.set_suppress_discontinuity_opcode(!m_properties.report_data_loss);
    #endif
    #if (OCPI_PARAM_drc_x310_db1_is_present())
          slaves.db1_data_src_qadc.set_suppress_discontinuity_opcode(!m_properties.report_data_loss);
    #endif

    return RCC_OK;
  }


  // notification that status property will be read
  RCCResult status_read()
  {
    log(8, "DRC_X310: DRC status_read");
    return DrcProxyBase::status_read();
  }

  RCCResult calib_manual_written()
  {
    const struct Calib_manual* calib = m_properties.calib_manual.data;
    RCCResult result;

    log(8, "DRC_X310: DRC calib_manual written");

    for (size_t i = 0; i < m_daughter_board.size(); i++)
    {
      result = m_daughter_board[i]->set_rx_correction(calib->Enable, calib->I_dc, calib->Q_dc, calib->A, calib->B);
      if (result != RCC_OK) {
        return result;
      }
      calib++;

      result = m_daughter_board[i]->set_tx_correction(calib->Enable, calib->I_dc, calib->Q_dc, calib->A, calib->B);
      if (result != RCC_OK) {
        return result;
      }
      calib++;
    }

    return RCC_OK;
  }


  RCCResult run(bool timedout)
  {
    RCCResult result;

    //log(8, "DRC_X310: DRC run");

    // return RCC_DONE; // change this as needed for this worker to do something useful
    // return RCC_ADVANCE; when all inputs/outputs should be advanced each time "run" is called.
    // return RCC_ADVANCE_DONE; when all inputs/outputs should be advanced, and there is nothing more to do.
    // return RCC_DONE; when there is nothing more to do, and inputs/outputs do not need to be advanced.
    result = DrcProxyBase::run(timedout);

    //log(8, "DRC_X310: DRC run RETURNED %d", result);
    return result;
  }


  RCCResult get_daughter_board(const Port port, std::shared_ptr<DaughterBoard> &db)
  {
    switch (port) {
      case Port::TXRX_A: case Port::RX2_A: case Port::CAL_A:
        if (m_daughter_board.size() < 1) {
          log(OCPI_LOG_BAD, "Configuration is for db0 but this is not present");
          return RCC_ERROR;
        }
        db = m_daughter_board[0];
        break;

      case Port::TXRX_B: case Port::RX2_B: case Port::CAL_B:
        if (m_daughter_board.size() < 2) {
          log(OCPI_LOG_BAD, "Configuration is for db1 but this is not present");
          return RCC_ERROR;
        }
        db = m_daughter_board[1];
        break;

      default:
        log(OCPI_LOG_BAD, "Invalid Port Name");
        db = nullptr;
        return RCC_ERROR;
    }
    return RCC_OK;
  }


  RCCResult prepare_config(unsigned config)
  {
    log(8, "DRC_X310: DRC prepare_config: %u", config);
    return RCC_OK;
  }


  void reset_port_map()
  {
    // For each port set the list of directions in use to empty list
    m_port_map[Port::TXRX_A] = std::vector<Direction>();
    m_port_map[Port::RX2_A]  = std::vector<Direction>();
    m_port_map[Port::TXRX_B] = std::vector<Direction>();
    m_port_map[Port::RX2_B]  = std::vector<Direction>();
    m_port_map[Port::CAL_A]  = std::vector<Direction>();
    m_port_map[Port::CAL_B]  = std::vector<Direction>();
  }


  RCCResult validate_port(Port port, Direction direction, size_t debug_chno)
  {
    // Adds @direction to the list mapped to by @port and checks everything
    // is still OK.
    // Returns RCC_ERROR if too many channels use the same port.

    // Is the direction compatible with the port?  RX can be any port so only
    // check if TX.
    if (direction == Direction::TX && (port == Port::RX2_A || port == Port::RX2_B)) {
      log(OCPI_LOG_BAD, "Can't assign TX channel (chno=%zu) to RX port (%s)", debug_chno, port_to_string(port).c_str());
      return RCC_ERROR;
    }

    // Find the entry in the port list, add our direction, and check
    if (m_port_map.count(port) == 0)
    {
      log(OCPI_LOG_BAD, "Port %s not found in port map", port_to_string(port).c_str());
      return RCC_ERROR;
    }
    std::vector<Direction> & direction_list = m_port_map.at(port);
    direction_list.push_back(direction);

    switch (port)
    {
      case Port::TXRX_A:
      case Port::TXRX_B:
      case Port::RX2_A:
      case Port::RX2_B:
        // Check at most one channel is assigned to this port
        if (direction_list.size() > 1)
        {
          log(OCPI_LOG_BAD, "Can't assign more than one channel to the same physical port (%s)", port_to_string(port).c_str());
          return RCC_ERROR;
        }
        break;

      case Port::CAL_A:
      case Port::CAL_B:
        // Check number of channels assigned to this CAL port
        if (direction_list.size() > 2)
        {
          log(OCPI_LOG_BAD, "Can't assign more than two channels to CAL (%s)", port_to_string(port).c_str());
          return RCC_ERROR;
        }

        // Check directions of channels assigned to this CAL port
        if (direction_list.size() == 2 && direction_list[0] == direction_list[1])
        {
          log(OCPI_LOG_BAD, "Can't assign channels of same direction to CAL (%s)", port_to_string(port).c_str());
          return RCC_ERROR;
        }
        break;

      default:
        // Impossible
        log(OCPI_LOG_BAD, "Unknown port");
        return RCC_ERROR;
    }

    return RCC_OK;
  }


  RCCResult validate_ports()
  {
    // Checks the final m_port_map.  Ensures CAL_A and CAL_B have either 0 or
    // 2 items, not 1.
    size_t cal_size;

    cal_size = m_port_map[Port::CAL_A].size();
    if (cal_size != 0 && cal_size != 2) {
      log(OCPI_LOG_BAD, "CAL_A port only has a single channel assigned");
      return RCC_ERROR;
    }

    cal_size = m_port_map[Port::CAL_B].size();
    if (cal_size != 0 && cal_size != 2) {
      log(OCPI_LOG_BAD, "CAL_B port only has a single channel assigned");
      return RCC_ERROR;
    }

    return RCC_OK;
  }


  RCCResult start_config(unsigned config)
  {
    RCCResult result = RCC_ERROR;

    log(8, "DRC_X310: DRC start_config: %u", config);
    log(8, "DRC_X310: CONFIGURATIONS %lu", m_properties.configurations.size());


    // get the started configuration
    auto &conf = m_properties.configurations.data[config];

    // check each of the channels
    log(8, "DRC_X310: CHANNELS %lu", conf.channels.size());

    // Validate sampling rates.  They must all be the same because the LMK
    // generates a shared clock.
    if (conf.channels.size() > 0) {
      double prev_sample_rate = conf.channels.data[0].sampling_rate_Msps;
      for (size_t chno = 1; chno<conf.channels.size(); chno++)
      {
        double this_sample_rate = conf.channels.data[chno].sampling_rate_Msps;
        if (!Lmk04816Controller::frequencies_are_equal(this_sample_rate, prev_sample_rate))
        {
          log(OCPI_LOG_BAD, "All sampling rates must be the same");
          return RCC_ERROR;
        }
        prev_sample_rate = this_sample_rate;
      }
    }

    reset_port_map();

    for (size_t chno = 0; chno<conf.channels.size(); chno++)
    {
      auto &chan = conf.channels.data[chno];

      Port port;
      result = get_port(chan.rf_port_name, chan.rf_port_num, port);
      if (result != RCC_OK)
      {
        return result;
      }

      Direction direction = chan.rx ? Direction::RX : Direction::TX;
      result = validate_port(port, direction, chno);
      if (result != RCC_OK)
      {
        return result;
      }

      // get the daughter board the channel is using
      std::shared_ptr<DaughterBoard> db;
      result = get_daughter_board(port, db);
      if (result != RCC_OK)
      {
        return result;
      }
      // configure the channel
      result = config_channel(*db, chan, port);
      if (result != RCC_OK)
      {
        return result;
      }
    }

    // Validation after all channels have been processed
    result = validate_ports();
    if (result != RCC_OK) {
      return result;
    }

    // start the clock generator
    result = m_lmk04816->start();
    if (result != RCC_OK) {
      return result;
    }

    // get the actual daughter-board clock rate being used
    double dboard_clock_rate = m_lmk04816->get_dboard_clock_rate();

    // start the daughter boards
    for (size_t i = 0; i < m_daughter_board.size(); i++)
    {
      std::shared_ptr<DaughterBoard> & db = m_daughter_board[i];

      result = db->cache_dboard_clock_rate_hz(dboard_clock_rate);
      if (result != RCC_OK) {
        return result;
      }

      result = db->start();
      if (result != RCC_OK) {
        return result;
      }
    }

    // Now everything is re-configured for this daughterboard we can start
    // the FE and ADC/DAC.
    for (size_t chno = 0; chno<conf.channels.size(); chno++)
    {
      auto &chan = conf.channels.data[chno];

      Port port;
      result = get_port(chan.rf_port_name, chan.rf_port_num, port);
      if (result != RCC_OK)
      {
        return result;
      }

      std::shared_ptr<DaughterBoard> db;
      result = get_daughter_board(port, db);
      if (result != RCC_OK)
      {
        return result;
      }

      if (chan.rx)
      {
        result = db->set_rx_fe_enable(true);
        if (result != RCC_OK) {
          return result;
        }
        result = db->set_rx_adc_enable(true);
        if (result != RCC_OK) {
          return result;
        }
      }
      else
      {
        result = db->set_tx_dac_enable(true);
        if (result != RCC_OK) {
          return result;
        }
        result = db->set_tx_fe_enable(true);
        if (result != RCC_OK) {
          return result;
        }
      }
    }

    return result;
  }


  RCCResult config_channel(DaughterBoard& db, struct ConfigurationsChannels chan, Port port)
  {
    RCCResult result;
    // the clock generator is used to set the sample-rate is set in the clock rate
    if (m_lmk04816->cache_master_clock_rate(chan.sampling_rate_Msps * 1e6) != RCC_OK)
    {
      log(0, "DRC_X310: ERROR unsupported sample-rate (%f)", chan.sampling_rate_Msps);
      return RCC_ERROR;
    }

    if (chan.rx)
    {
      log(8, "DRC_X310: CHANNEL %s IS RX", chan.description);
      log(8, "DRC_X310: RX GAIN %f", chan.gain_dB);
      log(8, "DRC_X310: RX FREQ %f", chan.tuning_freq_MHz);
      log(8, "DRC_X310: RX SAMPLE RATE %f", chan.sampling_rate_Msps);

      result = db.cache_rx_gain(chan.gain_dB);
      if (result != RCC_OK) {
        return result;
      }

      result = db.cache_rx_freq_set_iq_correction(chan.tuning_freq_MHz * 1e6);
      if (result != RCC_OK) {
        return result;
      }

      result = db.cache_rx_ant(port);
      if (result != RCC_OK) {
        return result;
      }
    }
    else
    {
      log(8, "DRC_X310: CHANNEL %s IS TX", chan.description);
      log(8, "DRC_X310: TX GAIN %f", chan.gain_dB);
      log(8, "DRC_X310: TX FREQ %f", chan.tuning_freq_MHz);
      log(8, "DRC_X310: TX SAMPLE RATE %f", chan.sampling_rate_Msps);

      result = db.cache_tx_gain(chan.gain_dB);
      if (result != RCC_OK) {
        return result;
      }

      result = db.cache_tx_freq_set_iq_correction(chan.tuning_freq_MHz * 1e6);
      if (result != RCC_OK) {
        return result;
      }

      result = db.cache_tx_ant(port);
      if (result != RCC_OK) {
        return result;
      }
    }

    return RCC_OK;
  }


  RCCResult stop_config(unsigned config)
  {
    RCCResult result = RCC_ERROR;

    log(8, "DRC_X310: DRC stop_config: %u", config);

    // get the configuration to be stopped
    auto &conf = m_properties.configurations.data[config];

    // check each of the channels
    for (size_t chno=0; chno<conf.channels.size(); chno++)
    {
      auto &chan = conf.channels.data[chno];

      // get the daughter board the channel is using
      Port port;
      result = get_port(chan.rf_port_name, chan.rf_port_num, port);
      if (result != RCC_OK) {
        return result;
      }
      // get the daughter board the channel is using
      std::shared_ptr<DaughterBoard> db;
      result = get_daughter_board(port, db);
      if (result != RCC_OK)
      {
        return result;
      }
      // configure the channel
      result = stop_channel(*db, chan);
      if (result != RCC_OK)
      {
        return result;
      }
    }

    return RCC_OK;
  }


  RCCResult disable_channel(DaughterBoard& db, struct ConfigurationsChannels chan)
  {
    RCCResult result;
    if (chan.rx)
    {
      result = db.set_rx_adc_enable(false);
      if (result != RCC_OK) {
        return result;
      }
      result = db.set_rx_fe_enable(false);
      if (result != RCC_OK) {
        return result;
      }
    }
    else
    {
      result = db.set_tx_fe_enable(false);
      if (result != RCC_OK) {
        return result;
      }
      result = db.set_tx_dac_enable(false);
      if (result != RCC_OK) {
        return result;
      }
    }

    return RCC_OK;
  }


  RCCResult stop_channel(DaughterBoard& db, struct ConfigurationsChannels chan)
  {
    return disable_channel(db, chan);
  }


  RCCResult release_config(unsigned config)
  {
    RCCResult result = RCC_ERROR;

    log(8, "DRC_X310: DRC release_config: %u", config);

    // get the configuration to be stopped
    auto &conf = m_properties.configurations.data[config];

    // check each of the channels
    for (size_t chno=0; chno<conf.channels.size(); chno++)
    {
      auto &chan = conf.channels.data[chno];

      Port port;
      result = get_port(chan.rf_port_name, chan.rf_port_num, port);
      if (result != RCC_OK) {
        return result;
      }
      // get the daughter board the channel is using
      std::shared_ptr<DaughterBoard> db;
      result = get_daughter_board(port, db);
      if (result != RCC_OK)
      {
        return result;
      }
      // configure the channel
      result = release_channel(*db, chan);
      if (result != RCC_OK)
      {
        return result;
      }
    }

    return RCC_OK;
  }


  RCCResult release_channel(DaughterBoard& db, struct ConfigurationsChannels chan)
  {
    return disable_channel(db, chan);
  }


  RCCResult status_config(unsigned config)
  {
    RCCResult result;
    // this function is called after the configuration has
    // been copied into the status. these can now be over written
    // with the actual values being used.
    log(8, "DRC_X310: DRC status_config: %u", config);

    // get the configuration to be stopped
    auto &conf = m_properties.configurations.data[config];

    // check each of the channels
    for (size_t chno=0; chno<conf.channels.size(); chno++)
    {
      auto &chan = conf.channels.data[chno];

      Port port;
      result = get_port(chan.rf_port_name, chan.rf_port_num, port);
      if (result != RCC_OK) {
        return result;
      }
      // get the daughter board the channel is using
      std::shared_ptr<DaughterBoard> db;
      result = get_daughter_board(port, db);
      if (result != RCC_OK)
      {
        return result;
      }
      // configure the channel
      auto &status = m_properties.status.data[config];

      // return the actual frequency being used
      if (chan.rx)
      {
        status.channels.data[chno].tuning_freq_MHz = db->get_rx_freq() / 1e6;
      }
      else
      {
        status.channels.data[chno].tuning_freq_MHz = db->get_tx_freq() / 1e6;
      }
    }

    return RCC_OK;
  }


private:
  void debug_dump_eeprom();
  void debug_dump_eeprom(MBEepromMap const& e);
  void debug_dump_eeprom(DBEepromMap const& e);

private:
  // DeviceControllers
  std::shared_ptr<Lmk04816Controller>         m_lmk04816;
  std::shared_ptr<EepromController>           m_eeprom;
  std::vector<std::shared_ptr<DaughterBoard>> m_daughter_board;

  // State to validate ports used in config
  std::map<Port, std::vector<Direction>> m_port_map;
};


void Drc_x310Worker::debug_dump_eeprom()
{
  debug_dump_eeprom(m_eeprom->mb_eeprom());
  debug_dump_eeprom(m_eeprom->db_rx_eeprom(0));
  debug_dump_eeprom(m_eeprom->db_tx_eeprom(0));
  debug_dump_eeprom(m_eeprom->db_rx_eeprom(1));
  debug_dump_eeprom(m_eeprom->db_tx_eeprom(1));
}


void Drc_x310Worker::debug_dump_eeprom(MBEepromMap const& e)
{
  log(OCPI_LOG_INFO, "Motherboard EEPROM:");
  log(OCPI_LOG_INFO, "  revision:        %d", (int)e.revision);
  log(OCPI_LOG_INFO, "  product:         %d", (int)e.product);
  log(OCPI_LOG_INFO, "  revision_compat: %d", (int)e.revision_compat);
  log(OCPI_LOG_INFO, "  mac_addr0:       %02x:%02x:%02x:%02x:%02x:%02x",(int)e.mac_addr0[0], (int)e.mac_addr0[1], (int)e.mac_addr0[2], (int)e.mac_addr0[3], (int)e.mac_addr0[4], (int)e.mac_addr0[5]);
  log(OCPI_LOG_INFO, "  mac_addr1:       %02x:%02x:%02x:%02x:%02x:%02x", (int)e.mac_addr1[0], (int)e.mac_addr1[1], (int)e.mac_addr1[2], (int)e.mac_addr1[3], (int)e.mac_addr1[4], (int)e.mac_addr1[5]);
  log(OCPI_LOG_INFO, "  gateway:         %d.%d.%d.%d", (e.gateway >> 0) & 0xff, (e.gateway >> 8) & 0xff, (e.gateway >> 16) & 0xff, (e.gateway >> 24) & 0xff);
  for (unsigned i = 0; i < 4; i++)
  {
    log(OCPI_LOG_INFO, "  subnet[%u]:       %u.%u.%u.%u", i, (e.subnet[i] >> 0) & 0xff, (e.subnet[i] >> 8) & 0xff, (e.subnet[i] >> 16) & 0xff, (e.subnet[i] >> 24) & 0xff);
  }
  for (unsigned i = 0; i < 4; i++)
  {
    log(OCPI_LOG_INFO, "  ip_addr[%u]:      %u.%u.%u.%u", i, (e.ip_addr[i] >> 0) & 0xff, (e.ip_addr[i] >> 8) & 0xff, (e.ip_addr[i] >> 16) & 0xff, (e.ip_addr[i] >> 24) & 0xff);
  }
  log(OCPI_LOG_INFO, "  name:            %.23s", e.name);
  log(OCPI_LOG_INFO, "  serial:          %.9s", e.serial);
}


void Drc_x310Worker::debug_dump_eeprom(DBEepromMap const& e)
{
  log(OCPI_LOG_INFO, "Daughterboard EEPROM:");

  if (!e.is_valid())
  {
    log(OCPI_LOG_INFO, "  Not present");
    return;
  }

  log(OCPI_LOG_INFO, "  id:       %d", (int)e.id);
  log(OCPI_LOG_INFO, "  rev:      %d", (int)e.rev);
  log(OCPI_LOG_INFO, "  offset_0: %d", (int)e.offset_0);
  log(OCPI_LOG_INFO, "  offset_1: %d", (int)e.offset_1);
  log(OCPI_LOG_INFO, "  serial:   %.22s", e.serial);
}


DRC_X310_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
DRC_X310_END_INFO
