// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_ADS62P48_CONTROLLER_HH__
#define OCPI_RCC_WORKER_DRC_X310_ADS62P48_CONTROLLER_HH__


#include <memory>
#include "RCC_Worker.hh"
#include "device_base.hh"
#include "ads62p48_regs.hh"


using OCPI::RCC::RCCResult;


// Forward declarations
class Ads62p48Interface;


enum class Ads62p48Pattern
{
  ONES,
  ZEROS,
  CUSTOM,
  RAMP,
  NORMAL,
};


class Ads62p48Controller : DeviceControllerBase
{
public:
  Ads62p48Controller(std::shared_ptr<Ads62p48Interface> slave);

  // Reset and configure the ADC
  RCCResult start();

  // Set the ADC's internal gain
  //
  // `gain_db` must be in the range 0 to 6 dB
  RCCResult cache_gain(double gain_db);

  // Configure the ADC to transmit a test pattern
  //
  // The `a_num` and `b_num` numbers are used when the `CUSTOM` pattern is selected
  RCCResult set_test_pattern(Ads62p48Pattern pattern, uint8_t a_num = 0, uint8_t b_num = 0);

  // Enable or disable the device worker output over the dev signals
  RCCResult set_rx_adc_enable(bool enable);

private:
  // Calibrate the clock-to-data delay lines in the device worker
  RCCResult calibrate_delay_lines();
  RCCResult set_gain();

  std::shared_ptr<Ads62p48Interface> m_slave;

  ads62p48_regs_t m_ads62p48_regs;

  double m_gain_db;
};


#endif /* OCPI_RCC_WORKER_DRC_X310_ADS62P48_CONTROLLER_HH__ */
