// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_UBX160_HH__
#define OCPI_RCC_WORKER_DRC_X310_UBX160_HH__

#include "RCC_Worker.hh"
using namespace OCPI::RCC;

#include <mutex>
#include "device_base.hh"
#include "ubx160_regs.hh"
#include "x310_rf_port.hh"


// Forward declarations
class Ubx160IoInterface;
class Ubx160CpldInterface;
class Max2871Interface;
class Max2871Controller;
class EepromController;


enum class XcvrMode
{
  TX,
  RX,
  FDX,
  TDD,
};


enum class PowerMode
{
  PERFORMANCE,
  POWERSAVE,
};


enum class LoMode
{
  FRACTIONAL,
  INTEGER,
};


class Ubx160Controller : DeviceControllerBase
{
public:
  Ubx160Controller(
    size_t slot,
    std::shared_ptr<Ubx160IoInterface> ubx160_io,
    std::shared_ptr<Ubx160CpldInterface> ubx160_cpld,
    std::shared_ptr<Max2871Interface> max2871_rxlo1,
    std::shared_ptr<Max2871Interface> max2871_rxlo2,
    std::shared_ptr<Max2871Interface> max2871_txlo1,
    std::shared_ptr<Max2871Interface> max2871_txlo2,
    std::shared_ptr<EepromController> eeprom_controller);

  RCCResult start();

  RCCResult set_tx_enable(bool enable);
  RCCResult set_rx_enable(bool enable);
  RCCResult cache_dboard_clock_rate_hz(double rate_hz);
  RCCResult cache_tx_gain(double gain_db);
  RCCResult cache_rx_gain(double gain_db);
  RCCResult cache_tx_freq(double freq_hz);
  RCCResult cache_rx_freq(double freq_hz);
  RCCResult cache_rx_ant(Port port);
  RCCResult cache_tx_ant(Port port);
  double    get_tx_freq();
  double    get_rx_freq();
  RCCResult wait_lo_locked();
  RCCResult shutdown();

private:
  void set_cpld_field(ubx_cpld_field_id_t id, uint32_t value);
  void write_cpld_reg();
  RCCResult set_tx_enable();
  RCCResult set_rx_enable();
  RCCResult set_tx_gain();
  RCCResult set_rx_gain();
  RCCResult set_tx_freq();
  RCCResult set_rx_freq();
  RCCResult set_tx_ant();
  RCCResult set_rx_ant();
  RCCResult set_power_mode();
  RCCResult set_xcvr_mode();
  RCCResult cache_temp_comp_mode();

  std::shared_ptr<Ubx160IoInterface> m_ubx160_io;
  std::shared_ptr<Ubx160CpldInterface> m_ubx160_cpld;
  std::shared_ptr<Max2871Interface> m_max2871_rxlo1;
  std::shared_ptr<Max2871Interface> m_max2871_rxlo2;
  std::shared_ptr<Max2871Interface> m_max2871_txlo1;
  std::shared_ptr<Max2871Interface> m_max2871_txlo2;
  std::shared_ptr<EepromController> m_eeprom_controller;

  // Which daughter board slot is this card in -- 0 or 1
  const size_t    m_slot;

  double          m_dboard_clock_rate_hz;
  bool            m_high_isolation;
  const double    m_highest_pfd_freq_hz;

  XcvrMode        m_xcvr_mode;
  PowerMode       m_power_mode;
  bool            m_temp_comp_enabled;
  double          m_setup_time;

  bool            m_rx_enable;
  bool            m_rxlo_locked;
  double          m_rxlo_gain;
  double          m_rxlo_freq;
  LoMode          m_rxlo_mode_n;
  double          m_rxlo_int_n_step;
  double          m_rxlo_actual_frequency;
  Port            m_rx_ant;

  bool            m_tx_enable;
  bool            m_txlo_locked;
  double          m_txlo_gain;
  LoMode          m_txlo_mode_n;
  double          m_txlo_int_n_step;
  double          m_txlo_freq;
  double          m_txlo_actual_frequency;
  Port            m_tx_ant;
  bool            m_started;

  std::mutex      m_mutex;
  ubx_cpld_reg_t  m_cpld_reg;

  std::shared_ptr<Max2871Controller> m_rxlo1;
  std::shared_ptr<Max2871Controller> m_rxlo2;
  std::shared_ptr<Max2871Controller> m_txlo1;
  std::shared_ptr<Max2871Controller> m_txlo2;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_UBX160_HH__ */
