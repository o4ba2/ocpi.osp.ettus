// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <cstring>
#include <mutex>
#include <thread>

#include "drc_x310_limits.hh"
#include "ubx160_controller.hh"
#include "slaves/ubx160_io_slave.hh"
#include "slaves/ubx160_cpld_slave.hh"
#include "slaves/max2871_slave.hh"
#include "eeprom_controller.hh"
#include "max2871_controller.hh"


#define A_IgnoreFloatEqual \
    _Pragma("GCC diagnostic push") \
    _Pragma("GCC diagnostic ignored \"-Wfloat-equal\"") \

#define A_Pop \
    _Pragma("GCC diagnostic pop") \


#define fMHz (1e6)


static const uint16_t UBX_PROTO_V3_TX_ID   = 0x73;
static const uint16_t UBX_PROTO_V3_RX_ID   = 0x74;
static const uint16_t UBX_PROTO_V4_TX_ID   = 0x75;
static const uint16_t UBX_PROTO_V4_RX_ID   = 0x76;
static const uint16_t UBX_V1_40MHZ_TX_ID   = 0x77;
static const uint16_t UBX_V1_40MHZ_RX_ID   = 0x78;
static const uint16_t UBX_V1_160MHZ_TX_ID  = 0x79;
static const uint16_t UBX_V1_160MHZ_RX_ID  = 0x7A;
static const uint16_t UBX_V2_40MHZ_TX_ID   = 0x7B;
static const uint16_t UBX_V2_40MHZ_RX_ID   = 0x7C;
static const uint16_t UBX_V2_160MHZ_TX_ID  = 0x7D;
static const uint16_t UBX_V2_160MHZ_RX_ID  = 0x7E;
static const uint16_t UBX_LP_160MHZ_TX_ID  = 0x0200;
static const uint16_t UBX_LP_160MHZ_RX_ID  = 0x0201;
static const uint16_t UBX_TDD_160MHZ_TX_ID = 0x0202;
static const uint16_t UBX_TDD_160MHZ_RX_ID = 0x0203;


static double clip(double start, double stop, double value)
{
  if (value < start) {
    return start;
  }
  if (value > stop) {
    return stop;
  }
  return value;
}


Ubx160Controller::Ubx160Controller(
  size_t slot,
  std::shared_ptr<Ubx160IoInterface> ubx160_io,
  std::shared_ptr<Ubx160CpldInterface> ubx160_cpld,
  std::shared_ptr<Max2871Interface> max2871_rxlo1,
  std::shared_ptr<Max2871Interface> max2871_rxlo2,
  std::shared_ptr<Max2871Interface> max2871_txlo1,
  std::shared_ptr<Max2871Interface> max2871_txlo2,
  std::shared_ptr<EepromController> eeprom_controller
):
  m_ubx160_io(ubx160_io),
  m_ubx160_cpld(ubx160_cpld),
  m_max2871_rxlo1(max2871_rxlo1),
  m_max2871_rxlo2(max2871_rxlo2),
  m_max2871_txlo1(max2871_txlo1),
  m_max2871_txlo2(max2871_txlo2),
  m_eeprom_controller(eeprom_controller),
  m_slot(slot),
  m_dboard_clock_rate_hz(50e6),
  m_high_isolation(false),
  m_highest_pfd_freq_hz(50e6),
  m_xcvr_mode(XcvrMode::FDX),
  m_power_mode(PowerMode::PERFORMANCE),
  m_temp_comp_enabled(false),
  m_setup_time(1.0),
  m_rx_enable(false),
  m_rxlo_locked(true),
  m_rxlo_gain(10.0),
  m_rxlo_freq(2400e6),
  m_rxlo_mode_n(LoMode::FRACTIONAL),
  m_rxlo_int_n_step(50e6),
  m_rx_ant(Port::RX2_A),
  m_tx_enable(false),
  m_txlo_locked(true),
  m_txlo_gain(30.0),
  m_txlo_mode_n(LoMode::FRACTIONAL),
  m_txlo_int_n_step(50e6),
  m_txlo_freq(2400e6),
  m_tx_ant(Port::TXRX_A)
{
  log(8, "DRC_X310: Ubx160Controller constructor");
}


RCCResult Ubx160Controller::start()
{
  // Check that a daughterboard has been fitted
  if (!m_eeprom_controller->db_rx_eeprom(m_slot).is_valid())
  {
    log(OCPI_LOG_BAD, "Daughterboard not fitted in slot %zu", m_slot);
    return RCC_ERROR;
  }

  uint16_t rx_id = m_eeprom_controller->db_rx_eeprom(m_slot).id;
  uint16_t tx_id = m_eeprom_controller->db_tx_eeprom(m_slot).id;
  uint16_t revision = m_eeprom_controller->db_rx_eeprom(m_slot).rev;

  float bw = 40e6;
  int rev = 0;
  m_high_isolation = false;
  if (rx_id == UBX_PROTO_V3_RX_ID && tx_id == UBX_PROTO_V3_TX_ID)
  {
    rev = 0;
  }
  else if (rx_id == UBX_PROTO_V4_RX_ID && tx_id == UBX_PROTO_V4_TX_ID)
  {
    rev = 1;
  }
  else if (rx_id == UBX_V1_40MHZ_RX_ID && tx_id == UBX_V1_40MHZ_TX_ID)
  {
    rev = 1;
  }
  else if (rx_id == UBX_V2_40MHZ_RX_ID && tx_id == UBX_V2_40MHZ_TX_ID)
  {
    rev = 2;
    if (revision >= 4) {
        m_high_isolation = true;
    }
  }
  else if (rx_id == UBX_V1_160MHZ_RX_ID && tx_id == UBX_V1_160MHZ_TX_ID)
  {
    bw  = 160e6;
    rev = 1;
  }
  else if (rx_id == UBX_V2_160MHZ_RX_ID && tx_id == UBX_V2_160MHZ_TX_ID)
  {
    bw  = 160e6;
    rev = 2;
    if (revision >= 4) {
      m_high_isolation = true;
    }
  }
  else if (rx_id == UBX_LP_160MHZ_RX_ID && tx_id == UBX_LP_160MHZ_TX_ID)
  {
    // The LP version behaves and looks like a regular UBX-160 v2
    bw  = 160e6;
    rev = 2;
  }
  else if (rx_id == UBX_TDD_160MHZ_RX_ID && tx_id == UBX_TDD_160MHZ_TX_ID)
  {
    bw  = 160e6;
    rev = 2;
    m_high_isolation = true;
  }
  else
  {
    log(OCPI_LOG_BAD, "Invalid daughterboard in slot %zu. Expected a UBX-160", m_slot);
    return RCC_ERROR;
  }

  if (rev < 1 || rev > 2) {
    log(OCPI_LOG_BAD, "Only revisions 1 and 2 are supported. Revision is %d", rev);
    return RCC_ERROR;
  }

  A_IgnoreFloatEqual
  if (bw != 160e6) {
    log(OCPI_LOG_BAD, "Only the 160MHz bandwidth daughterboard is currently supported");
    return RCC_ERROR;
  }
  A_Pop

  if (!m_started) {
    m_started = true;
    // set default GPIO values
    m_ubx160_io->set_TX_GAIN(0);
    m_ubx160_io->set_CPLD_RST_N(0);
    m_ubx160_io->set_RX_ANT(1);

    m_ubx160_io->set_TX_EN_N(1);
    m_ubx160_io->set_RX_EN_N(1);
    m_ubx160_io->set_RX_GAIN(0);

    // hold CPLD reset for minimum of 20 ms
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    m_ubx160_io->set_CPLD_RST_N(1);  // Active LOW

    // Initialise LOs
    m_rxlo1 = std::make_shared<Max2871Controller>(m_max2871_rxlo1);
    m_rxlo2 = std::make_shared<Max2871Controller>(m_max2871_rxlo2);
    m_txlo1 = std::make_shared<Max2871Controller>(m_max2871_txlo1);
    m_txlo2 = std::make_shared<Max2871Controller>(m_max2871_txlo2);
  }

  std::vector<std::shared_ptr<Max2871Controller>> los{m_txlo1, m_txlo2, m_rxlo1, m_rxlo2};

  for (std::shared_ptr<Max2871Controller> lo : los)
  {
    lo->cache_auto_retune(false);
    // lo->cache_cycle_slip_mode(true);  // tried it - caused longer lock times
    lo->cache_charge_pump_current(Max2871Controller::CHARGE_PUMP_CURRENT_5_12MA);
    lo->cache_muxout_mode(Max2871Controller::MUXOUT_SYNC);
    lo->cache_ld_pin_mode(Max2871Controller::LD_PIN_MODE_DLD);
  }

  // Initialize CPLD register
  m_cpld_reg.value = 0;
  write_cpld_reg();

  // set default values
  RCCResult res = set_power_mode();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_power_mode returned an error (%d)", res);
    return res;
  }

  res = set_xcvr_mode();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_xcvr_mode returned an error (%d)", res);
    return res;
  }

  res = cache_temp_comp_mode();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 cache_temp_comp_mode returned an error (%d)", res);
    return res;
  }

  res = set_tx_ant();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_tx_ant returned an error (%d)", res);
    return res;
  }

  res = set_tx_gain();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_tx_gain returned an error (%d)", res);
    return res;
  }

  res = set_tx_freq();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_tx_freq returned an error (%d)", res);
    return res;
  }

  res = set_rx_ant();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_rx_ant returned an error (%d)", res);
    return res;
  }

  res = set_rx_gain();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_rx_gain returned an error (%d)", res);
    return res;
  }

  res = set_rx_freq();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_rx_freq returned an error (%d)", res);
    return res;
  }

  res = wait_lo_locked();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 wait_lo_locked returned an error (%d)", res);
    return res;
  }

  // enable the DAC output (active low)
  res = set_tx_enable();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_tx_enable returned an error (%d)", res);
    return res;
  }

  // enable the ADC (active low)
  res = set_rx_enable();
  if (res != RCC_OK)
  {
    log(OCPI_LOG_BAD, "ERROR: ubx160 set_rx_enable returned an error (%d)", res);
    return res;
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::set_tx_enable(bool enable)
{
  m_tx_enable = enable;
  return set_tx_enable();
}


RCCResult Ubx160Controller::set_rx_enable(bool enable)
{
  m_rx_enable = enable;
  return set_rx_enable();
}


RCCResult Ubx160Controller::cache_tx_gain(double gain_db)
{
  m_txlo_gain = gain_db;
  return RCC_OK;
}


RCCResult Ubx160Controller::cache_rx_gain(double gain_db)
{
  m_rxlo_gain = gain_db;
  return RCC_OK;
}


RCCResult Ubx160Controller::cache_tx_freq(double freq_hz)
{
  m_txlo_freq = freq_hz;
  return RCC_OK;
}


double Ubx160Controller::get_tx_freq()
{
  return m_txlo_actual_frequency;
}


RCCResult Ubx160Controller::cache_rx_freq(double freq_hz)
{
  m_rxlo_freq = freq_hz;
  return RCC_OK;
}


double Ubx160Controller::get_rx_freq()
{
  return m_rxlo_actual_frequency;
}


RCCResult Ubx160Controller::set_tx_ant()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  switch (m_tx_ant) {
    case Port::TXRX_A:
    case Port::TXRX_B:
      set_cpld_field(CAL_ENABLE, false);
      break;

    case Port::CAL_A:
    case Port::CAL_B:
      set_cpld_field(CAL_ENABLE, true);
      break;

    default:
      log(OCPI_LOG_BAD, "Invalid port option for TX antenna");
      return RCC_ERROR;
  }

  write_cpld_reg();
  return RCC_OK;
}


RCCResult Ubx160Controller::set_rx_ant()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  // There can be long transients on TX, so force on the TX PA
  // except when in powersave mode (to save power) or on early
  // boards that had lower TX-RX isolation when the RX antenna
  // is set to TX/RX (to prevent higher noise floor on RX).
  // Setting the xcvr_mode to TDD will force on the PA when
  // not in powersave mode regardless of the board revision.
  switch (m_rx_ant) {
    case Port::TXRX_A:
    case Port::TXRX_B:
      {
        m_ubx160_io->set_RX_ANT(0);
        // Force on TX PA for boards with high isolation or if the user sets the TDD
        // mode
        uint8_t val = 0;
        if ((m_power_mode != PowerMode::POWERSAVE) && (m_high_isolation || (m_xcvr_mode == XcvrMode::TDD))) {
          val = 1;
        }
        set_cpld_field(TXDRV_FORCEON, val);
        break;
      }

    default:
      m_ubx160_io->set_RX_ANT(1);
      set_cpld_field(TXDRV_FORCEON, (m_power_mode == PowerMode::POWERSAVE ? 0 : 1)); // Keep PA on
      break;
  }

  write_cpld_reg();
  return RCC_OK;
}


RCCResult Ubx160Controller::cache_dboard_clock_rate_hz(double rate_hz)
{
  m_dboard_clock_rate_hz = rate_hz;
  return RCC_OK;
}


RCCResult Ubx160Controller::wait_lo_locked()
{
  auto end_time = std::chrono::steady_clock::now() + std::chrono::seconds(int64_t(m_setup_time));

  while (std::chrono::steady_clock::now() < end_time)
  {
    if (m_ubx160_io->get_TX_LO_LOCKED() && m_ubx160_io->get_RX_LO_LOCKED())
    {
      return RCC_OK;
    }

    std::this_thread::sleep_for(std::chrono::microseconds(100));
  }

  log(OCPI_LOG_BAD, "Timed out waiting for consecutive locks on RX");
  return RCC_ERROR;
}


RCCResult Ubx160Controller::shutdown()
{
  log(OCPI_LOG_DEBUG, "Shutting down max workers");
  m_rxlo1->shutdown();
  m_rxlo2->shutdown();
  m_txlo1->shutdown();
  m_txlo2->shutdown();
  return RCC_OK;
}


void Ubx160Controller::set_cpld_field(ubx_cpld_field_id_t id, uint32_t value)
{
  m_cpld_reg.set_field(id, value);
}


void Ubx160Controller::write_cpld_reg()
{
  m_ubx160_cpld->set_cpld_reg(m_cpld_reg.value);
}


/***********************************************************************
* Gain Handling
**********************************************************************/


RCCResult Ubx160Controller::set_tx_gain()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  double gain = clip(DRC_X310_MIN_TX_GAIN_DB, DRC_X310_MAX_TX_GAIN_DB, m_txlo_gain);
  int attn_code = int(std::floor(gain * 2.0));
  unsigned ubx_tx_atten_val = ((attn_code & 0x3F) << 10);
  m_ubx160_io->set_TX_GAIN(attn_code);

  log(OCPI_LOG_DEBUG, "UBX TX Gain: %f dB, Code: %d, IO Bits 0x%04x",
      gain, attn_code, ubx_tx_atten_val);

  return RCC_OK;
}


RCCResult Ubx160Controller::set_rx_gain()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  double gain = clip(DRC_X310_MIN_RX_ANALOGUE_GAIN_DB, DRC_X310_MAX_RX_ANALOGUE_GAIN_DB, m_rxlo_gain);
  int attn_code = int(std::floor(gain * 2.0));
  unsigned ubx_rx_atten_val = ((attn_code & 0x3F) << 10);
  m_ubx160_io->set_RX_GAIN(attn_code);

  log(OCPI_LOG_DEBUG, "UBX RX Gain: %f dB, Code: %d, IO Bits 0x%04x",
      gain, attn_code, ubx_rx_atten_val);

  return RCC_OK;
}


/***********************************************************************
* Frequency Handling
**********************************************************************/


RCCResult Ubx160Controller::set_tx_freq()
{
  std::lock_guard<std::mutex> lock(m_mutex);
  double freq_lo1 = 0.0;
  double freq_lo2 = 0.0;
  double ref_freq = m_dboard_clock_rate_hz;
  bool is_int_n = false;

  /*
    * If the user sets 'mode_n=integer' in the tuning args, the user wishes to
    * tune in Integer-N mode, which can result in better spur
    * performance on some mixers. The default is fractional tuning.
    */
  log(OCPI_LOG_DEBUG, "UBX TX: the requested frequency is %f MHz",
      (m_txlo_freq / 1e6));

  double target_pfd_freq = m_highest_pfd_freq_hz;
  is_int_n = m_txlo_mode_n == LoMode::INTEGER;
  if (is_int_n) {
    target_pfd_freq = m_txlo_int_n_step;
    if (target_pfd_freq > m_highest_pfd_freq_hz) {
      log(OCPI_LOG_DEBUG,
          "Requested int_n_step of %f MHz too large, clipping to %f MHz",
          (target_pfd_freq / 1e6), (m_highest_pfd_freq_hz / 1e6));
      target_pfd_freq = m_highest_pfd_freq_hz;
    }
  }

  // Clip the frequency to the valid range
  double freq = clip(DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6, DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6, m_txlo_freq);

  // Power up/down LOs
  if (m_txlo1->is_shutdown()) {
    m_txlo1->power_up();
  }
  if (m_txlo2->is_shutdown() &&
      (m_power_mode == PowerMode::PERFORMANCE || freq < (500 * fMHz))) {
    m_txlo2->power_up();
  } else if (freq >= 500 * fMHz && m_power_mode == PowerMode::POWERSAVE) {
    m_txlo2->shutdown();
  }

  m_txlo1->config_for_sync(false);
  if (not m_txlo2->is_shutdown()) {
    m_txlo2->config_for_sync(false);
  }

  // Set up registers for the requested frequency
  if (freq < (500 * fMHz)) {
    set_cpld_field(TXLO1_FSEL3, 0);
    set_cpld_field(TXLO1_FSEL2, 1);
    set_cpld_field(TXLO1_FSEL1, 0);
    set_cpld_field(TXLB_SEL, 1);
    set_cpld_field(TXHB_SEL, 0);
    // Set LO1 to IF of 2100 MHz (offset from RX IF to reduce leakage)
    freq_lo1 = m_txlo1->cache_frequency(2100 * fMHz, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
    // Set LO2 to IF minus desired frequency
    freq_lo2 = m_txlo2->cache_frequency(freq_lo1 - freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo2->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= (500 * fMHz)) && (freq <= (800 * fMHz))) {
    set_cpld_field(TXLO1_FSEL3, 0);
    set_cpld_field(TXLO1_FSEL2, 0);
    set_cpld_field(TXLO1_FSEL1, 1);
    set_cpld_field(TXLB_SEL, 0);
    set_cpld_field(TXHB_SEL, 1);
    freq_lo1 = m_txlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq > (800 * fMHz)) && (freq <= (1000 * fMHz))) {
    set_cpld_field(TXLO1_FSEL3, 0);
    set_cpld_field(TXLO1_FSEL2, 0);
    set_cpld_field(TXLO1_FSEL1, 1);
    set_cpld_field(TXLB_SEL, 0);
    set_cpld_field(TXHB_SEL, 1);
    freq_lo1 = m_txlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
  } else if ((freq > (1000 * fMHz)) && (freq <= (2200 * fMHz))) {
    set_cpld_field(TXLO1_FSEL3, 0);
    set_cpld_field(TXLO1_FSEL2, 1);
    set_cpld_field(TXLO1_FSEL1, 0);
    set_cpld_field(TXLB_SEL, 0);
    set_cpld_field(TXHB_SEL, 1);
    freq_lo1 = m_txlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq > (2200 * fMHz)) && (freq <= (2500 * fMHz))) {
    set_cpld_field(TXLO1_FSEL3, 0);
    set_cpld_field(TXLO1_FSEL2, 1);
    set_cpld_field(TXLO1_FSEL1, 0);
    set_cpld_field(TXLB_SEL, 0);
    set_cpld_field(TXHB_SEL, 1);
    freq_lo1 = m_txlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq > (2500 * fMHz)) && (freq <= (6000 * fMHz))) {
    set_cpld_field(TXLO1_FSEL3, 1);
    set_cpld_field(TXLO1_FSEL2, 0);
    set_cpld_field(TXLO1_FSEL1, 0);
    set_cpld_field(TXLB_SEL, 0);
    set_cpld_field(TXHB_SEL, 1);
    freq_lo1 = m_txlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_txlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
  }

  // commit the settings
  write_cpld_reg();
  m_txlo1->commit();
  if (freq < (500 * fMHz)) {
    m_txlo2->commit();
  }

  m_txlo_actual_frequency = freq_lo1 - freq_lo2;
  log(OCPI_LOG_DEBUG, "UBX TX: the actual frequency is %f MHz",
      (m_txlo_actual_frequency / 1e6));

  return RCC_OK;
}


RCCResult Ubx160Controller::set_rx_freq()
{
  std::lock_guard<std::mutex> lock(m_mutex);
  double freq_lo1 = 0.0;
  double freq_lo2 = 0.0;
  double ref_freq = m_dboard_clock_rate_hz;
  bool is_int_n = false;

  /*
    * If the user sets 'mode_n=integer' in the tuning args, the user wishes to
    * tune in Integer-N mode, which can result in better spur
    * performance on some mixers. The default is fractional tuning.
    */
  log(OCPI_LOG_DEBUG, "UBX RX: the requested frequency is %f MHz",
      (m_rxlo_freq / 1e6));

  double target_pfd_freq = m_highest_pfd_freq_hz;
  is_int_n = m_rxlo_mode_n == LoMode::INTEGER;
  if (is_int_n) {
    target_pfd_freq = m_rxlo_int_n_step;
    if (target_pfd_freq > m_highest_pfd_freq_hz) {
      log(OCPI_LOG_DEBUG,
          "Requested int_n_step of %f MHz too large, clipping to %f MHz",
          (target_pfd_freq / 1e6), (m_highest_pfd_freq_hz / 1e6));
      target_pfd_freq = m_highest_pfd_freq_hz;
    }
  }

  // Clip the frequency to the valid range
  double freq = clip(DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6, DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6, m_rxlo_freq);

  // Power up/down LOs
  if (m_rxlo1->is_shutdown()) {
    m_rxlo1->power_up();
  }
  if (m_rxlo2->is_shutdown() &&
      (m_power_mode == PowerMode::PERFORMANCE || freq < 500 * fMHz)) {
    m_rxlo2->power_up();
  } else if (freq >= 500 * fMHz && m_power_mode == PowerMode::POWERSAVE) {
    m_rxlo2->shutdown();
  }

  m_rxlo1->config_for_sync(false);
  if (not m_rxlo2->is_shutdown()) {
    m_rxlo2->config_for_sync(false);
  }

  // Work with frequencies
  if (freq < 100 * fMHz) {
    set_cpld_field(SEL_LNA1, 0);
    set_cpld_field(SEL_LNA2, 1);
    set_cpld_field(RXLO1_FSEL3, 1);
    set_cpld_field(RXLO1_FSEL2, 0);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 1);
    set_cpld_field(RXHB_SEL, 0);
    // Set LO1 to IF of 2380 MHz (2440 MHz filter center minus 60 MHz offset to
    // minimize LO leakage)
    freq_lo1 = m_rxlo1->cache_frequency(2380 * fMHz, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
    // Set LO2 to IF minus desired frequency
    freq_lo2 = m_rxlo2->cache_frequency(freq_lo1 - freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo2->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 100 * fMHz) && (freq < 500 * fMHz)) {
    set_cpld_field(SEL_LNA1, 0);
    set_cpld_field(SEL_LNA2, 1);
    set_cpld_field(RXLO1_FSEL3, 1);
    set_cpld_field(RXLO1_FSEL2, 0);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 1);
    set_cpld_field(RXHB_SEL, 0);
    // Set LO1 to IF of 2440 (center of filter)
    freq_lo1 = m_rxlo1->cache_frequency(2440 * fMHz, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
    // Set LO2 to IF minus desired frequency
    freq_lo2 = m_rxlo2->cache_frequency(freq_lo1 - freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 500 * fMHz) && (freq < 800 * fMHz)) {
    set_cpld_field(SEL_LNA1, 0);
    set_cpld_field(SEL_LNA2, 1);
    set_cpld_field(RXLO1_FSEL3, 0);
    set_cpld_field(RXLO1_FSEL2, 0);
    set_cpld_field(RXLO1_FSEL1, 1);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 800 * fMHz) && (freq < 1000 * fMHz)) {
    set_cpld_field(SEL_LNA1, 0);
    set_cpld_field(SEL_LNA2, 1);
    set_cpld_field(RXLO1_FSEL3, 0);
    set_cpld_field(RXLO1_FSEL2, 0);
    set_cpld_field(RXLO1_FSEL1, 1);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
  } else if ((freq >= 1000 * fMHz) && (freq < 1500 * fMHz)) {
    set_cpld_field(SEL_LNA1, 0);
    set_cpld_field(SEL_LNA2, 1);
    set_cpld_field(RXLO1_FSEL3, 0);
    set_cpld_field(RXLO1_FSEL2, 1);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 1500 * fMHz) && (freq < 2200 * fMHz)) {
    set_cpld_field(SEL_LNA1, 1);
    set_cpld_field(SEL_LNA2, 0);
    set_cpld_field(RXLO1_FSEL3, 0);
    set_cpld_field(RXLO1_FSEL2, 1);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 2200 * fMHz) && (freq < 2500 * fMHz)) {
    set_cpld_field(SEL_LNA1, 1);
    set_cpld_field(SEL_LNA2, 0);
    set_cpld_field(RXLO1_FSEL3, 0);
    set_cpld_field(RXLO1_FSEL2, 1);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_2DBM);
  } else if ((freq >= 2500 * fMHz) && (freq <= 6000 * fMHz)) {
    set_cpld_field(SEL_LNA1, 1);
    set_cpld_field(SEL_LNA2, 0);
    set_cpld_field(RXLO1_FSEL3, 1);
    set_cpld_field(RXLO1_FSEL2, 0);
    set_cpld_field(RXLO1_FSEL1, 0);
    set_cpld_field(RXLB_SEL, 0);
    set_cpld_field(RXHB_SEL, 1);
    freq_lo1 = m_rxlo1->cache_frequency(freq, ref_freq, target_pfd_freq, is_int_n);
    m_rxlo1->cache_output_power(Max2871Controller::OUTPUT_POWER_5DBM);
  }

  // commit the settings
  write_cpld_reg();
  m_rxlo1->commit();
  if (freq < (500 * fMHz)) {
    m_rxlo2->commit();
  }

  m_rxlo_actual_frequency = freq_lo1 - freq_lo2;
  log(OCPI_LOG_DEBUG, "UBX RX: the actual frequency is %f MHz",
      (m_rxlo_actual_frequency / 1e6));

  return RCC_OK;
}

/***********************************************************************
* Setting Modes
**********************************************************************/

RCCResult Ubx160Controller::set_power_mode()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  if (m_power_mode == PowerMode::PERFORMANCE) {
    // performance mode attempts to reduce tuning and settling time
    // as much as possible without adding noise.

    // RXLNA2 has a ~100ms warm up time, so the LNAs are forced on
    // here to reduce the settling time as much as possible.  The
    // force on signals are gated by the LNA selection so the LNAs
    // are turned on/off during tuning.  Unfortunately, that means
    // there is still a long settling time when tuning from the high
    // band (>1.5 GHz) to the low band (<1.5 GHz).
    set_cpld_field(RXLNA1_FORCEON, 1);
    set_cpld_field(RXLNA2_FORCEON, 1);

    // Placeholders in case some components need to be forced on to
    // reduce settling time.  Note that some FORCEON lines are still gated
    // by other bits in the CPLD register and are asserted during
    // frequency tuning.
    set_cpld_field(RXAMP_FORCEON, 1);
    set_cpld_field(RXDEMOD_FORCEON, 1);
    set_cpld_field(RXDRV_FORCEON, 1);
    set_cpld_field(RXMIXER_FORCEON, 0);
    set_cpld_field(RXLO1_FORCEON, 1);
    set_cpld_field(RXLO2_FORCEON, 1);
    /*
    //set_cpld_field(TXDRV_FORCEON, 1);  // controlled by RX antenna selection
    set_cpld_field(TXMOD_FORCEON, 0);
    set_cpld_field(TXMIXER_FORCEON, 0);
    set_cpld_field(TXLO1_FORCEON, 0);
    set_cpld_field(TXLO2_FORCEON, 0);
    */
    write_cpld_reg();
  } else if (m_power_mode == PowerMode::POWERSAVE) {
    // powersave mode attempts to use the least amount of power possible
    // by powering on components only when needed.  Longer tuning and
    // settling times are expected.

    // Clear the LNA force on bits.
    set_cpld_field(RXLNA1_FORCEON, 0);
    set_cpld_field(RXLNA2_FORCEON, 0);

    /*
    // Placeholders in case other force on bits need to be set or cleared.
    set_cpld_field(RXAMP_FORCEON, 0);
    set_cpld_field(RXDEMOD_FORCEON, 0);
    set_cpld_field(RXDRV_FORCEON, 0);
    set_cpld_field(RXMIXER_FORCEON, 0);
    set_cpld_field(RXLO1_FORCEON, 0);
    set_cpld_field(RXLO2_FORCEON, 0);
    //set_cpld_field(TXDRV_FORCEON, 1);  // controlled by RX antenna selection
    set_cpld_field(TXMOD_FORCEON, 0);
    set_cpld_field(TXMIXER_FORCEON, 0);
    set_cpld_field(TXLO1_FORCEON, 0);
    set_cpld_field(TXLO2_FORCEON, 0);
    */

    write_cpld_reg();
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::set_xcvr_mode()
{
  std::lock_guard<std::mutex> lock(m_mutex);
  // TODO:  Add implementation.  (This TODO carried across from original UHD code)
  // The intent is to add behavior based on whether
  // the board is in TX, RX, or full duplex mode
  // to reduce power consumption and RF noise.
  if (m_xcvr_mode == XcvrMode::TDD) {
    set_cpld_field(TXDRV_FORCEON, 1);
    write_cpld_reg();
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::cache_temp_comp_mode()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  for (const auto &lo : {m_txlo1, m_txlo2, m_rxlo1, m_rxlo2}) {
    lo->cache_auto_retune(m_temp_comp_enabled);
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::set_tx_enable()
{
  // enable the DAC output (active low)
  if (m_tx_enable)
  {
    m_ubx160_io->set_TX_EN_N(0);
  }
  else
  {
    m_ubx160_io->set_TX_EN_N(1);
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::set_rx_enable()
{
  // enable the ADC (active low)
  if (m_rx_enable)
  {
    m_ubx160_io->set_RX_EN_N(0);
  }
  else
  {
    m_ubx160_io->set_RX_EN_N(1);
  }

  return RCC_OK;
}


RCCResult Ubx160Controller::cache_tx_ant(Port port)
{
  m_tx_ant = port;
  return RCC_OK;
}


RCCResult Ubx160Controller::cache_rx_ant(Port port)
{
  m_rx_ant = port;
  return RCC_OK;
}
