// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "ads62p48_controller.hh"
#include "slaves/ads62p48_slave.hh"

using namespace OCPI::RCC;


Ads62p48Controller::Ads62p48Controller(std::shared_ptr<Ads62p48Interface> slave) : m_slave(slave)
{
  m_gain_db = 0.0;
}


RCCResult Ads62p48Controller::start()
{
  // Reset ADC
  m_ads62p48_regs.reset = 1;
  m_slave->set_reg_0x00(m_ads62p48_regs.get_reg(0x00));
  m_ads62p48_regs.reset = 0;

  // Desired configuration
  m_ads62p48_regs.enable_low_speed_mode = 0;
  m_ads62p48_regs.ref              = ads62p48_regs_t::REF_INTERNAL;
  m_ads62p48_regs.standby          = ads62p48_regs_t::STANDBY_NORMAL;
  m_ads62p48_regs.power_down       = ads62p48_regs_t::POWER_DOWN_NORMAL;
  m_ads62p48_regs.lvds_cmos        = ads62p48_regs_t::LVDS_CMOS_DDR_LVDS;
  m_ads62p48_regs.channel_control  = ads62p48_regs_t::CHANNEL_CONTROL_INDEPENDENT;
  m_ads62p48_regs.data_format      = ads62p48_regs_t::DATA_FORMAT_2S_COMPLIMENT;
  m_ads62p48_regs.clk_out_pos_edge = ads62p48_regs_t::CLK_OUT_POS_EDGE_MINUS4_26;
  m_ads62p48_regs.clk_out_neg_edge = ads62p48_regs_t::CLK_OUT_NEG_EDGE_MINUS4_26;

  // Program device
  m_slave->set_reg_0x00(m_ads62p48_regs.get_reg(0));
  m_slave->set_reg_0x20(m_ads62p48_regs.get_reg(0x20));
  m_slave->set_reg_0x3f(m_ads62p48_regs.get_reg(0x3f));
  m_slave->set_reg_0x40(m_ads62p48_regs.get_reg(0x40));
  m_slave->set_reg_0x41(m_ads62p48_regs.get_reg(0x41));
  m_slave->set_reg_0x44(m_ads62p48_regs.get_reg(0x44));
  m_slave->set_reg_0x50(m_ads62p48_regs.get_reg(0x50));
  m_slave->set_reg_0x51(m_ads62p48_regs.get_reg(0x51));
  m_slave->set_reg_0x52(m_ads62p48_regs.get_reg(0x52));
  m_slave->set_reg_0x53(m_ads62p48_regs.get_reg(0x53));
  m_slave->set_reg_0x55(m_ads62p48_regs.get_reg(0x55));
  m_slave->set_reg_0x57(m_ads62p48_regs.get_reg(0x57));
  m_slave->set_reg_0x62(m_ads62p48_regs.get_reg(0x62));
  m_slave->set_reg_0x63(m_ads62p48_regs.get_reg(0x63));
  m_slave->set_reg_0x66(m_ads62p48_regs.get_reg(0x66));
  m_slave->set_reg_0x68(m_ads62p48_regs.get_reg(0x68));
  m_slave->set_reg_0x6a(m_ads62p48_regs.get_reg(0x6a));
  m_slave->set_reg_0x75(m_ads62p48_regs.get_reg(0x75));
  m_slave->set_reg_0x76(m_ads62p48_regs.get_reg(0x76));

  RCCResult res;

  res = set_gain();
  if (res != RCC_OK) {
    return res;
  }

  res = calibrate_delay_lines();
  if (res != RCC_OK) {
    return res;
  }

  log(9, "ADS62P48 Proxy: Started");
  return RCC_OK;
}


RCCResult Ads62p48Controller::cache_gain(double gain_db)
{
  m_gain_db = gain_db;
  return RCC_OK;
}


RCCResult Ads62p48Controller::set_gain()
{
  // Clamp the gain to the range 0 db to 6 dB
  if (m_gain_db < 0.0)
  {
    log(OCPI_LOG_BAD, "Gain is below minimum of 0.0, clamping to 0.0");
    m_gain_db = 0.0;
  }
  else if (m_gain_db > 6.0)
  {
    log(OCPI_LOG_BAD, "Gain is above maximum of 6.0, clamping to 6.0");
    m_gain_db = 6.0;
  }

  // `gain_bits` increments from 0 to 12, representing [0..6] dB gain in steps of 0.5dB
  uint8_t gain_bits = static_cast<uint8_t>(round(m_gain_db * 2.0));

  m_ads62p48_regs.gain_chA = gain_bits;
  m_ads62p48_regs.gain_chB = gain_bits;
  m_slave->set_reg_0x55(m_ads62p48_regs.get_reg(0x55));
  m_slave->set_reg_0x68(m_ads62p48_regs.get_reg(0x68));

  log(9, "ADS62P48 Proxy: set gain to gain_bits=%u from gain=%f", gain_bits, m_gain_db);
  return RCC_OK;
}


RCCResult Ads62p48Controller::set_test_pattern(Ads62p48Pattern pattern, uint8_t a_num, uint8_t b_num)
{
  m_ads62p48_regs.custom_pattern_low  = a_num;
  m_ads62p48_regs.custom_pattern_high = b_num;

  switch (pattern)
  {
    case Ads62p48Pattern::ONES:
      m_ads62p48_regs.test_patterns_chA = ads62p48_regs_t::TEST_PATTERNS_CHA_ONES;
      m_ads62p48_regs.test_patterns_chB = ads62p48_regs_t::TEST_PATTERNS_CHB_ONES;
      break;

    case Ads62p48Pattern::ZEROS:
      m_ads62p48_regs.test_patterns_chA = ads62p48_regs_t::TEST_PATTERNS_CHA_ZEROS;
      m_ads62p48_regs.test_patterns_chB = ads62p48_regs_t::TEST_PATTERNS_CHB_ZEROS;
      break;

    case Ads62p48Pattern::CUSTOM:
      m_ads62p48_regs.test_patterns_chA = ads62p48_regs_t::TEST_PATTERNS_CHA_CUSTOM;
      m_ads62p48_regs.test_patterns_chB = ads62p48_regs_t::TEST_PATTERNS_CHB_CUSTOM;
      break;

    case Ads62p48Pattern::RAMP:
      m_ads62p48_regs.test_patterns_chA = ads62p48_regs_t::TEST_PATTERNS_CHA_RAMP;
      m_ads62p48_regs.test_patterns_chB = ads62p48_regs_t::TEST_PATTERNS_CHB_RAMP;
      break;

    case Ads62p48Pattern::NORMAL:
      m_ads62p48_regs.test_patterns_chA = ads62p48_regs_t::TEST_PATTERNS_CHA_NORMAL;
      m_ads62p48_regs.test_patterns_chB = ads62p48_regs_t::TEST_PATTERNS_CHB_NORMAL;
      break;

    default:
      log(0, "ADS62P48 Proxy: test pattern unrecognised");
      return RCC_ERROR;
  }

  m_slave->set_reg_0x51(m_ads62p48_regs.get_reg(0x51));
  m_slave->set_reg_0x52(m_ads62p48_regs.get_reg(0x52));
  m_slave->set_reg_0x62(m_ads62p48_regs.get_reg(0x62));
  m_slave->set_reg_0x75(m_ads62p48_regs.get_reg(0x75));

  return RCC_OK;
}


RCCResult Ads62p48Controller::set_rx_adc_enable(bool enable)
{
  // Set the enable property
  //
  // NOTE: This just controls the valid signal on the dev signals interface to
  // the `data_src_qadc_csts` worker, effectively acting as a gate on the sample
  // stream
  m_slave->set_enable(enable);
  return RCC_OK;
}


// Calibrate the clock-to-data delay lines in the device worker
//
// This calibration operates by enabling the ramp test pattern mode of the ADC,
// causing it to output an incrementing ramp on each of the two channels. A
// pattern checker in the worker detects this ramp allowing us to check if the
// ADC data is being sampled correctly. We then sweep through the delay line
// values and look for a range of values that result in correctly sampled ADC
// data. The delay lines are then set to the centre of this range, and the ADC
// configured back into normal operation.
RCCResult Ads62p48Controller::calibrate_delay_lines()
{
  // Pattern checker states
  const uint8_t PATTERN_CHECK_LOCK = 2;
  const uint8_t PATTERN_CHECK_FAIL = 3;

  // Track the largest span of valid delays seen
  uint8_t best_valid_range_start = 0;
  uint8_t best_valid_range_len = 0;

  // Current valid span
  uint8_t valid_range_start = 0;
  uint8_t valid_range_len = 0;

  // Are we currently in a span of valid delays?
  bool valid = false;

  // Configure RAMP test pattern as required by the pattern checker logic
  set_test_pattern(Ads62p48Pattern::RAMP);

  // Sweep over the delay line settings
  for (uint8_t delay = 0; delay < 32; delay++)
  {
    // Set the delay line
    m_slave->set_idelay_value(delay);

    // Wait until the pattern checker either locks or fails
    uint8_t check = 0;
    while (check != PATTERN_CHECK_LOCK && check != PATTERN_CHECK_FAIL)
    {
      check = m_slave->get_pattern_check();
    }

    if (check == PATTERN_CHECK_LOCK)
    {
      if (!valid)
      {
        valid = true;
        valid_range_start = delay;
      }

      valid_range_len++;

      if (valid_range_len > best_valid_range_len)
      {
        best_valid_range_start = valid_range_start;
        best_valid_range_len   = valid_range_len;
      }
    }
    else
    {
      valid = false;
      valid_range_len = 0;
    }
  }

  // Remove the test pattern
  set_test_pattern(Ads62p48Pattern::NORMAL);

  // Require at least one valid delay line setting
  if (best_valid_range_len > 0)
  {
    // Set the delay to the middle of the valid span
    m_slave->set_idelay_value(best_valid_range_start + best_valid_range_len / 2);
    return RCC_OK;
  }
  else
  {
    log(0, "ADS62P48 Proxy: Delay line calbration failed");
    return RCC_ERROR;
  }
}
