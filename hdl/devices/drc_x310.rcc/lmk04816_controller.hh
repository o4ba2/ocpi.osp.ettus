// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_LMK04816_CONTROLLER_HH__
#define OCPI_RCC_WORKER_DRC_X310_LMK04816_CONTROLLER_HH__

#include <memory>
#include "RCC_Worker.hh"
#include "device_base.hh"
#include "lmk04816_regs.hh"


using OCPI::RCC::RCCResult;


// Forward declarations
class Lmk04816Interface;
class EepromController;


class Lmk04816Controller: DeviceControllerBase
{
public:
  Lmk04816Controller(std::shared_ptr<Lmk04816Interface> slave, std::shared_ptr<EepromController> eeprom_controller);

  RCCResult start();
  RCCResult cache_master_clock_rate(const double master_clock_rate);
  double get_dboard_clock_rate();

  static bool frequencies_are_equal(
    const double a,
    const double b);

private:
  std::shared_ptr<Lmk04816Interface> m_slave;
  std::shared_ptr<EepromController> m_eeprom_controller;

  double m_master_clock_rate;
  double m_dboard_clock_rate;
  double m_system_ref_rate;
  double m_vco_freq;
  uint32_t  m_lock_timeout_s;

  struct x300_clk_delays {
    x300_clk_delays()
      : fpga_dly_ns(0.0)
      , adc_dly_ns(0.0)
      , dac_dly_ns(0.0)
      , db_rx_dly_ns(0.0)
      , db_tx_dly_ns(0.0)
    {}
    x300_clk_delays(double fpga, double adc, double dac, double db_rx, double db_tx)
      : fpga_dly_ns(fpga)
      , adc_dly_ns(adc)
      , dac_dly_ns(dac)
      , db_rx_dly_ns(db_rx)
      , db_tx_dly_ns(db_tx)
    {}

    double fpga_dly_ns;
    double adc_dly_ns;
    double dac_dly_ns;
    double db_rx_dly_ns;
    double db_tx_dly_ns;
  };

  enum x300_clock_which_t {
    X300_CLOCK_WHICH_ADC0,
    X300_CLOCK_WHICH_ADC1,
    X300_CLOCK_WHICH_DAC0,
    X300_CLOCK_WHICH_DAC1,
    X300_CLOCK_WHICH_DB0_RX,
    X300_CLOCK_WHICH_DB0_TX,
    X300_CLOCK_WHICH_DB1_RX,
    X300_CLOCK_WHICH_DB1_TX,
    X300_CLOCK_WHICH_FPGA,
  };

  enum opmode_t {
    INVALID,
    m10M_200M_NOZDEL,     // used for debug purposes only
    m10M_200M_ZDEL,       // Normal mode
    m11_52M_184_32M_ZDEL, // LTE with 11.52 MHz ref
    m23_04M_184_32M_ZDEL, // LTE with 23.04 MHz ref
    m30_72M_184_32M_ZDEL, // LTE with external ref, aka CPRI Mode
    m10M_184_32M_NOZDEL,  // LTE with 10 MHz ref
    m10M_120M_ZDEL,       // NI USRP 120 MHz Clocking
    m10M_AUTO_NOZDEL
  };

  enum lmk04816_state_t {
    INIT_REQUIRED,
    ACTIVE
  };

  // Tune the FPGA->ADC clock delay to ensure a safe ADC_SSCLK -> RADIO_CLK crossing.
  // If the FPGA_CLK is delayed, we also need to delay the reference clocks going to the DAC
  // because the data interface clock is generated from FPGA_CLK.
  const x300_clk_delays X300_REV0_6_CLK_DELAYS = x300_clk_delays(
    /*fpga=*/0.000, /*adc=*/2.200, /*dac=*/0.000, /*db_rx=*/0.000, /*db_tx=*/0.000);

  const x300_clk_delays X300_REV7_CLK_DELAYS = x300_clk_delays(
    /*fpga=*/0.000, /*adc=*/0.000, /*dac=*/0.000, /*db_rx=*/0.000, /*db_tx=*/0.000);

  static constexpr double DOUBLE_PRECISION_DELTA = 1e-5;

  lmk04816_state_t m_state;
  lmk04816_regs_t m_lmk04816_regs;
  x300_clk_delays m_delays;

  bool init();
  void reset_clocks();
  void sync_clocks();

  Lmk04816Controller::opmode_t get_clocking_mode(
    const double master_clock_rate,
    const double system_ref_rate) const;

  double get_vco_clock_rate(
    const Lmk04816Controller::opmode_t clocking_mode) const;

  bool cache_clocking_mode(
    const uint32_t hw_rev,
    const Lmk04816Controller::opmode_t clocking_mode);

  RCCResult set_clock_delay(
    const x300_clock_which_t which,
    const double delay_ns,
    const bool resync = true);
};

#endif /* OCPI_RCC_WORKER_DRC_X310_LMK04816_CONTROLLER_HH__ */
