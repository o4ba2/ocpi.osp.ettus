#include <cstring>
#include "eeprom_controller.hh"
#include "slaves/ubx160_eeprom_slave.hh"


// Helper functions
static void read_uint8(uint8_t& result, uint8_t *buffer, size_t offset);
static void read_uint16(uint16_t& result, uint8_t *buffer, size_t offset);
static void read_uint32(uint32_t& result, uint8_t *buffer, size_t offset);
static void read_mac_addr(uint8_t *result, uint8_t *buffer, size_t offset);
static void read_string(uint8_t *result, uint8_t *buffer, size_t offset, size_t max_len);


bool DBEepromMap::is_valid() const
{
  // All daughterboards should have a magic value of 0xdb
  return magic_value == 0xdb;
}


EepromController::EepromController(std::shared_ptr<Ubx160EepromInterface> slave) : m_slave(slave)
{
}


OCPI::RCC::RCCResult EepromController::start()
{
  // Read the contents of all of the EEPROMs
  read(Device::MB, m_mb_eeprom);
  read(Device::DB0_RX, m_db_rx_eeprom[0]);
  read(Device::DB0_TX, m_db_tx_eeprom[0]);
  read(Device::DB1_RX, m_db_rx_eeprom[1]);
  read(Device::DB1_TX, m_db_tx_eeprom[1]);

  return OCPI::RCC::RCC_OK;
}


uint8_t EepromController::read_byte(Device device, uint16_t addr)
{
  // Set the device address
  m_slave->set_control(static_cast<uint8_t>(device));

  // Set the memory address
  m_slave->set_addr(addr);

  // The MB EEPROM uses 2 byte addresses, the DB EEPROMs 1 byte addresses
  m_slave->set_addr_size_bytes(device == Device::MB ? 2 : 1);

  // Do the I2C transaction
  return m_slave->get_read_byte();
}


void EepromController::read(Device device, MBEepromMap& data)
{
  uint8_t buffer[108];

  for (size_t i = 0; i < sizeof(buffer); i++)
  {
    buffer[i] = read_byte(device, i);
  }

  read_uint16  (data.revision,        buffer, 0);
  read_uint16  (data.product,         buffer, 2);
  read_uint16  (data.revision_compat, buffer, 4);
  read_mac_addr(data.mac_addr0,       buffer, 8);
  read_mac_addr(data.mac_addr1,       buffer, 16);
  read_uint32  (data.gateway,         buffer, 24);
  read_uint32  (data.subnet[0],       buffer, 28);
  read_uint32  (data.subnet[1],       buffer, 32);
  read_uint32  (data.subnet[2],       buffer, 36);
  read_uint32  (data.subnet[3],       buffer, 40);
  read_uint32  (data.ip_addr[0],      buffer, 44);
  read_uint32  (data.ip_addr[1],      buffer, 48);
  read_uint32  (data.ip_addr[2],      buffer, 52);
  read_uint32  (data.ip_addr[3],      buffer, 56);
  read_string  (data.name,            buffer, 76, 23);
  read_string  (data.serial,          buffer, 99, 9);
}


void EepromController::read(Device device, DBEepromMap& data)
{
  uint8_t buffer[32];
  uint8_t accum = 0;

  for (size_t i = 0; i < sizeof(buffer); i++)
  {
    buffer[i] = read_byte(device, i);
    accum += buffer[i];
  }

  // The checksum is calculated such that the 8-bit sum of all bytes is 0
  if (accum != 0)
  {
    // Clear the data
    std::memset(&data, 0, sizeof(data));
    return;
  }

  read_uint8 (data.magic_value, buffer, 0);
  read_uint16(data.id,          buffer, 1);
  read_uint16(data.rev,         buffer, 3);
  read_uint16(data.offset_0,    buffer, 5);
  read_uint16(data.offset_1,    buffer, 7);
  read_string(data.serial,      buffer, 9, 22);
}


//-- Helper functions for deserialising ---------------------------------------


static void read_uint8(uint8_t& result, uint8_t *buffer, size_t offset)
{
  result = buffer[offset];
}


static void read_uint16(uint16_t& result, uint8_t *buffer, size_t offset)
{
  result =
    ((uint16_t)buffer[offset + 0] << 0) |
    ((uint16_t)buffer[offset + 1] << 8);
}


static void read_uint32(uint32_t& result, uint8_t *buffer, size_t offset)
{
  result =
    ((uint32_t)buffer[offset + 0] <<  0) |
    ((uint32_t)buffer[offset + 1] <<  8) |
    ((uint32_t)buffer[offset + 2] << 16) |
    ((uint32_t)buffer[offset + 3] << 24);
}


static void read_mac_addr(uint8_t *result, uint8_t *buffer, size_t offset)
{
  memcpy(result, &buffer[offset], 6);
}


static void read_string(uint8_t *result, uint8_t *buffer, size_t offset, size_t max_len)
{
  std::strncpy((char *)result, (char *)&buffer[offset], max_len);

  // Make sure that the string is nul terminated
  result[max_len - 1] = 0;
}
