// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <unistd.h>
#include "lmk04816_controller.hh"
#include "slaves/lmk04816_slave.hh"
#include "eeprom_controller.hh"


using namespace OCPI::RCC;


Lmk04816Controller::Lmk04816Controller(std::shared_ptr<Lmk04816Interface> slave, std::shared_ptr<EepromController> eeprom_controller):
  m_slave(slave),
  m_eeprom_controller(eeprom_controller),
  m_master_clock_rate(200e6),
  m_dboard_clock_rate(50e6),
  m_system_ref_rate(10e6),
  m_vco_freq(2400e6),
  m_lock_timeout_s(10),
  m_state(lmk04816_state_t::INIT_REQUIRED)
{
}


RCCResult Lmk04816Controller::Lmk04816Controller::start()
{
  // Only initialise the LMK04816 if it is not already 
  // running or if we need to change the current configuration.
  // This prevents unecessarily restarting the clock generator.  
  // Restarting the clock generator should only be required for
  // a change of sampling rate.
  if (m_state == lmk04816_state_t::INIT_REQUIRED)
  {  
    if (!init())
    {
      return RCC_ERROR;
    }

    m_state = lmk04816_state_t::ACTIVE;
  }

  return RCC_OK;
}


RCCResult Lmk04816Controller::cache_master_clock_rate(const double master_clock_rate)
{
  // check if the master clock rate is being changed
  if (!frequencies_are_equal(m_master_clock_rate, master_clock_rate))
  {
    m_master_clock_rate = master_clock_rate;
    m_state = lmk04816_state_t::INIT_REQUIRED;
  }

  return RCC_OK;
}


double Lmk04816Controller::get_dboard_clock_rate()
{
  uint16_t dboard_div = static_cast<uint16_t>(std::ceil(m_vco_freq / m_dboard_clock_rate));

  return (m_vco_freq / dboard_div);
}


bool Lmk04816Controller::Lmk04816Controller::init()
{
  // The X3xx has two primary rates. The first is the
  // m_system_ref_rate, which is sourced from the "clock_source"/"value" field
  // of the property tree, and whose value can be 10e6, 11.52e6, 23.04e6,
  // or 30.72e6.
  // The m_system_ref_rate is the input to the clocking system, and what
  // comes out is a disciplined master clock running at the m_master_clock_rate.
  // As such, only certain combinations of system reference rates and master clock
  // rates are supported. Additionally, a subset of these will operate in "zero
  // delay" mode.

  // Get the hardware revision from the EEPROM
  uint32_t hw_rev = m_eeprom_controller->mb_eeprom().revision;

  // get the clocking mode we need to use
  opmode_t clocking_mode = get_clocking_mode(m_master_clock_rate, m_system_ref_rate);
  if (clocking_mode == opmode_t::INVALID)
  {
    log(0, "ERROR: unsupported master_clock_rate / system_ref_rate  (%f,%f)",
      m_master_clock_rate, m_system_ref_rate);
    return false;
  }

  // Mode setup - based on the  clocking mode
  if (!cache_clocking_mode(hw_rev, clocking_mode))
  {
    log(0, "ERROR: unsupported clockimg_mode (%d)", (int)clocking_mode);
    return false;
  }

  // Reset the LMK clock controller.
  m_lmk04816_regs.RESET = lmk04816_regs_t::RESET_RESET;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  m_lmk04816_regs.RESET = lmk04816_regs_t::RESET_NO_RESET;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  // Compute clock dividers
  uint16_t master_clock_div =
          static_cast<uint16_t>(std::ceil(m_vco_freq / m_master_clock_rate));

  uint16_t dboard_div =
          static_cast<uint16_t>(std::ceil(m_vco_freq / m_dboard_clock_rate));

  // Initial power-up
  m_lmk04816_regs.CLKout0_1_PD = lmk04816_regs_t::CLKOUT0_1_PD_POWER_UP;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  m_lmk04816_regs.CLKout0_1_DIV = master_clock_div;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  // Setup other registers
  // Register 1
  m_lmk04816_regs.CLKout2_3_PD  = lmk04816_regs_t::CLKOUT2_3_PD_POWER_UP;
  m_lmk04816_regs.CLKout2_3_DIV = dboard_div;

  // Register 2
  m_lmk04816_regs.CLKout4_5_PD  = lmk04816_regs_t::CLKOUT4_5_PD_POWER_UP;
  m_lmk04816_regs.CLKout4_5_DIV = dboard_div;

  // Register 3
  m_lmk04816_regs.CLKout6_7_DIV = master_clock_div;
  m_lmk04816_regs.CLKout6_7_OSCin_Sel = lmk04816_regs_t::CLKOUT6_7_OSCIN_SEL_VCO;

  // Register 4
  m_lmk04816_regs.CLKout8_9_DIV = master_clock_div;

  // Register 5
  m_lmk04816_regs.CLKout10_11_PD  = lmk04816_regs_t::CLKOUT10_11_PD_NORMAL;
  m_lmk04816_regs.CLKout10_11_DIV = static_cast<uint16_t>(std::ceil(m_vco_freq / m_system_ref_rate));

  // Register 6
  m_lmk04816_regs.CLKout0_TYPE = lmk04816_regs_t::CLKOUT0_TYPE_LVDS; // FPGA
  m_lmk04816_regs.CLKout1_TYPE = lmk04816_regs_t::CLKOUT1_TYPE_P_DOWN; // CPRI feedback clock, use LVDS
  m_lmk04816_regs.CLKout2_TYPE = lmk04816_regs_t::CLKOUT2_TYPE_LVPECL_700MVPP; // DB_0_RX
  m_lmk04816_regs.CLKout3_TYPE = lmk04816_regs_t::CLKOUT3_TYPE_LVPECL_700MVPP; // DB_1_RX

  // Register 7
  m_lmk04816_regs.CLKout4_TYPE = lmk04816_regs_t::CLKOUT4_TYPE_LVPECL_700MVPP; // DB_1_TX
  m_lmk04816_regs.CLKout5_TYPE = lmk04816_regs_t::CLKOUT5_TYPE_LVPECL_700MVPP; // DB_0_TX
  m_lmk04816_regs.CLKout6_TYPE = lmk04816_regs_t::CLKOUT6_TYPE_LVPECL_700MVPP; // DB0_DAC
  m_lmk04816_regs.CLKout7_TYPE = lmk04816_regs_t::CLKOUT7_TYPE_LVPECL_700MVPP; // DB1_DAC
  m_lmk04816_regs.CLKout8_TYPE = lmk04816_regs_t::CLKOUT8_TYPE_LVPECL_700MVPP; // DB0_ADC

  // Register 8
  m_lmk04816_regs.CLKout9_TYPE  = lmk04816_regs_t::CLKOUT9_TYPE_LVPECL_700MVPP; // DB1_ADC
  m_lmk04816_regs.CLKout10_TYPE = lmk04816_regs_t::CLKOUT10_TYPE_LVDS; // REF_CLKOUT
  m_lmk04816_regs.CLKout11_TYPE = lmk04816_regs_t::CLKOUT11_TYPE_P_DOWN; // Debug header, use LVPECL

  // Register 10
  m_lmk04816_regs.EN_OSCout0 = lmk04816_regs_t::EN_OSCOUT0_DISABLED; // Debug header
  m_lmk04816_regs.FEEDBACK_MUX    = 5; // use output 10 (REF OUT) for feedback
  m_lmk04816_regs.EN_FEEDBACK_MUX = lmk04816_regs_t::EN_FEEDBACK_MUX_ENABLED;

  // Register 11
  // MODE set in individual cases above
  m_lmk04816_regs.SYNC_QUAL = lmk04816_regs_t::SYNC_QUAL_FB_MUX;
  m_lmk04816_regs.EN_SYNC   = lmk04816_regs_t::EN_SYNC_ENABLE;
  m_lmk04816_regs.NO_SYNC_CLKout0_1 = lmk04816_regs_t::NO_SYNC_CLKOUT0_1_CLOCK_XY_SYNC;
  m_lmk04816_regs.NO_SYNC_CLKout2_3 = lmk04816_regs_t::NO_SYNC_CLKOUT2_3_CLOCK_XY_SYNC;
  m_lmk04816_regs.NO_SYNC_CLKout4_5 = lmk04816_regs_t::NO_SYNC_CLKOUT4_5_CLOCK_XY_SYNC;
  m_lmk04816_regs.NO_SYNC_CLKout6_7 = lmk04816_regs_t::NO_SYNC_CLKOUT6_7_CLOCK_XY_SYNC;
  m_lmk04816_regs.NO_SYNC_CLKout8_9 = lmk04816_regs_t::NO_SYNC_CLKOUT8_9_CLOCK_XY_SYNC;
  m_lmk04816_regs.NO_SYNC_CLKout10_11 = lmk04816_regs_t::NO_SYNC_CLKOUT10_11_CLOCK_XY_SYNC;
  m_lmk04816_regs.SYNC_TYPE = lmk04816_regs_t::SYNC_TYPE_INPUT;

  // Register 12
  m_lmk04816_regs.LD_MUX = lmk04816_regs_t::LD_MUX_BOTH;

  // Register 13
  m_lmk04816_regs.EN_CLKin0 = lmk04816_regs_t::EN_CLKIN0_NO_VALID_USE; // This is not connected
  m_lmk04816_regs.EN_CLKin2 = lmk04816_regs_t::EN_CLKIN2_NO_VALID_USE; // Used only for CPRI
  m_lmk04816_regs.Status_CLKin1_MUX = lmk04816_regs_t::STATUS_CLKIN1_MUX_UWIRE_RB;
  m_lmk04816_regs.CLKin_Select_MODE = lmk04816_regs_t::CLKIN_SELECT_MODE_CLKIN1_MAN;
  m_lmk04816_regs.HOLDOVER_MUX      = lmk04816_regs_t::HOLDOVER_MUX_PLL1_R;

  // Register 14
  m_lmk04816_regs.Status_CLKin1_TYPE = lmk04816_regs_t::STATUS_CLKIN1_TYPE_OUT_PUSH_PULL;
  m_lmk04816_regs.Status_CLKin0_TYPE = lmk04816_regs_t::STATUS_CLKIN0_TYPE_OUT_PUSH_PULL;

  // Register 26
  // PLL2_CP_GAIN_26 set above in individual cases
  m_lmk04816_regs.PLL2_CP_POL_26 = lmk04816_regs_t::PLL2_CP_POL_26_NEG_SLOPE;
  m_lmk04816_regs.EN_PLL2_REF_2X = lmk04816_regs_t::EN_PLL2_REF_2X_DOUBLED_FREQ_REF;

  // Register 27
  // PLL1_CP_GAIN_27 set in individual cases above
  // PLL1_R_27 set in the individual cases above

  // Register 28
  // PLL1_N_28 and PLL2_R_28 are set in the individual cases above

  // Register 29
  m_lmk04816_regs.PLL2_N_CAL_29 = m_lmk04816_regs.PLL2_N_30; // N_CAL should always match N
  m_lmk04816_regs.OSCin_FREQ_29 = lmk04816_regs_t::OSCIN_FREQ_29_63_TO_127MHZ;

  // Register 30
  // PLL2_P_30 set in individual cases above
  // PLL2_N_30 set in individual cases above
  if (hw_rev >= 7)
  {
    m_delays = X300_REV7_CLK_DELAYS;
  }
  else
  {
    m_delays = X300_REV0_6_CLK_DELAYS;
  }

  // Apply delay values
  set_clock_delay(X300_CLOCK_WHICH_FPGA, m_delays.fpga_dly_ns, false);
  set_clock_delay(X300_CLOCK_WHICH_DB0_RX, m_delays.db_rx_dly_ns, false); // Sets both Ch0 and Ch1
  set_clock_delay(X300_CLOCK_WHICH_DB0_TX, m_delays.db_tx_dly_ns, false); // Sets both Ch0 and Ch1
  set_clock_delay(X300_CLOCK_WHICH_ADC0, m_delays.adc_dly_ns, false); // Sets both Ch0 and Ch1
  set_clock_delay(X300_CLOCK_WHICH_DAC0, m_delays.dac_dly_ns, false); // Sets both Ch0 and Ch1

  // Write the configuration values into the LMK
  m_slave->set_r1(m_lmk04816_regs.get_reg_data(1));
  m_slave->set_r2(m_lmk04816_regs.get_reg_data(2));
  m_slave->set_r3(m_lmk04816_regs.get_reg_data(3));
  m_slave->set_r4(m_lmk04816_regs.get_reg_data(4));
  m_slave->set_r5(m_lmk04816_regs.get_reg_data(5));
  m_slave->set_r6(m_lmk04816_regs.get_reg_data(6));
  m_slave->set_r7(m_lmk04816_regs.get_reg_data(7));
  m_slave->set_r8(m_lmk04816_regs.get_reg_data(8));
  m_slave->set_r9(m_lmk04816_regs.get_reg_data(9));
  m_slave->set_r10(m_lmk04816_regs.get_reg_data(10));
  m_slave->set_r11(m_lmk04816_regs.get_reg_data(11));
  m_slave->set_r12(m_lmk04816_regs.get_reg_data(12));
  m_slave->set_r13(m_lmk04816_regs.get_reg_data(13));
  m_slave->set_r14(m_lmk04816_regs.get_reg_data(14));
  m_slave->set_r15(m_lmk04816_regs.get_reg_data(15));
  m_slave->set_r16(m_lmk04816_regs.get_reg_data(16));
  m_slave->set_r24(m_lmk04816_regs.get_reg_data(24));
  m_slave->set_r25(m_lmk04816_regs.get_reg_data(25));
  m_slave->set_r26(m_lmk04816_regs.get_reg_data(26));
  m_slave->set_r27(m_lmk04816_regs.get_reg_data(27));
  m_slave->set_r28(m_lmk04816_regs.get_reg_data(28));
  m_slave->set_r29(m_lmk04816_regs.get_reg_data(29));
  m_slave->set_r30(m_lmk04816_regs.get_reg_data(30));
  m_slave->set_r31(m_lmk04816_regs.get_reg_data(31));

  sync_clocks();

  // report status
  uint32_t retry = 0;
  const uint32_t sleep_time_us = 1000; // 1ms
  const uint32_t max_retries = (m_lock_timeout_s * 1000000) / sleep_time_us;
  for(; retry < max_retries; retry++)
  {
    uint32_t lmk04816_status = m_slave->get_status();
    uint32_t status = lmk04816_status & 0x3;
    uint32_t holdover = (lmk04816_status >> 2) & 1;
    uint32_t lock = (lmk04816_status >> 3) & 1;
    uint32_t sync = (lmk04816_status >> 4) & 1;
    log(9, "LMK04816 status=%u, holdover=%u, lock=%u, sync=%u", status, holdover, lock, sync);

    if (lock)
    {
      break;
    }

    usleep(sleep_time_us);
  }

  if (retry == max_retries)
  {
    log(0, "LMK04816 lock not acquired in %u s!", m_lock_timeout_s);
    return false;
  }

  return true;
}


void Lmk04816Controller::Lmk04816Controller::reset_clocks()
{
  m_lmk04816_regs.RESET = lmk04816_regs_t::RESET_RESET;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  m_lmk04816_regs.RESET = lmk04816_regs_t::RESET_NO_RESET;
  m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));

  m_slave->set_r1(m_lmk04816_regs.get_reg_data(1));
  m_slave->set_r2(m_lmk04816_regs.get_reg_data(2));
  m_slave->set_r3(m_lmk04816_regs.get_reg_data(3));
  m_slave->set_r4(m_lmk04816_regs.get_reg_data(4));
  m_slave->set_r5(m_lmk04816_regs.get_reg_data(5));
  m_slave->set_r6(m_lmk04816_regs.get_reg_data(6));
  m_slave->set_r7(m_lmk04816_regs.get_reg_data(7));
  m_slave->set_r8(m_lmk04816_regs.get_reg_data(8));
  m_slave->set_r9(m_lmk04816_regs.get_reg_data(9));
  m_slave->set_r10(m_lmk04816_regs.get_reg_data(10));
  m_slave->set_r11(m_lmk04816_regs.get_reg_data(11));
  m_slave->set_r12(m_lmk04816_regs.get_reg_data(12));
  m_slave->set_r13(m_lmk04816_regs.get_reg_data(13));
  m_slave->set_r14(m_lmk04816_regs.get_reg_data(14));
  m_slave->set_r15(m_lmk04816_regs.get_reg_data(15));
  m_slave->set_r24(m_lmk04816_regs.get_reg_data(24));
  m_slave->set_r25(m_lmk04816_regs.get_reg_data(25));
  m_slave->set_r26(m_lmk04816_regs.get_reg_data(26));
  m_slave->set_r27(m_lmk04816_regs.get_reg_data(27));
  m_slave->set_r28(m_lmk04816_regs.get_reg_data(28));
  m_slave->set_r29(m_lmk04816_regs.get_reg_data(29));
  m_slave->set_r30(m_lmk04816_regs.get_reg_data(30));
  m_slave->set_r31(m_lmk04816_regs.get_reg_data(31));

  sync_clocks();
}


void Lmk04816Controller::Lmk04816Controller::sync_clocks()
{
  // soft sync:
  // put the sync IO into output mode - FPGA must be input
  // write low, then write high - this triggers a soft sync
  m_lmk04816_regs.SYNC_POL_INV = lmk04816_regs_t::SYNC_POL_INV_SYNC_LOW;
  m_slave->set_r11(m_lmk04816_regs.get_reg_data(11));

  m_lmk04816_regs.SYNC_POL_INV = lmk04816_regs_t::SYNC_POL_INV_SYNC_HIGH;
  m_slave->set_r11(m_lmk04816_regs.get_reg_data(11));
}


RCCResult Lmk04816Controller::Lmk04816Controller::set_clock_delay(
  const x300_clock_which_t which,
  const double delay_ns,
  const bool resync)
{
  // All dividers have are delayed by 5 taps by default. The delay
  // set by this function is relative to the 5 tap delay
  static const uint16_t DDLY_MIN_TAPS = 5;
  static const uint16_t DDLY_MAX_TAPS = 522; // Extended mode

  // The resolution and range of the analog delay is fixed
  static const double ADLY_RES_NS = 0.025;
  static const double ADLY_MIN_NS = 0.500;
  static const double ADLY_MAX_NS = 0.975;

  // Each digital tap delays the clock by one VCO period
  double vco_period_ns      = 1.0e9 / m_vco_freq;
  double half_vco_period_ns = vco_period_ns / 2.0;

  // Implement as much of the requested delay using digital taps. Whatever is
  // leftover will be made up using the analog delay element and the half-cycle
  // digital tap. A caveat here is that the analog delay starts at ADLY_MIN_NS, so
  // we need to back off by that much when coming up with the digital taps so that
  // the difference can be made up using the analog delay.
  uint16_t ddly_taps = 0;
  if (delay_ns < ADLY_MIN_NS)
  {
    ddly_taps = static_cast<uint16_t>(std::floor((delay_ns) / vco_period_ns));
  }
  else
  {
    ddly_taps = static_cast<uint16_t>(std::floor((delay_ns - ADLY_MIN_NS) / vco_period_ns));
  }

  double leftover_delay = delay_ns - (vco_period_ns * ddly_taps);

  // Compute settings
  uint16_t ddly_value   = ddly_taps + DDLY_MIN_TAPS;
  bool adly_en          = false;
  uint8_t adly_value    = 0;
  uint8_t half_shift_en = 0;

  if (ddly_value > DDLY_MAX_TAPS)
  {
    log(0, "set_clock_delay: Requested delay is out of range (%u > %u)", ddly_value, DDLY_MAX_TAPS);
    return RCC_ERROR;
  }

  double coerced_delay = (vco_period_ns * ddly_taps);
  if (leftover_delay > ADLY_MAX_NS)
  {
    // The VCO is running too slowly for us to compensate the digital delay
    // difference using analog delay. Do the best we can.
    adly_en    = true;
    adly_value = static_cast<uint8_t>(std::lround((ADLY_MAX_NS - ADLY_MIN_NS) / ADLY_RES_NS));
    coerced_delay += ADLY_MAX_NS;
  }
  else if (leftover_delay >= ADLY_MIN_NS && leftover_delay <= ADLY_MAX_NS)
  {
    // The leftover delay can be compensated by the analog delay up to the analog
    // delay resolution
    adly_en    = true;
    adly_value = static_cast<uint8_t>(std::lround((leftover_delay - ADLY_MIN_NS) / ADLY_RES_NS));
    coerced_delay += ADLY_MIN_NS + (ADLY_RES_NS * adly_value);
  }
  else if (leftover_delay >= (ADLY_MIN_NS - half_vco_period_ns)
              && leftover_delay < ADLY_MIN_NS)
  {
    // The leftover delay if less than the minimum supported analog delay but if
    // we move the digital delay back by half a VCO cycle then it will be in the
    // range of the analog delay. So do that!
    adly_en       = true;
    adly_value    = static_cast<uint8_t>(std::lround((leftover_delay + half_vco_period_ns - ADLY_MIN_NS) / ADLY_RES_NS));
    half_shift_en = 1;
    coerced_delay += ADLY_MIN_NS + (ADLY_RES_NS * adly_value) - half_vco_period_ns;
  }
  else
  {
    // Even after moving the digital delay back by half a cycle, we cannot make up
    // the difference so give up on compensating for the difference from the
    // digital delay tap. If control reaches here then the value of leftover_delay
    // is possible very small and will still be close to what the client
    // requested.
  }

  log(10, "x300_clock_ctrl::set_clock_delay: Which=%d, Requested=%f, Digital "
          "Taps=%d, Half Shift=%s, Analog Delay=%d (%s), Coerced Delay=%f ns",
          which, delay_ns, ddly_value, (half_shift_en ? "ON" : "OFF"),
          ((int)adly_value), (adly_en ? "ON" : "OFF"), coerced_delay);

  // Apply settings
  switch (which)
  {
    // ------------------------------------------------------
    case X300_CLOCK_WHICH_FPGA:
      m_lmk04816_regs.CLKout0_1_DDLY = ddly_value;
      if (m_lmk04816_regs.CLKout0_1_DDLY > 12)
      {
        log(0,
            "m_lmk04816_regs.CLKout0_1_DDLY=%u > 12, might need to consider three extra clocks mode",
            m_lmk04816_regs.CLKout0_1_DDLY);
      }
      m_lmk04816_regs.CLKout0_1_HS   = half_shift_en;
      if (adly_en)
      {
        m_lmk04816_regs.CLKout0_ADLY_SEL = lmk04816_regs_t::CLKOUT0_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout1_ADLY_SEL = lmk04816_regs_t::CLKOUT1_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout0_1_ADLY = adly_value;
      }
      else
      {
        m_lmk04816_regs.CLKout0_ADLY_SEL = lmk04816_regs_t::CLKOUT0_ADLY_SEL_D_PD;
        m_lmk04816_regs.CLKout1_ADLY_SEL = lmk04816_regs_t::CLKOUT1_ADLY_SEL_D_PD;
      }

      m_slave->set_r0(m_lmk04816_regs.get_reg_data(0));
      m_slave->set_r6(m_lmk04816_regs.get_reg_data(6));
      m_delays.fpga_dly_ns = coerced_delay;
      break;

    // ------------------------------------------------------
    case X300_CLOCK_WHICH_DB0_RX:
    case X300_CLOCK_WHICH_DB1_RX:
      m_lmk04816_regs.CLKout2_3_DDLY = ddly_value;
      if (m_lmk04816_regs.CLKout2_3_DDLY > 12)
      {
        log(0,
            "m_lmk04816_regs.CLKout2_3_DDLY=%u > 12, might need to consider three extra clocks mode",
            m_lmk04816_regs.CLKout2_3_DDLY);
      }
      m_lmk04816_regs.CLKout2_3_HS   = half_shift_en;
      if (adly_en)
      {
        m_lmk04816_regs.CLKout2_ADLY_SEL = lmk04816_regs_t::CLKOUT2_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout3_ADLY_SEL = lmk04816_regs_t::CLKOUT3_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout2_3_ADLY = adly_value;
      }
      else
      {
        m_lmk04816_regs.CLKout2_ADLY_SEL = lmk04816_regs_t::CLKOUT2_ADLY_SEL_D_PD;
        m_lmk04816_regs.CLKout3_ADLY_SEL = lmk04816_regs_t::CLKOUT3_ADLY_SEL_D_PD;
      }
      m_slave->set_r1(m_lmk04816_regs.get_reg_data(1));
      m_slave->set_r6(m_lmk04816_regs.get_reg_data(6));
      m_delays.db_rx_dly_ns = coerced_delay;
      break;

    // ------------------------------------------------------
    case X300_CLOCK_WHICH_DB0_TX:
    case X300_CLOCK_WHICH_DB1_TX:
      m_lmk04816_regs.CLKout4_5_DDLY = ddly_value;
      if (m_lmk04816_regs.CLKout4_5_DDLY > 12)
      {
        log(0,
            "m_lmk04816_regs.CLKout4_5_DDLY=%u > 12, might need to consider three extra clocks mode",
            m_lmk04816_regs.CLKout4_5_DDLY);
      }
      m_lmk04816_regs.CLKout4_5_HS   = half_shift_en;
      if (adly_en)
      {
        m_lmk04816_regs.CLKout4_ADLY_SEL = lmk04816_regs_t::CLKOUT4_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout5_ADLY_SEL = lmk04816_regs_t::CLKOUT5_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout4_5_ADLY = adly_value;
      }
      else
      {
        m_lmk04816_regs.CLKout4_ADLY_SEL = lmk04816_regs_t::CLKOUT4_ADLY_SEL_D_PD;
        m_lmk04816_regs.CLKout5_ADLY_SEL = lmk04816_regs_t::CLKOUT5_ADLY_SEL_D_PD;
      }
      m_slave->set_r2(m_lmk04816_regs.get_reg_data(2));
      m_slave->set_r7(m_lmk04816_regs.get_reg_data(7));
      m_delays.db_tx_dly_ns = coerced_delay;
      break;

    // ------------------------------------------------------
    case X300_CLOCK_WHICH_DAC0:
    case X300_CLOCK_WHICH_DAC1:
      m_lmk04816_regs.CLKout6_7_DDLY = ddly_value;
      if (m_lmk04816_regs.CLKout6_7_DDLY > 12)
      {
        log(0,
            "m_lmk04816_regs.CLKout6_7_DDLY=%u > 12, might need to consider three extra clocks mode",
            m_lmk04816_regs.CLKout6_7_DDLY);
      }
      m_lmk04816_regs.CLKout6_7_HS   = half_shift_en;
      if (adly_en)
      {
        m_lmk04816_regs.CLKout6_ADLY_SEL = lmk04816_regs_t::CLKOUT6_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout7_ADLY_SEL = lmk04816_regs_t::CLKOUT7_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout6_7_ADLY = adly_value;
      }
      else
      {
        m_lmk04816_regs.CLKout6_ADLY_SEL = lmk04816_regs_t::CLKOUT6_ADLY_SEL_D_PD;
        m_lmk04816_regs.CLKout7_ADLY_SEL = lmk04816_regs_t::CLKOUT7_ADLY_SEL_D_PD;
      }
      m_slave->set_r3(m_lmk04816_regs.get_reg_data(3));
      m_slave->set_r7(m_lmk04816_regs.get_reg_data(7));
      m_delays.dac_dly_ns = coerced_delay;
      break;

    // ------------------------------------------------------
    case X300_CLOCK_WHICH_ADC0:
    case X300_CLOCK_WHICH_ADC1:
      m_lmk04816_regs.CLKout8_9_DDLY = ddly_value;
      if (m_lmk04816_regs.CLKout8_9_DDLY > 12)
      {
        log(0,
            "m_lmk04816_regs.CLKout8_9_DDLY=%u > 12, might need to consider three extra clocks mode",
            m_lmk04816_regs.CLKout8_9_DDLY);
      }
      m_lmk04816_regs.CLKout8_9_HS   = half_shift_en;
      if (adly_en)
      {
        m_lmk04816_regs.CLKout8_ADLY_SEL = lmk04816_regs_t::CLKOUT8_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout9_ADLY_SEL = lmk04816_regs_t::CLKOUT9_ADLY_SEL_D_BOTH;
        m_lmk04816_regs.CLKout8_9_ADLY = adly_value;
      }
      else
      {
        m_lmk04816_regs.CLKout8_ADLY_SEL = lmk04816_regs_t::CLKOUT8_ADLY_SEL_D_PD;
        m_lmk04816_regs.CLKout9_ADLY_SEL = lmk04816_regs_t::CLKOUT9_ADLY_SEL_D_PD;
      }
      m_slave->set_r4(m_lmk04816_regs.get_reg_data(4));
      m_slave->set_r8(m_lmk04816_regs.get_reg_data(8));
      m_delays.adc_dly_ns = coerced_delay;
      break;

    // ------------------------------------------------------
    default:
      log(0, "set_clock_delay: Requested source is invalid.");
      return RCC_ERROR;
  }

  // Delays are applied only on a sync event
  if (resync)
  {
    sync_clocks();
  }

  return RCC_OK;
}


Lmk04816Controller::opmode_t Lmk04816Controller::get_clocking_mode(
  const double master_clock_rate,
  const double system_ref_rate) const
{
  Lmk04816Controller::opmode_t clocking_mode = INVALID;

  if (frequencies_are_equal(system_ref_rate, 10e6))
  {
    if (frequencies_are_equal(master_clock_rate, 184.32e6))
    {
      // 10MHz reference, 184.32MHz master clock out, not zero delay
      clocking_mode = m10M_184_32M_NOZDEL;
    }
    else if (frequencies_are_equal(master_clock_rate, 200e6))
    {
      // 10MHz reference, 200MHz master clock out, zero delay
      clocking_mode = m10M_200M_ZDEL;
    }
  }

  return clocking_mode;
}


bool Lmk04816Controller::cache_clocking_mode(
  const uint32_t hw_rev,
  const Lmk04816Controller::opmode_t clocking_mode)
{
  bool success = true;

  switch (clocking_mode)
  {
    // -------------------------------------------------
    // used for debug purposes only
    case m10M_200M_NOZDEL:
    {
      m_vco_freq = 2400e6;
      m_lmk04816_regs.MODE = lmk04816_regs_t::MODE_DUAL_INT;

      // PLL1 - 2 MHz compare frequency
      m_lmk04816_regs.PLL1_N_28       = 48;
      m_lmk04816_regs.PLL1_R_27       = 5;
      m_lmk04816_regs.PLL1_CP_GAIN_27 = lmk04816_regs_t::PLL1_CP_GAIN_27_100UA;

      // PLL2 - 96 MHz compare frequency
      m_lmk04816_regs.PLL2_N_30 = 25;
      m_lmk04816_regs.PLL2_P_30 = lmk04816_regs_t::PLL2_P_30_DIV_2A;
      m_lmk04816_regs.PLL2_R_28 = 4;
      m_lmk04816_regs.PLL2_CP_GAIN_26 = lmk04816_regs_t::PLL2_CP_GAIN_26_3200UA;
    }
    break;

    // -------------------------------------------------
    // Normal mode
    case m10M_200M_ZDEL:
    {
      m_vco_freq = 2400e6;
      m_lmk04816_regs.MODE = lmk04816_regs_t::MODE_DUAL_INT_ZER_DELAY;

      // PLL1 - 2 MHz compare frequency
      m_lmk04816_regs.PLL1_N_28       = 5;
      m_lmk04816_regs.PLL1_R_27       = 5;
      m_lmk04816_regs.PLL1_CP_GAIN_27 = lmk04816_regs_t::PLL1_CP_GAIN_27_1600UA;

      // PLL2 - 96 MHz compare frequency
      m_lmk04816_regs.PLL2_N_30 = 5;
      m_lmk04816_regs.PLL2_P_30 = lmk04816_regs_t::PLL2_P_30_DIV_5;
      m_lmk04816_regs.PLL2_R_28 = 2;

      if (hw_rev <= 4)
      {
        m_lmk04816_regs.PLL2_CP_GAIN_26 = lmk04816_regs_t::PLL2_CP_GAIN_26_1600UA;
      }
      else
      {
        m_lmk04816_regs.PLL2_CP_GAIN_26 = lmk04816_regs_t::PLL2_CP_GAIN_26_400UA;
      }
    }
    break;

    // -------------------------------------------------
    // LTE with 10 MHz ref
    case m10M_184_32M_NOZDEL:
    {
      m_vco_freq = 2580.48e6;
      m_lmk04816_regs.MODE = lmk04816_regs_t::MODE_DUAL_INT;

      // PLL1 - 2 MHz compare frequency
      m_lmk04816_regs.PLL1_N_28       = 48;
      m_lmk04816_regs.PLL1_R_27       = 5;
      m_lmk04816_regs.PLL1_CP_GAIN_27 = lmk04816_regs_t::PLL1_CP_GAIN_27_1600UA;

      // PLL2 - 7.68 MHz compare frequency
      m_lmk04816_regs.PLL2_N_30       = 168;
      m_lmk04816_regs.PLL2_P_30       = lmk04816_regs_t::PLL2_P_30_DIV_2A;
      m_lmk04816_regs.PLL2_R_28       = 25;
      m_lmk04816_regs.PLL2_CP_GAIN_26 = lmk04816_regs_t::PLL2_CP_GAIN_26_100UA;

      m_lmk04816_regs.PLL2_R3_LF = lmk04816_regs_t::PLL2_R3_LF_4KILO_OHM;
      m_lmk04816_regs.PLL2_C3_LF = lmk04816_regs_t::PLL2_C3_LF_39PF;

      m_lmk04816_regs.PLL2_R4_LF = lmk04816_regs_t::PLL2_R4_LF_1KILO_OHM;
      m_lmk04816_regs.PLL2_C4_LF = lmk04816_regs_t::PLL2_C4_LF_71PF;
    }
    break;

    // -------------------------------------------------
    // other modes are not supported
    default:
      success = false;
      break;
  }

  return success;
}


bool Lmk04816Controller::frequencies_are_equal(const double a, const double b)
{
  bool are_equal = true;

  if (std::abs(a - b) > DOUBLE_PRECISION_DELTA)
  {
    are_equal = false;
  }

  return are_equal;
}
