// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef MAX2871_HPP_INCLUDED
#define MAX2871_HPP_INCLUDED

#include "max2871_regs.hh"
#include "OcpiDebugApi.hh"
#include <stdint.h>
#include <memory>


// Forward declarations
class Max2871Interface;


class Max2871Controller
{
public:
  /**
   * LD Pin Modes
   */
  typedef enum {
    LD_PIN_MODE_LOW,
    LD_PIN_MODE_DLD,
    LD_PIN_MODE_ALD,
    LD_PIN_MODE_HIGH
  } ld_pin_mode_t;

  /**
   * MUXOUT Modes
   */
  typedef enum {
    MUXOUT_TRI_STATE,
    MUXOUT_HIGH,
    MUXOUT_LOW,
    MUXOUT_RDIV,
    MUXOUT_NDIV,
    MUXOUT_ALD,
    MUXOUT_DLD,
    MUXOUT_SYNC,
    MUXOUT_SPI
  } muxout_mode_t;

  /**
   * Charge Pump Currents
   */
  typedef enum {
    CHARGE_PUMP_CURRENT_0_32MA,
    CHARGE_PUMP_CURRENT_0_64MA,
    CHARGE_PUMP_CURRENT_0_96MA,
    CHARGE_PUMP_CURRENT_1_28MA,
    CHARGE_PUMP_CURRENT_1_60MA,
    CHARGE_PUMP_CURRENT_1_92MA,
    CHARGE_PUMP_CURRENT_2_24MA,
    CHARGE_PUMP_CURRENT_2_56MA,
    CHARGE_PUMP_CURRENT_2_88MA,
    CHARGE_PUMP_CURRENT_3_20MA,
    CHARGE_PUMP_CURRENT_3_52MA,
    CHARGE_PUMP_CURRENT_3_84MA,
    CHARGE_PUMP_CURRENT_4_16MA,
    CHARGE_PUMP_CURRENT_4_48MA,
    CHARGE_PUMP_CURRENT_4_80MA,
    CHARGE_PUMP_CURRENT_5_12MA
  } charge_pump_current_t;

  /**
   * Output Powers
   */
  typedef enum {
    OUTPUT_POWER_M4DBM,
    OUTPUT_POWER_M1DBM,
    OUTPUT_POWER_2DBM,
    OUTPUT_POWER_5DBM
  } output_power_t;

  typedef enum {
    LOW_NOISE_AND_SPUR_LOW_NOISE,
    LOW_NOISE_AND_SPUR_LOW_SPUR_1,
    LOW_NOISE_AND_SPUR_LOW_SPUR_2
  } low_noise_and_spur_t;

  typedef enum {
    CLOCK_DIV_MODE_CLOCK_DIVIDER_OFF,
    CLOCK_DIV_MODE_FAST_LOCK,
    CLOCK_DIV_MODE_PHASE
  } clock_divider_mode_t;

  Max2871Controller(std::shared_ptr<Max2871Interface> slave);

  // Power up the synthesizer
  void power_up(void);

  // Shut down the synthesizer
  void shutdown(void);

  // Check if the synthesizer is shut down
  bool is_shutdown(void);

  // Set frequency
  //    `target_freq`:     target frequency
  //    `ref_freq`:        reference frequency
  //    `target_pfd_freq`: target phase detector frequency
  //    `is_int_n`:        enable integer-N tuning
  //  returns actual frequency
  double cache_frequency(double target_freq, double ref_freq, double target_pfd_freq, bool is_int_n);

  // Set output power
  void cache_output_power(output_power_t power);

  // Set lock detect pin mode
  void cache_ld_pin_mode(ld_pin_mode_t mode);

  // Set muxout pin mode
  void cache_muxout_mode(muxout_mode_t mode);

  // Set charge pump current
  void cache_charge_pump_current(charge_pump_current_t cp_current);

  // Enable or disable auto retune
  void cache_auto_retune(bool enabled);

  // Set clock divider mode
  void cache_clock_divider_mode(clock_divider_mode_t mode);

  // Enable or disable cycle slip mode
  void cache_cycle_slip_mode(bool enabled);

  // Set low noise and spur mode
  void cache_low_noise_and_spur(low_noise_and_spur_t mode);

  // Set phase
  void cache_phase(uint16_t phase);

  // Write values configured by the set_* functions.
  void commit();

  // Check whether this is in a state where it can be synchronized
  bool can_sync();

  // Configure synthesizer for phase synchronization
  void config_for_sync(bool enable);

protected:
  max2871_regs_t m_regs;
  bool m_can_sync;
  bool m_config_for_sync;
  bool m_write_all_regs;

private:
  std::shared_ptr<Max2871Interface> m_slave;
  bool m_delay_after_write;
};



#endif // MAX2871_HPP_INCLUDED
