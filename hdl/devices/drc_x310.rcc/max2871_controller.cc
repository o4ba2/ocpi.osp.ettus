// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>
#include <cmath>
#include <thread>
#include <vector>
#include "max2871_controller.hh"
#include "slaves/max2871_slave.hh"

/**
 * MAX2871
 */
// clang-format off
// Table of frequency ranges for each VCO value.
// The values were derived from sampling multiple
// units over a temperature range of -10 to 40 deg C.
typedef std::map<uint8_t, double> vco_map_t;
static const vco_map_t max2871_vco_map {
  {0, 2838472816.0},
  {1, 2879070053.0},
  {2, 2921202504.0},
  {3, 2960407579.0},
  {4, 3001687422.0},
  {5, 3048662562.0},
  {6, 3097511550.0},
  {7, 3145085864.0},
  {8, 3201050835.0},
  {9, 3259581909.0},
  {10, 3321408729.0},
  {11, 3375217285.0},
  {12, 3432807972.0},
  {13, 3503759088.0},
  {14, 3579011283.0},
  {15, 3683570865.0},
  {20, 3711845712.0},
  {21, 3762188221.0},
  {22, 3814209551.0},
  {23, 3865820020.0},
  {24, 3922520021.0},
  {25, 3981682709.0},
  {26, 4043154280.0},
  {27, 4100400020.0},
  {28, 4159647583.0},
  {29, 4228164842.0},
  {30, 4299359879.0},
  {31, 4395947962.0},
  {33, 4426512061.0},
  {34, 4480333656.0},
  {35, 4526297331.0},
  {36, 4574689510.0},
  {37, 4633102021.0},
  {38, 4693755616.0},
  {39, 4745624435.0},
  {40, 4803922123.0},
  {41, 4871523881.0},
  {42, 4942111286.0},
  {43, 5000192446.0},
  {44, 5059567510.0},
  {45, 5136258187.0},
  {46, 5215827295.0},
  {47, 5341282949.0},
  {49, 5389819310.0},
  {50, 5444868434.0},
  {51, 5500079705.0},
  {52, 5555329630.0},
  {53, 5615049833.0},
  {54, 5676098527.0},
  {55, 5744191577.0},
  {56, 5810869917.0},
  {57, 5879176194.0},
  {58, 5952430629.0},
  {59, 6016743964.0},
  {60, 6090658690.0},
  {61, 6128133570.0}
};
// clang-format on


Max2871Controller::Max2871Controller(std::shared_ptr<Max2871Interface> slave)
  : m_can_sync(false)
  , m_config_for_sync(false)
  , m_write_all_regs(true)
  , m_slave(slave)
  , m_delay_after_write(true)
{
  power_up();
}

void Max2871Controller::power_up(void)
{
  m_regs.power_down    = max2871_regs_t::POWER_DOWN_NORMAL;
  m_regs.double_buffer = max2871_regs_t::DOUBLE_BUFFER_ENABLED;

  // According to MAX2871 data sheets:
  // "Upon power-up, the registers should be programmed twice with at
  // least a 20ms pause between writes.  The first write ensures that
  // the device is enabled, and the second write starts the VCO
  // selection process."
  // The first write and the 20ms wait are done here.  The second write
  // is done when any other function that does a write to the registers
  // is called (such as tuning).
  m_write_all_regs    = true;
  m_delay_after_write = true;
  commit();
  m_write_all_regs = true; // Next call to commit() writes all regs
}

void Max2871Controller::shutdown(void)
{
  m_regs.rf_output_enable  = max2871_regs_t::RF_OUTPUT_ENABLE_DISABLED;
  m_regs.aux_output_enable = max2871_regs_t::AUX_OUTPUT_ENABLE_DISABLED;
  m_regs.power_down        = max2871_regs_t::POWER_DOWN_SHUTDOWN;
  commit();
}

bool Max2871Controller::is_shutdown(void)
{
  return (m_regs.power_down == max2871_regs_t::POWER_DOWN_SHUTDOWN);
}

double Max2871Controller::cache_frequency(
  double target_freq, double ref_freq, double target_pfd_freq, bool is_int_n)
{
  // map mode setting to valid integer divider (N) values
  static const int INT_N_MODE_DIV_MIN = 16;
  static const int INT_N_MODE_DIV_MAX = 65535;
  static const int FRAC_N_MODE_DIV_MIN = 19;
  static const int FRAC_N_MODE_DIV_MAX = 4091;

  // other ranges and constants from MAX2871 datasheets
  static const int CLOCK_DIV_MIN = 1;
  static const int CLOCK_DIV_MAX = 4095;
  static const int R_MAX = 1023;
  static const double MIN_VCO_FREQ = 3e9;
  static const double BS_FREQ      = 50e3;
  static const int MAX_BS_VALUE    = 1023;

  m_regs.feedback_select = max2871_regs_t::FEEDBACK_SELECT_DIVIDED;

  int T           = 0;
  int D           = ref_freq <= 10.0e6 ? 1 : 0;
  int R           = 0;
  int BS          = 0;
  int N           = 0;
  int FRAC        = 0;
  int MOD         = 4095;
  int RFdiv       = 1;
  double pfd_freq = target_pfd_freq;
  bool feedback_divided =
    (m_regs.feedback_select == max2871_regs_t::FEEDBACK_SELECT_DIVIDED);

  // increase RF divider until acceptable VCO frequency (MIN freq for MAX2871 VCO is
  // 3GHz)
  if (target_freq <= 0.0) {
    throw std::runtime_error("Ivalid frequency");
  }
  double vco_freq = target_freq;
  while (vco_freq < MIN_VCO_FREQ) {
    vco_freq *= 2;
    RFdiv *= 2;
  }

  // The feedback frequency can be the fundamental VCO frequency or
  // divided frequency.  The output divider for MAX2871 is actually
  // 2 dividers, but only the first (1/2/4/8/16) is included in the
  // feedback loop.
  int fb_divisor = feedback_divided ? (RFdiv > 16 ? 16 : RFdiv) : 1;

  /*
   * The goal here is to loop though possible R dividers,
   * band select clock dividers, N (int) dividers, and FRAC
   * (frac) dividers.
   *
   * Calculate the N and F dividers for each set of values.
   * The loop exits when it meets all of the constraints.
   * The resulting loop values are loaded into the registers.
   *
   * f_pfd = f_ref*(1+D)/(R*(1+T))
   * f_vco = (N + (FRAC/MOD))*f_pfd
   *     N = f_vco/f_pfd - FRAC/MOD = f_vco*((R*(T+1))/(f_ref*(1+D))) - FRAC/MOD
   * f_rf  = f_vco/RFdiv
   */
  for (R = int(ref_freq * (1 + D) / (target_pfd_freq * (1 + T))); R <= R_MAX;
     R++) {
    // PFD input frequency = f_ref/R ... ignoring Reference doubler/divide-by-2 (D &
    // T)
    pfd_freq = ref_freq * (1 + D) / (R * (1 + T));

    // keep the PFD frequency at or below target
    if (pfd_freq > target_pfd_freq)
      continue;

    // ignore fractional part of tuning
    N = int((vco_freq / pfd_freq) / fb_divisor);

    // Fractional-N calculation
    FRAC = int(std::lround(((vco_freq / pfd_freq) / fb_divisor - N) * MOD));

    if (is_int_n) {
      if (FRAC
        > (MOD / 2)) // Round integer such that actual freq is closest to target
        N++;
      FRAC = 0;
    }

    // keep N within int divider requirements
    if (is_int_n) {
      if (N <= INT_N_MODE_DIV_MIN)
        continue;
      if (N >= INT_N_MODE_DIV_MAX)
        continue;
    } else {
      if (N <= FRAC_N_MODE_DIV_MIN)
        continue;
      if (N >= FRAC_N_MODE_DIV_MAX)
        continue;
    }

    // keep pfd freq low enough to achieve 50kHz BS clock
    BS = static_cast<int>(std::ceil(pfd_freq / BS_FREQ));
    if (BS <= MAX_BS_VALUE)
      break;
  }
  if(R > R_MAX) {
    throw std::runtime_error("Invalid divider");
  }

  // Reference divide-by-2 for 50% duty cycle
  // if R even, move one divide by 2 to to regs.reference_divide_by_2
  if (R % 2 == 0) {
    T = 1;
    R /= 2;
  }

  // actual frequency calculation
  double actual_freq = double((N + (double(FRAC) / double(MOD))) * ref_freq
                * (1 + int(D)) / (R * (1 + int(T))))
             * fb_divisor / RFdiv;

  OCPI::OS::Log::print(OCPI_LOG_DEBUG,
    "MAX2871 - Intermediates: ref=%0.2f, outdiv=%f, fbdiv=%f",
    ref_freq, double(RFdiv * 2), double(N + double(FRAC) / double(MOD)));
  OCPI::OS::Log::print(OCPI_LOG_DEBUG,
    "MAX2871 - Tune: R=%d, BS=%d, N=%d, FRAC=%d, MOD=%d, T=%d, D=%d, RFdiv=%d, type=%s",
    R, BS, N, FRAC, MOD, T, D, RFdiv,
    ((is_int_n) ? "Integer-N" : "Fractional"));
  OCPI::OS::Log::print(OCPI_LOG_DEBUG,
    "MAX2871 - Frequencies (MHz): REQ=%0.2f, ACT=%0.2f, VCO=%0.2f, PFD=%0.2f, BAND=%0.2f",
    (target_freq / 1e6), (actual_freq / 1e6), (vco_freq / 1e6),
    (pfd_freq / 1e6), (pfd_freq / BS / 1e6));

  // load the register values
  m_regs.rf_output_enable = max2871_regs_t::RF_OUTPUT_ENABLE_ENABLED;

  if (is_int_n) {
    m_regs.cpl        = max2871_regs_t::CPL_DISABLED;
    m_regs.ldf        = max2871_regs_t::LDF_INT_N;
    m_regs.int_n_mode = max2871_regs_t::INT_N_MODE_INT_N;
  } else {
    m_regs.cpl        = max2871_regs_t::CPL_ENABLED;
    m_regs.ldf        = max2871_regs_t::LDF_FRAC_N;
    m_regs.int_n_mode = max2871_regs_t::INT_N_MODE_FRAC_N;
  }

  m_regs.lds = pfd_freq <= 32e6 ? max2871_regs_t::LDS_SLOW : max2871_regs_t::LDS_FAST;

  m_regs.frac_12_bit = FRAC;
  m_regs.int_16_bit  = N;
  m_regs.mod_12_bit  = MOD;
  m_regs.clock_divider_12_bit =
    std::max(CLOCK_DIV_MIN, int(std::ceil(400e-6 * pfd_freq / MOD)));
  if (m_regs.clock_divider_12_bit > CLOCK_DIV_MAX) {
    throw std::runtime_error("Invalid divider");
  }
  m_regs.r_counter_10_bit      = R;
  m_regs.reference_divide_by_2 = T ? max2871_regs_t::REFERENCE_DIVIDE_BY_2_ENABLED
                  : max2871_regs_t::REFERENCE_DIVIDE_BY_2_DISABLED;
  m_regs.reference_doubler = D ? max2871_regs_t::REFERENCE_DOUBLER_ENABLED
                : max2871_regs_t::REFERENCE_DOUBLER_DISABLED;
  m_regs.band_select_clock_div = BS & 0xFF;
  m_regs.bs_msb                = (BS & 0x300) >> 8;
  switch(RFdiv) {
    // map rf divider select output dividers to enums
    case 1:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV1;
        break;
    case 2:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV2;
        break;
    case 4:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV4;
        break;
    case 8:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV8;
        break;
    case 16:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV16;
        break;
    case 32:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV32;
        break;
    case 64:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV64;
        break;
    case 128:
        m_regs.rf_divider_select = max2871_regs_t::RF_DIVIDER_SELECT_DIV128;
        break;
    default:
        throw std::runtime_error("Invalid code path");
  };

  if (m_regs.clock_div_mode == max2871_regs_t::CLOCK_DIV_MODE_FAST_LOCK) {
    // Charge pump current needs to be set to lowest value in fast lock mode
    m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_0_32MA;
    // Make sure the register containing the charge pump current is written
    m_write_all_regs = true;
  }

  // To support phase synchronization on MAX2871, the same VCO
  // subband must be manually programmed on all synthesizers and
  // several registers must be set to specific values.
  if (m_config_for_sync) {
    // Need to manually program VCO value
    static const double MIN_VCO_FREQ = 3e9;
    double vco_freq                  = target_freq;
    while (vco_freq < MIN_VCO_FREQ)
      vco_freq *= 2;
    uint8_t vco_index = 0xFF;
    for (const vco_map_t::value_type& vco : max2871_vco_map) {
      if (vco_freq < vco.second) {
        vco_index = vco.first;
        break;
      }
    }
    if (vco_index == 0xFF)
      throw std::runtime_error("Invalid VCO frequency");

    // Settings required for phase synchronization as per MAX2871 datasheet
    m_regs.shutdown_vas       = max2871_regs_t::SHUTDOWN_VAS_DISABLED;
    m_regs.vco                = vco_index;
    m_regs.low_noise_and_spur = max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_NOISE;
    m_regs.f01                = max2871_regs_t::F01_FRAC_N;
    m_regs.aux_output_select  = max2871_regs_t::AUX_OUTPUT_SELECT_DIVIDED;
  } else {
    // Reset values to defaults
    m_regs.shutdown_vas =
      max2871_regs_t::SHUTDOWN_VAS_ENABLED; // turn VCO auto selection on
    m_regs.low_noise_and_spur = max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_SPUR_2;
    m_regs.f01                = max2871_regs_t::F01_AUTO;
    m_regs.aux_output_select  = max2871_regs_t::AUX_OUTPUT_SELECT_FUNDAMENTAL;
  }

  return actual_freq;
}

void Max2871Controller::cache_output_power(output_power_t power)
{
  switch (power) {
    case OUTPUT_POWER_M4DBM:
      m_regs.output_power = max2871_regs_t::OUTPUT_POWER_M4DBM;
      break;
    case OUTPUT_POWER_M1DBM:
      m_regs.output_power = max2871_regs_t::OUTPUT_POWER_M1DBM;
      break;
    case OUTPUT_POWER_2DBM:
      m_regs.output_power = max2871_regs_t::OUTPUT_POWER_2DBM;
      break;
    case OUTPUT_POWER_5DBM:
      m_regs.output_power = max2871_regs_t::OUTPUT_POWER_5DBM;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_ld_pin_mode(ld_pin_mode_t mode)
{
  switch (mode) {
    case LD_PIN_MODE_LOW:
      m_regs.ld_pin_mode = max2871_regs_t::LD_PIN_MODE_LOW;
      break;
    case LD_PIN_MODE_DLD:
      m_regs.ld_pin_mode = max2871_regs_t::LD_PIN_MODE_DLD;
      break;
    case LD_PIN_MODE_ALD:
      m_regs.ld_pin_mode = max2871_regs_t::LD_PIN_MODE_ALD;
      break;
    case LD_PIN_MODE_HIGH:
      m_regs.ld_pin_mode = max2871_regs_t::LD_PIN_MODE_HIGH;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_muxout_mode(muxout_mode_t mode)
{
  switch (mode) {
    case MUXOUT_SYNC:
      m_regs.muxout = max2871_regs_t::MUXOUT_SYNC;
      break;
    case MUXOUT_SPI:
      m_regs.muxout = max2871_regs_t::MUXOUT_SPI;
      break;
    case MUXOUT_TRI_STATE:
      m_regs.muxout = max2871_regs_t::MUXOUT_TRI_STATE;
      break;
    case MUXOUT_HIGH:
      m_regs.muxout = max2871_regs_t::MUXOUT_HIGH;
      break;
    case MUXOUT_LOW:
      m_regs.muxout = max2871_regs_t::MUXOUT_LOW;
      break;
    case MUXOUT_RDIV:
      m_regs.muxout = max2871_regs_t::MUXOUT_RDIV;
      break;
    case MUXOUT_NDIV:
      m_regs.muxout = max2871_regs_t::MUXOUT_NDIV;
      break;
    case MUXOUT_ALD:
      m_regs.muxout = max2871_regs_t::MUXOUT_ALD;
      break;
    case MUXOUT_DLD:
      m_regs.muxout = max2871_regs_t::MUXOUT_DLD;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_charge_pump_current(charge_pump_current_t cp_current)
{
  switch (cp_current) {
    case CHARGE_PUMP_CURRENT_0_32MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_0_32MA;
      break;
    case CHARGE_PUMP_CURRENT_0_64MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_0_64MA;
      break;
    case CHARGE_PUMP_CURRENT_0_96MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_0_96MA;
      break;
    case CHARGE_PUMP_CURRENT_1_28MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_1_28MA;
      break;
    case CHARGE_PUMP_CURRENT_1_60MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_1_60MA;
      break;
    case CHARGE_PUMP_CURRENT_1_92MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_1_92MA;
      break;
    case CHARGE_PUMP_CURRENT_2_24MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_2_24MA;
      break;
    case CHARGE_PUMP_CURRENT_2_56MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_2_56MA;
      break;
    case CHARGE_PUMP_CURRENT_2_88MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_2_88MA;
      break;
    case CHARGE_PUMP_CURRENT_3_20MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_3_20MA;
      break;
    case CHARGE_PUMP_CURRENT_3_52MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_3_52MA;
      break;
    case CHARGE_PUMP_CURRENT_3_84MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_3_84MA;
      break;
    case CHARGE_PUMP_CURRENT_4_16MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_4_16MA;
      break;
    case CHARGE_PUMP_CURRENT_4_48MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_4_48MA;
      break;
    case CHARGE_PUMP_CURRENT_4_80MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_4_80MA;
      break;
    case CHARGE_PUMP_CURRENT_5_12MA:
      m_regs.charge_pump_current = max2871_regs_t::CHARGE_PUMP_CURRENT_5_12MA;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_auto_retune(bool enabled)
{
  m_regs.retune = enabled ? max2871_regs_t::RETUNE_ENABLED
               : max2871_regs_t::RETUNE_DISABLED;
  m_regs.vas_dly = enabled ? max2871_regs_t::VAS_DLY_ENABLED
              : max2871_regs_t::VAS_DLY_DISABLED;
}

void Max2871Controller::cache_clock_divider_mode(clock_divider_mode_t mode)
{
  switch (mode) {
    case CLOCK_DIV_MODE_CLOCK_DIVIDER_OFF:
      m_regs.clock_div_mode = max2871_regs_t::CLOCK_DIV_MODE_CLOCK_DIVIDER_OFF;
      break;
    case CLOCK_DIV_MODE_FAST_LOCK:
      m_regs.clock_div_mode = max2871_regs_t::CLOCK_DIV_MODE_FAST_LOCK;
      break;
    case CLOCK_DIV_MODE_PHASE:
      m_regs.clock_div_mode = max2871_regs_t::CLOCK_DIV_MODE_PHASE;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_cycle_slip_mode(bool enabled)
{
  if (enabled)
    throw std::runtime_error(
      "Cycle slip mode not supported on this MAX2871 synthesizer.");
}

void Max2871Controller::cache_low_noise_and_spur(low_noise_and_spur_t mode)
{
  switch (mode) {
    case LOW_NOISE_AND_SPUR_LOW_NOISE:
      m_regs.low_noise_and_spur = max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_NOISE;
      break;
    case LOW_NOISE_AND_SPUR_LOW_SPUR_1:
      m_regs.low_noise_and_spur = max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_SPUR_1;
      break;
    case LOW_NOISE_AND_SPUR_LOW_SPUR_2:
      m_regs.low_noise_and_spur = max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_SPUR_2;
      break;
    default:
      throw std::runtime_error("Invalid code path");
  }
}

void Max2871Controller::cache_phase(uint16_t phase)
{
  m_regs.phase_12_bit = phase & 0xFFF;
}

void Max2871Controller::commit()
{
  // simplified to just always write all regs
  for (int addr = 5; addr >= 0; addr--) {
    m_slave->set_max2871_reg(m_regs.get_reg(uint32_t(addr)));
  }

  if (m_delay_after_write) {
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    m_delay_after_write = false;
  }

  // According to Maxim support, the following factors must be true to allow for
  // phase synchronization
  if (m_regs.int_n_mode == max2871_regs_t::INT_N_MODE_FRAC_N
    and m_regs.feedback_select == max2871_regs_t::FEEDBACK_SELECT_DIVIDED
    and m_regs.aux_output_select == max2871_regs_t::AUX_OUTPUT_SELECT_DIVIDED
    and m_regs.rf_divider_select <= max2871_regs_t::RF_DIVIDER_SELECT_DIV16
    and m_regs.low_noise_and_spur == max2871_regs_t::LOW_NOISE_AND_SPUR_LOW_NOISE
    and m_regs.f01 == max2871_regs_t::F01_FRAC_N
    and m_regs.reference_doubler == max2871_regs_t::REFERENCE_DOUBLER_DISABLED
    and m_regs.reference_divide_by_2
        == max2871_regs_t::REFERENCE_DIVIDE_BY_2_DISABLED
    and m_regs.r_counter_10_bit == 1) {
    m_can_sync = true;
  } else {
    m_can_sync = false;
  }
}

bool Max2871Controller::can_sync(void)
{
  return m_can_sync;
}

void Max2871Controller::config_for_sync(bool enable)
{
  m_config_for_sync = enable;
}
