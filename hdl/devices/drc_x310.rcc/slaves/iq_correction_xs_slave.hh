// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_IQ_CORRECTION_XS_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_IQ_CORRECTION_XS_SLAVE_HH__

#include <cstdint>

class IQCorrectionXsInterface
{
public:
    virtual ~IQCorrectionXsInterface() {}

    virtual bool    get_enable() = 0;
    virtual int16_t get_I_dc()   = 0;
    virtual int16_t get_Q_dc()   = 0;
    virtual int16_t get_A()      = 0;
    virtual int16_t get_B()      = 0;

    virtual void set_enable (bool val) = 0;
    virtual void set_I_dc   (int16_t val) = 0;
    virtual void set_Q_dc   (int16_t val) = 0;
    virtual void set_A      (int16_t val) = 0;
    virtual void set_B      (int16_t val) = 0;
};

template <typename T>
class IQCorrectionXsSlave : public IQCorrectionXsInterface
{
public:
    IQCorrectionXsSlave(T& slave) : m_slave(slave) {}

    bool    get_enable() override { return m_slave.get_enable(); }
    int16_t get_I_dc()   override { return m_slave.get_I_dc(); }
    int16_t get_Q_dc()   override { return m_slave.get_Q_dc(); }
    int16_t get_A()      override { return m_slave.get_A(); }
    int16_t get_B()      override { return m_slave.get_B(); }

    void set_enable (bool val) override { m_slave.set_enable(val); }
    void set_I_dc   (int16_t val) override { m_slave.set_I_dc(val); }
    void set_Q_dc   (int16_t val) override { m_slave.set_Q_dc(val); }
    void set_A      (int16_t val) override { m_slave.set_A(val); }
    void set_B      (int16_t val) override { m_slave.set_B(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_IQ_CORRECTION_XS_SLAVE_HH__ */