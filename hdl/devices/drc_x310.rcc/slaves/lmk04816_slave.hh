// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_LMK04816_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_LMK04816_SLAVE_HH__

#include <cstdint>

class Lmk04816Interface
{
public:
    virtual ~Lmk04816Interface() {}

    virtual uint32_t get_status() = 0;
    virtual uint32_t get_r0()     = 0;
    virtual uint32_t get_r1()     = 0;
    virtual uint32_t get_r2()     = 0;
    virtual uint32_t get_r3()     = 0;
    virtual uint32_t get_r4()     = 0;
    virtual uint32_t get_r5()     = 0;
    virtual uint32_t get_r6()     = 0;
    virtual uint32_t get_r7()     = 0;
    virtual uint32_t get_r8()     = 0;
    virtual uint32_t get_r9()     = 0;
    virtual uint32_t get_r10()    = 0;
    virtual uint32_t get_r11()    = 0;
    virtual uint32_t get_r12()    = 0;
    virtual uint32_t get_r13()    = 0;
    virtual uint32_t get_r14()    = 0;
    virtual uint32_t get_r15()    = 0;
    virtual uint32_t get_r16()    = 0;
    virtual uint32_t get_r24()    = 0;
    virtual uint32_t get_r25()    = 0;
    virtual uint32_t get_r26()    = 0;
    virtual uint32_t get_r27()    = 0;
    virtual uint32_t get_r28()    = 0;
    virtual uint32_t get_r29()    = 0;
    virtual uint32_t get_r30()    = 0;
    virtual uint32_t get_r31()    = 0;

    virtual void set_r0  (uint32_t val) = 0;
    virtual void set_r1  (uint32_t val) = 0;
    virtual void set_r2  (uint32_t val) = 0;
    virtual void set_r3  (uint32_t val) = 0;
    virtual void set_r4  (uint32_t val) = 0;
    virtual void set_r5  (uint32_t val) = 0;
    virtual void set_r6  (uint32_t val) = 0;
    virtual void set_r7  (uint32_t val) = 0;
    virtual void set_r8  (uint32_t val) = 0;
    virtual void set_r9  (uint32_t val) = 0;
    virtual void set_r10 (uint32_t val) = 0;
    virtual void set_r11 (uint32_t val) = 0;
    virtual void set_r12 (uint32_t val) = 0;
    virtual void set_r13 (uint32_t val) = 0;
    virtual void set_r14 (uint32_t val) = 0;
    virtual void set_r15 (uint32_t val) = 0;
    virtual void set_r16 (uint32_t val) = 0;
    virtual void set_r24 (uint32_t val) = 0;
    virtual void set_r25 (uint32_t val) = 0;
    virtual void set_r26 (uint32_t val) = 0;
    virtual void set_r27 (uint32_t val) = 0;
    virtual void set_r28 (uint32_t val) = 0;
    virtual void set_r29 (uint32_t val) = 0;
    virtual void set_r30 (uint32_t val) = 0;
    virtual void set_r31 (uint32_t val) = 0;
};

template <typename T>
class Lmk04816Slave : public Lmk04816Interface
{
public:
    Lmk04816Slave(T& slave) : m_slave(slave) {}

    uint32_t get_status() override { return m_slave.get_status(); }
    uint32_t get_r0()     override { return m_slave.get_r0(); }
    uint32_t get_r1()     override { return m_slave.get_r1(); }
    uint32_t get_r2()     override { return m_slave.get_r2(); }
    uint32_t get_r3()     override { return m_slave.get_r3(); }
    uint32_t get_r4()     override { return m_slave.get_r4(); }
    uint32_t get_r5()     override { return m_slave.get_r5(); }
    uint32_t get_r6()     override { return m_slave.get_r6(); }
    uint32_t get_r7()     override { return m_slave.get_r7(); }
    uint32_t get_r8()     override { return m_slave.get_r8(); }
    uint32_t get_r9()     override { return m_slave.get_r9(); }
    uint32_t get_r10()    override { return m_slave.get_r10(); }
    uint32_t get_r11()    override { return m_slave.get_r11(); }
    uint32_t get_r12()    override { return m_slave.get_r12(); }
    uint32_t get_r13()    override { return m_slave.get_r13(); }
    uint32_t get_r14()    override { return m_slave.get_r14(); }
    uint32_t get_r15()    override { return m_slave.get_r15(); }
    uint32_t get_r16()    override { return m_slave.get_r16(); }
    uint32_t get_r24()    override { return m_slave.get_r24(); }
    uint32_t get_r25()    override { return m_slave.get_r25(); }
    uint32_t get_r26()    override { return m_slave.get_r26(); }
    uint32_t get_r27()    override { return m_slave.get_r27(); }
    uint32_t get_r28()    override { return m_slave.get_r28(); }
    uint32_t get_r29()    override { return m_slave.get_r29(); }
    uint32_t get_r30()    override { return m_slave.get_r30(); }
    uint32_t get_r31()    override { return m_slave.get_r31(); }

    void set_r0  (uint32_t val) override { m_slave.set_r0(val); }
    void set_r1  (uint32_t val) override { m_slave.set_r1(val); }
    void set_r2  (uint32_t val) override { m_slave.set_r2(val); }
    void set_r3  (uint32_t val) override { m_slave.set_r3(val); }
    void set_r4  (uint32_t val) override { m_slave.set_r4(val); }
    void set_r5  (uint32_t val) override { m_slave.set_r5(val); }
    void set_r6  (uint32_t val) override { m_slave.set_r6(val); }
    void set_r7  (uint32_t val) override { m_slave.set_r7(val); }
    void set_r8  (uint32_t val) override { m_slave.set_r8(val); }
    void set_r9  (uint32_t val) override { m_slave.set_r9(val); }
    void set_r10 (uint32_t val) override { m_slave.set_r10(val); }
    void set_r11 (uint32_t val) override { m_slave.set_r11(val); }
    void set_r12 (uint32_t val) override { m_slave.set_r12(val); }
    void set_r13 (uint32_t val) override { m_slave.set_r13(val); }
    void set_r14 (uint32_t val) override { m_slave.set_r14(val); }
    void set_r15 (uint32_t val) override { m_slave.set_r15(val); }
    void set_r16 (uint32_t val) override { m_slave.set_r16(val); }
    void set_r24 (uint32_t val) override { m_slave.set_r24(val); }
    void set_r25 (uint32_t val) override { m_slave.set_r25(val); }
    void set_r26 (uint32_t val) override { m_slave.set_r26(val); }
    void set_r27 (uint32_t val) override { m_slave.set_r27(val); }
    void set_r28 (uint32_t val) override { m_slave.set_r28(val); }
    void set_r29 (uint32_t val) override { m_slave.set_r29(val); }
    void set_r30 (uint32_t val) override { m_slave.set_r30(val); }
    void set_r31 (uint32_t val) override { m_slave.set_r31(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_LMK04816_SLAVE_HH__ */