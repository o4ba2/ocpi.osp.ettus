// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_UBX160_EEPROM_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_UBX160_EEPROM_SLAVE_HH__

#include <cstdint>

class Ubx160EepromInterface
{
public:
    virtual ~Ubx160EepromInterface() {}

    virtual uint32_t get_control()         = 0;
    virtual uint32_t get_addr_size_bytes() = 0;
    virtual uint32_t get_addr()            = 0;
    virtual uint8_t get_read_byte()       = 0;

    virtual void set_control         (uint32_t val) = 0;
    virtual void set_addr_size_bytes (uint32_t val) = 0;
    virtual void set_addr            (uint32_t val) = 0;
};

template <typename T>
class Ubx160EepromSlave : public Ubx160EepromInterface
{
public:
    Ubx160EepromSlave(T& slave) : m_slave(slave) {}

    uint32_t get_control()         override { return m_slave.get_control(); }
    uint32_t get_addr_size_bytes() override { return m_slave.get_addr_size_bytes(); }
    uint32_t get_addr()            override { return m_slave.get_addr(); }
    uint8_t get_read_byte()       override { return m_slave.get_read_byte(); }

    void set_control         (uint32_t val) override { m_slave.set_control(val); }
    void set_addr_size_bytes (uint32_t val) override { m_slave.set_addr_size_bytes(val); }
    void set_addr            (uint32_t val) override { m_slave.set_addr(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_UBX160_EEPROM_SLAVE_HH__ */