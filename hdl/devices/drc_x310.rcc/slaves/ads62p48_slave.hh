// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_ADS62P48_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_ADS62P48_SLAVE_HH__

#include <cstdint>

class Ads62p48Interface
{
public:
    virtual ~Ads62p48Interface() {}

    virtual uint8_t get_idelay_value()  = 0;
    virtual uint8_t get_pattern_check() = 0;
    virtual bool get_enable()        = 0;
    virtual uint8_t get_reg_0x00()      = 0;
    virtual uint8_t get_reg_0x20()      = 0;
    virtual uint8_t get_reg_0x3f()      = 0;
    virtual uint8_t get_reg_0x40()      = 0;
    virtual uint8_t get_reg_0x41()      = 0;
    virtual uint8_t get_reg_0x44()      = 0;
    virtual uint8_t get_reg_0x50()      = 0;
    virtual uint8_t get_reg_0x51()      = 0;
    virtual uint8_t get_reg_0x52()      = 0;
    virtual uint8_t get_reg_0x53()      = 0;
    virtual uint8_t get_reg_0x55()      = 0;
    virtual uint8_t get_reg_0x57()      = 0;
    virtual uint8_t get_reg_0x62()      = 0;
    virtual uint8_t get_reg_0x63()      = 0;
    virtual uint8_t get_reg_0x66()      = 0;
    virtual uint8_t get_reg_0x68()      = 0;
    virtual uint8_t get_reg_0x6a()      = 0;
    virtual uint8_t get_reg_0x75()      = 0;
    virtual uint8_t get_reg_0x76()      = 0;

    virtual void set_idelay_value (uint8_t val) = 0;
    virtual void set_enable       (bool val) = 0;
    virtual void set_reg_0x00     (uint8_t val) = 0;
    virtual void set_reg_0x20     (uint8_t val) = 0;
    virtual void set_reg_0x3f     (uint8_t val) = 0;
    virtual void set_reg_0x40     (uint8_t val) = 0;
    virtual void set_reg_0x41     (uint8_t val) = 0;
    virtual void set_reg_0x44     (uint8_t val) = 0;
    virtual void set_reg_0x50     (uint8_t val) = 0;
    virtual void set_reg_0x51     (uint8_t val) = 0;
    virtual void set_reg_0x52     (uint8_t val) = 0;
    virtual void set_reg_0x53     (uint8_t val) = 0;
    virtual void set_reg_0x55     (uint8_t val) = 0;
    virtual void set_reg_0x57     (uint8_t val) = 0;
    virtual void set_reg_0x62     (uint8_t val) = 0;
    virtual void set_reg_0x63     (uint8_t val) = 0;
    virtual void set_reg_0x66     (uint8_t val) = 0;
    virtual void set_reg_0x68     (uint8_t val) = 0;
    virtual void set_reg_0x6a     (uint8_t val) = 0;
    virtual void set_reg_0x75     (uint8_t val) = 0;
    virtual void set_reg_0x76     (uint8_t val) = 0;
};

template <typename T>
class Ads62p48Slave : public Ads62p48Interface
{
public:
    Ads62p48Slave(T& slave) : m_slave(slave) {}

    uint8_t get_idelay_value()  override { return m_slave.get_idelay_value(); }
    uint8_t get_pattern_check() override { return m_slave.get_pattern_check(); }
    bool get_enable()        override { return m_slave.get_enable(); }
    uint8_t get_reg_0x00()      override { return m_slave.get_reg_0x00(); }
    uint8_t get_reg_0x20()      override { return m_slave.get_reg_0x20(); }
    uint8_t get_reg_0x3f()      override { return m_slave.get_reg_0x3f(); }
    uint8_t get_reg_0x40()      override { return m_slave.get_reg_0x40(); }
    uint8_t get_reg_0x41()      override { return m_slave.get_reg_0x41(); }
    uint8_t get_reg_0x44()      override { return m_slave.get_reg_0x44(); }
    uint8_t get_reg_0x50()      override { return m_slave.get_reg_0x50(); }
    uint8_t get_reg_0x51()      override { return m_slave.get_reg_0x51(); }
    uint8_t get_reg_0x52()      override { return m_slave.get_reg_0x52(); }
    uint8_t get_reg_0x53()      override { return m_slave.get_reg_0x53(); }
    uint8_t get_reg_0x55()      override { return m_slave.get_reg_0x55(); }
    uint8_t get_reg_0x57()      override { return m_slave.get_reg_0x57(); }
    uint8_t get_reg_0x62()      override { return m_slave.get_reg_0x62(); }
    uint8_t get_reg_0x63()      override { return m_slave.get_reg_0x63(); }
    uint8_t get_reg_0x66()      override { return m_slave.get_reg_0x66(); }
    uint8_t get_reg_0x68()      override { return m_slave.get_reg_0x68(); }
    uint8_t get_reg_0x6a()      override { return m_slave.get_reg_0x6a(); }
    uint8_t get_reg_0x75()      override { return m_slave.get_reg_0x75(); }
    uint8_t get_reg_0x76()      override { return m_slave.get_reg_0x76(); }

    void set_idelay_value (uint8_t val) override { m_slave.set_idelay_value(val); }
    void set_enable       (bool val) override { m_slave.set_enable(val); }
    void set_reg_0x00     (uint8_t val) override { m_slave.set_reg_0x00(val); }
    void set_reg_0x20     (uint8_t val) override { m_slave.set_reg_0x20(val); }
    void set_reg_0x3f     (uint8_t val) override { m_slave.set_reg_0x3f(val); }
    void set_reg_0x40     (uint8_t val) override { m_slave.set_reg_0x40(val); }
    void set_reg_0x41     (uint8_t val) override { m_slave.set_reg_0x41(val); }
    void set_reg_0x44     (uint8_t val) override { m_slave.set_reg_0x44(val); }
    void set_reg_0x50     (uint8_t val) override { m_slave.set_reg_0x50(val); }
    void set_reg_0x51     (uint8_t val) override { m_slave.set_reg_0x51(val); }
    void set_reg_0x52     (uint8_t val) override { m_slave.set_reg_0x52(val); }
    void set_reg_0x53     (uint8_t val) override { m_slave.set_reg_0x53(val); }
    void set_reg_0x55     (uint8_t val) override { m_slave.set_reg_0x55(val); }
    void set_reg_0x57     (uint8_t val) override { m_slave.set_reg_0x57(val); }
    void set_reg_0x62     (uint8_t val) override { m_slave.set_reg_0x62(val); }
    void set_reg_0x63     (uint8_t val) override { m_slave.set_reg_0x63(val); }
    void set_reg_0x66     (uint8_t val) override { m_slave.set_reg_0x66(val); }
    void set_reg_0x68     (uint8_t val) override { m_slave.set_reg_0x68(val); }
    void set_reg_0x6a     (uint8_t val) override { m_slave.set_reg_0x6a(val); }
    void set_reg_0x75     (uint8_t val) override { m_slave.set_reg_0x75(val); }
    void set_reg_0x76     (uint8_t val) override { m_slave.set_reg_0x76(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_ADS62P48_SLAVE_HH__ */