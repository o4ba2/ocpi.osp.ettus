// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_MAX2871_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_MAX2871_SLAVE_HH__

#include <cstdint>

class Max2871Interface
{
public:
    virtual ~Max2871Interface() {}

    virtual uint32_t get_max2871_reg() = 0;

    virtual void set_max2871_reg (uint32_t val) = 0;
};

template <typename T>
class Max2871Slave : public Max2871Interface
{
public:
    Max2871Slave(T& slave) : m_slave(slave) {}

    uint32_t get_max2871_reg() override { return m_slave.get_max2871_reg(); }

    void set_max2871_reg (uint32_t val) override { m_slave.set_max2871_reg(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_MAX2871_SLAVE_HH__ */