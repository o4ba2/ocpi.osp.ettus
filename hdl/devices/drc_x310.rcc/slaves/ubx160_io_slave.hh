// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_UBX160_IO_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_UBX160_IO_SLAVE_HH__

#include <cstdint>

class Ubx160IoInterface
{
public:
    virtual ~Ubx160IoInterface() {}

    virtual uint8_t get_RX_LO_LOCKED() = 0;
    virtual uint8_t get_TX_LO_LOCKED() = 0;
    virtual uint8_t get_RXLO1_SYNC()   = 0;
    virtual uint8_t get_RXLO2_SYNC()   = 0;
    virtual uint8_t get_TXLO1_SYNC()   = 0;
    virtual uint8_t get_TXLO2_SYNC()   = 0;
    virtual uint8_t get_CPLD_RST_N()   = 0;
    virtual uint8_t get_RX_GAIN()      = 0;
    virtual uint8_t get_TX_GAIN()      = 0;
    virtual uint8_t get_RX_ANT()       = 0;
    virtual uint8_t get_TX_EN_N()      = 0;
    virtual uint8_t get_RX_EN_N()      = 0;

    virtual void set_CPLD_RST_N (uint8_t val) = 0;
    virtual void set_RX_GAIN    (uint8_t val) = 0;
    virtual void set_TX_GAIN    (uint8_t val) = 0;
    virtual void set_RX_ANT     (uint8_t val) = 0;
    virtual void set_TX_EN_N    (uint8_t val) = 0;
    virtual void set_RX_EN_N    (uint8_t val) = 0;
};

template <typename T>
class Ubx160IoSlave : public Ubx160IoInterface
{
public:
    Ubx160IoSlave(T& slave) : m_slave(slave) {}

    uint8_t get_RX_LO_LOCKED() override { return m_slave.get_RX_LO_LOCKED(); }
    uint8_t get_TX_LO_LOCKED() override { return m_slave.get_TX_LO_LOCKED(); }
    uint8_t get_RXLO1_SYNC()   override { return m_slave.get_RXLO1_SYNC(); }
    uint8_t get_RXLO2_SYNC()   override { return m_slave.get_RXLO2_SYNC(); }
    uint8_t get_TXLO1_SYNC()   override { return m_slave.get_TXLO1_SYNC(); }
    uint8_t get_TXLO2_SYNC()   override { return m_slave.get_TXLO2_SYNC(); }
    uint8_t get_CPLD_RST_N()   override { return m_slave.get_CPLD_RST_N(); }
    uint8_t get_RX_GAIN()      override { return m_slave.get_RX_GAIN(); }
    uint8_t get_TX_GAIN()      override { return m_slave.get_TX_GAIN(); }
    uint8_t get_RX_ANT()       override { return m_slave.get_RX_ANT(); }
    uint8_t get_TX_EN_N()      override { return m_slave.get_TX_EN_N(); }
    uint8_t get_RX_EN_N()      override { return m_slave.get_RX_EN_N(); }

    void set_CPLD_RST_N (uint8_t val) override { m_slave.set_CPLD_RST_N(val); }
    void set_RX_GAIN    (uint8_t val) override { m_slave.set_RX_GAIN(val); }
    void set_TX_GAIN    (uint8_t val) override { m_slave.set_TX_GAIN(val); }
    void set_RX_ANT     (uint8_t val) override { m_slave.set_RX_ANT(val); }
    void set_TX_EN_N    (uint8_t val) override { m_slave.set_TX_EN_N(val); }
    void set_RX_EN_N    (uint8_t val) override { m_slave.set_RX_EN_N(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_UBX160_IO_SLAVE_HH__ */