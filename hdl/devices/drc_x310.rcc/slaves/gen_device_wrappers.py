#!/usr/bin/env python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

from xml.etree import ElementTree
import pathlib
from collections import namedtuple


# Devices to generate slave wrappers for
SLAVES = (
    "ad9146",
    "ads62p48",
    "lmk04816",
    "max2871",
    "ubx160_cpld",
    "ubx160_io",
    "ubx160_eeprom",
)


#-- Templates -----------------------------------------------------------------


SLAVE_TEMPLATE = """
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_{slave_name_upper}_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_{slave_name_upper}_SLAVE_HH__

#include <cstdint>

class {slave_name}Interface
{{
public:
    virtual ~{slave_name}Interface() {{}}

    {base_methods}
}};

template <typename T>
class {slave_name}Slave : public {slave_name}Interface
{{
public:
    {slave_name}Slave(T& slave) : m_slave(slave) {{}}

    {derived_methods}

private:
    T& m_slave;
}};

#endif /* OCPI_RCC_WORKER_DRC_X310_{slave_name_upper}_SLAVE_HH__ */
"""


BASE_METHOD_GET_TEMPLATE = """
    virtual {prop_type} get_{prop_name}(){name_pad} = 0;"""


DERIVED_METHOD_GET_TEMPLATE = """
    {prop_type} get_{prop_name}(){name_pad} override {{ return m_slave.get_{prop_name}(); }}"""


BASE_METHOD_SET_TEMPLATE = """
    virtual void set_{prop_name}{name_pad} ({prop_type} val) = 0;"""


DERIVED_METHOD_SET_TEMPLATE = """
    void set_{prop_name}{name_pad} ({prop_type} val) override {{ m_slave.set_{prop_name}(val); }}"""


#------------------------------------------------------------------------------


SLAVES_DIR = pathlib.Path(__file__).resolve().parent
DEVICES_DIR = SLAVES_DIR / ".." / ".."
SPECS_DIR = DEVICES_DIR / "specs"


# Map from OpenCPI XML types to the corresposing C++ type
TYPE_MAP = {
    "bool":   "bool",
    "uchar":  "uint8_t",
    "ushort": "uint16_t",
    "ulong":  "uint32_t",
}


Property = namedtuple("Property", (
    "name",       # str
    "type",       # str
    "writable",   # bool
    "parameter",  # bool
))


def to_title_case(name):
    return "".join(word.capitalize() for word in name.split("_"))


def bool_from_str(s):
    """Convert from a string in the XML to bool"""
    s = s.lower()
    if s in ("0", "false"):
        return False
    if s in ("1", "true"):
        return True
    raise RuntimeError(f"Invalid bool: {s}")


def property_from_xml_node(node):
    """Create a `Property` object from a XML node"""

    # Normalise all keys to lowercase as the OpenCPI XML is not case-sensitive
    attrib = {k.lower() : v for k, v in node.attrib.items()}

    return Property(
        attrib["name"],
        TYPE_MAP[attrib["type"].lower()],
        bool_from_str(attrib.get("writable", "0")),
        bool_from_str(attrib.get("parameter", "0")),
    )


def get_properties(path):
    """Read an OWD and return a list of all properties it defines"""

    text = path.read_text()

    # The OpenCPI XML allows <xi:include> tags but does not require an explicit
    # definition of the `xi` namespace; ElementTree on the other hand requires
    # all namespaces to be defined... lets just remove the prefix to avoid the
    # problem
    text = text.replace("xi:", "")

    root = ElementTree.fromstring(text)

    # Find all <property> nodes and parse them into `Property` objects
    properties = [
        property_from_xml_node(node)
        for node in root.findall("property")
    ]

    # Recursively parse all files included by <xi:include href="...">
    for include in root.findall("include"):
        # Look in the specs dir for the included XML
        include_path = SPECS_DIR / f"{include.attrib['href']}.xml"
        properties.extend(get_properties(include_path))

    return properties


def render_slave(slave, properties):
    """Render C++ code given a slave name and a list of properties"""
    base_set_methods = ""
    base_get_methods = ""
    derived_set_methods = ""
    derived_get_methods = ""

    # Skip padding properties and parameters (compile time constants)
    properties = [
        p for p in properties
        if not p.name.startswith("ocpi_pad") and not p.parameter
    ]

    get_name_width = max(len(p.name) for p in properties)
    set_name_width = max(len(p.name) for p in properties if p.writable)

    for p in properties:
        args = {"prop_name": p.name, "prop_type": p.type}

        args["name_pad"] = " " * (get_name_width - len(p.name))
        base_get_methods += BASE_METHOD_GET_TEMPLATE.format(**args)
        derived_get_methods += DERIVED_METHOD_GET_TEMPLATE.format(**args)

        if p.writable:
            args["name_pad"] = " " * (set_name_width - len(p.name))
            base_set_methods += BASE_METHOD_SET_TEMPLATE.format(**args)
            derived_set_methods += DERIVED_METHOD_SET_TEMPLATE.format(**args)

    base_methods = base_get_methods + "\n" + base_set_methods
    derived_methods = derived_get_methods + "\n" + derived_set_methods

    return SLAVE_TEMPLATE.format(
        slave_name_upper=slave.upper(),
        slave_name=to_title_case(slave),
        base_methods=base_methods.strip(),
        derived_methods=derived_methods.strip(),
    ).strip()


def main():
    for slave in SLAVES:
        owd_xml_path = DEVICES_DIR / f"{slave}.hdl" / f"{slave}.xml"
        properties = get_properties(owd_xml_path)

        cpp_code = render_slave(slave, properties)

        with open(SLAVES_DIR / f"{slave}_slave.hh", "wt") as f:
            f.write(cpp_code)


if __name__ == "__main__":
    main()
