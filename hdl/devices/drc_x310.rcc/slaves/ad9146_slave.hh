// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef OCPI_RCC_WORKER_DRC_X310_AD9146_SLAVE_HH__
#define OCPI_RCC_WORKER_DRC_X310_AD9146_SLAVE_HH__

#include <cstdint>

class Ad9146Interface
{
public:
    virtual ~Ad9146Interface() {}

    virtual bool get_enable()                 = 0;
    virtual uint8_t get_comm()                   = 0;
    virtual uint8_t get_power_control()          = 0;
    virtual uint8_t get_tx_enable_control()      = 0;
    virtual uint8_t get_data_format()            = 0;
    virtual uint8_t get_interrupt_enable_0()     = 0;
    virtual uint8_t get_interrupt_enable_1()     = 0;
    virtual uint8_t get_event_flag_0()           = 0;
    virtual uint8_t get_event_flag_1()           = 0;
    virtual uint8_t get_clock_receiver_control() = 0;
    virtual uint8_t get_pll_control_0()          = 0;
    virtual uint8_t get_pll_control_1()          = 0;
    virtual uint8_t get_pll_control_2()          = 0;
    virtual uint8_t get_pll_status_0()           = 0;
    virtual uint8_t get_pll_status_1()           = 0;
    virtual uint8_t get_sync_control_0()         = 0;
    virtual uint8_t get_sync_control_1()         = 0;
    virtual uint8_t get_sync_status_0()          = 0;
    virtual uint8_t get_sync_status_1()          = 0;
    virtual uint8_t get_data_receiver_status()   = 0;
    virtual uint8_t get_dci_delay()              = 0;
    virtual uint8_t get_fifo_control()           = 0;
    virtual uint8_t get_fifo_status_0()          = 0;
    virtual uint8_t get_fifo_status_1()          = 0;
    virtual uint8_t get_datapath_control()       = 0;
    virtual uint8_t get_hb1_control()            = 0;
    virtual uint8_t get_hb2_control()            = 0;
    virtual uint8_t get_datapath_config()        = 0;
    virtual uint8_t get_chip_id()                = 0;
    virtual uint8_t get_i_phase_adj_lsb()        = 0;
    virtual uint8_t get_i_phase_adj_msb()        = 0;
    virtual uint8_t get_q_phase_adj_lsb()        = 0;
    virtual uint8_t get_q_phase_adj_msb()        = 0;
    virtual uint8_t get_i_dac_offset_lsb()       = 0;
    virtual uint8_t get_i_dac_offset_msb()       = 0;
    virtual uint8_t get_q_dac_offset_lsb()       = 0;
    virtual uint8_t get_q_dac_offset_msb()       = 0;
    virtual uint8_t get_i_dac_fs_adjust()        = 0;
    virtual uint8_t get_i_dac_control()          = 0;
    virtual uint8_t get_i_aux_dac_data()         = 0;
    virtual uint8_t get_i_aux_dac_control()      = 0;
    virtual uint8_t get_q_dac_fs_adjust()        = 0;
    virtual uint8_t get_q_dac_control()          = 0;
    virtual uint8_t get_q_aux_dac_data()         = 0;
    virtual uint8_t get_q_aux_dac_control()      = 0;
    virtual uint8_t get_die_temp_range_control() = 0;
    virtual uint8_t get_die_temp_lsb()           = 0;
    virtual uint8_t get_die_temp_msb()           = 0;
    virtual uint8_t get_sed_control()            = 0;
    virtual uint8_t get_compare_i0_lsbs()        = 0;
    virtual uint8_t get_compare_i0_msbs()        = 0;
    virtual uint8_t get_compare_q0_lsbs()        = 0;
    virtual uint8_t get_compare_q0_msbs()        = 0;
    virtual uint8_t get_compare_i1_lsbs()        = 0;
    virtual uint8_t get_compare_i1_msbs()        = 0;
    virtual uint8_t get_compare_q1_lsbs()        = 0;
    virtual uint8_t get_compare_q1_msbs()        = 0;
    virtual uint8_t get_sed_i_lsbs()             = 0;
    virtual uint8_t get_sed_i_msbs()             = 0;
    virtual uint8_t get_sed_q_lsbs()             = 0;
    virtual uint8_t get_sed_q_msbs()             = 0;
    virtual uint8_t get_revision()               = 0;

    virtual void set_enable                 (bool val) = 0;
    virtual void set_comm                   (uint8_t val) = 0;
    virtual void set_power_control          (uint8_t val) = 0;
    virtual void set_tx_enable_control      (uint8_t val) = 0;
    virtual void set_data_format            (uint8_t val) = 0;
    virtual void set_interrupt_enable_0     (uint8_t val) = 0;
    virtual void set_interrupt_enable_1     (uint8_t val) = 0;
    virtual void set_event_flag_0           (uint8_t val) = 0;
    virtual void set_event_flag_1           (uint8_t val) = 0;
    virtual void set_clock_receiver_control (uint8_t val) = 0;
    virtual void set_pll_control_0          (uint8_t val) = 0;
    virtual void set_pll_control_1          (uint8_t val) = 0;
    virtual void set_pll_control_2          (uint8_t val) = 0;
    virtual void set_pll_status_0           (uint8_t val) = 0;
    virtual void set_pll_status_1           (uint8_t val) = 0;
    virtual void set_sync_control_0         (uint8_t val) = 0;
    virtual void set_sync_control_1         (uint8_t val) = 0;
    virtual void set_sync_status_0          (uint8_t val) = 0;
    virtual void set_sync_status_1          (uint8_t val) = 0;
    virtual void set_data_receiver_status   (uint8_t val) = 0;
    virtual void set_dci_delay              (uint8_t val) = 0;
    virtual void set_fifo_control           (uint8_t val) = 0;
    virtual void set_fifo_status_0          (uint8_t val) = 0;
    virtual void set_fifo_status_1          (uint8_t val) = 0;
    virtual void set_datapath_control       (uint8_t val) = 0;
    virtual void set_hb1_control            (uint8_t val) = 0;
    virtual void set_hb2_control            (uint8_t val) = 0;
    virtual void set_datapath_config        (uint8_t val) = 0;
    virtual void set_chip_id                (uint8_t val) = 0;
    virtual void set_i_phase_adj_lsb        (uint8_t val) = 0;
    virtual void set_i_phase_adj_msb        (uint8_t val) = 0;
    virtual void set_q_phase_adj_lsb        (uint8_t val) = 0;
    virtual void set_q_phase_adj_msb        (uint8_t val) = 0;
    virtual void set_i_dac_offset_lsb       (uint8_t val) = 0;
    virtual void set_i_dac_offset_msb       (uint8_t val) = 0;
    virtual void set_q_dac_offset_lsb       (uint8_t val) = 0;
    virtual void set_q_dac_offset_msb       (uint8_t val) = 0;
    virtual void set_i_dac_fs_adjust        (uint8_t val) = 0;
    virtual void set_i_dac_control          (uint8_t val) = 0;
    virtual void set_i_aux_dac_data         (uint8_t val) = 0;
    virtual void set_i_aux_dac_control      (uint8_t val) = 0;
    virtual void set_q_dac_fs_adjust        (uint8_t val) = 0;
    virtual void set_q_dac_control          (uint8_t val) = 0;
    virtual void set_q_aux_dac_data         (uint8_t val) = 0;
    virtual void set_q_aux_dac_control      (uint8_t val) = 0;
    virtual void set_die_temp_range_control (uint8_t val) = 0;
    virtual void set_die_temp_lsb           (uint8_t val) = 0;
    virtual void set_die_temp_msb           (uint8_t val) = 0;
    virtual void set_sed_control            (uint8_t val) = 0;
    virtual void set_compare_i0_lsbs        (uint8_t val) = 0;
    virtual void set_compare_i0_msbs        (uint8_t val) = 0;
    virtual void set_compare_q0_lsbs        (uint8_t val) = 0;
    virtual void set_compare_q0_msbs        (uint8_t val) = 0;
    virtual void set_compare_i1_lsbs        (uint8_t val) = 0;
    virtual void set_compare_i1_msbs        (uint8_t val) = 0;
    virtual void set_compare_q1_lsbs        (uint8_t val) = 0;
    virtual void set_compare_q1_msbs        (uint8_t val) = 0;
    virtual void set_sed_i_lsbs             (uint8_t val) = 0;
    virtual void set_sed_i_msbs             (uint8_t val) = 0;
    virtual void set_sed_q_lsbs             (uint8_t val) = 0;
    virtual void set_sed_q_msbs             (uint8_t val) = 0;
    virtual void set_revision               (uint8_t val) = 0;
};

template <typename T>
class Ad9146Slave : public Ad9146Interface
{
public:
    Ad9146Slave(T& slave) : m_slave(slave) {}

    bool get_enable()                 override { return m_slave.get_enable(); }
    uint8_t get_comm()                   override { return m_slave.get_comm(); }
    uint8_t get_power_control()          override { return m_slave.get_power_control(); }
    uint8_t get_tx_enable_control()      override { return m_slave.get_tx_enable_control(); }
    uint8_t get_data_format()            override { return m_slave.get_data_format(); }
    uint8_t get_interrupt_enable_0()     override { return m_slave.get_interrupt_enable_0(); }
    uint8_t get_interrupt_enable_1()     override { return m_slave.get_interrupt_enable_1(); }
    uint8_t get_event_flag_0()           override { return m_slave.get_event_flag_0(); }
    uint8_t get_event_flag_1()           override { return m_slave.get_event_flag_1(); }
    uint8_t get_clock_receiver_control() override { return m_slave.get_clock_receiver_control(); }
    uint8_t get_pll_control_0()          override { return m_slave.get_pll_control_0(); }
    uint8_t get_pll_control_1()          override { return m_slave.get_pll_control_1(); }
    uint8_t get_pll_control_2()          override { return m_slave.get_pll_control_2(); }
    uint8_t get_pll_status_0()           override { return m_slave.get_pll_status_0(); }
    uint8_t get_pll_status_1()           override { return m_slave.get_pll_status_1(); }
    uint8_t get_sync_control_0()         override { return m_slave.get_sync_control_0(); }
    uint8_t get_sync_control_1()         override { return m_slave.get_sync_control_1(); }
    uint8_t get_sync_status_0()          override { return m_slave.get_sync_status_0(); }
    uint8_t get_sync_status_1()          override { return m_slave.get_sync_status_1(); }
    uint8_t get_data_receiver_status()   override { return m_slave.get_data_receiver_status(); }
    uint8_t get_dci_delay()              override { return m_slave.get_dci_delay(); }
    uint8_t get_fifo_control()           override { return m_slave.get_fifo_control(); }
    uint8_t get_fifo_status_0()          override { return m_slave.get_fifo_status_0(); }
    uint8_t get_fifo_status_1()          override { return m_slave.get_fifo_status_1(); }
    uint8_t get_datapath_control()       override { return m_slave.get_datapath_control(); }
    uint8_t get_hb1_control()            override { return m_slave.get_hb1_control(); }
    uint8_t get_hb2_control()            override { return m_slave.get_hb2_control(); }
    uint8_t get_datapath_config()        override { return m_slave.get_datapath_config(); }
    uint8_t get_chip_id()                override { return m_slave.get_chip_id(); }
    uint8_t get_i_phase_adj_lsb()        override { return m_slave.get_i_phase_adj_lsb(); }
    uint8_t get_i_phase_adj_msb()        override { return m_slave.get_i_phase_adj_msb(); }
    uint8_t get_q_phase_adj_lsb()        override { return m_slave.get_q_phase_adj_lsb(); }
    uint8_t get_q_phase_adj_msb()        override { return m_slave.get_q_phase_adj_msb(); }
    uint8_t get_i_dac_offset_lsb()       override { return m_slave.get_i_dac_offset_lsb(); }
    uint8_t get_i_dac_offset_msb()       override { return m_slave.get_i_dac_offset_msb(); }
    uint8_t get_q_dac_offset_lsb()       override { return m_slave.get_q_dac_offset_lsb(); }
    uint8_t get_q_dac_offset_msb()       override { return m_slave.get_q_dac_offset_msb(); }
    uint8_t get_i_dac_fs_adjust()        override { return m_slave.get_i_dac_fs_adjust(); }
    uint8_t get_i_dac_control()          override { return m_slave.get_i_dac_control(); }
    uint8_t get_i_aux_dac_data()         override { return m_slave.get_i_aux_dac_data(); }
    uint8_t get_i_aux_dac_control()      override { return m_slave.get_i_aux_dac_control(); }
    uint8_t get_q_dac_fs_adjust()        override { return m_slave.get_q_dac_fs_adjust(); }
    uint8_t get_q_dac_control()          override { return m_slave.get_q_dac_control(); }
    uint8_t get_q_aux_dac_data()         override { return m_slave.get_q_aux_dac_data(); }
    uint8_t get_q_aux_dac_control()      override { return m_slave.get_q_aux_dac_control(); }
    uint8_t get_die_temp_range_control() override { return m_slave.get_die_temp_range_control(); }
    uint8_t get_die_temp_lsb()           override { return m_slave.get_die_temp_lsb(); }
    uint8_t get_die_temp_msb()           override { return m_slave.get_die_temp_msb(); }
    uint8_t get_sed_control()            override { return m_slave.get_sed_control(); }
    uint8_t get_compare_i0_lsbs()        override { return m_slave.get_compare_i0_lsbs(); }
    uint8_t get_compare_i0_msbs()        override { return m_slave.get_compare_i0_msbs(); }
    uint8_t get_compare_q0_lsbs()        override { return m_slave.get_compare_q0_lsbs(); }
    uint8_t get_compare_q0_msbs()        override { return m_slave.get_compare_q0_msbs(); }
    uint8_t get_compare_i1_lsbs()        override { return m_slave.get_compare_i1_lsbs(); }
    uint8_t get_compare_i1_msbs()        override { return m_slave.get_compare_i1_msbs(); }
    uint8_t get_compare_q1_lsbs()        override { return m_slave.get_compare_q1_lsbs(); }
    uint8_t get_compare_q1_msbs()        override { return m_slave.get_compare_q1_msbs(); }
    uint8_t get_sed_i_lsbs()             override { return m_slave.get_sed_i_lsbs(); }
    uint8_t get_sed_i_msbs()             override { return m_slave.get_sed_i_msbs(); }
    uint8_t get_sed_q_lsbs()             override { return m_slave.get_sed_q_lsbs(); }
    uint8_t get_sed_q_msbs()             override { return m_slave.get_sed_q_msbs(); }
    uint8_t get_revision()               override { return m_slave.get_revision(); }

    void set_enable                 (bool val) override { m_slave.set_enable(val); }
    void set_comm                   (uint8_t val) override { m_slave.set_comm(val); }
    void set_power_control          (uint8_t val) override { m_slave.set_power_control(val); }
    void set_tx_enable_control      (uint8_t val) override { m_slave.set_tx_enable_control(val); }
    void set_data_format            (uint8_t val) override { m_slave.set_data_format(val); }
    void set_interrupt_enable_0     (uint8_t val) override { m_slave.set_interrupt_enable_0(val); }
    void set_interrupt_enable_1     (uint8_t val) override { m_slave.set_interrupt_enable_1(val); }
    void set_event_flag_0           (uint8_t val) override { m_slave.set_event_flag_0(val); }
    void set_event_flag_1           (uint8_t val) override { m_slave.set_event_flag_1(val); }
    void set_clock_receiver_control (uint8_t val) override { m_slave.set_clock_receiver_control(val); }
    void set_pll_control_0          (uint8_t val) override { m_slave.set_pll_control_0(val); }
    void set_pll_control_1          (uint8_t val) override { m_slave.set_pll_control_1(val); }
    void set_pll_control_2          (uint8_t val) override { m_slave.set_pll_control_2(val); }
    void set_pll_status_0           (uint8_t val) override { m_slave.set_pll_status_0(val); }
    void set_pll_status_1           (uint8_t val) override { m_slave.set_pll_status_1(val); }
    void set_sync_control_0         (uint8_t val) override { m_slave.set_sync_control_0(val); }
    void set_sync_control_1         (uint8_t val) override { m_slave.set_sync_control_1(val); }
    void set_sync_status_0          (uint8_t val) override { m_slave.set_sync_status_0(val); }
    void set_sync_status_1          (uint8_t val) override { m_slave.set_sync_status_1(val); }
    void set_data_receiver_status   (uint8_t val) override { m_slave.set_data_receiver_status(val); }
    void set_dci_delay              (uint8_t val) override { m_slave.set_dci_delay(val); }
    void set_fifo_control           (uint8_t val) override { m_slave.set_fifo_control(val); }
    void set_fifo_status_0          (uint8_t val) override { m_slave.set_fifo_status_0(val); }
    void set_fifo_status_1          (uint8_t val) override { m_slave.set_fifo_status_1(val); }
    void set_datapath_control       (uint8_t val) override { m_slave.set_datapath_control(val); }
    void set_hb1_control            (uint8_t val) override { m_slave.set_hb1_control(val); }
    void set_hb2_control            (uint8_t val) override { m_slave.set_hb2_control(val); }
    void set_datapath_config        (uint8_t val) override { m_slave.set_datapath_config(val); }
    void set_chip_id                (uint8_t val) override { m_slave.set_chip_id(val); }
    void set_i_phase_adj_lsb        (uint8_t val) override { m_slave.set_i_phase_adj_lsb(val); }
    void set_i_phase_adj_msb        (uint8_t val) override { m_slave.set_i_phase_adj_msb(val); }
    void set_q_phase_adj_lsb        (uint8_t val) override { m_slave.set_q_phase_adj_lsb(val); }
    void set_q_phase_adj_msb        (uint8_t val) override { m_slave.set_q_phase_adj_msb(val); }
    void set_i_dac_offset_lsb       (uint8_t val) override { m_slave.set_i_dac_offset_lsb(val); }
    void set_i_dac_offset_msb       (uint8_t val) override { m_slave.set_i_dac_offset_msb(val); }
    void set_q_dac_offset_lsb       (uint8_t val) override { m_slave.set_q_dac_offset_lsb(val); }
    void set_q_dac_offset_msb       (uint8_t val) override { m_slave.set_q_dac_offset_msb(val); }
    void set_i_dac_fs_adjust        (uint8_t val) override { m_slave.set_i_dac_fs_adjust(val); }
    void set_i_dac_control          (uint8_t val) override { m_slave.set_i_dac_control(val); }
    void set_i_aux_dac_data         (uint8_t val) override { m_slave.set_i_aux_dac_data(val); }
    void set_i_aux_dac_control      (uint8_t val) override { m_slave.set_i_aux_dac_control(val); }
    void set_q_dac_fs_adjust        (uint8_t val) override { m_slave.set_q_dac_fs_adjust(val); }
    void set_q_dac_control          (uint8_t val) override { m_slave.set_q_dac_control(val); }
    void set_q_aux_dac_data         (uint8_t val) override { m_slave.set_q_aux_dac_data(val); }
    void set_q_aux_dac_control      (uint8_t val) override { m_slave.set_q_aux_dac_control(val); }
    void set_die_temp_range_control (uint8_t val) override { m_slave.set_die_temp_range_control(val); }
    void set_die_temp_lsb           (uint8_t val) override { m_slave.set_die_temp_lsb(val); }
    void set_die_temp_msb           (uint8_t val) override { m_slave.set_die_temp_msb(val); }
    void set_sed_control            (uint8_t val) override { m_slave.set_sed_control(val); }
    void set_compare_i0_lsbs        (uint8_t val) override { m_slave.set_compare_i0_lsbs(val); }
    void set_compare_i0_msbs        (uint8_t val) override { m_slave.set_compare_i0_msbs(val); }
    void set_compare_q0_lsbs        (uint8_t val) override { m_slave.set_compare_q0_lsbs(val); }
    void set_compare_q0_msbs        (uint8_t val) override { m_slave.set_compare_q0_msbs(val); }
    void set_compare_i1_lsbs        (uint8_t val) override { m_slave.set_compare_i1_lsbs(val); }
    void set_compare_i1_msbs        (uint8_t val) override { m_slave.set_compare_i1_msbs(val); }
    void set_compare_q1_lsbs        (uint8_t val) override { m_slave.set_compare_q1_lsbs(val); }
    void set_compare_q1_msbs        (uint8_t val) override { m_slave.set_compare_q1_msbs(val); }
    void set_sed_i_lsbs             (uint8_t val) override { m_slave.set_sed_i_lsbs(val); }
    void set_sed_i_msbs             (uint8_t val) override { m_slave.set_sed_i_msbs(val); }
    void set_sed_q_lsbs             (uint8_t val) override { m_slave.set_sed_q_lsbs(val); }
    void set_sed_q_msbs             (uint8_t val) override { m_slave.set_sed_q_msbs(val); }
    void set_revision               (uint8_t val) override { m_slave.set_revision(val); }

private:
    T& m_slave;
};

#endif /* OCPI_RCC_WORKER_DRC_X310_AD9146_SLAVE_HH__ */