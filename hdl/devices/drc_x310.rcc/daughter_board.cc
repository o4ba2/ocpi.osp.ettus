// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include "OcpiDebugApi.hh"
#include "daughter_board.hh"
#include "drc_x310_limits.hh"
#include "ad9146_controller.hh"
#include "ads62p48_controller.hh"
#include "ubx160_controller.hh"
#include "iqcorrection_controller.hh"


using namespace OCPI::RCC;
using namespace OCPI::OS;


DaughterBoard::DaughterBoard(
  std::shared_ptr<Ad9146Controller> ad9146,
  std::shared_ptr<Ads62p48Controller> ads62p48,
  std::shared_ptr<IQCorrectionController> rx_iqcorrection,
  std::shared_ptr<IQCorrectionController> tx_iqcorrection,
  std::shared_ptr<Ubx160Controller> ubx160
) :
  m_ad9146(ad9146),
  m_ads62p48(ads62p48),
  m_rx_iqcorrection(rx_iqcorrection),
  m_tx_iqcorrection(tx_iqcorrection),
  m_ubx160(ubx160)
{
}


RCCResult DaughterBoard::start()
{
  RCCResult result;

  // start the ADC
  result = m_ads62p48->start();
  if (result != RCC_OK)
  {
    return result;
  }

  // start the DAC
  result = m_ad9146->start();
  if (result != RCC_OK)
  {
    return result;
  }

  // start the daughter board
  result = m_ubx160->start();
  if (result != RCC_OK)
  {
    return result;
  }

  // start the transmit iq corrector
  result = m_tx_iqcorrection->start();
  if (result != RCC_OK)
  {
    return result;
  }

  // start the receive iq corrector
  result = m_rx_iqcorrection->start();
  if (result != RCC_OK)
  {
    return result;
  }

  m_is_shutdown = false;
  return result;
}


RCCResult DaughterBoard::set_tx_fe_enable(bool enable)
{
  return m_ubx160->set_tx_enable(enable);
}


RCCResult DaughterBoard::set_rx_fe_enable(bool enable)
{
  return m_ubx160->set_rx_enable(enable);
}


RCCResult DaughterBoard::set_tx_dac_enable(bool enable)
{
  return m_ad9146->set_tx_dac_enable(enable);
}


RCCResult DaughterBoard::set_rx_adc_enable(bool enable)
{
  return m_ads62p48->set_rx_adc_enable(enable);
}


RCCResult DaughterBoard::cache_dboard_clock_rate_hz(double rate_hz)
{
  return m_ubx160->cache_dboard_clock_rate_hz(rate_hz);
}


RCCResult DaughterBoard::cache_tx_gain(double gain_db)
{
  if ((gain_db < DRC_X310_MIN_TX_GAIN_DB) || (gain_db > DRC_X310_MAX_TX_GAIN_DB))
  {
    Log::print(OCPI_LOG_BAD, "TX gain out of range.  Min = %f; max = %f; attempted = %f",
                            DRC_X310_MIN_TX_GAIN_DB,
                            DRC_X310_MAX_TX_GAIN_DB,
                            gain_db);
    return RCC_ERROR;
  }

  return m_ubx160->cache_tx_gain(gain_db);
}


RCCResult DaughterBoard::cache_rx_gain(double gain_db)
{
  if ((gain_db < DRC_X310_MIN_RX_ANALOGUE_GAIN_DB + DRC_X310_MIN_RX_ADC_GAIN_DB) ||
      (gain_db > DRC_X310_MAX_RX_ANALOGUE_GAIN_DB + DRC_X310_MAX_RX_ADC_GAIN_DB))
  {
    Log::print(OCPI_LOG_BAD, "RX gain out of range.  Min = %f; max = %f; attempted = %f",
                            DRC_X310_MIN_RX_ANALOGUE_GAIN_DB + DRC_X310_MIN_RX_ADC_GAIN_DB,
                            DRC_X310_MAX_RX_ANALOGUE_GAIN_DB + DRC_X310_MAX_RX_ADC_GAIN_DB,
                            gain_db);
    return RCC_ERROR;
  }

  double analogue_gain = std::min(DRC_X310_MAX_RX_ANALOGUE_GAIN_DB, gain_db);
  double digital_gain  = std::max(DRC_X310_MIN_RX_ADC_GAIN_DB, gain_db - DRC_X310_MAX_RX_ANALOGUE_GAIN_DB);

  RCCResult res = m_ubx160->cache_rx_gain(analogue_gain);
  if (res != RCC_OK){
    return res;
  }

  return m_ads62p48->cache_gain(digital_gain);
}


RCCResult DaughterBoard::cache_tx_freq_set_iq_correction(double freq_hz)
{
  if ((freq_hz < (DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6)) ||
      (freq_hz > (DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6)))
  {
    Log::print(OCPI_LOG_BAD, "TX freq out of range.  Min = %f; max = %f; attempted = %f",
                            DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6,
                            DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6,
                            freq_hz);
    return RCC_ERROR;
  }

  // set transmit DC and IQ correction
  RCCResult result = m_tx_iqcorrection->set_tx_correction(freq_hz);
  if (result != RCC_OK)
  {
    return result;
  }

  // set the transmit carrier frequency
  return m_ubx160->cache_tx_freq(freq_hz);
}


double DaughterBoard::get_tx_freq()
{
  return m_ubx160->get_tx_freq();
}


RCCResult DaughterBoard::cache_rx_freq_set_iq_correction(double freq_hz)
{
  if ((freq_hz < (DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6)) ||
      (freq_hz > (DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6)))
  {
    Log::print(OCPI_LOG_BAD, "RX freq out of range.  Min = %f; max = %f; attempted = %f",
                            DRC_X310_MIN_TUNING_FREQ_MHZ * 1e6,
                            DRC_X310_MAX_TUNING_FREQ_MHZ * 1e6,
                            freq_hz);
    return RCC_ERROR;
  }

  // set receive DC and IQ correction
  RCCResult result = m_rx_iqcorrection->set_rx_correction(freq_hz);
  if (result != RCC_OK)
  {
    return result;
  }

  // set the receive carrier frequency
  return m_ubx160->cache_rx_freq(freq_hz);
}


double DaughterBoard::get_rx_freq()
{
  return m_ubx160->get_rx_freq();
}


RCCResult DaughterBoard::cache_rx_ant(Port port)
{
  return m_ubx160->cache_rx_ant(port);
}


RCCResult DaughterBoard::cache_tx_ant(Port port)
{
  return m_ubx160->cache_tx_ant(port);
}


RCCResult DaughterBoard::set_rx_correction(bool enable, double I_dc, double Q_dc, double A, double B)
{
  RCCResult result = m_tx_iqcorrection->enable_correction(enable);
  if (result != RCC_OK)
  {
    return result;
  }

  return m_rx_iqcorrection->set_correction(I_dc, Q_dc, A, B);
}


RCCResult DaughterBoard::set_tx_correction(bool enable, double I_dc, double Q_dc, double A, double B)
{
  RCCResult result = m_tx_iqcorrection->enable_correction(enable);
  if (result != RCC_OK)
  {
    return result;
  }

  return m_tx_iqcorrection->set_correction(I_dc, Q_dc, A, B);
}


RCCResult DaughterBoard::wait_lo_locked()
{
  return m_ubx160->wait_lo_locked();
}


RCCResult DaughterBoard::set_correction_calib_path_base(std::string calib_path, std::string calib_base)
{
  RCCResult res;

  res = m_rx_iqcorrection->set_calib_path_base(calib_path, calib_base);
  if (res != RCC_OK) {
    return res;
  }

  res = m_tx_iqcorrection->set_calib_path_base(calib_path, calib_base);
  if (res != RCC_OK) {
    return res;
  }

  return RCC_OK;
}


RCCResult DaughterBoard::shutdown()
{
  // Checks if the db is already shutdown
  if (m_is_shutdown) {
    return RCC_OK;
  }

  RCCResult result;
  result  = this->set_rx_adc_enable(false);
  if (result != RCC_OK) {
    return result ;
  }

  result  = this->set_rx_fe_enable(false);
  if (result != RCC_OK) {
    return result ;
  }

  result  = this->set_tx_fe_enable(false);
  if (result != RCC_OK) {
    return result ;
  }

  result  = this->set_tx_dac_enable(false);
  if (result != RCC_OK) {
    return result ;
  }

  m_is_shutdown = true;
  return m_ubx160->shutdown();
}
