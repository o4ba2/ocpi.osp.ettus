.. perf documentation


.. _perf-application:


DG-RDMA Performance Test (``perf``)
===================================
Ettus USRP X310 (single UBX-160 daughter board) DG-RDMA Performance Test application.

Description
-----------
This application is used to measure the performance of the transfer of packets between
the FPGA (HDL container) and the PC (RCC container) over an ethernet interface using 
the OpenCPI DG-RDMA protocol.
Two workers are used to perform the test

* ocpi.osp.ettus.perftest (HDL)
* ocpi.osp.ettus.perfmeas (RCC)

Hardware Portability
--------------------
This application can be run using the X310 platform. The (FPGA) bitstream used can be a single or dual ethernet interfaces.
It has been tested using the smoketest_assy assembly. This in turn uses either to cfg_base_single_eth.xml or cfg_base_dual_eth.xml configuration.

Execution
---------

Prerequisites
~~~~~~~~~~~~~
Prior to running the application the following prerequisites apply

* x310 platform needs to have been installed (see x310 getting-started-guide)
* The setup-runtime.sh needs to be correctly modified and run

The script located at ``opencpi/projects/osps/ocpi.osp.ettus/applications/perf/run_perf.sh`` contains commented out lines for full-duplex and both
half-duplex directions.  The desired lines should be uncommented.

Command(s)
~~~~~~~~~~
The application can be run using the following command line

.. code-block:: bash

    $ cd opencpi/projects/osps/ocpi.osp.ettus/applications/perf
    $ ./run_perf.sh

Verification
------------
Running the application as shown above causes the worker properties to be written (dumped) 
before the application is started and after it has run. These properties show the number of
packets and bytes transmitted and received by both the HDL and RCC workers, as well as the
transfer rates achieved.

Worker Artifacts
----------------
None
