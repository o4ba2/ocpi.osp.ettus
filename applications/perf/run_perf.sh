# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

#/bin/bash

set -x

# Full-duplex
# export PERFTEST_PROPS="-p perftest=tx_beats=2047 -p perftest=tx_messages=99999 -p perftest=tx_run=1"
# export PERFMEAS_PROPS="-p perfmeas=rx_exp_messages=100000 -p perfmeas=tx_message_words=2048 -p perfmeas=tx_messages=10000"

# Half-duplex (up)
export PERFMEAS_PROPS="-p perfmeas=tx_message_words=2048 -p perfmeas=tx_messages=10000"

# Half-duplex (down)
# export PERFTEST_PROPS="-p perftest=tx_beats=2047 -p perftest=tx_messages=9999 -p perftest=tx_run=1"
# export PERFMEAS_PROPS="-p perfmeas=rx_exp_messages=10000"

ocpirun "$@" -d -l 8 -v $PERFTEST_PROPS $PERFMEAS_PROPS -t 20 perf.xml

