#!/usr/bin/env python3

# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import struct

import numpy as np


DEFAULT_LENGTH = 65535
DEFAULT_DECIMATION_FACTOR = 100
DEFAULT_FREQUENCY = 0.1  # MHz
DEFAULT_AMPLITUDE = 1000


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("length", nargs="?", default=DEFAULT_LENGTH, type=int)
    parser.add_argument("decimation_factor", nargs="?", default=DEFAULT_DECIMATION_FACTOR, type=int)
    parser.add_argument("frequency", nargs="?", default=DEFAULT_FREQUENCY, type=float, help="Baseband frequency in MHz")
    parser.add_argument("amplitude", nargs="?", default=DEFAULT_AMPLITUDE, type=int, help="Amplitude in ADC counts")
    args = parser.parse_args()

    length = args.length
    frequency = args.frequency
    decimation_factor = args.decimation_factor
    amplitude = args.amplitude

    samples_per_cycle = round(200 / (frequency * decimation_factor))
    phase = np.linspace(0, 2 * np.pi, samples_per_cycle, endpoint=False)
    cosine = np.int16(np.cos(phase) * amplitude)
    sine = np.int16(np.sin(phase) * amplitude)

    with open('Test_file_in.dat', 'wb') as f:
        for _ in range(length):
            for cos_value, sin_value in zip(cosine, sine):
                f.write(struct.pack('<hh', cos_value, sin_value))


if __name__ == "__main__":
    main()
