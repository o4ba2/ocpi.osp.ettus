.. loopback documentation


.. _loopback-application:


Loopback (``loopback``)
=======================
Ettus USRP X310 (single UBX-160 daughter board) Loopback test application.

Description
-----------
This application tests both the transmit and receive paths of an Ettus USRP X310 using a single UBX-160 daughter board.
The X310 hardware is connected to the PC using (dual) ethernet interfaces. Data and control packets are sent between
the PC and the X310 using OpenCPI DGRDMA functionality.

An RCC file reader component reads baseband samples from a file (Test_file_in.dat) these are sent to an HDL
interpolater component. This up samples the data to 200 Msps. The output of the interpolator is connected
to the X310 Digital-Radio-Controller (DRC). The DRC modulates the baseband signal onto a carrier frequency of 2400MHz.
The modulated signal is transmitted out of the SMA antenna port TX/RX. This port can be connected to the 
RX antenna port via an attenuator (30dB).
The received signal is demodulated back to baseband by the DRC. The receive port of the DRC is connected to 
an HDL CIC decimator. This worker down-samples the baseband signal (by a factor of 100) back down to 2Msps. The output
of the decimator is connected to an RCC file writer. The received baseband samples are written to file Test_file_out.dat.
In this way the baseband samples from the input file follow the entire transmit and receive paths before being 
written to the output file. 

The format of the samples is defined by the protocol `complex_short_timed_sample-prot`.
(alternating real and imaginary 16-bit signed samples)

.. code-block:: xml

  <operation name="sample">
    <argument name="data" type="struct" sequencelength="4096">
      <member name="real" type="short"/>
      <member name="imaginary" type="short"/>
    </argument>
  </operation>

Hardware Portability
--------------------

This application can be run using the X310 platform. The (FPGA) bitstream used can be a single or dual
UBX-160 daughter board configuration, and can use either dual or single ethernet interfaces.
It has been tested using the loopback_assy assembly. This in turn uses the cfg_dual_ubx_160.xml configuration
(i.e. dual daughter board / dual ethernet).

Execution
---------

Prerequisites
~~~~~~~~~~~~~
Prior to running the application the following prerequisites apply

* x310 platform needs to have been installed (see x310 getting-started-guide)
* The setup-runtime.sh needs to be correctly modified and run

Command(s)
~~~~~~~~~~
The application can be run using the following command line

.. code-block:: bash

    $ cd opencpi/projects/osps/ocpi.osp.ettus/applications
    $ ocpirun -d -l 8 ./loopback/looback.xml


Verification
------------
The contents of the output file Test_file_out.dat can be checked (using a suitable waveform viewer software or python script)

Worker Properties
-----------------

The DRC transmit and receive paths are configured by the application (loopback.xml) using the DRC configuration property

.. code-block:: xml

    <Property name="configurations" value="
    {
        description full duplex,
        recoverable false,
        channels {
            {
                description TX_0,
                rx false,
                tuning_freq_Mhz 2400.0,
                bandwidth_3db_Mhz 1.0,
                sampling_rate_Msps 200.0,
                samples_are_complex true,
                gain_mode auto,
                gain_dB 20.0,
                tolerance_tuning_freq_mhz 0.01,
                tolerance_bandwidth_3dB_Mhz 0.01,
                tolerance_sampling_rate_msps 0.01,
                tolerance_gain_db 1,
                rf_port_name TX/RX_A,
                app_port_num 0
            },
            {
                description RX_0,
                rx true,
                tuning_freq_Mhz 2400.0,
                bandwidth_3db_Mhz 1.0,
                sampling_rate_Msps 200.0,
                samples_are_complex true,
                gain_mode auto,
                gain_dB 20.0,
                tolerance_tuning_freq_mhz 0.01,
                tolerance_bandwidth_3dB_Mhz 0.01,
                tolerance_sampling_rate_msps 0.01,
                tolerance_gain_db 1,
                rf_port_name RX2_A,
                app_port_num 0
            }
        }
    }"

The configuration defines the carrier frequency (`tuning_freq_Mhz`) this should be in the rage 50.0 to 6000.0 MHz
The sample rate (`sampling_rate_Msps`) should be set to either 200.0Mhz or 184.32MHz.
The Transmit and receive gains (`gain_dB`) can be set in the range 0.0 to 31.5 dB.
`rf_port_name` TX/RX_A specifies the transmit signal is output on the X310 "TX/RX" SMA port.
`rf_port_name` RX2_A specifies the receive signal is taken from the X310 RX2 SMA port.

Worker Artifacts
----------------
The output file (Test_file_out.dat), containing the received baseband samples, is produced by running this application.
