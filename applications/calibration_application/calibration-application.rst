.. calibration documentation


.. _calibration-application:


Calibration (``calibration_application``)
=========================================
Ettus USRP X310 (single or dual UBX-160 daughter board) Calibration application.


Description
-----------
The calibration application can run a number of calibrations which can be used to significantly improve the RF performance of the X310.

A signal_generator HDL worker produces a constant transmission. This is received through the DRC's receive port where it is mixed down to DC from RF frequencies. This is then split into two separate channels and mixed with a constant tone, so that one channel selects the transmission frequency offset, where the main signal will be present, and the other selects the negative of the transmission frequency offset, where the image will be present. Two cic_decimators then downsample each channel 1000 times, and feed the signal into a monitor HDL worker. This accumulates samples and stores them in properties which can be read by the actual calibration application.

The application supports three types of calibration: RX_IQ, TX_IQ and TX_DC. IQ corrections account for the phase and gain differences on the transmit and receive tracks, and the DC correction offsets any local oscillator leakage at the centre frequency. For more information about how these corrections work see https://files.ettus.com/manual/page_calibration.html or research direct-conversion receivers.

By changing the correction values of the iq_corrector HDL workers in the DRC, the values for each type of correction can be adjusted until they produce the best possible result for a given frequency.


Hardware Portability
--------------------
This application can be run using the X310 platform. The (FPGA) bitstream used can be a single or dual
UBX-160 daughter board configuration, and can use either dual or single ethernet interfaces.
It has been tested using the calibration_assy assembly. This in turn uses the cfg_dual_ubx_160.xml configuration
(i.e. dual daughter board / dual ethernet).

Execution
---------

Software Prerequisites
~~~~~~~~~~~~~~~~~~~~~~
Prior to running the application the following prerequisites apply

* x310 platform needs to have been installed (see x310 getting-started-guide)
* The setup-runtime.sh needs to be correctly modified and run

If you want to use a single daughterboard container then you will need to modify the application xml by removing all the components listed under DB1, and the last two channels in the DRC configurations.


Physical Prerequisites
~~~~~~~~~~~~~~~~~~~~~~
If your X310 contains revision 5 (also referred to as rev. E) UBX160 daughterboards, then no setup is required. However older daughterboards (e.g. revision 3 / rev C) may require an external coaxial cable between the TX/RX and RX2 ports on each daughterboard. This is because early revisions of the UBX160 daughterboard have no internal calibration path. However, the TX to RX isolation on them is also quite low, particularly around 3 GHz. Due to this you may get reasonable calibration results even without an external connection between the ports, however it will likely contain some anomalies. Consequently you should always use an external connection with 40dB of attenuation to calibrate early revision boards.

IQ correction is somewhat temperature dependent, since the electrical characteristics of the tracks will themselves vary with temperature. Consequently, for best results you should try to calibrate in an environment similar to your use case, allowing time for the X310 to equilibrate.


Command(s)
~~~~~~~~~~
The application can be run using the following command line:

.. code-block:: bash

    $ cd opencpi/projects/osps/ocpi.osp.ettus/applications/calibration_application
    $ ocpidev build
    $ sudo setcap CAP_NET_RAW+eip $(readlink -f ./target-centos7/calibration_application)
    $ ./target-centos7/calibration_application --help

This will display usage information for the application.

To use the surface and gain plot features you will need to create the folders you specify, or else the files will not be produced.


Verification
------------
.. list-table:: Calibration Outputs
   :header-rows: 1

   * - Output
     - Verification
   * - IQ or DC correction files
     - By comparing against files generated by the Ettus USRP calibration utilities.
   * - IQ or DC surface plots
     - These show the suppression response as the correction values are swept from -1 to 1.
       These plots should all have a single, narrow peak close to the centre (typically < 0.1).
       Multiple peaks or other artifacts can indicate too much gain, which is clipping the receive signal.
   * - Gain ramps
     - These should increase linearly over some of the range, and then round off to a flat line after the compression point.
       They may be noisy at low gains when the noise floor is high.
       At higher frequencies or with high attenuation the whole line may be linear with no round off.
       Erratic behaviour after the compression point indicates integer overflow from the ADC.
       If all or nearly all of the range is flat, it indicates that the signal will be clipped at every receive gain, so the transmit gain should be reduced, or more attenuation added.


Worker Artifacts
-----------------
The application will produce up to six main files, each of the form ``<cal-type>_<daughterboard_serial_number>.csv``, for example ``tx_iq_3204E1F.csv``. These files each contain some metadata about the setup, and then a list of calibration results. These consist of the frequency, the required correction values, the achieved suppression and the improvement in suppression relative to having no calibration at all.

Running the application multiple times will overwrite these files so be sure to save any you wish to keep to another location.

Surface and gain plots will be produced in the folders you created and specified, if you are using this feature.
