// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// Standard Includes
#include <chrono>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

// Local Includes
#include "OcpiApi.hh"
namespace OA = OCPI::API;

// Constants
#define DRC_CONFIG 0
#define MIN_FREQ 50e6
#define MAX_FREQ 6e9
#define FREQ_STEP 100e6
#define MIN_RX_GAIN 0.0
#define MAX_RX_GAIN 37.5
#define MIN_TX_GAIN 0.0
#define MAX_TX_GAIN 31.5
#define DAUGHTER_BOARD_0 0
#define DAUGHTER_BOARD_1 2
#define MAX_NOISE_VARIANCE 10.0
#define GAIN_STEP 3
#define COMPRESSION_THRESHOLD 0.5
#define SEARCH_STEPS 5

// Enums
enum class CalType { RX_IQ, TX_IQ, TX_DC };
enum class Channel { Signal, Image };
enum class Direction { TX, RX };
enum class Daughterboard { DB0, DB1 };

// Structs
struct result_t {
  double freq, real_corr, imag_corr, best, delta;
};

struct CommandLineOptions {
  bool print_help = false;
  double tx_wave_ampl = 0.35;
  double tx_wave_freq = 507.123e3;
  double tx_offset = 0.9344e6;
  double freq_start = 1e8;
  double freq_stop = 6e9;
  double freq_step = 1e8;
  double precision = 0.0001;
  std::string surface_folder = "";
  std::string dump_folder = "";
  std::vector<int> daughterboards;
  std::vector<CalType> cal_types;
};

// Helper Functions
CalType str_to_cal(const char *type) {
  if (std::strcmp(type, "rx_iq") == 0) {
    return CalType::RX_IQ;
  } else if (std::strcmp(type, "tx_iq") == 0) {
    return CalType::TX_IQ;
  } else if (std::strcmp(type, "tx_dc") == 0) {
    return CalType::TX_DC;
  }
  throw "Unrecognised calibration type";
}

std::string cal_to_str(const CalType type) {
  switch (type) {
  case CalType::RX_IQ:
    return "rx_iq";
  case CalType::TX_IQ:
    return "tx_iq";
  case CalType::TX_DC:
    return "tx_dc";
  default:
    throw "Invalid calibration type";
  }
}

int get_channel_index(const Daughterboard db, const Direction dir) {
  switch (db) {
  case Daughterboard::DB0:
    switch (dir) {
    case Direction::TX:
      return 0;
    case Direction::RX:
      return 1;
    default:
      throw "Invalid direction";
    }
    break;
  case Daughterboard::DB1:
    switch (dir) {
    case Direction::TX:
      return 2;
    case Direction::RX:
      return 3;
    default:
      throw "Invalid direction";
    }
    break;
  default:
    throw "Invalid Daughterboard";
  }
}

int get_iq_index(const Daughterboard db, const Direction dir) {
  switch (db) {
  case Daughterboard::DB0:
    switch (dir) {
    case Direction::TX:
      return 1;
    case Direction::RX:
      return 0;
    default:
      throw "Invalid direction";
    }
    break;
  case Daughterboard::DB1:
    switch (dir) {
    case Direction::TX:
      return 3;
    case Direction::RX:
      return 2;
    default:
      throw "Invalid direction";
    }
    break;
  default:
    throw "Invalid Daughterboard";
  }
}

// Classes
class System {
public:
  static System build(const OA::Application &app, const Daughterboard db) {
    switch (db) {
    case Daughterboard::DB0:
      return System(app, "A");
    case Daughterboard::DB1:
      return System(app, "B");
    default:
      throw "Invalid daughterboard";
    }
  }

  void enable(const bool enable) {
    m_drc_iq_correction.setValue(
        enable, {get_iq_index(m_daughterboard, Direction::RX), "Enable"});
    m_drc_iq_correction.setValue(
        enable, {get_iq_index(m_daughterboard, Direction::TX), "Enable"});
  }

  // (Queuing) Tunes the RX frequency as close to freq MHz as
  // possible, and the TX frequency as close to freq + TX_OFFSET MHz
  // as possible.
  void queue_tune(const double freq, const double offset) {
    const double rx_target_freq = freq;
    const double tx_target_freq = rx_target_freq + offset;
    if (rx_target_freq < MIN_FREQ) {
      throw "Trying to tune below minimum frequency!";
    } else if (tx_target_freq > MAX_FREQ) {
      throw "Trying to tune above maximum frequency!";
    }
    m_drc_configs.setValue(rx_target_freq / 1e6,
                           {DRC_CONFIG, "channels",
                            get_channel_index(m_daughterboard, Direction::RX),
                            "tuning_freq_Mhz"});
    m_drc_configs.setValue(tx_target_freq / 1e6,
                           {DRC_CONFIG, "channels",
                            get_channel_index(m_daughterboard, Direction::TX),
                            "tuning_freq_Mhz"});
  }

  // (Queuing) Sets the gain on channel direction
  void queue_set_gain(const Direction dir, const double gain) {
    switch (dir) {
    case Direction::RX:
      if (gain < MIN_RX_GAIN) {
        throw "Trying to set RX gain to less than minimum gain";
      } else if (gain > MAX_RX_GAIN) {
        throw "Trying to set RX gain to more than maximum gain";
      }
      break;
    case Direction::TX:
      if (gain < MIN_TX_GAIN) {
        throw "Trying to set TX gain to less than minimum gain";
      } else if (gain > MAX_TX_GAIN) {
        throw "Trying to set TX gain to more than maximum gain";
      }
    }
    m_drc_configs.setValue(gain, {DRC_CONFIG, "channels",
                                  get_channel_index(m_daughterboard, dir),
                                  "gain_dB"});
  }

  // (Sync) Enable/Disable transmission
  void sync_transmit_enable(const bool enable) {
    m_tx_enable.setBoolValue(enable);
    sleep(20000);
  }

  // (Sync) sets the iq balance for RX or TX on the given daughter board
  void sync_set_iq_balance(const Direction dir, const double real_corr,
                           const double imag_corr) {
    m_drc_iq_correction.setValue(real_corr,
                                 {get_iq_index(m_daughterboard, dir), "A"});
    m_drc_iq_correction.setValue(imag_corr,
                                 {get_iq_index(m_daughterboard, dir), "B"});
    sleep(40);
  }

  // (Sync) sets the dc offset for RX or TX on the given daughter board
  void sync_set_tx_dc_offset(const double real_corr, const double imag_corr) {
    m_drc_iq_correction.setValue(
        real_corr, {get_iq_index(m_daughterboard, Direction::TX), "I_dc"});
    m_drc_iq_correction.setValue(
        imag_corr, {get_iq_index(m_daughterboard, Direction::TX), "Q_dc"});
    sleep(20);
  }

  // (Sync) Sets the instep for the given channel based on freq
  void sync_set_mixer_freq(const Channel channel, const double freq) {
    const double sampling_rate = get_sampling_rate(Direction::RX);
    const long instep_constant =
        (long)(0.5 + -pow(2, 32) * (freq / sampling_rate));

    switch (channel) {
    case Channel::Signal:
      m_ch0_mixer_instep.setLongValue(instep_constant);
      break;
    case Channel::Image:
      m_ch1_mixer_instep.setLongValue(instep_constant);
      break;
    default:
      throw "Unrecognised channel number";
    }

    sleep(20);
  }

  // (Sync) Sets the signal generator step size and carrier
  // amplitude based on the target frequency and amplitude
  void sync_set_signal(const double freq, const double ampl) {
    const double sampling_rate = get_sampling_rate(Direction::TX);
    const long step_size = (long)(0.5 + freq * 4294967296 / sampling_rate);
    const ushort carrier_amplitude = std::floor((ampl * 19872));

    m_tx_step_size.setLongValue(step_size);
    m_tx_amplitude.setUShortValue(carrier_amplitude);

    sleep(100000);
  }

  // Updates all the queued system configurations
  void commit() {
    m_drc_stop.setValue(DRC_CONFIG);
    m_drc_start.setValue(DRC_CONFIG);
    sleep(40000);
  }

  // Gets the actual frequency offset (TX - RX)
  double get_freq_offset() {
    double rx = m_drc_status.getValue<double>(
        {DRC_CONFIG, "channels",
         get_channel_index(m_daughterboard, Direction::RX), "tuning_freq_Mhz"});
    double tx = m_drc_status.getValue<double>(
        {DRC_CONFIG, "channels",
         get_channel_index(m_daughterboard, Direction::TX), "tuning_freq_Mhz"});

    return 1e6 * (tx - rx);
  }

  // Get the signal amplitude on the given channel
  // NB: this is clipped to always be > 0
  double get_signal_dbrms(const Channel channel) {
    long i_res, q_res;
    // Throw first sample to get clean data

    switch (channel) {
    case Channel::Signal:
      while (!m_ch0_valid.getBoolValue()) {
        sleep(5);
      };

      m_ch0_i_val.getLongValue();
      m_ch0_q_val.getLongValue();

      while (!m_ch0_valid.getBoolValue()) {
        sleep(5);
      };

      i_res = m_ch0_i_val.getLongValue();
      q_res = m_ch0_q_val.getLongValue();
      break;
    case Channel::Image:
      while (!m_ch1_valid.getBoolValue()) {
        sleep(5);
      };

      m_ch1_i_val.getLongValue();
      m_ch1_q_val.getLongValue();

      while (!m_ch1_valid.getBoolValue()) {
        sleep(5);
      };

      i_res = m_ch1_i_val.getLongValue();
      q_res = m_ch1_q_val.getLongValue();
      break;
    default:
      throw "Unrecognised Channel";
    }

    const double signal = 10 * log10(i_res * i_res + q_res * q_res);

    // If there is almost no signal this will be close to -inf,
    // so clip this to a sensible number
    return signal < 0 ? 0 : signal;
  }

  // Get the system's daughterboard as a string
  std::string get_daughterboard() {
    switch (m_daughterboard) {
    case Daughterboard::DB0:
      return "DB0";
    case Daughterboard::DB1:
      return "DB1";
    default:
      throw "Unrecognised daughterboard type";
    }
  }

  std::string get_serial() {
    switch (m_daughterboard) {
      case Daughterboard::DB0:
        return m_drc_serial.getValue<std::string>({0, "sn"});
      case Daughterboard::DB1:
        return m_drc_serial.getValue<std::string>({1, "sn"});
      default:
        throw "Unrecognised daughterboard number";
    }
  }

private:
  System(const OA::Application &app, const std::string db)
      : m_drc_start(app, "drc", "start"), m_drc_stop(app, "drc", "stop"),
        m_drc_status(app, "drc", "status"),
        m_drc_configs(app, "drc.configurations"),
        m_drc_serial(app, "drc", "daughter_board"),
        m_drc_iq_correction(app, "drc.calib_manual"),
        m_tx_enable(app, "signal_generator_" + db, "enable"),
        m_tx_step_size(app, "signal_generator_" + db, "step_size"),
        m_tx_amplitude(app, "signal_generator_" + db, "carrier_amplitude"),
        m_ch0_mixer_instep(app, "ch0_constant_" + db, "value"),
        m_ch0_i_val(app, "ch0_monitor_" + db, "i_value"),
        m_ch0_q_val(app, "ch0_monitor_" + db, "q_value"),
        m_ch0_valid(app, "ch0_monitor_" + db, "data_valid"),
        m_ch1_mixer_instep(app, "ch1_constant_" + db, "value"),
        m_ch1_i_val(app, "ch1_monitor_" + db, "i_value"),
        m_ch1_q_val(app, "ch1_monitor_" + db, "q_value"),
        m_ch1_valid(app, "ch1_monitor_" + db, "data_valid") {

    if (db == "A") {
      m_daughterboard = Daughterboard::DB0;
    } else if (db == "B") {
      m_daughterboard = Daughterboard::DB1;
    } else {
      throw "Unrecognised daughterboard type";
    }
  }

  // Gets the sampling rate from the DRC
  double get_sampling_rate(const Direction dir) {
    return m_drc_configs.getValue<double>(
               {DRC_CONFIG, "channels", get_channel_index(m_daughterboard, dir),
                "sampling_rate_Msps"}) *
           1e6;
  }

  // Sleep for micros microseconds
  void sleep(const int micros) {
    std::this_thread::sleep_for(std::chrono::microseconds(micros));
  }

  Daughterboard m_daughterboard;

  OA::Property m_drc_start, m_drc_stop, m_drc_status, m_drc_configs, m_drc_serial,
      m_drc_iq_correction, m_tx_enable, m_tx_step_size, m_tx_amplitude,
      m_ch0_mixer_instep, m_ch0_i_val, m_ch0_q_val, m_ch0_valid,
      m_ch1_mixer_instep, m_ch1_i_val, m_ch1_q_val, m_ch1_valid;
};

// Functions

// Calculates the gain required to exceed the noise floor
// by MAX_NOISE_VARIANCE
double gain_to_exceed_floor(System &sys) {
  double floor, res, rx_gain = MIN_RX_GAIN;
  sys.queue_set_gain(Direction::RX, rx_gain);
  sys.commit();
  sys.sync_transmit_enable(false);

  floor = std::max(sys.get_signal_dbrms(Channel::Signal), 30.0);
  std::cerr << "Measured Noise Floor @ " << floor << "dB\n";

  sys.sync_transmit_enable(true);

  while (rx_gain < MAX_RX_GAIN - GAIN_STEP) {
    res = sys.get_signal_dbrms(Channel::Signal);
    if (res > floor + MAX_NOISE_VARIANCE) {
      break;
    }
    rx_gain += GAIN_STEP;
    sys.queue_set_gain(Direction::RX, rx_gain);
    sys.commit();
  }
  std::cerr << "Need " << rx_gain << "dB to exceed floor by "
            << MAX_NOISE_VARIANCE << std::endl;
  return rx_gain;
}

double get_optimal_rx_gain(System &sys) {
  double curr_dbrms, prev_dbrms, delta, rx_gain = gain_to_exceed_floor(sys);
  sys.queue_set_gain(Direction::RX, rx_gain);
  sys.commit();

  prev_dbrms = sys.get_signal_dbrms(Channel::Signal);

  for (rx_gain += GAIN_STEP; rx_gain <= MAX_RX_GAIN; rx_gain += GAIN_STEP) {
    sys.queue_set_gain(Direction::RX, rx_gain);
    sys.commit();

    curr_dbrms = sys.get_signal_dbrms(Channel::Signal);
    delta = curr_dbrms - prev_dbrms;

    if (delta < GAIN_STEP * COMPRESSION_THRESHOLD) {
      break;
    }
    prev_dbrms = curr_dbrms;
  }
  rx_gain = (rx_gain - 4 * GAIN_STEP);
  rx_gain = rx_gain < MIN_RX_GAIN ? MIN_RX_GAIN : rx_gain;
  std::cerr << "Decided on optimal gain of " << rx_gain << "dB\n";
  return rx_gain;
}

void dump_surface(System &sys, const CalType cal, const double freq,
                  const CommandLineOptions &opts) {
  double suppression;
  if (opts.surface_folder == "") {
    return;
  }
  std::ofstream f;
  std::string ofile = opts.surface_folder + "/" + cal_to_str(cal) + "-" +
                      std::to_string(freq) + "-" + sys.get_daughterboard() +
                      "-csv";
  f.open(ofile);
  for (double a_corr = -1.0; a_corr <= 1.0; a_corr += 0.1) {
    for (double b_corr = -1.0; b_corr <= 1.0; b_corr += 0.1) {
      switch (cal) {
      case CalType::RX_IQ:
        sys.sync_set_iq_balance(Direction::RX, a_corr, b_corr);
        suppression = sys.get_signal_dbrms(Channel::Signal) -
                      sys.get_signal_dbrms(Channel::Image);
        break;
      case CalType::TX_IQ:
        sys.sync_set_iq_balance(Direction::TX, a_corr, b_corr);
        suppression = sys.get_signal_dbrms(Channel::Signal) -
                      sys.get_signal_dbrms(Channel::Image);
        break;
      case CalType::TX_DC:
        sys.sync_set_tx_dc_offset(a_corr, b_corr);
        suppression = sys.get_signal_dbrms(Channel::Signal);
        break;
      default:
        throw "Unrecognised calibration type";
      }
      f << a_corr << "," << b_corr << "," << suppression << "\n";
    }
  }
  f.close();
}

void dump_ramp(System &sys, const CalType cal, const double freq,
               const CommandLineOptions &opts) {
  if (opts.dump_folder == "") {
    return;
  }
  std::ofstream f;
  std::string ofile = opts.dump_folder + "/" + cal_to_str(cal) + "-" +
                      std::to_string(freq) + "-" + sys.get_daughterboard() +
                      "-csv";
  f.open(ofile);
  for (double rx_gain = MIN_RX_GAIN; rx_gain <= MAX_RX_GAIN; rx_gain += 1) {
    sys.queue_set_gain(Direction::RX, rx_gain);
    sys.commit();
    f << rx_gain << "," << sys.get_signal_dbrms(Channel::Signal) << "\n";
  }
  f.close();
}

// Get the best correction for the system at the specified values
result_t get_best_correction(System &sys, const double freq, const CalType cal,
                             const CommandLineOptions &opts) {
  std::cout << "\nFreq: " << freq << "\n";
  double a_corr_start, a_corr_step, a_corr_stop;
  double b_corr_start, b_corr_step, b_corr_stop;
  double initial_suppression, suppression, a_corr, b_corr;
  double best_suppression, best_a_corr, best_b_corr;
  if (freq + opts.tx_offset > MAX_FREQ) {
    sys.queue_tune(MAX_FREQ - opts.tx_offset, opts.tx_offset);
  } else if (freq < MIN_FREQ) {
    sys.queue_tune(MIN_FREQ, opts.tx_offset);
  } else {
    sys.queue_tune(freq, opts.tx_offset);
  }
  sys.queue_set_gain(Direction::TX, 20.0);
  sys.commit();

  const double tone_freq = sys.get_freq_offset();
  sys.sync_set_iq_balance(Direction::RX, 0.0, 0.0);
  sys.sync_set_iq_balance(Direction::TX, 0.0, 0.0);
  sys.sync_set_tx_dc_offset(0, 0);

  switch (cal) {
  case CalType::TX_IQ:
    sys.sync_set_mixer_freq(Channel::Signal, tone_freq + opts.tx_wave_freq);
    sys.sync_set_mixer_freq(Channel::Image, tone_freq - opts.tx_wave_freq);
    break;
  default:
    sys.sync_set_mixer_freq(Channel::Signal, tone_freq);
    sys.sync_set_mixer_freq(Channel::Image, -tone_freq);
    break;
  }

  dump_ramp(sys, cal, freq, opts);
  switch (cal) {
  case CalType::TX_DC:
    sys.queue_set_gain(Direction::RX, 10);
    break;
  default:
    sys.queue_set_gain(Direction::RX, get_optimal_rx_gain(sys));
    break;
  }
  sys.commit();

  switch (cal) {
  case CalType::TX_DC:
    initial_suppression = sys.get_signal_dbrms(Channel::Signal);
    break;
  default:
    initial_suppression = sys.get_signal_dbrms(Channel::Signal) -
                          sys.get_signal_dbrms(Channel::Image);
    break;
  }
  best_suppression = initial_suppression;

  // bounds and results from searching
  a_corr_start = -0.2;
  a_corr_stop = 0.2;
  a_corr_step = (a_corr_stop - a_corr_start) / (SEARCH_STEPS + 1);
  b_corr_start = -0.2;
  b_corr_stop = 0.2;
  b_corr_step = (b_corr_stop - b_corr_start) / (SEARCH_STEPS + 1);
  best_a_corr = 0;
  best_b_corr = 0;
  dump_surface(sys, cal, freq, opts);
  while (a_corr_step >= opts.precision or b_corr_step >= opts.precision) {
    for (a_corr = a_corr_start + a_corr_step;
         a_corr <= a_corr_stop - a_corr_step; a_corr += a_corr_step) {
      for (b_corr = b_corr_start + b_corr_step;
           b_corr <= b_corr_stop - b_corr_step; b_corr += b_corr_step) {
        switch (cal) {
        case CalType::RX_IQ:
          sys.sync_set_iq_balance(Direction::RX, a_corr, b_corr);
          suppression = sys.get_signal_dbrms(Channel::Signal) -
                        sys.get_signal_dbrms(Channel::Image);
          break;
        case CalType::TX_IQ:
          sys.sync_set_iq_balance(Direction::TX, a_corr, b_corr);
          suppression = sys.get_signal_dbrms(Channel::Signal) -
                        sys.get_signal_dbrms(Channel::Image);
          break;
        case CalType::TX_DC:
          sys.sync_set_tx_dc_offset(a_corr, b_corr);
          suppression = sys.get_signal_dbrms(Channel::Signal);
          break;
        default:
          throw "Unrecognised calibration type";
        }

        switch (cal) {
        case CalType::TX_DC:
          if (suppression < best_suppression) {
            best_suppression = suppression;
            best_a_corr = a_corr;
            best_b_corr = b_corr;
          }
          break;
        default:
          if (suppression > best_suppression) {
            best_suppression = suppression;
            best_a_corr = a_corr;
            best_b_corr = b_corr;
          }
          break;
        }
      }
    }

    a_corr_start = best_a_corr - a_corr_step;
    a_corr_stop = best_a_corr + a_corr_step;
    a_corr_step = (a_corr_stop - a_corr_start) / (SEARCH_STEPS + 1);
    b_corr_start = best_b_corr - b_corr_step;
    b_corr_stop = best_b_corr + b_corr_step;
    b_corr_step = (b_corr_stop - b_corr_start) / (SEARCH_STEPS + 1);
  }
  std::cout << "Best Suppression: " << best_suppression << "\n";
  double delta;
  switch (cal) {
  case CalType::TX_DC:
    delta = initial_suppression - best_suppression;
    break;
  default:
    delta = best_suppression - initial_suppression;
    break;
  }
  return result_t{
      freq, best_a_corr, best_b_corr, best_suppression, delta,
  };
}

// Save the results to a csv
void save_results(const std::vector<result_t> results, const std::string name,
                  const std::string serial) {
  std::cout << "Saving results of \"" << name << "\" to file" << std::endl;
  std::ofstream f;
  std::string ofile = name + serial + ".csv";
  f.open(ofile);
  f << "name," << name << std::endl;
  f << "serial," << serial << std::endl;
  f << "timestamp," << std::time(nullptr) << std::endl;
  f << "version,v1.0" << std::endl;
  f << "DATA STARTS HERE" << std::endl;
  f << "lo_frequency,correction_real,correction_imag,measured,delta"
    << std::endl;
  for (auto res : results) {
    f << res.freq << "," << res.real_corr << "," << res.imag_corr << ","
      << res.best << "," << res.delta << std::endl;
  }
  f.close();
}

// Perform a calibration of type cal on the specified daughterboard and
// save the results to a file
void calibrate(System sys, const CalType cal, const CommandLineOptions opts) {
  std::vector<result_t> results;

  switch (cal) {
  case CalType::TX_IQ:
    sys.sync_set_signal(opts.tx_wave_freq, opts.tx_wave_ampl);
    break;
  case CalType::RX_IQ:
    sys.sync_set_signal(0.0, opts.tx_wave_ampl);
    break;
  case CalType::TX_DC:
    sys.sync_set_signal(0.0, 0.0);
    break;
  default:
    throw "Invalid calibration type";
  }

  sys.sync_transmit_enable(true);

  for (double freq = opts.freq_start; freq <= opts.freq_stop;
       freq += opts.freq_step) {
    results.push_back(get_best_correction(sys, freq, cal, opts));
  }

  save_results(results, cal_to_str(cal) + "_", sys.get_serial());
}

CommandLineOptions parse_command_line_options(int argc, char **argv) {
  CommandLineOptions opts{};
  try {
    for (int i = 1; i < argc; i++) {
      if (std::strcmp(argv[i], "--help") == 0) {
        opts.print_help = true;
      } else if (std::strcmp(argv[i], "--tx-wave-ampl") == 0) {
        if (++i < argc) {
          opts.tx_wave_ampl = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--tx-wave-freq") == 0) {
        if (++i < argc) {
          opts.tx_wave_freq = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--tx-offset") == 0) {
        if (++i < argc) {
          opts.tx_offset = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--freq-start") == 0) {
        if (++i < argc) {
          opts.freq_start = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--freq-stop") == 0) {
        if (++i < argc) {
          opts.freq_stop = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--freq-step") == 0) {
        if (++i < argc) {
          opts.freq_step = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--precision") == 0) {
        if (++i < argc) {
          opts.precision = std::stod(argv[i]);
        }
      } else if (std::strcmp(argv[i], "--surface-folder") == 0) {
        if (++i < argc) {
          opts.surface_folder = argv[i];
        }
      } else if (std::strcmp(argv[i], "--dump-folder") == 0) {
        if (++i < argc) {
          opts.dump_folder = argv[i];
        }
      } else if (std::strcmp(argv[i], "--db") == 0) {
        while (++i < argc) {
          try {
            opts.daughterboards.push_back(std::stoi(argv[i]));
          } catch (std::invalid_argument) {
            i--;
            break;
          }
        }
      } else if (std::strcmp(argv[i], "--types") == 0) {
        while (++i < argc) {
          try {
            opts.cal_types.push_back(str_to_cal(argv[i]));
          } catch (std::invalid_argument) {
            i--;
            break;
          }
        }
      }
    }
  } catch (std::invalid_argument) {
    std::cerr << "Could not convert an argument into a double value, check you "
                 "have typed the arguments correctly\n";
    std::exit(EINVAL);
  }

  return opts;
}

void print_usage() {
  std::cerr << "Usage: calibrate_application [options] --db (0 | 1 | 0 1) "
               "--types (rx_iq | tx_iq | tx_dc)\n";
  std::cerr << "Options:\n";
  std::cerr << "\t--help              Display this message\n";
  std::cerr << "\t--tx-wave-ampl      Amplitude of the TX transmit wave scaled "
               "between 0 and 1. Default is 0.35\n";
  std::cerr << "\t--tx-wave-freq      Frequency (Hz) of the TX tone above "
               "transmit carrier. Default is 507.123e3\n";
  std::cerr << "\t--tx-offset         Frequency (Hz) offset of TX signal above "
               "center frequency. Defalut is 0.9344e6\n";
  std::cerr << "\t--freq-start        Frequency (Hz) to start the sweep at. "
               "Default is 100e6\n";
  std::cerr << "\t--freq-stop         Frequency (Hz) to stop the sweep at. "
               "Default is 6e9\n";
  std::cerr << "\t--freq-step         Frequency (Hz) between measurements. "
               "Default is 100e6\n";
  std::cerr << "\t--precision         Accuracy of calibration parameters. "
               "Default is 0.0001\n";
  std::cerr << "\t--surface-folder    Folder to save correction surface plots "
               "in, empty to skip. Default is empty\n";
  std::cerr << "\t--dump-folder       Folder to save gain ramp plots in, empty "
               "to skip. Default is empty\n";
  std::cerr
      << "\t--db                List of daughterboards to calibrate (0 or 1)\n";
  std::cerr << "\t--types             List of calibration types (rx_iq, tx_iq, "
               "tx_dc)\n";
}

void print_options(const CommandLineOptions &opts) {
  std::cerr << "Running application with following options:\n";
  std::cerr << "Help:                      " << (int)opts.print_help << "\n";
  std::cerr << "Surface plots:             " << opts.surface_folder << "\n";
  std::cerr << "Dump plots:                " << opts.dump_folder << "\n";
  std::cerr << "TX Wave Amplitude:         " << opts.tx_wave_ampl << "\n";
  std::cerr << "TX Wave Frequency:         " << opts.tx_wave_freq << "\n";
  std::cerr << "TX Offset:                 " << opts.tx_offset << "\n";
  std::cerr << "Frequency Start:           " << opts.freq_start << "\n";
  std::cerr << "Frequency Stop:            " << opts.freq_stop << "\n";
  std::cerr << "Frequency Step:            " << opts.freq_step << "\n";
  std::cerr << "Precision:                 " << opts.precision << "\n";

  std::cerr << "Running on daughterboards: ";
  for (auto db : opts.daughterboards) {
    std::cerr << db << " ";
  }

  std::cerr << "\nRunning cals:              ";
  for (auto cal : opts.cal_types) {
    std::cerr << cal_to_str(cal) << " ";
  }
  std::cerr << "\n";
}

int main(int argc, char **argv) {
  try {
    CommandLineOptions opts = parse_command_line_options(argc, argv);
    if (opts.print_help) {
      print_usage();
      std::exit(0);
    }

    if (opts.daughterboards.empty()) {
      std::cerr << "No daughterboards specified, please specify 0, 1 or some "
                   "combination\n";
      std::exit(EINVAL);
    }

    if (opts.cal_types.empty()) {
      std::cerr << "No calibration types specified, please specify rx_iq, "
                   "tx_iq, tx_dc or some combination\n";
      std::exit(EINVAL);
    }

    if (opts.freq_stop < opts.freq_start) {
      std::cerr << "Stop frequency is below start frequency\n";
      std::exit(EINVAL);
    }
    print_options(opts);

    OA::Application app("calibration_application.xml");

    app.initialize(); // all resources have been allocated
    app.start();

    std::vector<System> systems;

    for (int db : opts.daughterboards) {
      switch (db) {
      case 0:
        systems.push_back(System::build(app, Daughterboard::DB0));
        break;
      case 1:
        systems.push_back(System::build(app, Daughterboard::DB1));
        break;
      default:
        throw "Unrecognised daughterboard number";
      }
    }

    for (unsigned i = 0; i < systems.size(); i++) {
      for (unsigned j = 0; j < systems.size(); j++) {
        systems[j].enable(false);
      }
      systems[i].enable(true);
      for (CalType cal : opts.cal_types) {
        calibrate(systems[i], cal, opts);
      }
    }

    app.stop();
  } catch (const char *e) {
    std::cerr << "Failure: " << e << "\n";
    return 1;
  }
};
