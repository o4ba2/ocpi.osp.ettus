#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include "OcpiApi.hh"

namespace OA = OCPI::API;

enum Half_t {
    LOWER,
    UPPER
};

uint32_t set_gpo(uint32_t mask, uint32_t val, Half_t half) {
    // @mask and @val are assumed to contain 6 bits of data each.
    // Return output for property gpo_mask_data.
    // I.e. mask in top 16 bits, val in lower 16 bits.  Within their
    // respsective words mask and val are shifted to the correct half.

    mask = (mask & 0x3F) << 16;
    val  = val  & 0x3F;

    if (half == LOWER) {
        return mask | val;
    } else {
        return (mask << 6) | (val << 6);
    }
}

bool check_input(uint16_t expected_data, uint32_t actual, Half_t half) {
    // Extract the data bits from the @actual property we read given the
    // current @half.
    // Return true if OK, false otherwise.
    uint16_t data = ((half == LOWER) ? actual : (actual >> 6)) & 0x3F;
    if (data == expected_data) {
        return true;
    } else {
        printf("Expected 0x%x got 0x%x (full value = 0x%x)\n", expected_data, data, actual);
        return false;
    }
}

bool single_side_test(Half_t output_half, OA::Application & app) {
    // Perform a one-way test.  E.g. output pins = 1-6, input pins = 7-12 (or
    // vice versa).
    // Return true on success, false on failure

    Half_t input_half = (output_half == UPPER) ? LOWER : UPPER;

    uint16_t in_val;
    bool err {false};

    // Set direction bits
    app.setPropertyValue("x310_gpio.direction_mask_data", (output_half == LOWER) ? 0x0FFF003F : 0xFFF0FC0);

    // Reset output to a known state
    // tests.
    // Data = 00 0000
    // Mask = 11 1111
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x3F, 0x0, output_half));

    // Set a test pattern on the output bits
    // Data = 01 0101
    // Mask = 11 1111
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x3F, 0x15, output_half));

    // Check input corresponds
    in_val = app.getPropertyValue<uint16_t>("x310_gpio.gpi_data");
    err |= !check_input(0x15, in_val, input_half);

    // Invert output bits
    // Data = 10 1010
    // Mask = 11 1111
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x3F, 0x2A, output_half));

    // Check input corresponds
    in_val = app.getPropertyValue<uint16_t>("x310_gpio.gpi_data");
    err |= !check_input(0x2A, in_val, input_half);

    // Test we can set a single bit using the mask and wait for a bit
    // Data = 11 1111
    // Mask = 00 0001
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x01, 0x3F, output_half));

    // Check input corresponds
    in_val = app.getPropertyValue<uint16_t>("x310_gpio.gpi_data");
    err |= !check_input(0x2B, in_val, input_half);

    return err;
}

bool test_gpo_mask_enable(OA::Application & app) {
    // Test that the when we unset the lowest bit of gpo_mask_enable then
    // property writes to gpo_mask_data have no effect
    
    uint16_t in_val;
    bool err {false};

    // Set direction bits
    // Pins 1-6 output; pins 7-12 input
    app.setPropertyValue("x310_gpio.direction_mask_data", 0x0FFF003F);

    // Reset output to a known state
    // tests.
    // Data = 00 0000
    // Mask = 11 1111
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x3F, 0x0, LOWER));

    // Check input corresponds
    in_val = app.getPropertyValue<uint16_t>("x310_gpio.gpi_data");
    err |= !check_input(0x00, in_val, UPPER);

    // Unset gpo_mask_enable(0) to disable gpo property writes
    app.setPropertyValue("x310_gpio.gpo_mask_enable", 0x2);

    // Write a new value to gpo_mask_data
    // Data = 11 1111
    // Mask = 11 1111
    app.setPropertyValue("x310_gpio.gpo_mask_data", set_gpo(0x3F, 0x3F, LOWER));

    // Check it does not propegate through to the input
    in_val = app.getPropertyValue<uint16_t>("x310_gpio.gpi_data");
    err |= !check_input(0x00, in_val, UPPER);

    return err;
}

int main(/*int argc, char **argv*/) 
{

    // ==================================================================== //
      // Assumptions:
      // - GPIOs connected with jumper leads between data pins as follows:
      //    0 - 6
      //    1 - 7
      //    2 - 8
      //    3 - 9
      //    4 - 10
      //    5 - 11
      // - x310_gpio has been build with parameter EVENT_MODE=true.
      // ==================================================================== //

    try
    {
        OA::PValue params[] = {
            OA::PVUChar("logLevel", 7),
            OA::PVBool("dump", true),
            OA::PVBool("verbose", true),
            OA::PVEnd
        };

        OA::Application app("app_test_gpio_properties.xml", params);
        app.initialize(); // all resources have been allocated
        app.start();

        bool err {false};
        bool this_err {false};

        printf("Starting tests with pins 1-6 output\n");
        this_err = single_side_test(LOWER, app);
        if (this_err) printf("Test failed\n");
        else          printf("Test passed\n");
        err |= this_err;

        printf("Starting tests with pins 7-12 output\n");
        this_err = single_side_test(UPPER, app);
        if (this_err) printf("Test failed\n");
        else          printf("Test passed\n");
        err |= this_err;

        printf("Starting gpo_mask_enable test\n");
        this_err = test_gpo_mask_enable(app);
        if (this_err) printf("Test failed\n");
        else          printf("Test passed\n");
        err |= this_err;

        // do end-of-run processing like dump properties
        app.finish();

        if (err) {
            throw std::string("A test did not pass");
        }
    }
    catch (std::string &e)
    {
        std::cerr << "app failed: " << e << std::endl;
        return 1;
    }

  return 0;
}
