#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# Check that this script is being sourced
if [ "${0##*/}" != "bash" ] && [ "${0##*/}" != "-bash" ]; then
    echo "ERROR: Script should be sourced with bash"
    exit 1
fi

export OCPI_LIBRARY_PATH=/opencpi/projects/assets/artifacts:/opencpi/projects/core/artifacts:/opencpi/projects/osps/ocpi.osp.ettus/exports/artifacts:/opencpi/projects/tutorial/artifacts
# export OCPI_LOG_LEVEL=20
export OCPI_SYSTEM_CONFIG=/opencpi/projects/osps/ocpi.osp.ettus/applications/system.xml
export OCPI_ENABLE_HDL_NETWORK_DISCOVERY=1
export OCPI_ETHER_INTERFACE=enp10s0f0
