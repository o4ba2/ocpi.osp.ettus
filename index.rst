.. X310 top level project documentation


Ettus Research USRP X310 System Support Project
===============================================
OpenCPI System support Project for the Ettus Research USRP X310 SDR.

.. toctree::
   :maxdepth: 2

   applications/applications
   components/components
   hdl/devices/devices
   hdl/primitives/primitives
   hdl/platforms/platforms
