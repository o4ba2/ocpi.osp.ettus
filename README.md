# Ettus
This is the source distribution of the OpenCPI System Support Project (OSP) for Ettus hardware. 

OpenCPI is Open Source Software, licensed with LGPL3, See LICENSE.txt

# Supported RCC Platforms
N310_v4_0_0

# Supported HDL Platforms
x310
