# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
mkfile_dir:=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))
yocto_sdk_dir:=$(mkfile_dir)/gen/sdk

tooldir:=$(yocto_sdk_dir)/../opencpi-bin
OcpiCrossCompile=$(tooldir)/arm-oe-linux-gnueabi-

OcpiKernelCrossCompile=arm-oe-linux-gnueabi-
OcpiKernelDir=$(yocto_sdk_dir)/sysroots/cortexa9t2hf-neon-oe-linux-gnueabi/usr/src/kernel
OcpiKernelEnv=$(yocto_sdk_dir)/environment-setup-cortexa9t2hf-neon-oe-linux-gnueabi

OcpiPlatformOs=linux
OcpiPlatformOsVersion=ettus_n310_v4
OcpiPlatformArch=arm
