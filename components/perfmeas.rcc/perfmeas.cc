// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Wed Apr  7 14:17:29 2021 PDT
 * BASED ON THE FILE: perfmeas.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the perfmeas worker in C++
 */

#include <chrono>
#include "perfmeas-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace PerfmeasWorkerTypes;


class PerfmeasWorker : public PerfmeasWorkerBase {
  typedef std::chrono::time_point<std::chrono::steady_clock> time_point_t;

  RunCondition m_rc;

  time_point_t m_tstart;

  uint32_t *m_tx_data;
  uint32_t m_tx_data_len;
  uint32_t m_tx_data_bytes;

  void update_elapsed(void) {
    auto now = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_s = (now - m_tstart);
    m_properties.elapsed_s = elapsed_s.count();
  }

  RCCResult start() {
    m_tx_data_len = 2 * m_properties.tx_message_words;
    m_tx_data_bytes = m_tx_data_len * sizeof(uint32_t);
    m_tx_data = new uint32_t[m_tx_data_len];

    for (uint32_t i = 0u; i < m_tx_data_len; i++) {
      m_tx_data[i] = i;
    }

    m_tstart = std::chrono::steady_clock::now();
    setRunCondition(&m_rc);
    return RCC_OK;
  }

  RCCResult stop() {
    delete[] m_tx_data;
    m_tx_data = NULL;
    return RCC_OK;
  }

  RCCResult run(bool /*timedout*/) {
    bool tx_done = m_properties.tx_messages_sent == m_properties.tx_messages;
    bool rx_done = m_properties.rx_messages == m_properties.rx_exp_messages;

    if (tx_done && rx_done) {
      return RCC_DONE;
    }

    if (!tx_done && tx.hasBuffer()) {
      tx.setLength(m_tx_data_bytes);
      memcpy(tx.data(), m_tx_data, m_tx_data_bytes);
      m_properties.tx_messages_sent++;
      m_properties.tx_bytes_sent += m_tx_data_bytes;

      update_elapsed();
      double rate = static_cast<double>(m_properties.tx_bytes_sent) / m_properties.elapsed_s;
      m_properties.up_rate_mbit_s = rate * 8.0 / 1.0e6;

      tx.advance();
    }

    if (!rx_done && rx.hasBuffer()) {
      size_t length = rx.length();
      m_properties.rx_messages++;
      m_properties.rx_last_message_bytes = length;
      m_properties.rx_bytes += length;

      update_elapsed();
      double rate = static_cast<double>(m_properties.rx_bytes) / m_properties.elapsed_s;
      m_properties.down_rate_mbit_s = rate * 8.0 / 1.0e6;

      rx.advance();
    }

    return RCC_OK;
  }

public:
  PerfmeasWorker() : m_rc(1 << PERFMEAS_RX, 1 << PERFMEAS_TX, RCC_NO_PORTS) {}
};

PERFMEAS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
PERFMEAS_END_INFO
