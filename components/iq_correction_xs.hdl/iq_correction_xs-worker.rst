.. iq_correction_xs HDL worker

:orphan:

.. _iq_correction_xs-HDL-worker:


``iq_correction_xs`` HDL Worker
===============================
Ettus x310 IQ correction worker.

Detail
------
This worker does not have any subdevices. It takes in an iq data stream and applies an iq and dc correction to it.

This worker is controlled by the drc_x310.rcc worker. It is not necessary for the application to
access the worker's properties.

This worker does not drive any GPIO, SPI or LED signals.

In order to improve the RF performance of the x310 platform, it can be calibrated to produce two complex correction values, one for IQ and DC. The IQ value corrects for the physical differences between the in-phase and quadrature tracks, which otherwise produce measurable phase and attenuation discrepancies. The DC value removes any LO leakage at the centre frequency. This worker applies those correction values to the data stream.

It can be enabled or disabled by setting its ``enable`` property, and the actual correction values applied can be accessed through the ``I_dc``, ``Q_dc``, ``A`` and ``B`` properties. Applications do not need to do this directly, as this can all be controlled through the drc_x310.rcc worker.

If the worker is disabled then it is effectively bypassed, and will just pass through the data from its input port to its output port.


.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
