-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of worker is
  signal rx_messages : ulong_t;
  signal rx_bytes : ulonglong_t;
  signal rx_lastmsg_bytes : ulong_t;

  signal rx_msg_bytes : ulong_t;

  type tx_state_t is (TX_IDLE, TX_START, TX_BUSY, TX_DONE, TX_WAIT);
  signal tx_state : tx_state_t;

  signal tx_beat_count : ulong_t;
  signal tx_message_count : ulong_t;
  signal tx_message_gap_count : ulong_t;

  signal tx_beats_r : ulong_t;
  signal tx_messages_r : ulong_t;
  signal tx_message_gap_r : ulong_t;
begin
  props_out.rx_messages <= rx_messages;
  props_out.rx_bytes <= rx_bytes;
  props_out.rx_lastmsg_bytes <= rx_lastmsg_bytes;

  -- RX port
  rx_out.take <= rx_in.ready;

  rx_sm : process(ctl_in.clk)
    variable thismsg_bytes : ulong_t;
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        rx_messages <= to_ulong(0);
        rx_bytes <= to_ulonglong(0);
        rx_lastmsg_bytes <= to_ulong(0);

        thismsg_bytes := to_ulong(0);
      else
        if rx_in.ready = '1' then
          if rx_in.som = '1' then
            thismsg_bytes := to_ulong(0);
          end if;

          if rx_in.valid = '1' then
            case rx_in.byte_enable is
              when "00000001" => thismsg_bytes := thismsg_bytes + 1;
              when "00000011" => thismsg_bytes := thismsg_bytes + 2;
              when "00000111" => thismsg_bytes := thismsg_bytes + 3;
              when "00001111" => thismsg_bytes := thismsg_bytes + 4;
              when "00011111" => thismsg_bytes := thismsg_bytes + 5;
              when "00111111" => thismsg_bytes := thismsg_bytes + 6;
              when "01111111" => thismsg_bytes := thismsg_bytes + 7;
              when "11111111" => thismsg_bytes := thismsg_bytes + 8;
              when others => null;
            end case;
          end if;

          if rx_in.eom = '1' then
            rx_messages <= rx_messages + 1;
            rx_bytes <= rx_bytes + thismsg_bytes;
            rx_lastmsg_bytes <= thismsg_bytes;
          end if;
        end if;
      end if;
    end if;
  end process rx_sm;

  -- TX port
  tx_out.byte_enable <= "11111111";
  tx_out.data <= from_ulong((tx_beat_count sll 1) + 1) & from_ulong(tx_beat_count sll 1);
  tx_out.give <= to_bool(tx_in.ready = '1' and (tx_state = TX_BUSY or tx_state = TX_DONE));
  tx_out.valid <= to_bool(tx_state = TX_BUSY);

  tx_sm : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        tx_state <= TX_IDLE;
        tx_out.som <= '0';
        tx_out.eom <= '0';

        tx_beats_r <= to_ulong(0);
        tx_messages_r <= to_ulong(0);
        tx_message_gap_r <= to_ulong(0);

        tx_beat_count <= to_ulong(0);
        tx_message_count <= to_ulong(0);
        tx_message_gap_count <= to_ulong(0);

      else
        case tx_state is
          when TX_IDLE =>
            if props_in.tx_run_written = '1' and props_in.tx_run(0) = '1' then
              tx_beats_r <= props_in.tx_beats;
              tx_messages_r <= props_in.tx_messages;
              tx_message_gap_r <= props_in.tx_message_gap;

              tx_message_count <= to_ulong(0);
              tx_state <= TX_START;
            end if;

          when TX_START =>
            tx_beat_count <= to_ulong(0);
            tx_state <= TX_BUSY;
            tx_out.som <= '1';

          when TX_BUSY =>
            if tx_in.ready = '1' then
              tx_out.som <= '0';
              if tx_beat_count = tx_beats_r then
                tx_out.eom <= '1';
                tx_state <= TX_DONE;
              else
                tx_beat_count <= tx_beat_count + 1;
              end if;
            end if;

          when TX_DONE =>
            if tx_in.ready = '1' then
              tx_out.eom <= '0';
              if props_in.tx_run(0) = '0' or tx_message_count = tx_messages_r then
                tx_state <= TX_IDLE;
              else
                tx_message_count <= tx_message_count + 1;
                tx_message_gap_count <= to_ulong(0);
                tx_state <= TX_WAIT;
              end if;
            end if;

          when TX_WAIT =>
            if props_in.tx_run(0) = '0' then
              tx_state <= TX_IDLE;
            elsif tx_message_gap_count = tx_message_gap_r then
              tx_state <= TX_START;
            else
              tx_message_gap_count <= tx_message_gap_count + 1;
            end if;
        end case;
      end if;
    end if;
  end process tx_sm;

end rtl;
