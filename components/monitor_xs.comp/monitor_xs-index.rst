.. _monitor_xs:


Channel Monitor (``monitor_xs``)
=======================================
Ettus x310 channel monitor worker.

Design
------
This worker allows an ACI application to access accumulated samples from the a data stream.

This worker is expected to be read by an ACI application.

Interface
---------
The interface of the monitor_xs.hdl worker is defined in ``../specs/monitor_xs-spec.xml``

.. literalinclude:: ../specs/monitor_xs-spec.xml
    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
The input data stream ports of the monitor uses protocol complex_short_timed_sample-prot.xml
The data stream can produce Discontinuity OpCodes (dependent on the setting of DRC property report_data_loss).

Properties
~~~~~~~~~~
The properties of this worker can be found in ``./monitor_xs.xml``
These properties are expected to be read by an ACI application and are not used by the drc_x310.rcc worker.

.. .. ocpi_documentation_properties::

Ports
~~~~~
The iq_correction_xs.hdl worker has one input port on which it receives data.

.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../monitor_xs.hdl

Dependencies
------------
There are no dependencies to other opencpi elements.

There is a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``iq_correction_xs`` are:

 * It only supports the ``complex_short_timed_sample`` opcode.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
