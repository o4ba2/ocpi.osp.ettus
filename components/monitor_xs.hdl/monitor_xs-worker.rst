.. monitor_xs HDL worker

:orphan:

.. _monitor_xs-HDL-worker:


``monitor_xs`` HDL Worker
=========================
Ettus x310 channel monitor worker.

Detail
------
This worker does not have any subdevices. It takes in an iq data stream accumulates the samples, storing the results in its properties.

This worker is controlled by the drc_x310.rcc worker. An application is expected to read the ``i_value``, ``q_value``, and ``data_valid`` property of the worker.

The ``data_valid`` signal should always be checked before reading ``i_value`` or ``q_value``, as otherwise they may contain arbitrary data. If the data on the channel is disrupted, for example due to retuning, changes in receive setup etc... then it is also advisable to throw away the value the first time ``data_valid`` is asserted.

If you read just one value, for example just ``i_value``, then the worker will store the matching ``q_value`` until it is also read. This is to avoid an ACI application reading values that were actually taken from different samples.

This worker does not drive any GPIO, SPI or LED signals.

.. ocpi_documentation_worker::

Utilization
-----------
.. ocpi_documentation_utilization::
