-- THIS FILE WAS ORIGINALLY GENERATED ON Fri May 27 10:14:59 2022 UTC
-- BASED ON THE FILE: monitor_xs.xml
-- YOU *ARE* EXPECTED TO EDIT IT
-- This file initially contains the architecture skeleton for worker: monitor_xs

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of worker is
  
  constant fresh_timeout    : integer := 50000;
  constant down_sample_rate : integer := 999;
  
  signal both_read          : boolean;
  signal neither_read       : boolean;
  signal i_read             : boolean;
  signal q_read             : boolean;
  signal is_fresh           : boolean;
  signal fresh_counter      : integer range 0 to fresh_timeout;

  signal down_sample_count  : integer range 0 to down_sample_rate;
  signal i_accum            : long_t;
  signal q_accum            : long_t;
  signal short_values       : short_array_t(0 to 1);
  
begin
  input_out.take <= btrue;
  
  is_fresh <= fresh_counter < fresh_timeout;
  
  both_read <= i_read and q_read;
  neither_read <= i_read nor q_read;
  
  props_out.data_valid <= to_bool(is_fresh and neither_read);
  
  short_values <= to_short_array(input_in.data);

  process (ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        fresh_counter     <= 50000;
        down_sample_count <= 0;
        i_read            <= false;
        q_read            <= false;
        i_accum           <= to_long(0);
        q_accum           <= to_long(0);
        props_out.i_value <= to_long(0);
        props_out.q_value <= to_long(0);
      else
        if props_in.i_value_read = '1' then
          i_read <= true;
        end if;

        if props_in.q_value_read = '1' then
          q_read <= true;
        end if;

        if is_fresh then
          fresh_counter <= fresh_counter + 1;
        end if;

        if down_sample_count = down_sample_rate then
          if both_read or neither_read then
            props_out.i_value <= i_accum;
            props_out.q_value <= q_accum;
            
            fresh_counter <= 0;
            i_read <= false;
            q_read <= false;
          end if;
          down_sample_count <= 0;

          i_accum           <= to_long(0);
          q_accum           <= to_long(0);
        elsif input_in.valid = '1' and input_in.opcode = complex_short_timed_sample_sample_op_e then
          down_sample_count <= down_sample_count + 1;

          i_accum <= i_accum + short_values(0);
          q_accum <= q_accum + short_values(1);
        end if;
      end if;
    end if;
  end process;
end rtl;
