/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Wed Jul  6 14:08:55 2022 UTC
 * BASED ON THE FILE: gpio_test.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the gpio_test worker in C++
 */

#include "gpio_test_ports-worker.hh"
#include <chrono>
#include <thread>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Gpio_test_portsWorkerTypes;

class Gpio_test_portsWorker : public Gpio_test_portsWorkerBase {
  RunCondition m_runCondition;

  private:
    enum Half_t {
      LOWER,
      UPPER
    };

    std::string binf(uint16_t val) {
      std::string formatted("0000000000000000");
      for (int i = 0; i < 16; i++) {
        formatted[15 - i] = (val & 1) ? '1': '0';
        val >>= 1; 
      }
      // Add spaces and pipe
      std::string out(formatted.substr(0, 4) + " | " + formatted.substr(4, 4) + " " + formatted.substr(8, 2) + '|' + formatted.substr(10, 2) + " " + formatted.substr(12, 4));
      return out;
    }
  
    void advance_in_to_latest() {
      // Advances the input buffer until there are no buffers remaining.
      // Latch the latest data.  Combine successive masks using bitwise OR to
      // get change of outstanding buffers.

      m_in_mask = 0;
      in.advance();
      while (in.hasBuffer()) {
        if (in.hasBuffer()) {
          m_in_data = in.gpio().mask_data().data;
          m_in_mask |= in.gpio().mask_data().mask;
          log(7, "m_in_data %s (0x%x)", binf(m_in_data).c_str(), m_in_data);
          log(7, "m_in_mask %s (0x%x)", binf(m_in_mask).c_str(), m_in_mask);
        }
        in.advance();
      }
      log(7, "--------------------------------");
    }

    uint16_t set(uint16_t val, Half_t half) {
      // Sets val into either the upper or lower 6 bits according to the
      // diagram below.  Sets other bits to 0.
      // XXXX XXXX XXXX
      // |-----||------|
      //  upper   lower
      val = val & 0x3F;
      if (half == UPPER) {
        return val << 6;
      }
      else {
        return val;
      }
    }

    uint16_t get(uint16_t val, Half_t half) {
      // Extracts either the upper or lower 6 bits depending
      return (half == LOWER) ? (val & 0x3F) : ((val & 0xFC0) >> 6);
    }

    uint16_t m_in_data;
    uint16_t m_in_mask;

  public:
    Gpio_test_portsWorker() : m_runCondition(nullptr, 0, false) {  // nullptr indicates run condition is always true.
      setRunCondition(&m_runCondition);
    };

    RCCResult run(bool /*timedout*/) {
      // ==================================================================== //
      // Assumptions:
      // - GPIOs connected with jumper leads between data pins as follows:
      //    0 - 6
      //    1 - 7
      //    2 - 8
      //    3 - 9
      //    4 - 10
      //    5 - 11
      // - x310_gpio has been build with parameter EVENT_MODE=true.
      // - There are enough message buffers for x310_gpio not to have to drop
      //   data
      // ==================================================================== //

      RCCResult res;

      res = single_side_test(LOWER);
      if (res != RCC_OK) {
        return res;
      }

      res = single_side_test(UPPER);
      if (res != RCC_OK) {
        return res;
      }

      return RCC_DONE; // change this as needed for this worker to do something useful
      // return RCC_ADVANCE; when all inputs/outputs should be advanced each time "run" is called.
      // return RCC_ADVANCE_DONE; when all inputs/outputs should be advanced, and there is nothing more to do.
      // return RCC_DONE; when there is nothing more to do, and inputs/outputs do not need to be advanced.
    }

    RCCResult single_side_test(Half_t output_half) {
      Half_t input_half = (output_half == UPPER) ? LOWER : UPPER;

      // Set the direction bits.
      dir.gpio().mask_data().data = set(0x3F, output_half);
      dir.gpio().mask_data().mask = 0xFFF;
      dir.advance();

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      // Reset output to a known state and flush input before we start the
      // tests
      out.gpio().mask_data().data = set(0x0, output_half);  // I.e. 00 0000
      out.gpio().mask_data().mask = set(0x3F, output_half);  // I.e. 11 1111
      out.advance();
      advance_in_to_latest();

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      // Set output bits to sample data pattern and wait for a bit
      out.gpio().mask_data().data = set(0x15, output_half);  // I.e. 01 0101
      out.gpio().mask_data().mask = set(0x3F, output_half);  // I.e. 11 1111
      out.advance();

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      // Check inputs pick up outputs
      // Expect data = 01 0101 (0x15)
      // Expect mask to be the same.
      advance_in_to_latest();
      if (get(m_in_data, input_half) != 0x15) {
        log(7, "m_in_data expected 0x15 got 0x%x", get(m_in_data, input_half));
        return RCC_ERROR;
      }
      if (get(m_in_mask, input_half) != 0x015) {
        log(7, "m_in_mask expected 0x15 got 0x%x", get(m_in_mask, input_half));
        return RCC_ERROR;
      }

      // Invert output bits and wait for a bit
      out.gpio().mask_data().data = set(0x2A, output_half);  // I.e. 10 1010
      out.gpio().mask_data().mask = set(0x3F, output_half);  // I.e. 11 1111
      out.advance();

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      // Check inputs pick up outputs
      // Expect data = 10 1010 (0x2A)
      // Expect mask to be 11 1111 because all bits have changed (inverted)
      advance_in_to_latest();
      if (get(m_in_data, input_half) != 0x2A) {
        log(7, "m_in_data expected 0x2A got 0x%x", m_in_data >> 6);
        return RCC_ERROR;
      }
      if (get(m_in_mask, input_half) != 0x3F) {
        log(7, "m_in_mask expected 0x3F got 0x%x", m_in_mask >> 6);
        return RCC_ERROR;
      }

      // Test we can set a single bit using the mask and wait for a bit
      out.gpio().mask_data().data = set(0x3F, output_half);  // I.e. 11 1111
      out.gpio().mask_data().mask = set(0x01, output_half);  // I.e. 00 0001
      out.advance();

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      // Check inputs pick up outputs
      // Expect data = 10 1011 (0x2B)
      // Expect mask to be 00 0001
      advance_in_to_latest();
      if (get(m_in_data, input_half) != 0x2B) {
        log(7, "m_in_data expected 0x2B got 0x%x", m_in_data >> 6);
        return RCC_ERROR;
      }
      if (get(m_in_mask, input_half) != 0x01) {
        log(7, "m_in_mask expected 0x01 got 0x%x", m_in_mask >> 6);
        return RCC_ERROR;
      }

      return RCC_OK;

    }
};

GPIO_TEST_PORTS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
GPIO_TEST_PORTS_END_INFO
