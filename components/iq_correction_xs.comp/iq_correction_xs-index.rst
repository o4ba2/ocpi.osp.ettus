.. _iq_correction_xs:


IQ Correction (``iq_corrector_xs``)
=======================================
Ettus x310 IQ Correction worker.

Design
------
This worker provides IQ and DC correction coefficients to a stream of i and q data.

This worker is controlled by the drc_x310.rcc worker, which accesses all of its properties.

Interface
---------
The interface of the iq_corrector_xs.hdl worker is defined in ``../specs/iq_correction_xs-spec.xml``

.. literalinclude:: ../specs/iq_correction_xs-spec.xml
    :language: xml

Opcode handling
~~~~~~~~~~~~~~~
The both the transmit and receive data stream ports of the DRC use protocol complex_short_timed_sample-prot.xml
Transmit (DAC) data streams ignore all opcodes except **sample**.
Receive (ADC) data streams can produce Discontinuity OpCode (dependent on the setting of DRC property report_data_loss)

Properties
~~~~~~~~~~
The properties of this worker can be found in ``./iq_correction_xs.xml``
These properties are used by the drc_x310.rcc and do not need to be set by an application using the drc_x310. If you want manual control of the iq correction this can be done with the drc_x310.

.. .. ocpi_documentation_properties::

Ports
~~~~~
The iq_correction_xs.hdl worker has one input and one output port.

.. .. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../iq_correction_xs.hdl

Dependencies
------------
There are no dependencies to other opencpi elements.

There is a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``iq_correction_xs`` are:

 * It only supports the ``complex_short_timed_sample`` opcode.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
